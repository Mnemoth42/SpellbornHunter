using System.Collections;
using System.Collections.Generic;
using GameDevTV.Utils;
using UnityEditor;
using UnityEngine;

[CustomPropertyDrawer(typeof(Condition.Predicate))]
public class PredicateDrawer : PropertyDrawer
{
    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        SerializedProperty predicateData = property.FindPropertyRelative("predicateData");
        SerializedProperty dataDescription = predicateData.FindPropertyRelative("description");
        SerializedProperty description = property.FindPropertyRelative("description");
        description.stringValue = $"{dataDescription.stringValue}";
        EditorGUI.PropertyField(position, predicateData);
    }

    public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
    {
        SerializedProperty predicateData = property.FindPropertyRelative("predicateData");
        float result = EditorGUI.GetPropertyHeight(predicateData);

        return result + 10f;
    }
}