using System.Collections;
using System.Collections.Generic;
using System.Text;
using GameDevTV.Utils;
using UnityEditor;
using UnityEngine;

[CustomPropertyDrawer(typeof(Condition.Disjunction))]
public class DisjunctionDrawer : PropertyDrawer
{
    private float lastWidth = 0;
    private float lastHeight = 0;

    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        SerializedProperty orArray = property.FindPropertyRelative("or");
        SerializedProperty description = property.FindPropertyRelative("description");
        StringBuilder builder = new StringBuilder();
        if (orArray.arraySize > 1) builder.Append("(");
        for (int i = 0; i < orArray.arraySize; i++)
        {
            builder.Append(orArray.GetArrayElementAtIndex(i).FindPropertyRelative("description").stringValue);
            if (i < orArray.arraySize - 1) builder.Append(" || ");
        }

        if (orArray.arraySize > 1) builder.Append(")");
        description.stringValue = builder.ToString();
        lastWidth = position.width;
        Rect rect = position;
        rect.height = EditorGUI.GetPropertyHeight(description);
        lastHeight = rect.height;
        GUIContent content = new GUIContent(builder.ToString(), builder.ToString());
        EditorGUI.LabelField(rect, content);
        position.y += rect.height;
        position.height -= rect.height;
        EditorGUI.PropertyField(position, orArray);
    }

    public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
    {
        SerializedProperty orArray = property.FindPropertyRelative("or");
        SerializedProperty description = property.FindPropertyRelative("description");
        float result = EditorGUI.GetPropertyHeight(orArray);
        result += EditorGUI.GetPropertyHeight(description);
        return result;
    }

    private float GetStyleHeight(GUIStyle style, SerializedProperty description)
    {
        return style.CalcHeight(new GUIContent(description.stringValue), lastWidth);
    }
}