using System.Collections.Generic;
using System.Text;
using GameDevTV.Inventories;
using GameDevTV.Utils;
using TkrainDesigns.Quests;
using TkrainDesigns.Stats;
using UnityEditor;
using UnityEngine;

[CustomPropertyDrawer(typeof(PredicateData))]
public class PredicateDataDrawer : PropertyDrawer
{
    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        SerializedProperty predicate = property.FindPropertyRelative("predicate");
        SerializedProperty sTarget = property.FindPropertyRelative("target");
        SerializedProperty stringParameter = property.FindPropertyRelative("stringParameter");
        SerializedProperty floatParameter = property.FindPropertyRelative("floatParameter");
        SerializedProperty intParameter = property.FindPropertyRelative("intParameter");
        SerializedProperty enumParameter = property.FindPropertyRelative("enumParameter");
        SerializedProperty description = property.FindPropertyRelative("description");
        SerializedProperty negate = property.FindPropertyRelative("negate");
        float propHeight = EditorGUI.GetPropertyHeight(predicate);
        Rect rect = new Rect(position.x, position.y, position.width, propHeight);
        Rect halfRect = new Rect(rect.x, rect.y, position.width * .66f, propHeight);
        EditorGUI.PropertyField(halfRect, predicate, new GUIContent("Predicate"));
        halfRect.x = halfRect.xMax;
        halfRect.width = position.width * .33f;
        EditorGUI.PropertyField(halfRect, negate);
        EPredicate selectedPredicate = (EPredicate)predicate.enumValueIndex;
        StringBuilder builder = new StringBuilder();
        if (negate.boolValue) builder.Append("!");
        builder.Append($"{selectedPredicate.ToString()}(");
        switch (selectedPredicate)
        {
            case EPredicate.HasQuest:
            case EPredicate.CompletedQuest:
                rect.y += propHeight;
                builder.Append($"{DrawQuest(rect, sTarget)})");
                break;
            case EPredicate.HasInventoryItem:
            case EPredicate.HasItemEquipped:
                rect.y += propHeight;
                builder.Append($"{DrawInventoryItem(rect, sTarget)})");
                break;
            case EPredicate.HasInventoryItems:
                rect.y += propHeight;
                builder.Append($"{DrawInventoryItem(rect, sTarget)}, ");
                rect.y += propHeight;
                builder.Append($"{DrawIntSlider(rect, "Qty", intParameter, 1, 100)})");
                break;
            case EPredicate.HasLevel:
                rect.y += propHeight;
                builder.Append($"{DrawIntSlider(rect, "Level", intParameter, 1, 100)})");
                break;
            case EPredicate.HasTrait:
                rect.y += propHeight;
                EStat trait = (EStat)enumParameter.intValue;
                EditorGUI.BeginProperty(rect, new GUIContent("Trait"), enumParameter);
                EditorGUI.BeginChangeCheck();
                EStat newTrait = (EStat)EditorGUI.EnumPopup(rect, "Trait", trait);
                if (EditorGUI.EndChangeCheck())
                {
                    enumParameter.intValue = (int)newTrait;
                }

                builder.Append($"{trait.ToString()}, ");
                rect.y += propHeight;
                builder.Append($"{DrawIntSlider(rect, "Level", intParameter, 1, 100)})");
                break;
            case EPredicate.CompletedObjective:
                rect.y += propHeight;
                builder.Append($"{DrawQuest(rect, sTarget)}, ");
                rect.y += propHeight;
                builder.Append($"{DrawObjective(rect, sTarget, stringParameter)})");
                break;
        }

        description.stringValue = builder.ToString();
    }

    private static int DrawIntSlider(Rect rect, string caption, SerializedProperty intParameter, int minLevel,
                                     int maxLevel)
    {
        EditorGUI.BeginProperty(rect, new GUIContent(caption), intParameter);
        EditorGUI.BeginChangeCheck();
        int result = EditorGUI.IntSlider(rect, caption, intParameter.intValue, minLevel, maxLevel);
        if (EditorGUI.EndChangeCheck())
        {
            intParameter.intValue = result;
        }

        EditorGUI.EndProperty();
        return result;
    }

    public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
    {
        SerializedProperty predicate = property.FindPropertyRelative("predicate");
        float propHeight = EditorGUI.GetPropertyHeight(predicate);
        EPredicate selectedPredicate = (EPredicate)predicate.enumValueIndex;
        switch (selectedPredicate)
        {
            case EPredicate.HasLevel:
            case EPredicate.CompletedQuest:
            case EPredicate.HasQuest:
            case EPredicate.HasInventoryItem:
            case EPredicate.HasItemEquipped:
                return propHeight * 2.0f;
            case EPredicate.CompletedObjective:
            case EPredicate.HasInventoryItems:
            case EPredicate.HasTrait:
                return propHeight * 3.0f;
        }

        return propHeight * 2.0f;
    }

    private static string DrawQuest(Rect rect, SerializedProperty sTarget)
    {
        EditorGUI.BeginProperty(rect, new GUIContent("Quest"), sTarget);
        List<string> questNames = new List<string>();
        foreach (Quest quest in Resources.LoadAll<Quest>(""))
        {
            questNames.Add(quest.name);
        }

        EditorGUI.BeginChangeCheck();
        int newIndex = EditorGUI.Popup(rect, "Quest", questNames.IndexOf(sTarget.stringValue), questNames.ToArray());
        if (EditorGUI.EndChangeCheck())
        {
            sTarget.stringValue = questNames[newIndex];
        }

        EditorGUI.EndProperty();
        return newIndex < 0 ? "Select Quest" : sTarget.stringValue;
    }

    private static string DrawObjective(Rect rect, SerializedProperty sTarget, SerializedProperty stringParameter)
    {
        Quest quest = Quest.GetByName(sTarget.stringValue);
        if (quest == null)
        {
            EditorGUI.LabelField(rect, "No Quest Selected");
            return "ERROR";
        }

        List<string> objectives = new List<string>();
        List<string> objectiveNames = new List<string>();
        foreach (Quest.Objective objective in quest.GetObjectives())
        {
            objectives.Add(objective.description);
            objectiveNames.Add(objective.reference);
        }

        EditorGUI.BeginProperty(rect, new GUIContent("Objective"), stringParameter);
        EditorGUI.BeginChangeCheck();
        int newIndex = EditorGUI.Popup(rect, "Objective", objectiveNames.IndexOf(stringParameter.stringValue),
            objectives.ToArray());
        if (EditorGUI.EndChangeCheck())
        {
            stringParameter.stringValue = objectiveNames[newIndex];
        }

        EditorGUI.EndProperty();
        return newIndex < 0 ? "Select Objective" : objectives[newIndex];
    }

    private static string DrawInventoryItem(Rect rect, SerializedProperty sTarget)
    {
        EditorGUI.BeginProperty(rect, new GUIContent("Item"), sTarget);
        List<string> itemNames = new List<string>();
        List<string> itemIDs = new List<string>();
        foreach (InventoryItem item in Resources.LoadAll<InventoryItem>(""))
        {
            itemNames.Add(item.GetDisplayName());
            itemIDs.Add(item.GetItemID());
        }

        EditorGUI.BeginChangeCheck();
        int newIndex = EditorGUI.Popup(rect, "Item", itemIDs.IndexOf(sTarget.stringValue), itemNames.ToArray());
        if (EditorGUI.EndChangeCheck())
        {
            sTarget.stringValue = itemIDs[newIndex];
        }

        EditorGUI.EndProperty();
        return newIndex < 0 ? "Select InventoryItem" : itemNames[newIndex];
    }
}