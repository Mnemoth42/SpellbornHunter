﻿using UnityEngine;
using UnityEngine.Serialization;

namespace GameDevTV.Utils
{
    public enum EPredicate
    {
        HasQuest,
        CompletedQuest,
        CompletedObjective,
        HasInventoryItem,
        HasInventoryItems,
        HasLevel,
        HasTrait,
        HasItemEquipped,
    }

    [System.Serializable]
    public class PredicateData
    {
        [SerializeField] private EPredicate predicate = EPredicate.HasQuest;
        [SerializeField] private string target = "";
        [SerializeField] private string stringParameter = "";
        [SerializeField] private float floatParameter = 0;
        [SerializeField] private int intParameter = 0;
        [SerializeField] private bool negate = false;
        [SerializeField] private string description = ""; //Auto generated by propertydrawer.
        [SerializeField] private int enumParameter = 0;

        public EPredicate GetPredicate() => predicate;
        public string GetTarget() => target;
        public string GetStringParam() => stringParameter;
        public float GetFloatParam() => floatParameter;
        public int GetIntParam() => intParameter;
        public int GetEnumParameter() => enumParameter;
        public string GetDescription() => description;
        public bool Negate() => negate;
    }

    public interface IPredicateEvaluator
    {
        bool? Evaluate(string predicate, string[] parameters);
        bool? Evaluate(PredicateData data);
    }
}