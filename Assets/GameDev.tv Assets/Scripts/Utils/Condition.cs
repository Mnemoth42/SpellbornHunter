﻿using System.Collections.Generic;
using UnityEngine;

namespace GameDevTV.Utils
{
    [System.Serializable]
    public class Condition
    {
        [SerializeField] Disjunction[] and;

        public bool Check(IEnumerable<IPredicateEvaluator> evaluators)
        {
            foreach (Disjunction dis in and)
            {
                if (!dis.Check(evaluators))
                {
                    return false;
                }
            }

            return true;
        }

        [System.Serializable]
        public class Disjunction
        {
            [SerializeField] Predicate[] or;

            [SerializeField] private string description = "";

            public bool Check(IEnumerable<IPredicateEvaluator> evaluators)
            {
                foreach (Predicate pred in or)
                {
                    if (pred.Check(evaluators))
                    {
                        return true;
                    }
                }

                return false;
            }
        }

        [System.Serializable]
        public class Predicate
        {
            [SerializeField] private PredicateData predicateData;
            [SerializeField] private string description = "";

            public bool Check(IEnumerable<IPredicateEvaluator> evaluators)
            {
                Debug.Log($"Checking {description}");
                foreach (var evaluator in evaluators)
                {
                    bool? result = evaluator.Evaluate(predicateData);
                    if (result == null)
                    {
                        continue;
                    }

                    if (result == predicateData.Negate()) return false;
                }

                return true;
            }
        }
    }
}