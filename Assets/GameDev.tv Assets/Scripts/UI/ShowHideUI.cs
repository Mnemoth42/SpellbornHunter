﻿using UnityEngine;
using UnityEngine.InputSystem;

namespace GameDevTV.UI
{
    public class ShowHideUI : MonoBehaviour
    {
        [SerializeField] GameObject uiContainer = null;

        // Start is called before the first frame update
        void Start()
        {
            uiContainer.SetActive(false);
        }

        // Update is called once per frame
        public void ToggleUI(InputAction.CallbackContext context)
        {
            if (context.started)
                uiContainer.SetActive(!uiContainer.activeSelf);
        }
    }
}