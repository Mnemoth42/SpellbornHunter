﻿using UnityEngine;
using UnityEngine.UI;
using TMPro;
using GameDevTV.Inventories;

namespace GameDevTV.UI.Inventories
{
    /// <summary>
    /// Root of the tooltip prefab to expose properties to other classes.
    /// </summary>
    public class ItemTooltip : MonoBehaviour
    {
        // CONFIG DATA
        [SerializeField] private Image icon;
        [SerializeField] TextMeshProUGUI titleText = null;
        [SerializeField] TextMeshProUGUI bodyText = null;

        // PUBLIC

        public void Setup(InventoryItem item)
        {
            icon.sprite = item.GetIcon();
            titleText.text = item.GetDisplayName();
            bodyText.text = item.GetDescription();
        }
    }
}