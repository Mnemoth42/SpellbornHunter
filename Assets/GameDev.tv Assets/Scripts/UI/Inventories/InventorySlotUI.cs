﻿using GameDevTV.Core.UI.Dragging;
using GameDevTV.Inventories;
using TkrainDesigns.Stats;
using UnityEngine;

namespace GameDevTV.UI.Inventories
{
    public class InventorySlotUI : MonoBehaviour, IItemHolder, IDragContainer<InventoryItem>
    {
        // CONFIG DATA
        [SerializeField] InventoryItemIcon icon = null;
        [SerializeField] private int maxStackSize = 10;

        // STATE
        int index;
        InventoryItem item;
        Inventory inventory;
        private BaseStats stats;
        private Equipment equipment;

        // PUBLIC

        public void Setup(Inventory inventory, int index)
        {
            this.inventory = inventory;
            this.index = index;
            stats = inventory.GetComponent<BaseStats>();
            equipment = inventory.GetComponent<Equipment>();
            icon.SetItem(inventory.GetItemInSlot(index), inventory.GetNumberInSlot(index));
        }

        public int MaxAcceptable(InventoryItem item)
        {
            if (inventory.HasSpaceFor(item))
            {
                if (GetItem() == item && item.IsStackable())
                {
                    return maxStackSize - GetNumber();
                }

                return maxStackSize;
            }

            return 0;
        }

        public void AddItems(InventoryItem item, int number)
        {
            inventory.AddItemToSlot(index, item, number);
        }

        public InventoryItem GetItem()
        {
            return inventory.GetItemInSlot(index);
        }

        public int GetNumber()
        {
            return inventory.GetNumberInSlot(index);
        }

        public void RemoveItems(int number)
        {
            inventory.RemoveFromSlot(index, number);
        }

        private bool clicked = false;

        void ResetClick()
        {
            clicked = false;
        }

        public void TryEquipItem()
        {
            if (!clicked)
            {
                clicked = true;
                Invoke(nameof(ResetClick), .25f);
                return;
            }

            clicked = false;
            StopAllCoroutines();
            if (GetItem() is EquipableItem equipableItem)
            {
                if (equipment)
                {
                    InventoryItem currentItem = equipment.GetItemInSlot(equipableItem.GetAllowedEquipLocation());
                    if (stats.GetLevel() < equipableItem.GetLevel()) return;
                    if (!equipableItem.TestRequirements(stats.gameObject)) return;
                    equipment.RemoveItem(equipableItem.GetAllowedEquipLocation());
                    equipment.AddItem(equipableItem.GetAllowedEquipLocation(), equipableItem);
                    RemoveItems(GetNumber());
                    if (currentItem) AddItems(currentItem, 1);
                }
            }
            else
            {
                inventory.HandleInventoryClicked(index);
            }
        }
    }
}