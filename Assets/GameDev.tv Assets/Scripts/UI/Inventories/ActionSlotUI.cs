﻿using System;
using GameDevTV.Core.UI.Dragging;
using GameDevTV.Inventories;
using TkrainDesigns.Abilities;
using TkrainDesigns.Abilities.Targeting;
using UnityEngine;
using UnityEngine.UI;

namespace GameDevTV.UI.Inventories
{
    /// <summary>
    /// The UI slot for the player action bar.
    /// </summary>
    public class ActionSlotUI : MonoBehaviour, IItemHolder, IDragContainer<InventoryItem>
    {
        // CONFIG DATA
        [SerializeField] InventoryItemIcon icon = null;
        [SerializeField] int index = 0;
        [SerializeField] private KeyCode keyCode;
        [SerializeField] private Image cooldownImage;

        // CACHE
        ActionStore store;
        private CooldownStore cooldownStore;
        private Ability ability;


        // LIFECYCLE METHODS
        private void Awake()
        {
            store = GameObject.FindGameObjectWithTag("Player").GetComponent<ActionStore>();
            store.storeUpdated += UpdateIcon;
            cooldownStore = store.GetComponent<CooldownStore>();
            cooldownImage.raycastTarget = true;
        }


        private void Update()
        {
            if (Input.GetKeyDown(keyCode))
            {
                store.Use(index, store.gameObject);
            }

            if (cooldownImage)
            {
                cooldownImage.fillAmount = ability ? cooldownStore.GetFractionRemaining(ability.GetCooldownToken()) : 0;
            }
        }

        // PUBLIC

        public void AddItems(InventoryItem item, int number)
        {
            store.AddAction(item, index, number);
        }

        public InventoryItem GetItem()
        {
            return store.GetAction(index);
        }

        public int GetNumber()
        {
            return store.GetNumber(index);
        }

        public int MaxAcceptable(InventoryItem item)
        {
            return store.MaxAcceptable(item, index);
        }

        public void RemoveItems(int number)
        {
            store.RemoveItems(index, number);
        }

        // PRIVATE

        void UpdateIcon()
        {
            icon.SetItem(GetItem(), GetNumber());
            ability = GetItem() as Ability;
        }

        public void ButtonUse()
        {
            store.Use(index, store.gameObject);
        }
    }
}