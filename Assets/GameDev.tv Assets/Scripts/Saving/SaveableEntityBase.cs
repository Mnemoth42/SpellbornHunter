using System.Collections;
using System.Collections.Generic;
using Newtonsoft.Json.Linq;
using UnityEngine;

namespace GameDevTV.Saving
{
    /// <summary>
    /// While this class is used to capture/restore all ISaveables on the GameObject, this version won't
    /// automatically be picked up by the SavingSystem.  Use this on Respawnable characters instead of SaveableEntity
    /// and the RespawnManager can then use it restore the state of the item if it is currently spawned.
    /// </summary>
    public class SaveableEntityBase : MonoBehaviour
    {
        /// <summary>
        /// Will restore the state that was captured by `CaptureState`.
        /// </summary>
        /// <param name="state">
        /// The same object that was returned by `CaptureState`.
        /// </param>
        public virtual void RestoreState(JToken state)
        {
            IDictionary<string, JToken> stateDict = state.ToObject<JObject>();
            foreach (ISaveable saveable in GetComponents<ISaveable>())
            {
                string typeString = saveable.GetType().ToString();
                if (stateDict.ContainsKey(typeString))
                {
                    saveable.RestoreState(stateDict[typeString]);
                }
            }
        }

        /// <summary>
        /// Will capture the state of all `ISaveables` on this component and
        /// return a `System.Serializable` object that can restore this state
        /// later.
        /// </summary>
        public virtual JToken CaptureState()
        {
            JObject state = new JObject();
            IDictionary<string, JToken> stateDict = state;
            foreach (ISaveable saveable in GetComponents<ISaveable>())
            {
                stateDict[saveable.GetType().ToString()] = saveable.CaptureState();
            }

            return state;
        }
    }
}