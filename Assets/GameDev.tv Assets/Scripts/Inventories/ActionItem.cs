using System;
using UnityEngine;

namespace GameDevTV.Inventories
{
    /// <summary>
    /// An inventory item that can be placed in the action bar and "Used".
    /// </summary>
    /// <remarks>
    /// This class should be used as a base. Subclasses must implement the `Use`
    /// method.
    /// </remarks>
    public abstract class ActionItem : InventoryItem
    {
        // PUBLIC

        /// <summary>
        /// Trigger the use of this item. Override to provide functionality.
        /// Should return false if the item cannot be used.
        /// </summary>
        /// <param name="user">The character that is using this action.</param>
        public abstract bool Use(GameObject user, System.Action callback);


        public bool isConsumable()
        {
            return IsStackable();
        }

    #if UNITY_EDITOR
        protected override bool IsCategorySelectable(Enum location)
        {
            ItemCategory candidate = (ItemCategory)location;
            return IsStackable() ? candidate == ItemCategory.Potion : candidate == ItemCategory.Action;
        }
    #endif
    }
}