﻿using System;
using System.Collections.Generic;
using System.Linq;
using Cinemachine.Editor;
using GameDevTV.Saving;
using Newtonsoft.Json.Linq;
using UnityEditor;
using UnityEngine;
using UnityEngine.Serialization;
using UnityEditor.UIElements;
using UnityEngine.UIElements;
using Debug = UnityEngine.Debug;

namespace GameDevTV.Inventories
{
    /// <summary>
    /// A ScriptableObject that represents any item that can be put in an
    /// inventory.
    /// </summary>
    /// <remarks>
    /// In practice, you are likely to use a subclass such as `ActionItem` or
    /// `EquipableItem`.
    /// </remarks>
    public abstract class InventoryItem : RetrievableScriptableObject, ISerializationCallbackReceiver, ISaveable,
                                          IJsonSaveable
    {
    #region fields

        // CONFIG DATA
        [Tooltip("Item name to be displayed in UI.")] [SerializeField]
        string displayName = "";

        [Tooltip("Item description to be displayed in UI.")] [SerializeField] [TextArea]
        string description = "";

        [Tooltip("The UI icon to represent this item in the inventory.")] [SerializeField]
        Sprite icon = null;

        [Tooltip("The prefab that should be spawned when this item is dropped.")] [SerializeField]
        private Pickup pickup = null;

        [Tooltip("If true, multiple items of this type can be stacked in the same inventory slot.")] [SerializeField]
        private bool stackable = false;

        [SerializeField] private int level = 1;

        [FormerlySerializedAs("dropable")] [SerializeField]
        private bool droppable = true;

        [SerializeField] private ItemCategory category;
        [SerializeField] private int price = 10;

    #endregion

        // STATE
        static Dictionary<string, InventoryItem> itemLookupCache;

        // PUBLIC

        /// <summary>
        /// Get the inventory item instance from its UUID.
        /// </summary>
        /// <param name="itemID">
        /// String UUID that persists between game instances.
        /// </param>
        /// <returns>
        /// Inventory item instance corresponding to the ID.
        /// </returns>
        public static InventoryItem GetFromID(string itemID)
        {
            if (itemLookupCache == null)
            {
                itemLookupCache = new Dictionary<string, InventoryItem>();
                var itemList = Resources.LoadAll<InventoryItem>("");
                foreach (var item in itemList)
                {
                    if (itemLookupCache.ContainsKey(item.itemID))
                    {
                        Debug.LogError(
                            $"Looks like there's a duplicate GameDevTV.UI.InventorySystem ID for objects: {itemLookupCache[item.itemID]} and {item}");
                        continue;
                    }

                    itemLookupCache[item.itemID] = item;
                }
            }

            if (itemID == null || !itemLookupCache.ContainsKey(itemID)) return null;
            return itemLookupCache[itemID];
        }

        /// <summary>
        /// Spawn the pickup gameobject into the world.
        /// </summary>
        /// <param name="position">Where to spawn the pickup.</param>
        /// <param name="number">How many instances of the item does the pickup represent.</param>
        /// <returns>Reference to the pickup object spawned.</returns>
        public Pickup SpawnPickup(Vector3 position, int number)
        {
            InventoryItem itemToinsert = this;
            if (!stackable && !IsInstance())
            {
                itemToinsert = Instantiate(this);
                Debug.Log($"{this} is an original non stackable SO, instantiating {itemToinsert}");
                itemToinsert.Initialize();
            }

            var pickup = Instantiate(this.pickup);
            pickup.transform.position = position;
            pickup.Setup(itemToinsert, number);
            return pickup;
        }

        public virtual bool TestRequirements(GameObject user)
        {
            return true;
        }

        public Sprite GetIcon() => icon;

        public virtual bool IsStackable() => stackable;

        public override string GetDisplayName() => displayName;

        public override string GetDescription()
        {
            return $"Level {level}\n{description}\nBase Price {GetPrice()} coins";
        }

        public bool IsDroppable() => droppable;

        public virtual bool CanStack()
        {
            return true;
        }

        public ItemCategory GetCategory() => category;

        public virtual void Initialize()
        {
        }

        public virtual void OnValidate()
        {
            
        }

        public virtual int GetPrice() => price;

        public int GetLevel() => level;

        /// <summary>
        /// Override this for items in which any level should  be able to use the item.  Example:  For currencies.
        /// </summary>
        /// <returns></returns>
        protected virtual bool ShouldHaveLevel() => true;

        public void SetLevel(int value)
        {
            if (ShouldHaveLevel())
            {
                level = value;
            }
            else
            {
                level = 0;
            }
        }

        // PRIVATE

        void ISerializationCallbackReceiver.OnBeforeSerialize()
        {
            // Generate and save a new UUID if this is blank.
            if (string.IsNullOrWhiteSpace(itemID))
            {
                itemID = System.Guid.NewGuid().ToString();
            }
        }

        void ISerializationCallbackReceiver.OnAfterDeserialize()
        {
            // Require by the ISerializationCallbackReceiver but we don't need
            // to do anything with it.
        }

        public bool IsInstance()
        {
            var otherItem = InventoryItem.GetFromID(itemID);
            //Debug.Log($"Testing to see if item is an instance:  {this}=={otherItem} = {this.Equals(otherItem)}");
            return !this.Equals(otherItem);
        }

        public virtual JToken CaptureState()
        {
            return JToken.FromObject(name);
        }

        public virtual void RestoreState(JToken state)
        {
        }

        public virtual JToken CaptureAsJToken()
        {
            return JToken.FromObject("");
        }

        public virtual void RestoreFromJToken(JToken state)
        {
        }

    #region Editor

    #if UNITY_EDITOR
        #region IMGUI
        protected static bool DrawSection(ref bool section, string caption)
        {
            section = EditorGUILayout.Foldout(section, caption);
            return section;
        }

        void SetIcon(Sprite value)
        {
            if (icon == value) return;
            SetUndo("Change Icon");
            icon = value;
        }

        protected GUIStyle indent;

        protected void BeginIndent()
        {
            EditorGUILayout.BeginVertical(indent);
        }

        protected void EndIndent()
        {
            EditorGUILayout.EndVertical();
        }

        private bool drawBasicInventory;
        public bool drawOriginalInventory;

        public virtual void DrawInspector()
        {
            indent = new GUIStyle { padding = new RectOffset(10, 10, 0, 0) };
            if (!DrawSection(ref drawBasicInventory, "Basic Inventory")) return;
            BeginIndent();
            if (GUILayout.Button($"{itemID}"))
            {
                SetItem(ref itemID, System.Guid.NewGuid().ToString(), "Change Item ID");
            }

            SetItem(ref displayName, EditorGUILayout.TextField("Display Name", displayName), "Display Name");
            DrawTextArea(ref description, "Description",
                "Raw description.  Tooltip will automatically fill in extra fields");
            SetIcon((Sprite)EditorGUILayout.ObjectField("Icon", icon, typeof(Sprite), false));
            SetObject(ref pickup, SelectPrefabByComponent("Pickup", pickup), "Pickup");
            ItemCategory newCategory = (ItemCategory)EditorGUILayout.EnumPopup(new GUIContent("Category"), category,
                IsCategorySelectable, false);
            if (newCategory != category)
            {
                SetUndo("Change Category");
                category = newCategory;
            }

            DrawIntSlider(ref price, 1, 10000, "Price");
            if (CanStack())
            {
                DrawBool(ref stackable, "Is Stackable");
            }

            if (ShouldHaveLevel())
            {
                DrawIntSlider(ref level, 1, 100, "Level", "Minimum level to use/equip the item");
            }

            DrawBool(ref droppable, "Is Droppable");

            EndIndent();
        }

        protected virtual bool IsCategorySelectable(Enum location)
        {
            //ItemCategory candidate = (ItemCategory)location;
            return true;
        }

        [NonSerialized] protected List<string> toolbarStrings;
        public string[] toolbarStringArray;

        public void BuildToolbarStrings()
        {
            if (toolbarStrings == null)
            {
                toolbarStrings = new List<string>();
                AddToolbarString();
                toolbarStringArray = new string[toolbarStrings.Count];
                for (int i = 0; i < toolbarStrings.Count; i++)
                {
                    toolbarStringArray[i] = toolbarStrings[i];
                }
            }
        }

        /// <summary>
        /// Call base.AddToolbarString() then toolbarStrings.Add("String");
        /// </summary>
        protected virtual void AddToolbarString()
        {
            toolbarStrings.Add("Tooltip");
            toolbarStrings.Add("Pickup");
        }

        /// <summary>
        /// Overrides should test previewString to see if the Tooltip belongs to it.  If it does, draw the preview
        /// window.  If not, call base.HandlePreview(previewString, r, background);
        /// </summary>
        /// <param name="previewString"></param>
        /// <param name="r"></param>
        /// <param name="background"></param>
        public virtual void HandlePreview(string previewString, Rect r, GUIStyle background,
                                          PreviewRenderUtility renderUtility)
        {
            if (previewString.Equals("Tooltip"))
            {
                DrawTooltip(r);
                return;
            }

            if (previewString.Equals("Pickup"))
            {
                if (pickup == null)
                {
                    EditorGUI.LabelField(r, "No Pickup Selected!");
                    return;
                }

                RenderPreviewObject(r, background, renderUtility, pickup.gameObject);
            }
        }

        public void DrawTooltip(Rect r)
        {
            GUIStyle style = new GUIStyle();
            style.wordWrap = true;
            style.stretchHeight = true;
            style.alignment = TextAnchor.UpperCenter;
            style.fontSize = 16;
            style.richText = true;
            EditorGUI.LabelField(r,
                $"<color=#ffffff>{GetDisplayName()}</color><color=#888888>\n{GetDescription()}</color>",
                style);
        }


        private static GameObject lastItemRendered;

        public static void RenderPreviewObject(Rect r, GUIStyle background, PreviewRenderUtility renderUtility,
                                               GameObject objectToRender)
        {
            if (objectToRender == null)
            {
                EditorGUI.LabelField(r, "No Model");
                return;
            }

            if (lastItemRendered != objectToRender)
                renderUtility.BeginPreview(r, background);
            renderUtility.InstantiatePrefabInScene(objectToRender);
            renderUtility.Render(true, true);
            renderUtility.EndAndDrawPreview(r);
        }

        private int selectedWindow = 0;

        public virtual GameObject GetPreviewObject(string tooltipName)
        {
            if (tooltipName == "Pickup") return pickup.gameObject;
            return null;
        }

        public virtual bool shouldRedraw()
        {
            return false;
        }
#endregion
    
    #region UIToolkit

        [NonSerialized] protected ScrollView scrollContainer;
        [NonSerialized] protected Action callback;
        [NonSerialized] protected SerializedObject serializedObject;

        protected SerializedProperty FindProperty(string propertyName)
        {
            return serializedObject.FindProperty(propertyName);
        }
        
        protected void RedrawPreview()
        {
            callback?.Invoke();
        }
        
        protected void BindProperty<T>(out T bindableElement, string element, string property) where T:VisualElement, IBindable
        {
            FindElement(out bindableElement, element);
            bindableElement.BindProperty(GetProperty(property));
        }

        protected SerializedProperty GetProperty(string propertyToFind)
        {
           
            SerializedProperty property = serializedObject.FindProperty(propertyToFind);
            return property;
        }

        protected void ApplyModifiedProperties()
        {
            serializedObject.ApplyModifiedProperties();
        }

        protected void FindElement<T>(out T visualElement, string id) where T:VisualElement
        {
            visualElement = scrollContainer.Q<T>(id);
        }

        public virtual void CreateGUI(ScrollView container, System.Action _callback)
        {
            callback = _callback;
            scrollContainer = container;
            serializedObject = new SerializedObject(this);
            VisualTreeAsset basicInventoryItemUXML = Resources.Load<VisualTreeAsset>("BasicInventoryItemUXML");
            VisualElement basicInventoryItem = basicInventoryItemUXML.CloneTree();
            container.Add(basicInventoryItem);
            SetupItemIDButton();
            BindProperty(out TextField displayNameTextField, "displayNameTextField", "displayName");
            displayNameTextField.RegisterValueChangedCallback((evt) =>
            {
                if (evt.newValue.Contains("\t"))
                {
                    string newValue = evt.newValue.Replace("\t", "");
                    displayNameTextField.SetValueWithoutNotify(newValue);
                }
                ApplyModifiedProperties();
                RedrawPreview();
            });
            BindProperty(out TextField descriptionTextField, "descriptionTextField", "description");
            //descriptionTextField.isDelayed = true;
            
            descriptionTextField.RegisterValueChangedCallback((evt) =>
            {
                ApplyModifiedProperties();
                RedrawPreview();
            });
            SetupIconObjectField();
            SetupPickup();
            BindProperty(out EnumField itemCategoryEnumField, "itemCategoryEnumField", "category");
            BindProperty(out SliderInt priceIntSlider, "priceIntSlider", "price");
            priceIntSlider.RegisterValueChangedCallback((evt) =>
            {
                ApplyModifiedProperties();
                RedrawPreview();
            });
            BindProperty(out SliderInt levelIntSlider, "levelIntSlider", "level");
            levelIntSlider.RegisterValueChangedCallback((evt)=>
            {
                ApplyModifiedProperties();
                RedrawPreview();
            });
            BindProperty(out Toggle droppableToggle, "droppableToggle", "droppable");
            BindProperty(out Toggle stackableToggle, "stackableToggle", "stackable");
            if (!CanStack())
            {
                stackableToggle.style.display = DisplayStyle.None;
            }
            
        }

        private void SetupPickup()
        {
            List<Pickup> pickups = Statics.GetAssetsOfType<Pickup>();
            if (pickup == null)
            {
                pickup = pickups[0];
                EditorUtility.SetDirty(this);
            }

            FindElement(out DropdownField pickupDropdownField, "pickupDropdownField");
            pickupDropdownField.choices = Statics.GetNamesFromList(pickups);
            pickupDropdownField.index = pickups.IndexOf(pickup);
            pickupDropdownField.RegisterValueChangedCallback((evt) =>
            {
                var property = serializedObject.FindProperty("pickup");
                property.objectReferenceValue = pickups[pickupDropdownField.index];
                ApplyModifiedProperties();
            });
        }


        private void SetupIconObjectField()
        {
            BindProperty(out ObjectField iconObjectField, "iconObjectField", "icon");
            FindElement(out Image iconPreview, "iconPreview");
            if (icon != null)
            {
                iconPreview.sprite = icon;
            }

            iconObjectField.RegisterValueChangedCallback((evt) =>
            {
                iconPreview.sprite = evt.newValue == null ? null : (Sprite)evt.newValue;
                ApplyModifiedProperties();
                RedrawPreview();
            });
        }


        private void SetupItemIDButton()
        {
            FindElement(out Button itemIDButton, "itemIDButton");
            itemIDButton.text = $"ID = {itemID}. Click To Change";
            itemIDButton.clicked += () =>
            {
                ResetItemID();
                itemIDButton.text = $"ID = {itemID}, Click To Change";
            };
        }

        public void ResetItemID()
        {
            if (serializedObject == null) serializedObject = new SerializedObject(this);
            var property = serializedObject.FindProperty("itemID");
            property.stringValue = System.Guid.NewGuid().ToString();
            ApplyModifiedProperties();
        }

    #endregion

    #endif

    #endregion
    }
}