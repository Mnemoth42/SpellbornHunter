﻿using System;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace GameDevTV.Inventories
{
    public abstract class RetrievableScriptableObject : ScriptableObject, IHasItemID, ISerializationCallbackReceiver
    {
        [Header("Unique UUID for saving references to this Scriptable Object", order = 0)] [SerializeField]
        protected string itemID;

        public string GetItemID()
        {
            return itemID;
        }

        public void ReIssueItemID()
        {
            itemID = Guid.NewGuid().ToString();
        }

        public abstract string GetDisplayName();
        public abstract string GetDescription();

        void ISerializationCallbackReceiver.OnBeforeSerialize()
        {
            // Generate and save a new UUID if this is blank.
            if (string.IsNullOrWhiteSpace(itemID))
            {
                itemID = Guid.NewGuid().ToString();
            }
            // Test for multiple objects with the same UUID
            //var items = Resources.LoadAll<RetrievableScriptableObject>("").Where(p => p.GetItemID() == itemId).ToList();
            //if (items.Count > 1)
            //{
            //    itemId = Guid.NewGuid().ToString();
            //}
        }

        void ISerializationCallbackReceiver.OnAfterDeserialize()
        {
            // Require by the ISerializationCallbackReceiver but we don't need
            // to do anything with it.
        }

    #if UNITY_EDITOR

        /// <summary>
        /// Helper function to automatically manage Undo
        /// </summary>
        /// <param name="caption"></param>
        public void SetUndo(string caption = "Whatever You Just Did")
        {
            Undo.RecordObject(this, caption);
            EditorUtility.SetDirty(this);
        }


        /// <summary>
        /// Generic method to set a value.  Works mostly with any class that implements IComparable
        /// </summary>
        /// <param name="itemToChange"></param>
        /// <param name="value"></param>
        /// <param name="caption"></param>
        /// <typeparam name="T"></typeparam>
        protected void SetItem<T>(ref T itemToChange, T value, string caption = "item") where T : IComparable<T>
        {
            if (itemToChange.Equals(value)) return;
            SetUndo($"Set {caption}");
            itemToChange = value;
        }

        /// <summary>
        /// Generic item setting with Undo.  Does not do comparison checks, so is useable with a broader variety of classes
        /// </summary>
        /// <param name="itemToChange"></param>
        /// <param name="value"></param>
        /// <param name="caption"></param>
        /// <typeparam name="T"></typeparam>
        protected void SetItemNoCheck<T>(T itemToChange, T value, string caption = "item")
        {
            SetUndo($"Set {caption}");
            itemToChange = value;
        }

        /// <summary>
        /// Draws a float field and stores the value in itemToChange (which is passed by reference)
        /// </summary>
        /// <param name="itemToChange"></param>
        /// <param name="title"></param>
        /// <param name="tooltip"></param>
        protected void DrawFloat(ref float itemToChange, string title = "Float Value", string tooltip = "")
        {
            GUIContent label = new GUIContent(title, tooltip);
            SetItem(ref itemToChange, EditorGUILayout.FloatField(label, itemToChange), title);
        }

        /// <summary>
        /// Draws the referenced float value as an IntSlider (to keep values whole, but still return a float)  Stores
        /// result in the referenced float.
        /// </summary>
        /// <param name="itemToChange"></param>
        /// <param name="minValue"></param>
        /// <param name="maxValue"></param>
        /// <param name="title"></param>
        /// <param name="tooltip"></param>
        protected void DrawFloatAsIntSlider(ref float itemToChange, int minValue = 1, int maxValue = 100,
                                            string title = "Float Value", string tooltip = "")
        {
            GUIContent label = new GUIContent(title, tooltip);
            SetItem(ref itemToChange, EditorGUILayout.IntSlider(label, (int)itemToChange, minValue, maxValue), title);
        }

        /// <summary>
        /// Draws the referenced Int as an IntSlider.  Stores the result in the referenced int.
        /// </summary>
        /// <param name="itemToChange"></param>
        /// <param name="minValue"></param>
        /// <param name="maxValue"></param>
        /// <param name="title"></param>
        /// <param name="tooltip"></param>
        protected void DrawIntSlider(ref int itemToChange, int minValue = 1, int maxValue = 100,
                                     string title = "Int Value", string tooltip = "")
        {
            GUIContent label = new GUIContent(title, tooltip);
            SetItem(ref itemToChange, EditorGUILayout.IntSlider(label, itemToChange, minValue, maxValue), title);
        }

        /// <summary>
        /// Draws the referenced boolean value as a Boolean checkbox (toggle).  Stores result in the referenced value.
        /// </summary>
        /// <param name="itemToChange"></param>
        /// <param name="title"></param>
        /// <param name="tooltip"></param>
        public void DrawBool(ref bool itemToChange, string title = "Boolean Value", string tooltip = "")
        {
            GUIContent label = new GUIContent(title, tooltip);
            SetItem(ref itemToChange, EditorGUILayout.Toggle(label, itemToChange));
        }

        protected void DrawTextField(ref string itemToChange, string title = "String Value", string tooltip = "")
        {
            GUIContent label = new GUIContent(title, tooltip);
            SetItem(ref itemToChange, EditorGUILayout.TextField(label, itemToChange));
        }

        protected void DrawTextArea(ref string itemToChange, string title = "String Value", string tooltip = "")
        {
            GUIContent label = new GUIContent(title, tooltip);
            GUIStyle longStyle = new GUIStyle(GUI.skin.textArea) { wordWrap = true };
            EditorGUILayout.LabelField(label);
            SetItem(ref itemToChange, EditorGUILayout.TextArea(itemToChange, longStyle));
        }

        protected void AddToList<T>(List<T> listToAdd, T itemToAdd, string caption = "Add Item")
        {
            SetUndo(caption);
            listToAdd.Add(itemToAdd);
        }

        protected void RemoveFromList<T>(List<T> list, int index, string caption = "Remove Item")
        {
            SetUndo(caption);
            list.RemoveAt(index);
        }

        protected void SetObject<T>(ref T obj, T value, string caption) where T : MonoBehaviour
        {
            if (obj == value) return;
            SetUndo(caption);
            obj = value;
        }

        protected T SelectPrefabByComponent<T>(GUIContent label, T obj) where T : MonoBehaviour
        {
            List<T> pickupsList = Statics.GetAssetsOfType<T>();
            List<string> pickupNamesList = Statics.GetNamesFromList(pickupsList);
            T result = null;
            if (pickupsList.Count > 0)
            {
                int pickupSlot = Statics.FindNameInList(pickupNamesList, obj);
                pickupSlot = EditorGUILayout.Popup(label, pickupSlot, pickupNamesList.ToArray());
                if (pickupsList.Count > pickupSlot)
                {
                    result = pickupsList[pickupSlot];
                }
            }
            else
            {
                EditorGUILayout.HelpBox("No items found to assign!", MessageType.Error);
            }

            return result;
        }

        protected T SelectPrefabByComponentWithNull<T>(GUIContent label, T obj) where T : MonoBehaviour
        {
            List<T> pickupsList = Statics.GetAssetsOfType<T>();
            List<string> pickupNamesList = Statics.GetNamesFromList(pickupsList);
            T result = null;
            if (pickupsList.Count > 0)
            {
                pickupNamesList.Insert(0, "None");
                int pickupSlot = Statics.FindNameInList(pickupNamesList, obj);
                pickupSlot = EditorGUILayout.Popup(label, pickupSlot, pickupNamesList.ToArray());
                if (pickupSlot > 0)
                {
                    result = pickupsList[pickupSlot - 1];
                }
            }
            else
            {
                EditorGUILayout.HelpBox("No Pickups found to assign!", MessageType.Error);
            }

            return result;
        }

        protected T SelectPrefabByComponentWithNull<T>(string label, T obj) where T : MonoBehaviour
        {
            return SelectPrefabByComponentWithNull(new GUIContent(label), obj);
        }

        protected T SelectScriptableObject<T>(GUIContent label, T obj) where T : RetrievableScriptableObject
        {
            List<T> pickupsList = Statics.GetScriptableObjectsOfType<T>();
            List<string> pickupNamesList = Statics.GetNamesFromScriptableObjectList(pickupsList);
            T result = null;
            if (pickupsList.Count > 0)
            {
                int pickupSlot = Statics.FindNameInScriptableObjectList(pickupNamesList, obj);
                pickupSlot = EditorGUILayout.Popup(label, pickupSlot, pickupNamesList.ToArray());
                if (pickupsList.Count > pickupSlot)
                {
                    result = pickupsList[pickupSlot];
                }
            }
            else
            {
                EditorGUILayout.HelpBox($"No {typeof(T)} found to assign!", MessageType.Error);
            }

            return result;
        }


        protected T SelectPrefabByComponent<T>(string label, T obj) where T : MonoBehaviour
        {
            return SelectPrefabByComponent(new GUIContent(label), obj);
        }

        protected T SelectScriptableObject<T>(string label, T obj) where T : RetrievableScriptableObject
        {
            GUIContent contentLabel = new GUIContent(label);
            if (obj != null) contentLabel.tooltip = obj.GetDescription();
            return SelectScriptableObject(contentLabel, obj);
        }

        protected T SelectScriptableObject<T>(T obj) where T : RetrievableScriptableObject
        {
            List<T> pickupsList = Statics.GetScriptableObjectsOfType<T>();
            List<string> pickupNamesList = Statics.GetNamesFromScriptableObjectList(pickupsList);
            T result = null;
            if (pickupsList.Count > 0)
            {
                int pickupSlot = Statics.FindNameInScriptableObjectList(pickupNamesList, obj);
                pickupSlot = EditorGUILayout.Popup(pickupSlot, pickupNamesList.ToArray());
                if (pickupsList.Count > pickupSlot)
                {
                    result = pickupsList[pickupSlot];
                }
            }
            else
            {
                EditorGUILayout.HelpBox($"No {typeof(T)} found to assign!", MessageType.Error);
            }

            return result;
        }

        protected T SelectScriptableObjectWithNull<T>(T obj) where T : RetrievableScriptableObject
        {
            List<T> pickupsList = Statics.GetScriptableObjectsOfType<T>();
            List<string> pickupNamesList = Statics.GetNamesFromScriptableObjectList(pickupsList);
            T result = null;
            if (pickupsList.Count > 0)
            {
                pickupNamesList.Insert(0, "None");
                int pickupSlot = Statics.FindNameInScriptableObjectList(pickupNamesList, obj);
                pickupSlot = EditorGUILayout.Popup(pickupSlot, pickupNamesList.ToArray());
                if (pickupSlot > 0)
                {
                    result = pickupsList[pickupSlot - 1];
                }
            }
            else
            {
                EditorGUILayout.HelpBox($"No {typeof(T)} found to assign!", MessageType.Error);
            }

            return result;
        }


    #endif
    }
}