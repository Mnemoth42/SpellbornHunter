﻿namespace GameDevTV.Inventories
{
    public interface IHasItemID
    {
        string GetItemID();
    }
}