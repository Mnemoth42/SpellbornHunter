using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GameDevTV.Inventories
{
    public interface IInventoryClickHandler
    {
        int HandleInventoryItemClicked(InventoryItem item, int amount);
    }
}