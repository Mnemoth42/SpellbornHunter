namespace GameDevTV.Inventories
{
    public enum ItemCategory
    {
        None,
        Equipment,
        Material,
        Potion,
        Action,
        Special
    }
}