using System;
using System.Collections;
using System.Collections.Generic;
using GameDevTV.Saving;
using Newtonsoft.Json.Linq;
using UnityEngine;

namespace GameDevTV.Inventories
{
    [DisallowMultipleComponent]
    public class Purse : MonoBehaviour, IItemStore, ISaveable, IJsonSaveable
    {
        [SerializeField] private int initialCopper;

        private Dictionary<CurrencyTypes, int> purse = new Dictionary<CurrencyTypes, int>();

        public event Action onCurrencyChanged;

        private void Start()
        {
            if (!purse.ContainsKey(CurrencyTypes.Copper))
            {
                purse[CurrencyTypes.Copper] = initialCopper;
            }

            onCurrencyChanged?.Invoke();
        }


        public int GetBalance(CurrencyTypes type)
        {
            InitializeBalance(type);
            return purse[type];
        }


        public bool CanTransact(CurrencyTypes type, int amount)
        {
            InitializeBalance(type);
            return purse[type] - amount >= 0;
        }

        public bool AddToBalance(CurrencyTypes type, int amount)
        {
            InitializeBalance(type);
            if (amount < 0) return TakeFromBalance(type, -amount);
            purse[type] += amount;
            onCurrencyChanged?.Invoke();
            return true;
        }

        public bool TakeFromBalance(CurrencyTypes type, int amount)
        {
            InitializeBalance(type);
            if (amount < 0) return AddToBalance(type, -amount);
            if (amount > purse[type]) return false;
            purse[type] -= amount;
            onCurrencyChanged?.Invoke();
            return true;
        }

        private void InitializeBalance(CurrencyTypes type)
        {
            if (!purse.ContainsKey(type)) purse[type] = 0;
        }

        public int AddItem(InventoryItem item, int quantity)
        {
            if (item is CurrencyItem currencyItem)
            {
                AddToBalance(currencyItem.GetCurrencyType(), quantity);
                return quantity;
            }

            return 0;
        }

        public JToken CaptureState()
        {
            return JToken.FromObject(purse);
        }

        public void RestoreState(JToken state)
        {
            purse = state.ToObject<Dictionary<CurrencyTypes, int>>();
            onCurrencyChanged?.Invoke();
        }

        public JToken CaptureAsJToken()
        {
            return JToken.FromObject(purse);
        }

        public void RestoreFromJToken(JToken state)
        {
            purse = state.ToObject<Dictionary<CurrencyTypes, int>>();
            onCurrencyChanged?.Invoke();
        }
    }
}