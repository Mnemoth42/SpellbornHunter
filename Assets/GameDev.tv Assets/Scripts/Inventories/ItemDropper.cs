﻿using GameDevTV.Saving;
using System.Collections.Generic;
using Newtonsoft.Json.Linq;
using UnityEngine;

namespace GameDevTV.Inventories
{
    /// <summary>
    /// To be placed on anything that wishes to drop pickups into the world.
    /// Tracks the drops for saving and restoring.
    /// </summary>
    public class ItemDropper : MonoBehaviour, ISaveable, IJsonSaveable
    {
        // STATE
        private List<Pickup> droppedItems = new List<Pickup>();


        // PUBLIC

        /// <summary>
        /// Create a pickup at the current position.
        /// </summary>
        /// <param name="item">The item type for the pickup.</param>
        /// <param name="number">
        /// The number of items contained in the pickup. Only used if the item
        /// is stackable.
        /// </param>
        public void DropItem(InventoryItem item, int number)
        {
            SpawnPickup(item, GetDropLocation(), number);
        }

        /// <summary>
        /// Create a pickup at the current position.
        /// </summary>
        /// <param name="item">The item type for the pickup.</param>
        public void DropItem(InventoryItem item)
        {
            SpawnPickup(item, GetDropLocation(), 1);
        }

        // PROTECTED

        /// <summary>
        /// Override to set a custom method for locating a drop.
        /// </summary>
        /// <returns>The location the drop should be spawned.</returns>
        protected virtual Vector3 GetDropLocation()
        {
            return transform.position;
        }

        // PRIVATE

        public void SpawnPickup(InventoryItem item, Vector3 spawnLocation, int number)
        {
            var pickup = item.SpawnPickup(spawnLocation, number);
            droppedItems.Add(pickup);
        }

        [System.Serializable]
        private struct DropRecord
        {
            public string itemID;
            public Vector3 position;
            public int number;
            public JToken state;
        }

        JToken ISaveable.CaptureState()
        {
            RemoveDestroyedDrops();
            var droppedItemsList = new DropRecord[droppedItems.Count];
            for (int i = 0; i < droppedItemsList.Length; i++)
            {
                droppedItemsList[i].itemID = droppedItems[i].GetItem().GetItemID();
                droppedItemsList[i].position = droppedItems[i].transform.position;
                droppedItemsList[i].number = droppedItems[i].GetNumber();
                droppedItemsList[i].state = droppedItems[i].GetItem().CaptureState();
            }

            return JToken.FromObject(droppedItemsList);
        }

        void ISaveable.RestoreState(JToken state)
        {
            var droppedItemsList = state.ToObject<DropRecord[]>();
            foreach (var item in droppedItemsList)
            {
                var pickupItem = InventoryItem.GetFromID(item.itemID);
                if (!pickupItem.IsStackable())
                {
                    var newItem = Instantiate(pickupItem);
                    newItem.RestoreState(item.state);
                    pickupItem = newItem;
                }

                Vector3 position = item.position;
                int number = item.number;
                SpawnPickup(pickupItem, position, number);
            }
        }

        /// <summary>
        /// Remove any drops in the world that have subsequently been picked up.
        /// </summary>
        private void RemoveDestroyedDrops()
        {
            var newList = new List<Pickup>();
            foreach (var item in droppedItems)
            {
                if (item != null)
                {
                    newList.Add(item);
                }
            }

            droppedItems = newList;
        }

        [System.Serializable]
        private struct JsonDropRecord
        {
            public string itemID;
            public JToken position;
            public int number;
            public JToken state;
        }

        public JToken CaptureAsJToken()
        {
            RemoveDestroyedDrops();
            var droppedItemsList = new JsonDropRecord[droppedItems.Count];
            for (int i = 0; i < droppedItemsList.Length; i++)
            {
                droppedItemsList[i].itemID = droppedItems[i].GetItem().GetItemID();
                droppedItemsList[i].position = droppedItems[i].transform.position.ToToken();
                droppedItemsList[i].number = droppedItems[i].GetNumber();
                droppedItemsList[i].state = droppedItems[i].GetItem().CaptureState();
            }

            return JToken.FromObject(droppedItemsList);
        }

        public void RestoreFromJToken(JToken state)
        {
            var droppedItemsList = state.ToObject<JsonDropRecord[]>();
            foreach (var item in droppedItemsList)
            {
                var pickupItem = InventoryItem.GetFromID(item.itemID);
                if (!pickupItem.IsStackable())
                {
                    var newItem = Instantiate(pickupItem);
                    newItem.RestoreState(item.state);
                    pickupItem = newItem;
                }

                Vector3 position = item.position.ToVector3();
                int number = item.number;
                SpawnPickup(pickupItem, position, number);
            }
        }
    }
}