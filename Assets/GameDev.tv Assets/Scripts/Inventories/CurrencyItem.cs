using UnityEditor;
using UnityEngine;

namespace GameDevTV.Inventories
{
    [System.Serializable]
    public enum CurrencyTypes
    {
        Copper,
        Silver,
        Gold,
        Platinum,
        Irridium,
        Saphires,
        Rubies,
        Diamonds
    }

    /// <summary>
    ///   
    /// </summary>
    [CreateAssetMenu(fileName = "New Currency", menuName = "RPG/Currency/Coin Type")]
    public class CurrencyItem : InventoryItem
    {
        [SerializeField] private CurrencyTypes currencyType;

        public CurrencyTypes GetCurrencyType() => currencyType;

        protected override bool ShouldHaveLevel()
        {
            return false;
        }

    #if UNITY_EDITOR


        public override void DrawInspector()
        {
            base.DrawInspector();
            GUIContent content = new GUIContent("Currency Type",
                "Used to determine which balance to add to in Purse");
            EditorGUI.BeginChangeCheck();
            var newType = (CurrencyTypes)EditorGUILayout.EnumPopup(content, currencyType);
            if (EditorGUI.EndChangeCheck())
            {
                SetUndo("Change Currency Type");
                currencyType = newType;
            }
        }
    #endif
    }
}