﻿namespace GameDevTV.Inventories
{
    [System.Serializable]
    public struct InventoryRecord
    {
        public InventoryItem item;
        public int amount;

        public InventoryRecord(InventoryItem item, int amount)
        {
            this.item = item;
            this.amount = amount;
        }
    }
}