﻿using System;
using System.Collections.Generic;
using UnityEngine;
using GameDevTV.Saving;
using Newtonsoft.Json.Linq;

namespace GameDevTV.Inventories
{
    /// <summary>
    /// Provides a store for the items equipped to a player. Items are stored by
    /// their equip locations.
    /// 
    /// This component should be placed on the GameObject tagged "Player".
    /// </summary>
    [DisallowMultipleComponent]
    public class Equipment : MonoBehaviour, ISaveable, IJsonSaveable
    {
        // STATE
        Dictionary<EquipLocation, EquipableItem> equippedItems = new Dictionary<EquipLocation, EquipableItem>();

        // PUBLIC

        /// <summary>
        /// Broadcasts when the items in the slots are added/removed.
        /// </summary>
        public event Action equipmentUpdated;
        

        public Dictionary<EquipLocation, EquipableItem> EquippedItems => equippedItems;

        /// <summary>
        /// Return the item in the given equip location.
        /// </summary>
        public EquipableItem GetItemInSlot(EquipLocation equipLocation)
        {
            if (!equippedItems.ContainsKey(equipLocation))
            {
                return null;
            }

            return equippedItems[equipLocation];
        }

        /// <summary>
        /// Add an item to the given equip location. Do not attempt to equip to
        /// an incompatible slot.
        /// </summary>
        public void AddItem(EquipLocation slot, EquipableItem item)
        {
            Debug.Assert(item.GetAllowedEquipLocation() == slot);
            if (!Application.isPlaying)
            {
                // Debug.Log($"Equipping {item}");
            }

            equippedItems[slot] = item;

            if (equipmentUpdated != null)
            {
                equipmentUpdated();
            }
        }

        /// <summary>
        /// Remove the item for the given slot.
        /// </summary>
        public void RemoveItem(EquipLocation slot)
        {
            equippedItems.Remove(slot);
            if (equipmentUpdated != null)
            {
                equipmentUpdated();
            }
        }


        /// <summary>
        /// Enumerate through all the slots that currently contain items.
        /// </summary>
        public IEnumerable<EquipLocation> GetAllPopulatedSlots()
        {
            return equippedItems.Keys;
        }

        // PRIVATE

        [System.Serializable]
        struct EquipmentData
        {
            public string itemID;
            public JToken state;
        }

        JToken ISaveable.CaptureState()
        {
            var equippedItemsForSerialization = new Dictionary<EquipLocation, EquipmentData>();
            foreach (var pair in equippedItems)
            {
                EquipmentData data = new EquipmentData();
                data.itemID = pair.Value.GetItemID();
                data.state = pair.Value.CaptureState();
                equippedItemsForSerialization[pair.Key] = data;
            }

            return JToken.FromObject(equippedItemsForSerialization);
        }

        void ISaveable.RestoreState(JToken state)
        {
            equippedItems = new Dictionary<EquipLocation, EquipableItem>();
            IDictionary<string, JToken> stateDict = (JObject)state;
            var equippedItemsForSerialization = state.ToObject<Dictionary<EquipLocation, EquipmentData>>();

            foreach (var pair in equippedItemsForSerialization)
            {
                var item = (EquipableItem)InventoryItem.GetFromID(pair.Value.itemID);
                if (item != null)
                {
                    equippedItems[pair.Key] = Instantiate(item);
                    equippedItems[pair.Key].RestoreState(pair.Value.state);
                }
            }
        }


        public JToken CaptureAsJToken()
        {
            var equippedItemsForSerialization = new Dictionary<EquipLocation, EquipmentData>();
            foreach (var pair in equippedItems)
            {
                EquipmentData data = new EquipmentData
                                     {
                                         itemID = pair.Value.GetItemID(),
                                         state = pair.Value.CaptureState()
                                     };
                equippedItemsForSerialization[pair.Key] = data;
            }

            return JToken.FromObject(equippedItemsForSerialization);
        }

        public void RestoreFromJToken(JToken state)
        {
            equippedItems = new Dictionary<EquipLocation, EquipableItem>();
            IDictionary<string, JToken> stateDict = (JObject)state;
            var equippedItemsForSerialization = state.ToObject<Dictionary<EquipLocation, EquipmentData>>();

            foreach (var pair in equippedItemsForSerialization)
            {
                var item = (EquipableItem)InventoryItem.GetFromID(pair.Value.itemID);
                if (item != null)
                {
                    equippedItems[pair.Key] = Instantiate(item);
                    equippedItems[pair.Key].RestoreState(pair.Value.state);
                }
            }
        }

        public virtual bool CanEquip(EquipableItem equipableItem)
        {
            return equipableItem;
        }
    }
}