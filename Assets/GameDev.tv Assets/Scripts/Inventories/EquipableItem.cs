using System;
using GameDevTV.Utils;
using UnityEditor;
using UnityEditor.UIElements;
using UnityEngine;
using UnityEngine.UIElements;

namespace GameDevTV.Inventories
{
    /// <summary>
    /// An inventory item that can be equipped to the player. Weapons could be a
    /// subclass of this.
    /// </summary>
    public abstract class EquipableItem : InventoryItem
    {
        // CONFIG DATA
        [Tooltip("Where are we allowed to put this item.")] [SerializeField]
        EquipLocation allowedEquipLocation = EquipLocation.Weapon;
        [SerializeField] private Condition equipCondition = new Condition();

        // PUBLIC

        public EquipLocation GetAllowedEquipLocation()
        {
            return allowedEquipLocation;
        }

        public override bool CanStack()
        {
            return false;
        }

        protected void SetAllowedEquipLocation(EquipLocation location)
        {
            allowedEquipLocation = location;
        }

    #if UNITY_EDITOR
        void SetEquipLocation(EquipLocation value)
        {
            if (allowedEquipLocation == value) return;
            SetUndo("Equip Location");
            allowedEquipLocation = value;
        }

        protected virtual bool IsLocationSelectable(Enum location)
        {
            EquipLocation candidate = (EquipLocation)location;
            return candidate != EquipLocation.Weapon;
        }

        protected override bool IsCategorySelectable(Enum location)
        {
            ItemCategory candidate = (ItemCategory)location;
            return candidate == ItemCategory.Equipment;
        }


        public override void DrawInspector()
        {
            base.DrawInspector();
            BeginIndent();
            SetEquipLocation((EquipLocation)EditorGUILayout.EnumPopup(new GUIContent("EquipLocation"),
                allowedEquipLocation, IsLocationSelectable, false));
            EndIndent();
        }

        public override void CreateGUI(ScrollView container, Action _callback)
        {
            base.CreateGUI(container, _callback);
            VisualTreeAsset EquipableItemUXML = Resources.Load<VisualTreeAsset>("EquipableItemUXML");
            container.Add(EquipableItemUXML.CloneTree());
            BindProperty(out EnumField allowedEquipLocationEnumField, "allowedEquipLocationEnumField", "allowedEquipLocation");
            if (equipCondition == null) equipCondition = new Condition();
             BindProperty(out PropertyField equipConditionPropertyField, "equipConditionPropertyField", "equipCondition");
        }
    #endif
    }
}