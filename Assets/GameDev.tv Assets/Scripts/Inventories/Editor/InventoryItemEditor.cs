using TkrainDesigns.Inventories;
using UnityEditor;
using UnityEngine;

namespace GameDevTV.Inventories.Editors
{
    [CustomEditor(typeof(InventoryItem), true)]
    public class InventoryItemEditor : Editor
    {
        private InventoryItem currentItem;

        private InventoryItem item
        {
            get
            {
                InventoryItem potentialTarget = target as InventoryItem;
                if (currentItem == null)
                {
                    currentItem = potentialTarget;
                    return currentItem;
                }

                if (potentialTarget != null && potentialTarget.GetItemID() != currentItem.GetItemID())
                    currentItem = potentialTarget;
                return currentItem;
            }
        }

        public override void OnInspectorGUI()
        {
            if (GUILayout.Button(item.drawOriginalInventory ? "Change To Custom Editor" : "Change to Classic Editor"))
                item.drawOriginalInventory = !item.drawOriginalInventory;
            if (item.drawOriginalInventory)
                base.OnInspectorGUI();
            else
                item.DrawInspector();
        }

        private PreviewRenderUtility renderUtility;

        public override bool HasPreviewGUI()
        {
            item.BuildToolbarStrings();
            if (renderUtility == null)
            {
                renderUtility = new PreviewRenderUtility();
                var cameraTransform = renderUtility.camera.transform;
                cameraTransform.position = new Vector3(0, .5f, -6);
                cameraTransform.rotation = Quaternion.identity;
            }

            return true;
        }

        private int selectedWindow;
        private Vector2 drag = Vector2.zero;
        private float distance = 6.0f;

        public override void OnInteractivePreviewGUI(Rect r, GUIStyle background)
        {
            if (selectedWindow == 0)
                item.DrawTooltip(r);
            else
                DrawPreviewObject(r, background);
        }

        private GameObject instantiatedPreviewCharacter;

        private void DrawPreviewObject(Rect r, GUIStyle background)
        {
            drag = Drag2D(drag, r, ref distance);

            GameObject go = item.GetPreviewObject(item.toolbarStringArray[selectedWindow]);
            if (Event.current.type == EventType.Repaint && go != null)
            {
                instantiatedPreviewCharacter = null;
                renderUtility.Cleanup();
                renderUtility = new PreviewRenderUtility();
                if (go)
                {
                    instantiatedPreviewCharacter = renderUtility.InstantiatePrefabInScene(go);
                    instantiatedPreviewCharacter.transform.position = new Vector3(0, 0, 0);
                    if (instantiatedPreviewCharacter.TryGetComponent(out Equipment equipment) &&
                        item is EquipableItem equipableItem)
                    {
                        equipment.AddItem(equipableItem.GetAllowedEquipLocation(), equipableItem);
                        float y = -1;
                        // ReSharper disable once UnusedVariable
                        if (equipment.TryGetComponent(out Pickup pickup)) y = 0;
                        instantiatedPreviewCharacter.transform.position = new Vector3(0, y, 0);
                        instantiatedPreviewCharacter.transform.localRotation = Quaternion.Euler(0, 180, 0);
                        if (equipment.TryGetComponent(out CharacterGenerator generator)) generator.LoadArmor();
                    }
                    else
                    {
                        Debug.Log("Not Character");
                    }
                }

                Transform cameraTransform = renderUtility.camera.transform;
                cameraTransform.position = Vector2.zero;
                cameraTransform.rotation = Quaternion.Euler(new Vector3(-drag.y, -drag.x, 0));
                cameraTransform.position = cameraTransform.forward * -distance;

                renderUtility.BeginPreview(r, background);
                renderUtility.Render(true, true);
                renderUtility.EndAndDrawPreview(r);
            }
        }

        public override void OnPreviewSettings()
        {
            if (selectedWindow > 0 && GUILayout.Button("Reset Camera", EditorStyles.whiteMiniLabel))
                drag = Vector2.zero;
            item.BuildToolbarStrings();
            GUIStyle bold = EditorStyles.whiteMiniLabel;
            bold.richText = true;
            for (int i = 0; i < item.toolbarStringArray.Length; i++)
            {
                string value = i == selectedWindow
                    ? $"{item.toolbarStringArray[i].ToUpper()}"
                    : item.toolbarStringArray[i];

                if (GUILayout.Button(value, bold)) selectedWindow = i;
            }
        }

        private void OnDisable()
        {
            renderUtility?.Cleanup();
        }

        public static Vector2 Drag2D(Vector2 scrollPosition, Rect position, ref float distance)
        {
            int controlID = GUIUtility.GetControlID("Slider".GetHashCode(), FocusType.Passive);
            Event current = Event.current;

            switch (current.GetTypeForControl(controlID))
            {
                case EventType.MouseDown:
                    if (position.Contains(current.mousePosition) && position.width > 50f)
                    {
                        GUIUtility.hotControl = controlID;
                        current.Use();
                        EditorGUIUtility.SetWantsMouseJumping(1);
                    }

                    break;
                case EventType.MouseUp:
                    if (GUIUtility.hotControl == controlID) GUIUtility.hotControl = 0;
                    EditorGUIUtility.SetWantsMouseJumping(0);
                    break;
                case EventType.MouseDrag:
                    if (GUIUtility.hotControl == controlID)
                    {
                        {
                            if (current.shift)
                            {
                                distance += current.delta.y / Mathf.Min(position.width, position.height) * 280f;
                                distance = Mathf.Clamp(distance, 3, 9);
                            }
                            else
                            {
                                scrollPosition -= current.delta * ((!current.shift) ? 1 : 3) /
                                    Mathf.Min(position.width, position.height) * 140f;
                                scrollPosition.y = Mathf.Clamp(scrollPosition.y, -90f, 90f);
                            }
                        }

                        current.Use();
                        GUI.changed = true;
                    }

                    break;
            }

            return scrollPosition;
        }
    }
}