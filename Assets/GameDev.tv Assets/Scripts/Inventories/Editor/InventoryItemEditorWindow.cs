using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text;
using GameDevTV.Inventories;
using UnityEditor;
using UnityEditor.Rendering;
using UnityEditor.UIElements;
using UnityEngine;
using UnityEngine.UIElements;
using Object = UnityEngine.Object;

public class InventoryItemEditorWindow : EditorWindow
{
    private InventoryItem selected;
    private VisualElement rootContainer;
    private VisualElement mainContainer;
    private ScrollView leftPane;
    private VisualElement rightPane;
    
    [MenuItem("Window/TkrainDesigns/Inventory Item EditorWindow")]
    public static void ShowEditorWindow()
    {
        var window = CreateWindow<InventoryItemEditorWindow>();
        window.minSize = new Vector2(1000, 500);
    }

    [MenuItem("Assets/Edit Asset In IDE")]
    public static void EditAssetInIDE()
    {
        string filePath = AssetDatabase.GetAssetPath(Selection.activeObject);
        UnityEditorInternal.InternalEditorUtility.OpenFileAtLineExternal(filePath, 0, 0);
    }
    
    private void OnSelectionChange()
    {
        var candidate = EditorUtility.InstanceIDToObject(Selection.activeInstanceID) as InventoryItem;
        if (candidate != null)
        {
            if (candidate == selected) return;
            Debug.Log("OnSelectionChanged");
            selected = candidate;
            CreateGUI();
            Repaint();
        }
    }

    private void CreateGUI()
    {
        rootContainer = rootVisualElement;
        rootContainer.Clear();
        VisualTreeAsset InventoryItemEditorUXML = Resources.Load<VisualTreeAsset>("InventoryItemEditorUXML");
        mainContainer = InventoryItemEditorUXML.CloneTree();
        rootContainer.Add(mainContainer);
        leftPane = rootContainer.Q<ScrollView>("containerScrollView");
        rightPane = rootContainer.Q<VisualElement>("Preview");
        ObjectField selector = mainContainer.Q<ObjectField>("inventoryItemSelector");
        selector.value = selected;
        selector.RegisterValueChangedCallback(SelectNewInventoryItem);
        Button inventoryItemCloneButton = mainContainer.Q<Button>("inventoryItemCloneButton");
        inventoryItemCloneButton.clicked += () =>
        {
            if (!selected) return;
            CloneSelected();
        };
        if (selected)
        {
            selected.CreateGUI(leftPane, CreateTooltipPreview);
            CreateTooltipPreview();
        }
        else
        {
            rightPane.Hide();
            inventoryItemCloneButton.SetEnabled(false);
        }
    }

    private void CloneSelected()
    {
        var rawPath = AssetDatabase.GetAssetPath(selected);
        var path = Path.GetDirectoryName(rawPath);
        if (string.IsNullOrEmpty(path)) return;
        StringBuilder builder = new StringBuilder();
        
        int result = 0;
        var digits = new[] { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', ' ' };
        string rawName = selected.name.TrimEnd(digits);
        var stack = new Stack<char>();

        for (var i = selected.name.Length - 1; i >= 0; i--)
        {
            if (!char.IsNumber(selected.name[i]))
            {
                break;
            }

            stack.Push(selected.name[i]);
        }
        var counterString = new string(stack.ToArray());
        int counter = 1;
        if (int.TryParse(counterString, out counter))
        {
            Debug.Log($"Incrementing suffix from {counter} to {counter+1}");
            counter++;
        }
        
        var newName = $"{rawName} {counter}.asset";
        var newPath = Path.Combine(path, newName);
        while (File.Exists(newPath))
        {
            counter++;
            newName = $"{rawName} {counter}.asset";
            newPath = Path.Combine(path, newName);
        }
        AssetDatabase.CopyAsset(rawPath, newPath);
        var clone = AssetDatabase.LoadAssetAtPath<InventoryItem>(newPath);
        clone.ResetItemID();
        AssetDatabase.SaveAssets();
        Selection.activeObject = clone;
    }

    private static void SelectNewInventoryItem(ChangeEvent<Object> evt)
    {
        Selection.activeObject = evt.newValue as InventoryItem;
    }

    private void CreateTooltipPreview()
    {
        Button refreshButton = rightPane.Q<Button>();
        refreshButton.clicked += CreateTooltipPreview;
        Image iconPreviewImage = rightPane.Q<Image>("iconPreviewImage");
        iconPreviewImage.sprite = selected.GetIcon() == null ? null : selected.GetIcon();
        ObjectField iconObjectField = leftPane.Q<ObjectField>("iconObjectField");
        iconObjectField.RegisterValueChangedCallback((evt) =>
        {
            iconPreviewImage.sprite = evt.newValue == null ? null : (Sprite)evt.newValue;
        });

        Label displayNamePreview = rightPane.Q<Label>("displayNamePreview");
        displayNamePreview.text = selected.GetDisplayName();
        displayNamePreview.enableRichText = true;
        TextField displayNameTextField = leftPane.Q<TextField>();
        displayNameTextField.RegisterValueChangedCallback((evt) => { displayNamePreview.text = evt.newValue; });

        Label descriptionPreview = rightPane.Q<Label>("descriptionPreview");
        descriptionPreview.enableRichText = true;
        descriptionPreview.text = selected.GetDescription();
        TextField descriptionTextField = leftPane.Q<TextField>("descriptionTextField");
        descriptionTextField.RegisterValueChangedCallback((evt) => { descriptionPreview.text = evt.newValue; });
    }
}
