﻿using System;
using System.Collections.Generic;
using TkrainDesigns.UI;
using UnityEngine;

public class HealthChangeDisplaySpawner : MonoBehaviour
{
    public HealthChangeDisplay healthChangeDisplay;

    private Queue<Action> actions = new Queue<Action>();

    public void SpawnHealthChangeDisplay(float delta)
    {
        if (healthChangeDisplay)
        {
            actions.Enqueue(() =>
            {
                HealthChangeDisplay h = Instantiate(healthChangeDisplay, transform.position, Quaternion.identity);
                h.Initialize(delta);
            });
        }
    }

    public void SpawnHealthChangeString(string message)
    {
        if (healthChangeDisplay)
        {
            actions.Enqueue(() =>
            {
                HealthChangeDisplay h = Instantiate(healthChangeDisplay, transform.position, Quaternion.identity);
                h.Initialize(message);
            });
        }
    }

    private float last = 0;
    private bool emptyQueue = true;

    private void Update()
    {
        last += Time.deltaTime;
        if (actions.Count > 0 && (emptyQueue || last > .25f))
        {
            var action = actions.Dequeue();
            action.Invoke();
            last = 0;
            emptyQueue = actions.Count == 0;
        }
    }
}