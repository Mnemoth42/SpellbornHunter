using System.Collections;
using UnityEngine;
using DunGen;
using DunGen.DungeonCrawler;
using GameDevTV.Saving;
using Newtonsoft.Json.Linq;
using TkrainDesigns.Control;
using TkrainDesigns.Respawnables;
using TkrainDesigns.SceneManagement;
using TkrainDesigns.Stats;
using Unity.AI.Navigation;
using UnityEngine.AI;

//using UnityEngine.AI;

[RequireComponent(typeof(RuntimeDungeon))]
//[RequireComponent(typeof(NavMeshSurface))]
public class DungeonManager : MonoBehaviour, IJsonSaveable
{

    public event System.Action OnDungeonLoadedAndSurfaceFinished;
    private RuntimeDungeon runtimeDungeon;
    private NavMeshSurface surface;
    private Fader fader;
    private DungeonCrawlerNavMeshAdapter adapter;
    private GameObject player;
    
    private void Awake()
    {
        runtimeDungeon = GetComponent<RuntimeDungeon>();
        //runtimeDungeon.Generator.OnGenerationStatusChanged += OnDungeonGenerationStatusChanged;
        surface = GetComponent<NavMeshSurface>();
        fader = FindObjectOfType<Fader>();
        adapter = FindObjectOfType<DungeonCrawlerNavMeshAdapter>();
        adapter.OnNavmeshSurfaceBaked += OnSurfaceCompleted;
        player = GameObject.FindWithTag("Player");
        level = PlayerLevel + (PlayerLevel/4);
        FreezeAllControllers();
    }
    private int level = 1;
    

    public int Level
    {
        get => level;
        set => level = value;
    }

    public int PlayerLevel
    {
        get { return player.GetComponent<BaseStats>().GetLevel(); }
    }

    public void NextLevel()
    {
        level++;
        StartCoroutine(GenerateNewDungeon());
    }

    IEnumerator GenerateNewDungeon()
    {
        level += 1;
        FreezeAllControllers();
        yield return fader.FadeOut(2.0f);
        FindObjectOfType<SavingWrapper>().Save();
        FindObjectOfType<SavingWrapper>().LoadLastScene();
    }
    
    private void OnDestroy()
    {
        runtimeDungeon.Generator.OnGenerationStatusChanged -= OnDungeonGenerationStatusChanged;
    }

    void FreezeAllControllers()
    {
        foreach (BaseController controller in FindObjectsOfType<BaseController>())
        {
            controller.FreezeController();
        }
    }

    void EnableAllControllers()
    {
        foreach (BaseController controller in FindObjectsOfType<BaseController>())
        {
            controller.EnableController();
        }
    }
    
    void OnSurfaceCompleted()
    {
        StartCoroutine(FadeIn());
    }
    
    private void OnDungeonGenerationStatusChanged(DungeonGenerator generator, GenerationStatus status)
    {
        if (status == GenerationStatus.Complete)
        {
           // surface.BuildNavMesh();
            fader.FadeIn(3);
        }
    }

    IEnumerator FadeIn()
    {
        yield return fader.FadeIn(3);
        EnableAllControllers();
        OnDungeonLoadedAndSurfaceFinished?.Invoke();
    }
    
    public JToken CaptureAsJToken()
    {
        return level;
    }

    public void RestoreFromJToken(JToken state)
    {
        level = state.ToObject<int>();
    }
    
}
