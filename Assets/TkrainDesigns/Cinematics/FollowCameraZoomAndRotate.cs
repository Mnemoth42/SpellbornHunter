using Cinemachine;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.InputSystem;
using UnityEngine.Serialization;

namespace TkrainDesigns.Cinematics
{
    [RequireComponent(typeof(CinemachineVirtualCamera))]
    public class FollowCameraZoomAndRotate : MonoBehaviour
    {
        [FormerlySerializedAs("sensitivity")] [SerializeField]
        private float zoomSensitivity = 1.0f;

        [SerializeField] private float minimumZoom = 5;
        [SerializeField] private float maximumZoom = 20;
        [SerializeField] private float zoomSpeed = 5;
        [SerializeField] private float turnSensitivity = .1f;

        [SerializeField] CinemachineFramingTransposer transposer;

        private void Awake()
        {
            CinemachineVirtualCamera cam = GetComponent<CinemachineVirtualCamera>();
            transposer = cam.GetCinemachineComponent<CinemachineFramingTransposer>();
        }

        private float currentZoom = 10;

        private float desiredZoom = 10;

        private float currentRotation = 0;
        private float desiredRotation = 0;
        private float wheelValue;

        // Called by new input system.  
        public void OnScrollWheelTurn(InputAction.CallbackContext context)
        {
            if (EventSystem.current.IsPointerOverGameObject()) return;
            wheelValue = context.ReadValue<float>();
            desiredZoom -= context.ReadValue<float>() * zoomSensitivity;
            desiredZoom = Mathf.Clamp(desiredZoom, minimumZoom, maximumZoom);
        }

        private bool rotatePressed = false;
        private Vector2 lastFrameMousePosition;

        // called by new input system.  
        public void OnRotateButtonPressed(InputAction.CallbackContext context)
        {
            if (EventSystem.current.IsPointerOverGameObject()) return;
            bool currentRotationPressed = context.ReadValueAsButton();
            if (currentRotationPressed && !rotatePressed)
            {
                lastFrameMousePosition = Mouse.current.position.ReadValue();
            }

            rotatePressed = currentRotationPressed;
        }


        private void Update()
        {
            Rect screen = new Rect(0, 0, Screen.width, Screen.height);
            if (!screen.Contains(Mouse.current.position.ReadValue()))
            {
                return;
            }

            currentZoom = Mathf.Lerp(currentZoom, desiredZoom, Time.deltaTime * zoomSpeed);
            transposer.m_CameraDistance = currentZoom;
            if (rotatePressed)
            {
                ManageRotation();
            }
        }


        private void ManageRotation()
        {
            Vector2 currentPosition = Mouse.current.position.ReadValue();
            Vector2 deltaVector2 = currentPosition - lastFrameMousePosition;

            float delta = 0;

            if (Mathf.Abs(deltaVector2.x) > Mathf.Abs(deltaVector2.y))
            {
                delta = lastFrameMousePosition.y > Screen.height / 2.0f ? -deltaVector2.x : deltaVector2.x;
            }
            else
            {
                delta = lastFrameMousePosition.x < Screen.width / 2.0f ? -deltaVector2.y : deltaVector2.y;
            }

            lastFrameMousePosition = currentPosition;

            desiredRotation += (delta * turnSensitivity * Time.deltaTime);
            currentRotation = Mathf.Lerp(currentRotation, desiredRotation, Time.deltaTime * zoomSpeed);
            Vector3 eulers = transform.eulerAngles;
            eulers.y = currentRotation;
            transform.eulerAngles = eulers;
        }
    }
}