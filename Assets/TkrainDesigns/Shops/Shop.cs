using GameDevTV.Inventories;
using GameDevTV.Saving;
using TkrainDesigns.Stats;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json.Linq;
using TkrainDesigns.Characters;
using UnityEngine;
using Random = UnityEngine.Random;

namespace TkrainDesigns.Shops
{
    public class Shop : MonoBehaviour, IRaycastable, ISaveable, ISetLevel, IJsonSaveable
    {
        [System.Serializable]
        private class StockItemConfig
        {
            public static StockItemConfig CreateInstance(InventoryItem _item, int _initialStock,
                                                         int _buyingDiscountPercentage, int _unlockAtLevel)
            {
                return new StockItemConfig(_item, _initialStock, _buyingDiscountPercentage, _unlockAtLevel);
            }

            public InventoryItem item;
            public int initialStock;
            [Range(-100, 100)] public int buyingDiscountPercentage;
            [Range(1, 100)] public int unlockAtLevel = 1;
            public int chanceToDrop = 10;

            private StockItemConfig(InventoryItem _item, int _initialStock, int _buyingDiscountPercentage,
                                    int _unlockAtLevel)
            {
                item = _item;
                initialStock = _initialStock;
                buyingDiscountPercentage = _buyingDiscountPercentage;
                unlockAtLevel = _unlockAtLevel;
            }
        }
        [Tooltip("Names of shoppe owners.")]
        [SerializeField] private NameRepository names;
        [Tooltip("Names of Shops.  Brevity matters.")]
        [SerializeField] private NameRepository shopNames;
        
        [SerializeField] private List<StockItemConfig> stockItems = new List<StockItemConfig>();

        [Header("Discounts from charisma will never exceed this percentage (exclusive from StockItemConfig discounts")]
        [Range(0, 40)]
        [SerializeField]
        private float maximumDiscountFromCharisma = 30;

        [Header(
            "This affects the seller's general disposition.  Negative disposition will decrease barter discount, positive disposition will increase it.")]
        [Range(-50, 50)]
        [SerializeField]
        private float barterDiscountDisposition = 0;

        private Dictionary<InventoryItem, int> currentTransaction = new Dictionary<InventoryItem, int>();
        private Dictionary<InventoryItem, int> stock = new Dictionary<InventoryItem, int>();
        private int level = 1;
        private int intialPlayerLevel = 1;

        public void SetLevel(int newLevel, int playerLevel)
        {
            level = newLevel;
            intialPlayerLevel = playerLevel;
            BuildStock();
        }

        public event System.Action onChange;

        private bool selling = false;
        private ItemCategory category;
        private GameObject currentShopper = null;
        private Inventory shopperInventory = null;
        private Purse shopperPurse = null;
        private string createdName;
        private void Awake()
        {
            createdName = $"{names.GetName()}'s {shopNames.GetName()}";
            name = createdName;
            BuildStock();
        }

        private void BuildStock()
        {
            DungeonManager dungeonManager = FindObjectOfType<DungeonManager>();
            level = dungeonManager.Level;
            intialPlayerLevel = dungeonManager.PlayerLevel;
            
            if (level < intialPlayerLevel)
            {
                (level, intialPlayerLevel) = (intialPlayerLevel, level);
            }

            stock = new Dictionary<InventoryItem, int>();
            foreach (StockItemConfig stockItem in stockItems)
            {
                if (stockItem.item == null) continue;
                if (stockItem.item.IsStackable())
                {
                    stock.TryGetValue(stockItem.item, out int existing);
                    stock[stockItem.item] = stockItem.initialStock + existing;
                    //Debug.Log($"{name} adding {stockItem.initialStock} {stockItem.item.GetDisplayName()} to stock");
                }
                else
                {
                   for (int i = 0; i < stockItem.initialStock; i++)
                    {
                        int testRoll = Random.Range(0, 100);
                        if (testRoll > stockItem.chanceToDrop)
                        {
                            //Debug.Log($"Roll = {testRoll} > {stockItem.chanceToDrop}.  Not Dropping Item");
                            continue;
                        }

                        //Debug.Log($"Roll = {testRoll} < {stockItem.chanceToDrop}, adding new {stockItem.item.GetDisplayName()}");
                        InventoryItem newItem = Instantiate(stockItem.item);
                        newItem.SetLevel(Random.Range(intialPlayerLevel, level + 1));
                        newItem.Initialize();
                        stock[newItem] = 1;
                    }
                }
            }
        }

        public string ShopName => createdName;

        private Dictionary<InventoryItem, int> GatherShopperInventory()
        {
            Dictionary<InventoryItem, int> itemsInInventory = new Dictionary<InventoryItem, int>();
            if (!IsValidShopper()) return itemsInInventory;
            foreach (InventoryRecord shopperItem in shopperInventory.GetInventoryRecords())
            {
                if (itemsInInventory.ContainsKey(shopperItem.item))
                {
                    itemsInInventory[shopperItem.item] += shopperItem.amount;
                }
                else
                {
                    itemsInInventory[shopperItem.item] = shopperItem.amount;
                }
            }

            return itemsInInventory;
        }

        public IEnumerable<ShopItem> GetFilteredItems()
        {
            foreach (var item in GetAllItems())
            {
                var cat = item.GetItem().GetCategory();
                if (category == ItemCategory.None || cat == category) yield return item;
            }
        }

        public IEnumerable<ShopItem> GetAllItems()
        {
            Dictionary<InventoryItem, int> prices = GetPrices();
            Dictionary<InventoryItem, int> availabilities = GetAvailabilities();
            if (Selling())
            {
                foreach (var item in GatherShopperInventory())
                {
                    int quantity = 0;
                    if (currentTransaction.ContainsKey(item.Key))
                    {
                        quantity += currentTransaction[item.Key];
                    }

                    yield return new ShopItem(item.Key, item.Value, GetPrice(item.Key), quantity);
                }
            }
            else
            {
                int shopperLevel = GetShopperLevel();
                foreach (InventoryItem item in availabilities.Keys)
                {
                    if (stock[item] <= 0) continue;
                    int price = prices[item];
                    currentTransaction.TryGetValue(item, out var quantity);
                    int availability = availabilities[item];

                    yield return new ShopItem(item, availability, price, quantity);
                }
            }
        }


        public void SetShopper(GameObject shopper)
        {
            shopperInventory = (shopper == null) ? null : shopper.GetComponent<Inventory>();
            shopperPurse = (shopper == null) ? null : shopper.GetComponent<Purse>();
            ClearTransaction();
        }


        public void ConfirmTransaction()
        {
            if (shopperInventory == null) return;
            foreach (InventoryItem item in ItemsInTransaction())
            {
                if (Selling())
                {
                    SellItem(item);
                }
                else
                {
                    PurchaseItem(item);
                }

                NotifyOfChanges();
            }

            NotifyOfChanges();
        }

        private void SellItem(InventoryItem item)
        {
            if (shopperInventory.RemoveItem(item))
            {
                shopperPurse.AddToBalance(CurrencyTypes.Copper, GetPrice(item));
                currentTransaction[item]--;
                if (!stock.ContainsKey(item))
                {
                    stock[item] = 1;
                }
                else
                {
                    stock[item]++;
                }
            }
        }

        private void PurchaseItem(InventoryItem item)
        {
            if (shopperInventory.AddToFirstEmptySlot(item, 1))
            {
                if (shopperPurse.TakeFromBalance(CurrencyTypes.Copper, GetPrice(item)))
                {
                    currentTransaction[item]--;
                    stock[item]--;
                }
            }
            else
            {
                Debug.Log($"No room in inventory for {item.GetDisplayName()}");
            }
        }

        public bool CanTransact()
        {
            if (IsTransactionEmpty()) return false;
            if (!IsValidShopper()) return false;
            if (selling) return true;
            if (!HasSpaceInInventory()) return false;
            return TransactionTotal() <= shopperPurse.GetBalance(CurrencyTypes.Copper);
        }

        public bool CanPurchase(InventoryItem item)
        {
            if (!IsValidShopper()) return false;
            if (Selling()) return true;
            if (!HasSpaceInInventory()) return false;
            return TransactionTotal() + GetPrice(item) <= shopperPurse.GetBalance(CurrencyTypes.Copper);
        }

        public bool IsTransactionEmpty()
        {
            return (currentTransaction.Count == 0);
        }

        private bool IsValidShopper()
        {
            return (shopperPurse != null && shopperInventory != null);
        }

        private bool HasSpaceInInventory()
        {
            if (!IsValidShopper()) return false;
            return shopperInventory.HasSpaceFor(ItemsInTransaction());
        }

        private IEnumerable<InventoryItem> ItemsInTransaction()
        {
            Dictionary<InventoryItem, int> clonedTransaction = new Dictionary<InventoryItem, int>(currentTransaction);
            foreach (InventoryItem key in clonedTransaction.Keys)
            {
                for (int i = 0; i < clonedTransaction[key]; i++)
                {
                    yield return key;
                }
            }
        }

        public void ChangeTransactionType()
        {
            selling = !selling;
            currentTransaction.Clear();
            NotifyOfChanges();
        }

        public bool Selling() => selling;

        public void SetFilter(ItemCategory cat)
        {
            category = cat;
            NotifyOfChanges();
        }

        private void NotifyOfChanges()
        {
            onChange?.Invoke();
        }

        public ItemCategory GetFilter()
        {
            return category;
        }

        public float TransactionTotal()
        {
            int result = 0;
            foreach (var pair in currentTransaction)
            {
                result += GetPrice(pair.Key) * pair.Value;
            }

            return result;
        }

        private int GetPrice(InventoryItem item)
        {
            foreach (StockItemConfig config in stockItems)
            {
                if (config.item == item) return CalculatePrice(item);
            }

            return Selling() ? Mathf.Max(item.GetPrice() / 10, 1) : item.GetPrice();
        }

        private int CalculatePrice(InventoryItem item)
        {
            float discountPercentage = GetBestConfig(item).buyingDiscountPercentage;
            discountPercentage += GetBarterPercentage();
            float price = item.GetPrice() * (100 - discountPercentage) / 100.0f;
            if (Selling())
            {
                price = item.GetPrice() * .1f * (100 + discountPercentage) / 100.0f;
            }

            return Mathf.Max(1, Mathf.CeilToInt(price));
        }

        private float GetBarterPercentage()
        {
            float rawValue = shopperInventory.GetComponent<BaseStats>().GetStat(EStat.Bartering);
            rawValue *= (100f + barterDiscountDisposition) / 100.0f;
            return Mathf.Clamp(rawValue, -maximumDiscountFromCharisma, maximumDiscountFromCharisma);
        }

        private StockItemConfig GetBestConfig(InventoryItem item)
        {
            StockItemConfig config = StockItemConfig.CreateInstance(item, 0, 0, 0);
            int shopperLevel = GetShopperLevel();
            foreach (StockItemConfig testConfig in stockItems.OrderBy(p => p.unlockAtLevel))
            {
                if (testConfig.item != item) continue;
                if (testConfig.unlockAtLevel > shopperLevel) break;

                //At this point, we've found an item in the config that is <= shopperlevel, so we'll set the config to it.
                config = testConfig; //since they are sorted with Linq already, we know that we'll get the highest level config available.
            }

            return config;
        }

        private IEnumerable<StockItemConfig> GetConfigsOverLevel(InventoryItem item)
        {
            int shopperLevel = GetShopperLevel();
            foreach (var config in stockItems)
            {
                if (config.item != item) continue;
                if (config.unlockAtLevel > shopperLevel) yield return config;
            }
        }

        private Dictionary<InventoryItem, int> GetPrices()
        {
            Dictionary<InventoryItem, int> result = new Dictionary<InventoryItem, int>();
            if (!shopperInventory) return result;
            {
                foreach (var item in selling ? shopperInventory.GetInventoryAsDictionary().Keys : stock.Keys)
                {
                    result[item] = GetPrice(item);
                }
            }
            return result;
        }


        private Dictionary<InventoryItem, int> GetAvailabilities()
        {
            Dictionary<InventoryItem, int> result = new Dictionary<InventoryItem, int>();
            if (!shopperInventory) return result;
            if (selling)
            {
                return shopperInventory.GetInventoryAsDictionary();
            }

            int shopperLevel = GetShopperLevel();
            foreach (var item in stock.Keys)
            {
                if (shopperLevel < item.GetLevel())
                    continue; //it's never available if it's overlevel, regardless of config.
                result[item] = stock[item];
                foreach (var record in GetConfigsOverLevel(item))
                {
                    result[item] -= record.initialStock;
                }

                if (result[item] <= 0)
                {
                    result.Remove(item);
                }
            }

            return result;
        }

        public void AddToTransaction(InventoryItem item, int quantity)
        {
            Debug.Log($"Adding {item.GetDisplayName()} x {quantity}");
            if (quantity > 0 && !CanPurchase(item)) return;
            if (!currentTransaction.ContainsKey(item))
            {
                currentTransaction[item] = quantity;
            }
            else
            {
                currentTransaction[item] += quantity;
            }

            if (!Selling())
            {
                if (currentTransaction[item] > stock[item])
                {
                    currentTransaction[item] = stock[item];
                }
            }

            if (currentTransaction[item] <= 0) currentTransaction.Remove(item);
            NotifyOfChanges();
        }

        public int GetPriority()
        {
            return 0;
        }

        public ECursorType GetCursorType()
        {
            return ECursorType.Shop;
        }

        public bool HandleRayCast(GameObject callingController)
        {
            if (Input.GetMouseButtonDown(0))
            {
                //callingController.GetComponent<Shopper>().StartShoppingAction(this);
                callingController.GetComponent<IInteraction<Shop>>().HandleInteraction(this);
            }

            return true;
        }


        public int GetShopperLevel()
        {
            if (shopperInventory == null) return 1;
            BaseStats stats = shopperInventory.GetComponent<BaseStats>();
            if (stats)
            {
                return stats.GetLevel();
            }

            return 1;
        }

        public void ClearTransaction()
        {
            currentTransaction.Clear();
            NotifyOfChanges();
        }

        public JToken CaptureState()
        {
            Dictionary<string, int> savedStock = new Dictionary<string, int>();
            foreach (var pair in stock)
            {
                Debug.Log($"Saving {pair.Key.GetDisplayName()} qty {pair.Value}");
                savedStock[pair.Key.GetItemID()] = pair.Value;
            }

            return JToken.FromObject(savedStock);
        }

        public void RestoreState(JToken state)
        {
            Dictionary<string, int> savedStock = state.ToObject<Dictionary<string, int>>();
            stock.Clear();
            foreach (var pair in savedStock)
            {
                InventoryItem item = InventoryItem.GetFromID(pair.Key);
                if (item)
                {
                    //Debug.Log($"Restoring {item.GetDisplayName()} qty {pair.Value}");
                    stock[item] = pair.Value;
                }
            }
        }

        public JToken CaptureAsJToken()
        {
            return CaptureState();
        }

        public void RestoreFromJToken(JToken state)
        {
            RestoreState(state);
        }
    }
}