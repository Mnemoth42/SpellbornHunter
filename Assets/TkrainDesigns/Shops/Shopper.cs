using System;
using GameDevTV.Inventories;
using TkrainDesigns.Movement;
using TkrainDesigns.Stats;
using UnityEngine;

namespace TkrainDesigns.Shops
{
    [DisallowMultipleComponent]
    [RequireComponent(typeof(Purse))] //Needed for Shop
    [RequireComponent(typeof(BaseStats))] //Needed for Shop
    public class Shopper : MoveableActionBehavior<Shop>, IAction, IInteraction<Shop>
    {
        private Shop activeShop = null;


        public event Action activeShopChanged;

        protected override void Perform()
        {
            if (target == activeShop) return;
            Debug.Log($"Arrived at shop {target}");
            if (activeShop) activeShop.SetShopper(null);
            activeShop = target;
            if (activeShop) activeShop.SetShopper(gameObject);
            activeShopChanged?.Invoke();
        }

        public Shop GetActiveShop()
        {
            return activeShop;
        }

        public void Cancel()
        {
            base.Cancel();
            activeShop?.SetShopper(null);
            activeShop = null;
            activeShopChanged?.Invoke();
        }

        public Action GetResumeFunction()
        {
            return null;
        }

        public bool HandleInteraction(Shop caller)
        {
            StartAction(caller);
            return true;
        }
    }
}