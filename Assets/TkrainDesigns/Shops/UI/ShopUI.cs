using TMPro;
using UnityEditor.UIElements;
using UnityEngine;
using UnityEngine.UI;

namespace TkrainDesigns.Shops.UI
{
    public class ShopUI : MonoBehaviour
    {
        [SerializeField] private TextMeshProUGUI title = null;
        [SerializeField] private Button sellButton;
        [SerializeField] private TextMeshProUGUI selling = null;
        [SerializeField] private Transform content;
        [SerializeField] private RowUI rowPrefab;
        [SerializeField] private TextMeshProUGUI transactionTotal;
        [SerializeField] private Button transactionButton;
        [SerializeField] private TextMeshProUGUI transactionText;
        [SerializeField] private RectTransform parent;

        private Shopper shopper;
        private Shop currentShop = null;

        private void Awake()
        {
            shopper = GameObject.FindGameObjectWithTag("Player").GetComponent<Shopper>();
            if (!shopper) return;
            shopper.activeShopChanged += ShopChanged;
        }

        private void Start()
        {
            ShopChanged();
            if (sellButton)
            {
                sellButton.onClick.AddListener(ChangeTransactionType);
            }
        }

        private void ShopChanged()
        {
            Debug.Log($"ShopChanged");
            UnsubscribeFromShop();
            currentShop = shopper.GetActiveShop();
            gameObject.SetActive(currentShop != null);
            foreach (FilterButton filterButton in GetComponentsInChildren<FilterButton>())
            {
                filterButton.SetShop(currentShop);
            }

            if (currentShop) currentShop.onChange += RefreshUI;
            RefreshUI();
        }

        private void UnsubscribeFromShop()
        {
            if (currentShop)
            {
                currentShop.onChange -= RefreshUI;
            }
        }

        private void RefreshUI()
        {
            if (currentShop)
            {
                if (title)
                {
                    title.text = currentShop.ShopName;
                }

                if (selling)
                {
                    selling.text = currentShop.Selling() ? "Switch to Buying" : "Switch to Selling";
                }

                if (transactionButton)
                {
                    transactionButton.interactable = currentShop.CanTransact();
                }

                if (transactionText)
                {
                    transactionText.text = currentShop.Selling() ? "Sell" : "Buy";
                }

                foreach (FilterButton filterButton in GetComponentsInChildren<FilterButton>())
                {
                    filterButton.RefreshUI();
                }

                //Debug.Log("Destroying obsolete entries.");
                foreach (Transform child in content)
                {
                    //Debug.Log($"Destroying {child.name}");
                    Destroy(child.gameObject);
                }

                foreach (ShopItem item in currentShop.GetFilteredItems())
                {
                    RowUI row = Instantiate(rowPrefab, content);
                    row.Setup(item, currentShop);
                }

                transactionTotal.text = $"Total: {currentShop.TransactionTotal():N0}";
                parent.SetAsLastSibling();
            }
        }

        public void ChangeTransactionType()
        {
            currentShop.ChangeTransactionType();
        }


        public void CloseShop()
        {
            currentShop.ClearTransaction();
            UnsubscribeFromShop();
            gameObject.SetActive(false);
        }

        public void ConfirmTransaction()
        {
            currentShop.ConfirmTransaction();
        }
    }
}