using System.Collections;
using System.Collections.Generic;
using GameDevTV.Inventories;
using GameDevTV.UI.Inventories;
using UnityEngine.UI;
using TMPro;
using UnityEngine;
using UnityEngine.Serialization;

namespace TkrainDesigns.Shops.UI
{
    public class RowUI : MonoBehaviour, IItemHolder
    {
        [SerializeField] private ShopItemHolder icon;

        [FormerlySerializedAs("itemDescription")] [SerializeField]
        private TextMeshProUGUI itemName;

        [SerializeField] private TextMeshProUGUI available;
        [SerializeField] private TextMeshProUGUI price;
        [SerializeField] private TextMeshProUGUI quantity;
        [SerializeField] private Button AddButton;
        [SerializeField] private Button RemoveButton;

        private InventoryItem linkedItem = null;
        private Shop currentShop;

        public void Setup(ShopItem shopItem, Shop shop)
        {
            currentShop = shop;
            icon.SetItem(shopItem.GetItem());
            itemName.text = shopItem.GetDisplayName();
            available.text = $"{shopItem.GetAvailablity() - shopItem.GetQuantityInTransaction()}";
            price.text = $"{shopItem.GetPrice():N0}";
            quantity.text = $"{shopItem.GetQuantityInTransaction()}";
            linkedItem = shopItem.GetItem();
            AddButton.interactable = shopItem.GetQuantityInTransaction() < shopItem.GetAvailablity() &&
                                     shop.CanPurchase(shopItem.GetItem());
            RemoveButton.interactable = shopItem.GetQuantityInTransaction() > 0;
        }

        public InventoryItem GetItem()
        {
            return linkedItem;
        }

        public void Add()
        {
            Debug.Log("Add");
            currentShop.AddToTransaction(linkedItem, 1);
        }

        public void Remove()
        {
            Debug.Log("Remove");
            currentShop.AddToTransaction(linkedItem, -1);
        }
    }
}