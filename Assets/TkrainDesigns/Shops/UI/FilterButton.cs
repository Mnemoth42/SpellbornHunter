using System;
using System.Collections;
using System.Collections.Generic;
using GameDevTV.Inventories;
using UnityEngine;
using UnityEngine.UI;

namespace TkrainDesigns.Shops.UI
{
    public class FilterButton : MonoBehaviour
    {
        [SerializeField] private ItemCategory category;
        private Button button;
        private Shop shop;

        private void Awake()
        {
            button = GetComponent<Button>();
            button.onClick.AddListener(SelectFilter);
        }

        public void SetShop(Shop currentShop)
        {
            shop = currentShop;
        }

        public void RefreshUI()
        {
            if (shop == null)
            {
                button.interactable = false;
            }

            button.interactable = shop.GetFilter() != category;
        }

        private void SelectFilter()
        {
            if (!shop) return;
            shop.SetFilter(category);
        }
    }
}