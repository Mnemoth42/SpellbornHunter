using System;
using System.Collections;
using System.Collections.Generic;
using GameDevTV.Inventories;
using TMPro;
using UnityEngine;

namespace TkrainDesigns.Shops
{
    public class PurseUI : MonoBehaviour
    {
        private Purse purse;
        private TextMeshProUGUI text;

        private void Awake()
        {
            text = GetComponent<TextMeshProUGUI>();
            purse = GameObject.FindWithTag("Player").GetComponent<Purse>();
            if (purse)
            {
                purse.onCurrencyChanged += UpdatePurse;
            }
        }

        void UpdatePurse()
        {
            text.text = $"{purse.GetBalance(CurrencyTypes.Copper)}";
        }
    }
}