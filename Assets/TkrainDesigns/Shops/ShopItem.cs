using GameDevTV.Inventories;
using UnityEngine;

namespace TkrainDesigns.Shops
{
    [System.Serializable]
    public class ShopItem
    {
        [SerializeField] private InventoryItem item;
        [SerializeField] private int availability;
        [SerializeField] private int price;
        [SerializeField] private int quantityInTransaction;

        public ShopItem(InventoryItem _item, int _availablitity, int _price, int _quantityInTransaction)
        {
            item = _item;
            availability = _availablitity;
            price = _price;
            quantityInTransaction = _quantityInTransaction;
        }

        public InventoryItem GetItem() => item;
        public string GetDisplayName() => item.GetDisplayName();
        public int GetAvailablity() => availability;
        public int GetPrice() => price;
        public int GetQuantityInTransaction() => quantityInTransaction;
    }
}