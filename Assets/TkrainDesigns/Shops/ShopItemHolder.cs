using GameDevTV.Inventories;
using GameDevTV.UI.Inventories;
using UnityEngine;
using UnityEngine.UI;

public class ShopItemHolder : MonoBehaviour, IItemHolder
{
    public Image image;
    private InventoryItem item = null;

    public void SetItem(InventoryItem item)
    {
        this.item = item;
        image.sprite = item.GetIcon();
    }

    public InventoryItem GetItem()
    {
        return item;
    }
}