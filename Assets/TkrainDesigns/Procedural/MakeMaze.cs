﻿using System.Collections.Generic;
using UnityEngine;

public struct connector
{
    public int x, y;
    public int zone1;
    public int zone2;
    public int direction;
}

public enum CellType
{
    Emptypath,
    Wall
}


public class MakeMaze : Maze
{
    //  private List<Vector2Int> paths = new List<Vector2Int>();

    public List<connector> connectors = new List<connector>();
    public List<connector> doorWay = new List<connector>();
    private byte[,] mapZone;

    private readonly List<Rect> rooms = new List<Rect>();
    private byte zone = 1;

    public override void Generate()
    {
        mapZone = new byte[width, depth];

        for (var z = 0; z < depth; z++)
        for (var x = 0; x < width; x++)
            mapZone[x, z] = 0; //1 = wall  0 = corridor

        Generate(width, depth);
    }

    private void Generate(int x, int z)
    {
        //Random.seed = 0;

        MakeRooms();
        PlayerStartPosition = FindNewStartCell();
        Tunnel(PlayerStartPosition);
        RemoveSoloHoles();
        FindConnection();
        MergeZones();
    }

    private void MakeRooms()
    {
        var numRoomTries = 500;

        for (var i = 0; i < numRoomTries; i++)
        {
            var roomwidth = Random.Range(roomMinSize, roomMaxSize);
            var roomheight = Random.Range(roomMinSize, roomMaxSize);

            var x = Random.Range(0, width - roomwidth - 1);
            var y = Random.Range(0, depth - roomheight - 1);

            AddRoom(roomwidth, roomheight, x, y);
        }
    }

    private void AddRoom(int roomwidth, int roomheight, int x, int y)
    {
        var isRound = Random.Range(0, 5) < 2;
        if (TestOverlap(roomwidth, roomheight, x, y))
        {
            var room = new Rect(x, y, roomwidth, roomheight);
            rooms.Add(room);
            zone++;
            for (var x1 = x; x1 < x + roomwidth; x1++)
            for (var y1 = y; y1 < y + roomheight; y1++)
                //if(!isRound || new Vector2(x1,y1).magnitude<new Vector2(roomwidth,roomheight).magnitude)
            {
                mapZone[x1, y1] = zone;
                map[x1, y1] = 0;
            }
        }
    }

    private bool TestOverlap(int roomwidth, int roomheight, int x, int y)
    {
        var roombordersize = 2;

        if (x < 1 || y < 1 || x + roomwidth > width || y + roomheight > depth) return false;

        for (var x1 = x - roombordersize; x1 < x + roomwidth + roombordersize; x1++)
        for (var y1 = y - roombordersize; y1 < y + roomheight + roombordersize; y1++)
            if (x1 < 0 || x1 >= width || y1 < 0 || y1 >= depth)
                continue;
            else if (map[x1, y1] == 0) //overlap
                return false;

        return true;
    }

    private void Tunnel(Vector2Int playerEntrance)
    {
        var validcells = new List<Vector2Int>();
        var startcell = playerEntrance;
        if (startcell.x == 0 && startcell.y == 0) return;
        map[startcell.x, startcell.y] = 0;
        var lastCell = startcell;
        zone++;
        for (var a = 0; a < 300; a++)
        {
            validcells.Clear();
            if (startcell.y < depth && map[startcell.x, startcell.y + 1] == 1 &&
                CountSquareNeighbours(startcell.x, startcell.y + 1) == 1)
                validcells.Add(startcell + Vector2Int.up); //number of openspaces
            if (startcell.y > 0 && map[startcell.x, startcell.y - 1] == 1 &&
                CountSquareNeighbours(startcell.x, startcell.y - 1) == 1) validcells.Add(startcell + Vector2Int.down);
            if (startcell.x > 0 && map[startcell.x - 1, startcell.y] == 1 &&
                CountSquareNeighbours(startcell.x - 1, startcell.y) == 1) validcells.Add(startcell + Vector2Int.left);
            if (startcell.x < width && map[startcell.x + 1, startcell.y] == 1 &&
                CountSquareNeighbours(startcell.x + 1, startcell.y) == 1) validcells.Add(startcell + Vector2Int.right);

            if (validcells.Count > 0)
            {
                lastCell = startcell;
                startcell = validcells[Random.Range(0, validcells.Count)];
                mapZone[startcell.x, startcell.y] = zone;
                map[startcell.x, startcell.y] = 0;
            }
            else
            {
                startcell = lastCell;
                zone++;
                mapZone[startcell.x, startcell.y] = zone;
                map[startcell.x, startcell.y] = 0;
            }
        }
    }


    //Find a cell surrounded by walls, so not part of current dungeon
    private Vector2Int FindNewStartCell()
    {
        Debug.Log("Determingine Player Cell");
        for (var x1 = 1; x1 < width - 1; x1++)
        for (var y1 = 1; y1 < depth - 1; y1++)
            if (map[x1, y1] == 1)
                if (CountSquareNeighbours(x1, y1) == 0)
                {
                    Debug.Log($"Player Start Position = ({x1}, {y1})");
                    return new Vector2Int(x1, y1);
                }

        return new Vector2Int(0, 0);
    }


    private void FindConnection()
    {
        for (var x1 = 1; x1 < width - 1; x1++)
        for (var y1 = 1; y1 < depth - 1; y1++)
            Checkforzones(x1, y1);
    }

    private void Checkforzones(int x, int y)
    {
        if (map[x, y] == 0) return; //not a wall

        //if cells above and bellow this are both floor and also different zones, we can connect them with a possible doorway
        // there will mostly be multiple possible connections between two zones
        if (map[x, y + 1] == (byte)CellType.Emptypath && map[x, y - 1] == (byte)CellType.Emptypath)
            if (mapZone[x, y + 1] != mapZone[x, y - 1])
            {
                var conNector = new connector();
                conNector.x = x;
                conNector.y = y;
                conNector.zone1 = mapZone[x, y + 1];
                conNector.zone2 = mapZone[x, y - 1];
                conNector.direction = 1;
                connectors.Add(conNector);
                // map[x, y] = 0;
            }

        //if cells Left and right of  this are both floor and also different zones, we can connect them with a possible doorway
        if (map[x - 1, y] == (byte)CellType.Emptypath && map[x + 1, y] == (byte)CellType.Emptypath)
            if (mapZone[x - 1, y] != mapZone[x + 1, y])
            {
                var conNector = new connector();
                conNector.x = x;
                conNector.y = y;
                conNector.zone1 = mapZone[x - 1, y];
                conNector.zone2 = mapZone[x + 1, y];
                conNector.direction = 2;
                connectors.Add(conNector);
                // map[x, y] = 0;
            }
    }

    private void MergeZones()
    {
        var matchlist = new List<connector>();
        connector select;

        var startzone = 2;
        int mergezone;

        while (connectors.Count > 0)
        {
            matchlist.Clear();
            foreach (var item in connectors)
                if (item.zone1 == startzone || item.zone2 == startzone)
                    matchlist.Add(item);

            //Choose one connection from all possible connectors between startzone and selected zone
            // and make it into a possible door
            select = matchlist[Random.Range(0, matchlist.Count)];

            map[select.x, select.y] = 0;
            var x = select.x;
            var y = select.y;

            if (map[x, y - 1] == 1 && map[x, y + 1] == 1 && map[x - 1, y] == 0 && map[x + 1, y] == 0)
                doorWay.Add(select);

            if (map[x, y - 1] == 0 && map[x, y + 1] == 0 && map[x - 1, y] == 1 && map[x + 1, y] == 1)
                doorWay.Add(select);

            // remove merged region from list;
            if (select.zone1 == startzone) mergezone = select.zone2;
            else mergezone = select.zone1;

            for (var i = connectors.Count - 1; i >= 0; i--)
            {
                var currentconnector = connectors[i];

                if (currentconnector.zone1 == mergezone) currentconnector.zone1 = startzone;
                if (currentconnector.zone2 == mergezone) currentconnector.zone2 = startzone;
                connectors[i] = currentconnector;

                if (currentconnector.zone1 == currentconnector.zone2) connectors.RemoveAt(i);
            }
        }
    }

    //    //Remove and floor surounded by 4 walls, change to wall
    private void RemoveSoloHoles()
    {
        var solohole = 0;

        for (var x1 = 1; x1 < width - 1; x1++)
        for (var y1 = 1; y1 < depth - 1; y1++)
            if (map[x1, y1] == 0)
                if (CountSquareNeighbours(x1, y1) == 0)
                {
                    map[x1, y1] = 1;
                    solohole++;
                }
        //  print("solohole " + solohole);
    }
}