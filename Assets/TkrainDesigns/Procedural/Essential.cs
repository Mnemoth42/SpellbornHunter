using System.Collections;
using System.Collections.Generic;
using JetBrains.Annotations;
using UnityEditor.PackageManager;
using UnityEngine;

namespace TkrainDesigns.Procedural
{
    public class Essential : Maze
    {
        private List<Vector2Int> directions = new List<Vector2Int>()
                                              {
                                                  new Vector2Int(1, 0),
                                                  new Vector2Int(0, 1),
                                                  new Vector2Int(0, -1)
                                              };

        public override void Generate()
        {
            Vector2Int position = PlayerStartPosition;
            Stack<Vector2Int> breadcrumbs = new Stack<Vector2Int>();
            breadcrumbs.Push(position);
            SetMapvalue(position, 0);
            int attempts = 0;
            while (position.x <= width - 1 && attempts < 600)
            {
                Vector2Int testPosition = position + directions[Random.Range(0, 3)];
                if (IsInBounds(testPosition) && !IsOpen(testPosition))
                {
                    position = testPosition;
                    SetMapvalue(position);
                }
                else
                {
                    if (breadcrumbs.Count > 0) position = breadcrumbs.Pop();
                    attempts++;
                }
            }

            endPosition = position;
        }
    }
}