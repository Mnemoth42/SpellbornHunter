﻿using System.Collections.Generic;
using UnityEngine;

public class Wilsons : Maze
{
    List<MapLocation> unused = new List<MapLocation>();

    public override void Generate()
    {
        //Create a starting cell
        int x = Random.Range(2, width - 1);
        int z = Random.Range(2, depth - 1);
        map[x, z] = 2;
        int tries = 0;
        while ((GetAvailableCells() > 0) && (tries < 20000))
        {
            tries++;
            RandomWalk();
        }
    }

    int GetAvailableCells()
    {
        unused.Clear();
        for (int x = 1; x < width - 1; x++)
        for (int z = 1; z < depth - 1; z++)
        {
            if (CountSquareNeighbours(x, z, 2) == 0)
            {
                unused.Add(new MapLocation(x, z));
            }
        }

        return unused.Count;
    }

    void RandomWalk()
    {
        MapLocation loc = unused[Random.Range(0, unused.Count)];
        int cx = loc.x;
        int cz = loc.z;
        bool done = false;
        bool validPath = false;
        List<MapLocation> potentialPath = new List<MapLocation>();
        potentialPath.Add(new MapLocation(cx, cz));
        int loop = 0;
        while (!done && loop < 5000 && !validPath)
        {
            loop++;
            if (CountSquareNeighbours(cx, cz, 2) > 0) break;
            map[cx, cz] = 0;

            int rd = Random.Range(0, SquareDirections.Count);
            int nx = cx + SquareDirections[rd].x;
            int nz = cz + SquareDirections[rd].z;
            if (CountSquareNeighbours(nx, nz) < 2)
            {
                cx = nx;
                cz = nz;
                potentialPath.Add(new MapLocation(cx, cz));
            }

            validPath = CountSquareNeighbours(cx, cz, 2) == 1;

            done = !IsInBounds(cx, cz);
        }

        byte newValue = 1;
        if (validPath)
        {
            map[cx, cz] = 0;
            newValue = 2;
        }


        foreach (var element in potentialPath)
        {
            map[element.x, element.z] = newValue;
        }
    }
}