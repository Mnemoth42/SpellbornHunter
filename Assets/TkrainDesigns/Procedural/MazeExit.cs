using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TkrainDesigns.Procedural
{
    public class MazeExit : MonoBehaviour
    {
        private void OnTriggerEnter(Collider other)
        {
            Debug.Log($"MazeExit OnTriggerEnter {other.gameObject.name}");
            if (other.gameObject.CompareTag("Player"))
            {
                FindObjectOfType<DungeonManager>().NextLevel();
            }
        }
    }
}