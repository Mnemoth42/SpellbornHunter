﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SocialPlatforms;

public class Crawler : Maze
{
    [Range(0, 10)] public int verticalPaths = 3;
    [Range(0, 10)] public int horizontalPaths = 3;

    public override void Generate()
    {
        for (int i = 0; i < verticalPaths; i++)
        {
            Crawl(true);
        }

        for (int i = 0; i < horizontalPaths; i++)
        {
            Crawl(false);
        }
    }

    void Crawl(bool vertical)
    {
        bool done = false;
        int x = vertical ? Random.Range(1, width - 1) : 1;
        int z = vertical ? 1 : Random.Range(1, depth - 1);
        Rect rect = new Rect(.001f, .001f, width - 1.01f, depth + -1.01f);
        while (!done)
        {
            map[x, z] = 0;
            if (Chance(50))
            {
                if (vertical) z += 1;
                else x += 1;
            }
            else
            {
                int move = Random.Range(-1, 2);
                if (!rect.Contains(vertical ? new Vector2(x + move, z) : new Vector2(x, z + move))) move = -move;
                if (vertical) x += move;
                else z += move;
            }

            done = !rect.Contains(new Vector2(x, z));
        }
    }
}