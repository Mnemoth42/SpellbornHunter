using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

namespace TkrainDesigns.Procedural
{
    public class Nudger : MonoBehaviour
    {
        public Vector3 nudgeAmounts;

        private void Start()
        {
            foreach (Transform child in transform)
            {
                float x = Random.Range(0, nudgeAmounts.x);
                float y = Random.Range(0, nudgeAmounts.y);
                float z = Random.Range(0, nudgeAmounts.z);
                child.localPosition += new Vector3(x, y, z);
            }
        }
    }
}