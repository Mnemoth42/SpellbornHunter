﻿using System.Collections;
using System.Collections.Generic;
using GameDevTV.Saving;
using Newtonsoft.Json.Linq;
using TkrainDesigns;
using TkrainDesigns.Control;
using TkrainDesigns.SceneManagement;
using TkrainDesigns.Stats;
using Unity.AI.Navigation;
using UnityEngine;
using UnityEngine.AI;


[System.Serializable]
public class MapLocation
{
    public int x;
    public int z;

    public MapLocation(int _x, int _z)
    {
        x = _x;
        z = _z;
    }
}

public class Maze : MonoBehaviour, ISaveable, IJsonSaveable
{
    [Range(10, 100)] public int width = 30;
    [Range(10, 100)] public int depth = 30;
    [Range(1, 100)] public int scale = 6;
    [Range(0, 20)] public int roomCount = 3;
    [Range(2, 10)] public int roomMinSize = 3;
    [Range(2, 10)] public int roomMaxSize = 5;
    public Vector2Int PlayerStartPosition;
    public GameObject player;
    public GameObject exit;
    public GameObject entrance;
    public List<GameObject> straight = new List<GameObject>();
    public List<GameObject> deadEnd = new List<GameObject>();
    public List<GameObject> corner = new List<GameObject>();
    public List<GameObject> tJunction = new List<GameObject>();
    public List<GameObject> fullJunction = new List<GameObject>();
    public List<GameObject> wall = new List<GameObject>();
    public List<GameObject> floor = new List<GameObject>();
    public List<GameObject> ceiling = new List<GameObject>();
    public List<GameObject> doorway = new List<GameObject>();
    public List<GameObject> pillar = new List<GameObject>();
    public List<GameObject> trees = new List<GameObject>();
    public List<GameObject> buildings = new List<GameObject>();
    private int level = 1;


    public int Level
    {
        get => level;
        set => level = value;
    }

    public int PlayerLevel
    {
        get { return player.GetComponent<BaseStats>().GetLevel(); }
    }


    private NavMeshSurface surface;

    protected Vector2Int endPosition;

    public int[,] map;
    List<MapLocation> pillars = new List<MapLocation>();

    IEnumerator Start()
    {
        FindObjectOfType<Fader>().FadeOutImmediate();
        surface = GetComponent<NavMeshSurface>();
        yield return CreateLevel();
    }

    IEnumerator CreateLevel()
    {
        foreach (Transform t in transform)
        {
            Destroy(t.gameObject);
        }

        FindObjectOfType<SavingWrapper>().Load();
        yield return null;
        InitializeMap();
        yield return null;
        Generate();
        //AddRooms(roomCount, roomMinSize, roomMaxSize);
        yield return null;
        DrawMap();
        FreezeAllControllers();
        yield return null;
        surface.BuildNavMesh();
        yield return null;
        PlacePlayer();
        yield return FindObjectOfType<Fader>().FadeIn(2);
        EnableAllControllers();
    }

    public IEnumerator CreateNextLevel()
    {
        level += 1;
        width += 1;
        FreezeAllControllers();
        yield return FindObjectOfType<Fader>().FadeOut(2.0f);
        FindObjectOfType<SavingWrapper>().Save();
        FindObjectOfType<SavingWrapper>().LoadLastScene();
    }

    public void NextLevel()
    {
        StartCoroutine(CreateNextLevel());
    }

    void FreezeAllControllers()
    {
        foreach (BaseController controller in FindObjectsOfType<BaseController>())
        {
            controller.FreezeController();
        }
    }

    void EnableAllControllers()
    {
        foreach (BaseController controller in FindObjectsOfType<BaseController>())
        {
            controller.EnableController();
        }
    }

    void DrawExit()
    {
        PlacePiece(endPosition.x, endPosition.y, exit);
        PlaceWallPieces(endPosition.x, endPosition.y);
    }

    void DrawEntrance()
    {
        PlacePiece(PlayerStartPosition.x, PlayerStartPosition.y, entrance);
        PlaceWallPieces(PlayerStartPosition.x, PlayerStartPosition.y);
    }

    private void PlacePlayer()
    {
        player.GetComponent<NavMeshAgent>()
              .Warp(new Vector3(PlayerStartPosition.x * scale, 0, PlayerStartPosition.y * scale));
        //player.transform.position = new Vector3(PlayerStartPosition.x * scale, 0, PlayerStartPosition.y * scale);
    }

    public virtual void AddRooms(int count, int minSize, int maxSize)
    {
        for (int c = 0; c < count; c++)
        {
            int roomWidth = Random.Range(minSize, maxSize + 1);
            int roomDepth = Random.Range(minSize, maxSize + 1);
            int startX = Random.Range(2 + roomWidth, width - 2 - roomWidth);
            int startZ = Random.Range(2 + roomWidth, depth - 2 - roomWidth);
            for (int x = startX; x < width - 2 & x < startX + roomWidth; x++)
            {
                for (int z = startZ; z < depth & z < startZ + roomDepth; z++)
                {
                    map[x, z] = 0;
                }
            }
        }
    }

    public bool Chance(int chance)
    {
        return Random.Range(0, 100) < chance;
    }

    void DrawRawMap()
    {
        for (int z = 0; z < depth; z++)
        for (int x = 0; x < width; x++)
        {
            if (map[x, z] == 1)
            {
                PlacePiece(x, z);
            }
        }
    }

    GameObject RandomElement(List<GameObject> list)
    {
        if (list.Count == 0) return null;
        return list[Random.Range(0, list.Count)];
    }

    void DrawMap()
    {
        //DrawRawMap();
        GameObject tile;
        for (int z = 0; z < depth; z++)
        for (int x = 0; x < width; x++)
        {
            if ((x == endPosition.x) && (z == endPosition.y))
            {
                DrawExit();
            }
            else if (x == PlayerStartPosition.x && z == PlayerStartPosition.y)
            {
                DrawEntrance();
            }
            else if (map[x, z] == 1)
            {
                tile = PlacePiece(x, z, RandomElement(trees));
                tile.name = $"Trees ({x},{z})";
            }
            else if (Search2D(x, z, "ew")) //Corridor N/S |
            {
                tile = PlacePiece(x, z, RandomElement(straight));
                tile.name = $"Corridor N/S ({x},{z})";
                // player.transform.position = new Vector3(x * scale, 2, z * scale);
            }
            else if (Search2D(x, z, "ns")) //Corridor E/W _
            {
                tile = PlacePiece(x, z, RandomElement(straight), 90);
                tile.name = $"Corridor E/W ({x},{z})";
            }
            else if (Search2D(x, z, "wen")) //Dead End S 
            {
                tile = PlacePiece(x, z, RandomElement(deadEnd), 180);
                tile.name = $"Deadend S ({x},{z})";
            }
            else if (Search2D(x, z, "wes")) //Dead End N
            {
                tile = PlacePiece(x, z, RandomElement(deadEnd), 0);
                tile.name = $"Deadend N ({x},{z})";
            }
            else if (Search2D(x, z, "nes")) //Dead End W
            {
                tile = PlacePiece(x, z, RandomElement(deadEnd), 270);
                tile.name = $"Deadend W ({x},{z})";
            }
            else if (Search2D(x, z, "nws")) //Dead End e
            {
                tile = PlacePiece(x, z, RandomElement(deadEnd), 90);
                tile.name = $"Deadend E ({x},{z})";
            }
            else if (Search2D(x, z, new int[] { 1, 0, 1, 0, 0, 0, 1, 0, 1 })) //Crossroad
            {
                tile = PlacePiece(x, z, RandomElement(fullJunction));
                tile.name = $"Crossroads ({x},{z})";
            }
            else if (Search2D(x, z, new int[] { 1, 0, 5, 5, 0, 1, 5, 1, 5 }))
            {
                tile = PlacePiece(x, z, RandomElement(corner), 270);
                tile.name = $"Corner SE ({x},{z})";
            }
            else if (Search2D(x, z, new int[] { 5, 0, 1, 1, 0, 0, 5, 1, 5 }))
            {
                tile = PlacePiece(x, z, RandomElement(corner), 0);
                tile.name = $"Corner SW ({x},{z})";
            }
            else if (Search2D(x, z, new int[] { 5, 1, 1, 1, 0, 0, 5, 0, 1 }))
            {
                tile = PlacePiece(x, z, RandomElement(corner), 90);
                tile.name = $"Corner NW ({x},{z})";
            }
            else if (Search2D(x, z, new int[] { 5, 1, 5, 0, 0, 1, 1, 0, 5 }))
            {
                tile = PlacePiece(x, z, RandomElement(corner), 180);
                tile.name = $"Corner NE ({x},{z})";
            }
            else if (
                Search2D(x, z,
                    new int[] { 5, 0, 1, 1, 0, 0, 5, 0, 1 }) /*|| Search2D(x,z,new int[]{5,0,1,1,0,0,5,0,5})*/)
            {
                tile = PlacePiece(x, z, RandomElement(tJunction));
                tile.name = $"TJunction W ({x},{z})";
            }
            else if (Search2D(x, z, new int[] { 5, 1, 1, 0, 0, 0, 1, 0, 1 }))
            {
                tile = PlacePiece(x, z, RandomElement(tJunction), 90);
                tile.name = $"TJunction N ({x},{z})";
            }
            else if (Search2D(x, z, new int[] { 1, 0, 5, 0, 0, 1, 1, 0, 5 }))
            {
                tile = PlacePiece(x, z, RandomElement(tJunction), 180);
                tile.name = $"TJunction E ({x},{z})";
            }
            else if (
                Search2D(x, z,
                    new int[]
                    {
                        1, 0, 1, 0, 0, 0, 5, 1, 5
                    }) /*|| Search2D(x, z, new int[] { 1, 0, 5, 0, 0, 0, 5, 1, 5 })*/)
            {
                tile = PlacePiece(x, z, RandomElement(tJunction), 270);
                tile.name = $"TJunction S ({x},{z})";
            }
            else if ((CountSquareNeighbours(x, z) > 1 && CountDiagonalNeighbors(x, z) >= 1)
                  || (CountSquareNeighbours(x, z) >= 1 && CountDiagonalNeighbors(x, z) > 1))

            {
                tile = PlacePiece(x, z, RandomElement(floor));
                tile.name = $"Dungeon Floor ({x},{z})";
                TryPlaceBuildings(x, z);
                PlaceWallPieces(x, z);
                PlaceDoors(x, z);
            }
        }
    }

    void TryPlaceBuildings(int x, int z)
    {
        if (buildings.Count == 0) return;

        {
            Debug.Log($"{x},{z}");
            if (Random.Range(0, 10) < 2)
            {
                GameObject prefab = RandomElement(buildings);
                float rotation = Random.Range(0, 4) * 90;
                GameObject building = PlacePiece(x, z, prefab, rotation);
            }
        }
    }

    void PlaceDoors(int x, int z)
    {
        LocatePassage(x, z);
        if (north)
        {
            PlacePiece(x, z, RandomElement(doorway)).name = $"Doorway North ({x},{z})";
            //if (map[x + 1, z] == 0)
            //    PlacePiece(x, z, pillar, 90).name = $"Pillar NorthEast ({x},{z})";
        }

        if (south)
        {
            PlacePiece(x, z, RandomElement(doorway), 180).name = $"Doorway South ({x},{z})";
            //if (map[x - 1, z] == 0 )
            //    PlacePiece(x, z, pillar, 270).name = $"Pillar SouthWest ({x},{z})";
        }

        if (west)
        {
            PlacePiece(x, z, RandomElement(doorway), 270).name = $"Doorway West ({x},{z})";
            //if (map[x, z + 1] == 0)
            //    PlacePiece(x, z, pillar).name = $"Pillar NorthEast ({x},{z})";
        }

        if (east)
        {
            PlacePiece(x, z, RandomElement(doorway), 90).name = $"Doorway East ({x},{z})";
            //if (map[x, z - 1] == 0)
            //    PlacePiece(x, z, pillar, 180).name = $"Pillar SouthEast ({x},{z})";
        }
    }

    bool TestPillarLocation(int x, int z)
    {
        return pillars.Contains(new MapLocation(x, z));
    }

    GameObject TryPlacePillar(int x, int z)
    {
        if (TestPillarLocation(x, z)) return null;
        GameObject piece = PlacePiece(x, z, RandomElement(pillar));
        piece.name = $"Pillar ({x},{z})";
        return piece.gameObject;
    }

    void PlaceWallPieces(int x, int z)
    {
        GameObject tile;
        PlacePiece(x, z, RandomElement(ceiling)).name = "Dungeon Ceiling";
        LocateWalls(x, z);
        if (north)
        {
            PlacePiece(x, z, RandomElement(wall)).name = $"Wall North ({x},{z})";
            // if (IsOpen(x+1,z) && IsOpen(x+1,z+1))
            // {
            //     TryPlacePillar(x,z);
            // }
            //
            // if (IsOpen(x-1,z) && IsOpen(x-1,x+1))
            // {
            //     TryPlacePillar(x,z);
            // }
        }

        if (south)
        {
            PlacePiece(x, z, RandomElement(wall), 180).name = $"Wall South ({x},{z})";
            if (IsOpen(x - 1, z) && IsOpen(x - 1, z - 1))
                TryPlacePillar(x - 1, z - 1);
            if (IsOpen(x + 1, z) && IsOpen(x + 1, z - 1))
                TryPlacePillar(x, z - 1);
        }

        if (west)
        {
            PlacePiece(x, z, RandomElement(wall), 270).name = $"Wall West ({x},{z})";
            if (IsOpen(x, z + 1) && IsOpen(x - 1, z + 1))
                TryPlacePillar(x - 1, z);
            if (IsOpen(x, z - 1) && IsOpen(x - 1, z - 1))
                TryPlacePillar(x - 1, z - 1);
        }

        if (east)
        {
            PlacePiece(x, z, RandomElement(wall), 90).name = $"Wall East ({x},{z})";
            if (IsOpen(x, z + 1) && IsOpen(x + 1, z + 1))
                TryPlacePillar(x, z);
            if (IsOpen(x, z - 1) && IsOpen(x + 1, z - 1))
            {
                TryPlacePillar(x, z - 1);
            }
        }
    }

    bool IsOpen(int x, int z)
    {
        return (map[x, z] == 0);
    }

    protected bool IsOpen(Vector2Int position)
    {
        return IsOpen(position.x, position.y);
    }

    protected void SetMapvalue(Vector2Int position, int value = 0)
    {
        map[position.x, position.y] = value;
    }

    bool north;
    bool south;
    bool east;
    bool west;

    public void LocateWalls(int x, int z)
    {
        north = false;
        south = false;
        east = false;
        west = false;
        if (x <= 0 || x >= width - 1 || z <= 0 || z >= depth - 1) return;
        if (map[x, z + 1] == 1) north = true;
        if (map[x, z - 1] == 1) south = true;
        if (map[x + 1, z] == 1) east = true;
        if (map[x - 1, z] == 1) west = true;
    }

    public void LocatePassage(int x, int z)
    {
        north = false;
        south = false;
        east = false;
        west = false;
        if (x <= 0 || x >= width - 1 || z <= 0 || z >= depth - 1) return;
        //if (Search2D(x, z, new int[] { 1, 0, 1, 5, 0, 5, 5, 5, 5 })) north = true;
        //if (Search2D(x, z, new int[] { 5, 0, 1, 1, 0, 5, 5, 5, 5 })) north = true;
        //if (Search2D(x, z, new int[] { 1, 0, 5, 5, 0, 1, 5, 5, 5 })) north = true;
        if (IsOpen(x, z + 1) && !IsOpen(x - 1, z + 1) && !IsOpen(x + 1, z + 1)) north = true;

        //if (Search2D(x, z, new int[] { 5, 5, 5, 5, 0, 5, 1, 0, 1 })) south = true;
        //if (Search2D(x, z, new int[] { 5, 5, 5, 5, 0, 1, 1, 0, 5 })) south = true;
        //if (Search2D(x, z, new int[] { 5, 5, 5, 1, 0, 5, 5, 0, 1 })) south = true;
        if (IsOpen(x, z - 1) && !IsOpen(x - 1, z - 1) && !IsOpen(x + 1, z - 1)) south = true;

        //if (Search2D(x, z, new int[] { 1, 5, 5, 0, 0, 5, 1, 5, 5 })) west = true;
        //if (Search2D(x, z, new int[] { 1, 5, 5, 0, 0, 5, 5, 1, 5 })) west = true;
        //if (Search2D(x, z, new int[] { 5, 1, 5, 0, 0, 5, 1, 5, 5 })) west = true;
        if (IsOpen(x - 1, z) && !IsOpen(x - 1, z + 1) && !IsOpen(x - 1, z - 1)) west = true;

        //if (Search2D(x, z, new int[] { 5, 5, 1, 5, 5, 0, 5, 5, 1 })) east = true;
        //if (Search2D(x, z, new int[] { 5, 5, 1, 5, 5, 0, 5, 1, 5 })) east = true;
        //if (Search2D(x, z, new int[] { 5, 1, 5, 5, 5, 0, 5, 5, 1 })) east = true;
        if (IsOpen(x + 1, z) && !IsOpen(x + 1, z + 1) && !IsOpen(x + 1, z - 1)) east = true;
    }

    GameObject PlacePiece(int x, int z, GameObject prefab = null, float rotation = 0)
    {
        Vector3 pos = new Vector3(x * scale, 0, z * scale);

        GameObject wall = prefab == null
            ? GameObject.CreatePrimitive(PrimitiveType.Cube)
            : Instantiate(prefab, transform);
        if (prefab == null) wall.transform.localScale = new Vector3(scale * .95f, 1, scale * .95f);
        wall.transform.position = pos;
        wall.transform.localRotation = Quaternion.AngleAxis(rotation, Vector3.up);
        foreach (ISetLevel manager in wall.GetComponentsInChildren<ISetLevel>())
        {
            manager.SetLevel(level, PlayerLevel);
        }

        return wall;
    }


    public virtual void Generate()
    {
    }

    void InitializeMap()
    {
        map = new int[width, depth];
        for (int z = 0; z < depth; z++)
        for (int x = 0; x < width; x++)
        {
            map[x, z] = 1;
        }
    }

    public bool IsInBounds(int x, int z)
    {
        return x > 0 && z > 0 && x < width - 1 && z < depth - 1;
    }

    public bool IsInBounds(Vector2Int position)
    {
        return IsInBounds(position.x, position.y);
    }

    public int CountSquareNeighbors(Vector2Int position)
    {
        return CountSquareNeighbours(position.x, position.y, 0);
    }

    protected List<MapLocation> SquareDirections = new List<MapLocation>()
                                                   {
                                                       new MapLocation(0, 1),
                                                       new MapLocation(0, -1),
                                                       new MapLocation(1, 0),
                                                       new MapLocation(-1, 0)
                                                   };

    protected List<MapLocation> DiagonalDirections = new List<MapLocation>()
                                                     {
                                                         new MapLocation(1, 1),
                                                         new MapLocation(-1, -1),
                                                         new MapLocation(1, -1),
                                                         new MapLocation(-1, 1)
                                                     };

    public int CountSquareNeighbours(int x, int z, byte testValue = 0)
    {
        if (!IsInBounds(x, z)) return 5;
        int count = 0;
        foreach (MapLocation loc in SquareDirections)
        {
            if (map[x + loc.x, z + loc.z] == testValue) count++;
        }

        return count;
    }

    public int CountDiagonalNeighbors(int x, int z, byte testValue = 0)
    {
        if (!IsInBounds(x, z)) return 5;
        int count = 0;
        foreach (MapLocation loc in DiagonalDirections)
        {
            if (map[x + loc.x, z + loc.z] == testValue) count++;
        }

        return count;
    }

    bool Search2D(int c, int r, int[] pattern)
    {
        Rect rect = new Rect(0, 0, width - 1, depth - 1);
        int count = 0;
        int pos = 0;
        for (int z = 1; z > -2; z--)
        for (int x = -1; x < 2; x++)
        {
            if (!rect.Contains(new Vector2(c + x, r + z)))
            {
                count += pattern[pos] > 0 ? 1 : 0;
            }
            else if (pattern[pos] > 1 || pattern[pos] == map[c + x, r + z])
                count++;


            pos++;
        }

        return (count == 9);
    }

    bool Search2D(int c, int r, string mask)
    {
        int[] pattern = new int[] { 5, 0, 5, 0, 0, 0, 5, 0, 5 };
        string test = mask.ToLower();
        pattern[1] = mask.Contains("n") ? 1 : 0;
        pattern[3] = mask.Contains("w") ? 1 : 0;
        pattern[5] = mask.Contains("e") ? 1 : 0;
        pattern[7] = mask.Contains("s") ? 1 : 0;
        return Search2D(c, r, pattern);
    }

    public int CountAllNeighbors(int x, int z, byte testValue = 0)
    {
        return CountDiagonalNeighbors(x, z, testValue) + CountSquareNeighbours(x, z, testValue);
    }

    public JToken CaptureState()
    {
        return level;
    }

    public void RestoreState(JToken state)
    {
        level = state.ToObject<int>();
    }

    public JToken CaptureAsJToken()
    {
        return level;
    }

    public void RestoreFromJToken(JToken state)
    {
        level = state.ToObject<int>();
    }
}