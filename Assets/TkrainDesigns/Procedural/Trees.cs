using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

namespace TkrainDesigns.Procedural
{
    public class Trees : MonoBehaviour
    {
        public float size = 20;
        public int density = 100;
        public List<GameObject> trees = new List<GameObject>();

        private void Awake()
        {
            for (int i = 0; i < density; i++)
            {
                float x = Random.Range(-size / 2, size / 2);
                float z = Random.Range(-size / 2, size / 2);
                GameObject o = Instantiate(trees[Random.Range(0, trees.Count)], transform);
                o.transform.localPosition = new Vector3(x, 0, z);
                o.transform.localRotation = Quaternion.Euler(0, Random.Range(0, 360), 0);
            }
        }
    }
}