﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class Recursive : Maze
{
    public override void Generate()
    {
        Generate(Random.Range(1, width - 1), Random.Range(1, depth - 1));
    }

    void Generate(int x, int z)
    {
        if (CountSquareNeighbours(x, z) >= 2) return;
        map[x, z] = 0;
        var directions = SquareDirections.Select(v => new MapLocation(v.x, v.z)).ToList();
        directions.Shuffle();
        foreach (MapLocation l in directions)
        {
            Generate(x + l.x, z + l.z);
        }
    }
}