using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace TkrainDesigns.Procedural
{
    public class Scatter : MonoBehaviour
    {
        public float distance;

        private void Awake()
        {
            ScatterObjects();
        }

        [ContextMenu("Scatter")]
        private void ScatterObjects()
        {
            foreach (Transform child in transform)
            {
                child.localPosition = new Vector3(Random.Range(-distance / 2f, distance / 2f), Random.Range(-.1f, 0),
                    Random.Range(-distance / 2f, distance / 2f));
                child.localRotation = Quaternion.Euler(0, Random.Range(0, 360), 0);
            }
        }
    }
}