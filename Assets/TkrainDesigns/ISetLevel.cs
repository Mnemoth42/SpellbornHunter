namespace TkrainDesigns
{
    public interface ISetLevel
    {
        void SetLevel(int newLevel, int playerLevel);
    }
}