using TkrainDesigns.Attributes;
using UnityEngine;
using UnityEngine.InputSystem;

namespace TkrainDesigns.Combat
{
    [RequireComponent(typeof(Health))]
    [DisallowMultipleComponent]
    public class CombatTarget : MonoBehaviour, IRaycastable
    {
        private Health health;
        private Fighter fighter;

        private void Awake()
        {
            health = GetComponent<Health>();
            fighter = GetComponent<Fighter>();
        }


        public bool HandleRayCast(GameObject controller)
        {
            if (!health.IsAlive()) return false;
            if (!fighter.enabled) return false;
            if (gameObject == controller) return false;
            Fighter otherFighter = controller.GetComponent<Fighter>();
            if (otherFighter == null || !otherFighter.enabled) return false;
            if (Mouse.current.leftButton.IsPressed())
            {
                otherFighter.StartAction(health);
            }

            return true;
        }

        public int GetPriority()
        {
            return -1;
        }

        public ECursorType GetCursorType()
        {
            return ECursorType.Combat;
        }
    }
}