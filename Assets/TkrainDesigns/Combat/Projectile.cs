using System;
using System.Collections;
using System.Collections.Generic;
using TkrainDesigns.Attributes;
using TkrainDesigns.Stats;
using UnityEngine;

namespace TkrainDesigns.Combat
{
    public class Projectile : MonoBehaviour
    {
        [SerializeField] private float speed = 10.0f;
        private Health target;
        private GameObject damageDealer;
        private int damage;
        private IEnumerable<DamageCharacteristic> damageCharacteristics;

        public void SetTargetLocation(Vector3 getTargetedPoint, GameObject dealer, int damage,
                                      IEnumerable<DamageCharacteristic> characteristics)
        {
            damageDealer = dealer;
            this.damage = damage;
            damageCharacteristics = characteristics;
            transform.LookAt(getTargetedPoint);
        }

        public void SetTargetLocation(Vector3 getTargetedPoint, GameObject dealer, int damage)
        {
            List<DamageCharacteristic> tempList = new List<DamageCharacteristic>() { new DamageCharacteristic() };
            SetTargetLocation(getTargetedPoint, dealer, damage, tempList);
        }

        public void SetTarget(Health health, GameObject dealer, int damage,
                              IEnumerable<DamageCharacteristic> characteristics)
        {
            target = health;
            SetTargetLocation(health.transform.position + new Vector3(0, 1, 0), dealer, damage, characteristics);
        }

        public void SetTarget(Health health, GameObject dealer, int damage)
        {
            target = health;
            SetTargetLocation(health.transform.position + new Vector3(0, 1, 0), dealer, this.damage);
        }

        private void Update()
        {
            if (target)
            {
                if (target.IsAlive())
                {
                    transform.LookAt(target.transform.position + new Vector3(0, 1, 0));
                }
                else
                {
                    target = null;
                }
            }

            transform.Translate(Vector3.forward * (speed * Time.deltaTime));
        }

        private void OnTriggerEnter(Collider other)
        {
            if (other.gameObject == damageDealer) return; //avoid self harm
            if (other.TryGetComponent(out Health health))
            {
                health.TakeDamage(damage, damageDealer, damageCharacteristics);
                Destroy(gameObject);
            }
        }
    }
}