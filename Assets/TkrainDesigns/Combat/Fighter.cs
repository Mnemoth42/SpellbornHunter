using GameDevTV.Inventories;
using TkrainDesigns.Attributes;
using TkrainDesigns.Movement;
using UnityEngine;


namespace TkrainDesigns.Combat
{
    [DisallowMultipleComponent]
    [RequireComponent(typeof(Animator))]
    public class Fighter : MoveableActionBehavior<Health>
    {
        [SerializeField] private float attackRange = 3.0f;
        [SerializeField] private WeaponConfig defaultWeaponConfig;
        [SerializeField] private Transform rightHandTransform = null;
        [SerializeField] private Transform leftHandTransform = null;

        private WeaponConfig currentWeaponConfig;
        public Weapon currentWeapon;
        private int damage = 5;

        private Animator anim;
        private Equipment equipment;
        private static readonly int Attack = Animator.StringToHash("Attack");
        private static readonly int AbortAttack = Animator.StringToHash("AbortAttack");
        private const string AttackTag = "Attack";


        protected override void Awake()
        {
            base.Awake();
            anim = GetComponent<Animator>();
            acceptanceRadius = attackRange;
            if (TryGetComponent(out equipment))
            {
                equipment.equipmentUpdated += UpdateWeapon;
            }
        }

        private void Start()
        {
            UpdateWeapon();
        }

        public void UpdateWeapon()
        {
            if (!equipment)
            {
                EquipWeapon(defaultWeaponConfig);
                return;
            }

            var equipmentWeapon = equipment.GetItemInSlot(EquipLocation.Weapon);
            if (equipmentWeapon is WeaponConfig config)
            {
                EquipWeapon(config);
            }
            else
            {
                EquipWeapon(defaultWeaponConfig);
            }
        }

        void EquipWeapon(WeaponConfig config)
        {
            if (config == null) return;
            currentWeaponConfig = config;
            currentWeapon = currentWeaponConfig.SpawnWeapon(rightHandTransform, leftHandTransform, anim);
        }

        protected override void Perform()
        {
            if (IsCooldownExpired("Attack"))
            {
                Quaternion lookRotation = transform.position.FaceTowards(target).SubY(0).LookRotation();
                transform.rotation = lookRotation;
                damage = currentWeaponConfig.SetRandomAttackVariant(anim).ToInt();
                anim.SetTrigger(Attack);
                SetTimer("Attack", currentWeaponConfig.GetAttackSpeed());
            }
        }

        protected override bool IsValidAction()
        {
            return target != null && target.IsAlive();
        }

        protected override void HandleOutOfRange()
        {
            if (anim.GetCurrentAnimatorStateInfo(0).IsTag(AttackTag))
            {
                anim.SetTrigger(AbortAttack);
            }
        }

        public override void Cancel()
        {
            base.Cancel();
            //Debug.Log("Fighter Cancel");
            HandleOutOfRange();
        }

        void Hit()
        {
            if (target == null || !target.IsAlive()) return;
            
            if (currentWeapon)
            {
                currentWeapon.OnHit();
            }

            target.TakeDamage(damage, gameObject, currentWeaponConfig.GetCharacteristics());
        }

        public Transform GetHandTransform(bool isRightHand)
        {
            return isRightHand ? rightHandTransform : leftHandTransform;
        }

        public bool InCombat()
        {
            return target != null && target.IsAlive();
        }

        public GameObject GetTarget()
        {
            return target.gameObject;
        }
    }
}