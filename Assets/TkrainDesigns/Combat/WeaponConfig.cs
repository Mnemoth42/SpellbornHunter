using System;
using System.Collections.Generic;
using System.Linq;
using GameDevTV.Inventories;
using TkrainDesigns.Inventories;
using TkrainDesigns.Stats;
using UnityEditor;
using UnityEditor.UIElements;
using UnityEngine;
using UnityEngine.UIElements;
using Random = UnityEngine.Random;

namespace TkrainDesigns.Combat
{
    [CreateAssetMenu(fileName = "New WeaponConfig", menuName = "Equipment/WeaponConfig")]
    public class WeaponConfig : StatsEquipableItem
    {
        [SerializeField] private Weapon weapon;
        [SerializeField] private GameObject modelToSocket = null;
        [SerializeField] private Vector3 socketRotation;
        [SerializeField] private Vector3 socketScale = new Vector3(1, 1, 1);
        [Range(1, 100)]
        [Tooltip(
            "Added to character's attack value.  Damage = Attack Value + Base Damage - Opponent's Defense.  Final value is randomized.")]
        [SerializeField]
        private int baseDamage = 3;

        [Range(1, 100)]
        [Tooltip(
            "Higher is faster.  Time between attacks = Attack Speed / 60.  Example: 20 = 3 seconds, 30 = 2 seconds, 60 = 1 second. ")]
        [SerializeField]
        private int attackSpeed = 30;

        //[SerializeField] private AnimatorOverrideController controller;
        [SerializeField] private List<AttackVariant> attackVariants = new List<AttackVariant>();
        [SerializeField] private bool leftHanded = false;

        [SerializeField] private List<DamageCharacteristic> characteristics = new List<DamageCharacteristic>();

        public IEnumerable<DamageCharacteristic> GetCharacteristics()
        {
            if (characteristics.Count == 0)
            {
                yield return new DamageCharacteristic();
            }
            else
            {
                foreach (var characteristic in characteristics)
                {
                    yield return characteristic;
                }
            }
        }

        int BaseDamage
        {
            get { return baseDamage + GetLevel() / 3; }
        }

        private const string ModelName = "EquippedWeaponModel";

        public float GetBaseDamage() => baseDamage;
        public float GetAttackSpeed() => 60.0f / attackSpeed.ToFloat().Floor(1.0f);

        public float SetRandomAttackVariant(Animator anim)
        {
            int sum = attackVariants.Sum(variant => variant.weight);
            int selection = Random.Range(0, sum);
            foreach (AttackVariant variant in attackVariants)
            {
                selection -= variant.weight;
                if (selection <= 0)
                {
                    SetAnimatorController(anim, variant.controller);
                    return BaseDamage * variant.Intensity();
                }
            }

            return CalculateDamage(anim.gameObject, BaseDamage);
        }

        public float CalculateDamage(GameObject user, float damage)
        {
            BaseStats stats = user.GetComponent<BaseStats>();
            float percentageModifier = (100 + stats.GetPercentageModifiers(EStat.Offense)) / 100f;
            return damage * percentageModifier + stats.GetStat(EStat.Offense);
        }

        public override string GetDescription()
        {
            return $"{base.GetDescription()}\nBase Damage {BaseDamage}";
        }

        public Weapon SpawnWeapon(Transform rightHand, Transform leftHand, Animator anim)
        {
            DestroyOldWeaponModel(rightHand);
            DestroyOldWeaponModel(leftHand);
            SetRandomAttackVariant(anim);
            if (weapon == null)
            {
                Debug.Log($"{name} has no weapon model!");
                return null;
            }

            Transform parent = leftHanded ? leftHand : rightHand;
            Weapon weaponModel = Instantiate(weapon, parent);
            weaponModel.name = ModelName;
            if (modelToSocket != null)
            {
                GameObject socket = Instantiate(modelToSocket, weaponModel.transform);
                socket.transform.localEulerAngles = socketRotation;
                socket.transform.localScale = socketScale;
            }
            weaponModel.gameObject.layer = anim.gameObject.layer;
            foreach (Transform t in weaponModel.transform)
            {
                t.gameObject.layer = anim.gameObject.layer;
            }

            return weaponModel;
        }

        private void SetAnimatorController(Animator anim, RuntimeAnimatorController controllerToSet)
        {
            anim.runtimeAnimatorController = controllerToSet;
        }

        private static void DestroyOldWeaponModel(Transform container)
        {
            foreach (var child in container.Cast<Transform>().Where(child => child.name == ModelName))
            {
                Destroy(child.gameObject, .1f);
            }
        }

        public override void OnValidate()
        {
            base.OnValidate();
            if (GetAllowedEquipLocation() != EquipLocation.Weapon)
            {
                SetAllowedEquipLocation(EquipLocation.Weapon);
            }
        }

    #region Editor

    #if UNITY_EDITOR
        protected override bool IsLocationSelectable(Enum location)
        {
            return (EquipLocation)location == EquipLocation.Weapon;
        }

        void AddAttackVariant()
        {
            SetUndo("Add Attack Variant");
            attackVariants.Add(new AttackVariant());
        }

        void RemoveAttackVariant(int index)
        {
            SetUndo("Remove Attack Variant");
            attackVariants.RemoveAt(index);
        }

        private bool drawWeaponConfig = true;
        private bool drawVariants = true;
        private bool drawCharacteristics = true;

        public override void DrawInspector()
        {
            base.DrawInspector();
            if (!DrawSection(ref drawWeaponConfig, "Weapon Config")) return;
            BeginIndent();
            DrawIntSlider(ref baseDamage, 1, 100, "Base Damage",
                "Added to character's attack value.  Damage = Attack Value + Base Damage - Opponent's Defense.  Final value is randomized.");
            DrawIntSlider(ref attackSpeed, 1, 60, "Attack Speed",
                "Higher is faster.  Time between attacks = 60/attackSpeed.  Example: 20 = 3 seconds, 30 = 2 seconds, 60 = 1 second. ");
            EditorGUILayout.LabelField($"TBA:{GetAttackSpeed():F2} Seconds. (DPS: {baseDamage / GetAttackSpeed():F1})");
            SetObject(ref weapon, SelectPrefabByComponent("Weapon Model", weapon), "Weapon Model");
            DrawBool(ref leftHanded, "LeftHanded", "Determines which hand to place weapon.");
            if (DrawSection(ref drawVariants, $"Attack Variants {attackVariants.Count}"))
            {
                int variantToRemove = -1;
                BeginIndent();
                for (int i = 0; i < attackVariants.Count; i++)
                {
                    EditorGUILayout.Separator();
                    if (attackVariants[i].DrawInspector(this)) variantToRemove = i;
                }

                EndIndent();
                if (variantToRemove >= 0)
                {
                    RemoveAttackVariant(variantToRemove);
                }

                if (GUILayout.Button("Add Variant"))
                {
                    AddAttackVariant();
                }
            }

            DrawListCategory(characteristics, new DamageCharacteristic(), ref drawCharacteristics, "Damage Type");
            EndIndent();
        }

        protected override void AddToolbarString()
        {
            base.AddToolbarString();
            toolbarStrings.Add("Weapon Model");
        }


        [NonSerialized] private GameObject previewPrefab;

        public override void HandlePreview(string previewString, Rect r, GUIStyle background,
                                           PreviewRenderUtility renderUtility)
        {
            if (previewString.Equals("Weapon Model"))
            {
                GameObject objectToRender = weapon.gameObject;
                RenderPreviewObject(r, background, renderUtility, objectToRender);
                return;
            }

            base.HandlePreview(previewString, r, background, renderUtility);
        }

        public override GameObject GetPreviewObject(string tooltipName)
        {
            if (tooltipName == "Weapon Model") return weapon.gameObject;
            return base.GetPreviewObject(tooltipName);
        }

        [NonSerialized] private VisualTreeAsset AttackVariantEntryUXML;
        [NonSerialized] private Foldout attackVariantFoldout;
        [NonSerialized] private Foldout damageCharacteristicsFoldout;

        public override void CreateGUI(ScrollView container, Action _callback)
        {
            base.CreateGUI(container, _callback);
            VisualTreeAsset WeaponConfigUXML = Resources.Load<VisualTreeAsset>("WeaponConfigUXML");
            AttackVariantEntryUXML = Resources.Load<VisualTreeAsset>("AttackVariantEntryUXML");
            VisualElement WeaponConfigContainer = WeaponConfigUXML.CloneTree();
            container.Add(WeaponConfigContainer);
            
            DropdownField dropdownField = WeaponConfigContainer.Q<DropdownField>();
            List<Weapon> weapons = Statics.GetAssetsOfType<Weapon>();
            List<string> names = Statics.GetNamesFromList(weapons);
            var weaponProperty = FindProperty("weapon");
            if (weaponProperty.objectReferenceValue == null) weaponProperty.objectReferenceValue = weapons[0];
            dropdownField.choices = names;
            dropdownField.index = weapons.IndexOf(weaponProperty.objectReferenceValue as Weapon);
            dropdownField.RegisterValueChangedCallback((evt) =>
            {
                weaponProperty.objectReferenceValue = weapons[dropdownField.index];
                ApplyModifiedProperties();
            });
            PropertyField socketRotationPropertyField = WeaponConfigContainer.Q<PropertyField>("socketRotationPropertyField");
            socketRotationPropertyField.BindProperty(FindProperty("socketRotation"));
            socketRotationPropertyField.RegisterValueChangeCallback((evt) =>
            {
                ApplyModifiedProperties();
            });
            PropertyField socketScalePropertyfield =
                WeaponConfigContainer.Q<PropertyField>("socketScalePropertyField");
            socketScalePropertyfield.BindProperty(FindProperty("socketScale"));
            socketScalePropertyfield.RegisterValueChangeCallback((evt) =>
            {
                ApplyModifiedProperties();
            });
            if (modelToSocket == null)
            {
                socketRotationPropertyField.Hide();
                socketScalePropertyfield.Hide();
            }
            else
            {
                socketRotationPropertyField.Show();
                socketScalePropertyfield.Show();
            }
            PropertyField weaponSocketPropertyField =
                WeaponConfigContainer.Q<PropertyField>("weaponSocketPropertyField");
            weaponSocketPropertyField.BindProperty(FindProperty("modelToSocket"));
            weaponSocketPropertyField.RegisterValueChangeCallback((evt) =>
            {
                ApplyModifiedProperties();
                if (modelToSocket == null)
                {
                    socketRotationPropertyField.Hide();
                    socketScalePropertyfield.Hide();
                }
                else
                {
                    socketRotationPropertyField.Show();
                    socketScalePropertyfield.Hide();
                }
            });
            var baseDamageProperty = FindProperty("baseDamage");
            var attackSpeedProperty = FindProperty("attackSpeed");
            
            SliderInt weaponBaseDamageSlider = WeaponConfigContainer.Q<SliderInt>("weaponBaseDamageSlider");
            weaponBaseDamageSlider.BindProperty(baseDamageProperty);
            weaponBaseDamageSlider.RegisterValueChangedCallback((evt) =>
            {
                ApplyModifiedProperties();
                RedrawPreview();
            });

            SliderInt attackSpeedSlider = WeaponConfigContainer.Q<SliderInt>("weaponAttackSpeedSlider");
            attackSpeedSlider.BindProperty(attackSpeedProperty);
            attackSpeedSlider.label = $"Attack Speed ({60f/attackSpeedProperty.intValue:F2})";
            attackSpeedSlider.RegisterValueChangedCallback((evt) =>
            {
                ApplyModifiedProperties();
                attackSpeedSlider.label = $"Attack Speed ({60f/attackSpeedProperty.intValue:F2})";
                RedrawPreview();
            });
            Toggle toggle = WeaponConfigContainer.Q<Toggle>("leftHandedToggle");
            
            
            toggle.BindProperty(FindProperty("leftHanded"));
            toggle.RegisterValueChangedCallback((evt) =>
            {
                Debug.Log($"{evt.newValue}");
                ApplyModifiedProperties();
            });
            attackVariantFoldout = WeaponConfigContainer.Q<Foldout>("attackVariantsFoldout");
            SetupAttackVariants();
            damageCharacteristicsFoldout = WeaponConfigContainer.Q<Foldout>("damageCharacteristicsFoldout");
            SetupDamageCharacteristics();
        }

        void SetupDamageCharacteristics()
        {
            Foldout foldout = damageCharacteristicsFoldout;
            foldout.Clear();
            var property = FindProperty("characteristics");
            for (int i = 0; i < property.arraySize; i++)
            {
                int index = i;
                var element = property.GetArrayElementAtIndex(i);
                VisualElement container = ModifierEntryUXML.CloneTree();
                EnumField enumField = container.Q<EnumField>();
                enumField.BindProperty(element.FindPropertyRelative("damageType"));
                enumField.RegisterValueChangedCallback((evt) =>
                {
                    ApplyModifiedProperties();
                    RedrawPreview();
                });
                Slider slider = container.Q<Slider>();
                slider.highValue = 2.0f;
                slider.lowValue = 0;
                slider.BindProperty(element.FindPropertyRelative("amount"));
                slider.RegisterValueChangedCallback((evt) =>
                {
                    ApplyModifiedProperties();
                    RedrawPreview();
                });
                SliderInt sliderInt = container.Q<SliderInt>();
                sliderInt.Hide();
                Button deleteButton = container.Q<Button>();
                deleteButton.clicked += () =>
                {
                    property.DeleteArrayElementAtIndex(index);
                    ApplyModifiedProperties();
                    RedrawPreview();
                    SetupDamageCharacteristics();
                };
                foldout.Add(container);
            }
            Button addButton = new Button(() =>
            {
                property.InsertArrayElementAtIndex(property.arraySize);
                ApplyModifiedProperties();
                RedrawPreview();
                SetupDamageCharacteristics();
            });
            addButton.text = "Add Damage Characteristic";
            foldout.Add(addButton);
        }
        
        void SetupAttackVariants()
        {
            Foldout foldout = attackVariantFoldout;
            foldout.Clear();
            var property = FindProperty("attackVariants");
            for (int i = 0; i < property.arraySize; i++)
            {
                var element = property.GetArrayElementAtIndex(i);
                int index = i;
                VisualElement container = AttackVariantEntryUXML.CloneTree();
                
                SliderInt intensitySliderInt = container.Q<SliderInt>("intensitySliderInt");
                intensitySliderInt.BindProperty(element.FindPropertyRelative("intensity"));
                intensitySliderInt.RegisterValueChangedCallback((evt) =>
                {
                    ApplyModifiedProperties();
                });
                SliderInt weightSliderInt = container.Q<SliderInt>("weightSliderInt");
                weightSliderInt.BindProperty(element.FindPropertyRelative("weight"));
                weightSliderInt.RegisterValueChangedCallback((evt) =>
                {
                    ApplyModifiedProperties();
                });
                ObjectField objectField = container.Q<ObjectField>();
                objectField.objectType = typeof(AnimatorOverrideController);
                objectField.BindProperty(element.FindPropertyRelative("controller"));
                objectField.RegisterValueChangedCallback((evt) =>
                {
                    ApplyModifiedProperties();
                    ShowOrHideSliders(evt.newValue==null, weightSliderInt, intensitySliderInt, objectField);
                });
                ShowOrHideSliders(attackVariants[i]==null, weightSliderInt, intensitySliderInt, objectField);
                Button button = container.Q<Button>();
                button.clicked += () =>
                {
                    property.DeleteArrayElementAtIndex(index);
                    ApplyModifiedProperties();
                    SetupAttackVariants();
                };
                foldout.Add(container);
            }

            Button addButton = new Button(() =>
            {
                property.InsertArrayElementAtIndex(property.arraySize);
                ApplyModifiedProperties();
                SetupAttackVariants();
            });
            addButton.text = "Add Attack Variant";
            foldout.Add(addButton);
        }

        private static void ShowOrHideSliders(bool noValidController, SliderInt weightSliderInt, SliderInt intensitySliderInt,
                                              ObjectField objectField)
        {
            if (noValidController)
            {
                weightSliderInt.Hide();
                intensitySliderInt.Hide();
                objectField.label = "Select Controller";
            }
            else
            {
                weightSliderInt.Show();
                intensitySliderInt.Show();
                objectField.label = "";
            }
        }

    #endif

    #endregion
    }
}