using UnityEditor;
using UnityEngine;

namespace TkrainDesigns.Combat
{
    [System.Serializable]
    public class AttackVariant
    {
        public RuntimeAnimatorController controller;
        public int intensity = 100;
        public int weight = 1;

        public float Intensity()
        {
            return intensity / 100.0f;
        }

    #if UNITY_EDITOR
        public bool DrawInspector(ScriptableObject owner)
        {
            EditorGUI.BeginChangeCheck();
            GUIContent label = new GUIContent(controller != null ? controller.name : "Select Override",
                "Animator Override that will be used for this attack variation");
            var tempController =
                (RuntimeAnimatorController)EditorGUILayout.ObjectField(label, controller,
                    typeof(RuntimeAnimatorController), false);
            label.text = "Intensity";
            label.tooltip = "Percentage of Base Damage this animation will impart.";
            int damage = EditorGUILayout.IntSlider(label, intensity, 50, 200);
            label.text = "Weight";
            label.tooltip =
                "Used to factor odds of this attack being used.  Each variant is given this number of tickets into a lottery style decision.";
            int tickets = EditorGUILayout.IntSlider(label, weight, 1, 100);
            if (EditorGUI.EndChangeCheck())
            {
                Undo.RecordObject(owner, "Change Attack Variant");
                controller = tempController;
                intensity = damage;
                weight = tickets;
                EditorUtility.SetDirty(owner);
            }

            bool result = GUILayout.Button("-");
            return result;
        }

    #endif
    }
}