using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Weapon : MonoBehaviour
{
    public UnityEvent onHitEvent;

    public void OnHit()
    {
        onHitEvent?.Invoke();
    }
}