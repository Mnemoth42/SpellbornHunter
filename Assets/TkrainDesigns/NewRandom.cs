using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace TkrainDesigns
{
    [System.Serializable]
    public class WeightedListElement<T>
    {
        public int probability = 1;
        public T element;
    }

    public static class RandomHelpers
    {
        /// <summary>
        /// Returns a random element from this list of items.
        /// </summary>
        /// <param name="list">This list</param>
        /// <typeparam name="T">The element of this list's type</typeparam>
        /// <returns>One random element, or default if list is empty</returns>
        public static T RandomElement<T>(this List<T> list, T fallback = default)
        {
            if (list.Count == 0) return fallback;
            return list[Random.Range(0, list.Count)];
        }

        /// <summary>
        /// Returns a random element from this array.
        /// </summary>
        /// <param name="array">this array</param>
        /// <typeparam name="T">An element type</typeparam>
        /// <returns></returns>
        public static T RandomElement<T>(this T[] array, T fallback = default)
        {
            return RandomElement(array.ToList(), fallback);
        }

        public static T RandomElement<T>(this IEnumerable<T> collection, T fallback = default)
        {
            return RandomElement(collection.ToList(), fallback);
        }

        public static T RandomWeightedElement<T>(this List<WeightedListElement<T>> list)
        {
            if (list.Count == 0) return default;
            int totalprobablity = list.Sum(element => element.probability);
            int selection = Random.Range(0, totalprobablity);
            foreach (var element in list)
            {
                selection -= element.probability;
                if (selection <= 0) return element.element;
            }

            return default;
        }

        public static T RandomWeightedElement<T>(this WeightedListElement<T>[] array)
        {
            return RandomWeightedElement(array.ToList());
        }

        public static T RandomWeightedElement<T>(this IEnumerable<WeightedListElement<T>> collection)
        {
            return RandomWeightedElement(collection.ToList());
        }

        public static GameObject RandomElement(this List<GameObject> list)
        {
            if (list.Count == 0) return null;
            return list[Random.Range(0, list.Count)];
        }

        /// <summary>
        /// Returns a float between min and max with the probability weighted towards the center of the spread.
        /// </summary>
        /// <param name="min"></param>
        /// <param name="max"></param>
        /// <returns></returns>
        public static float RangeWeighted(float min, float max)
        {
            if (min > max)
            {
                (min, max) = (max, min);
            }

            float center = (min + max) / 2.0f;
            AnimationCurve minCurve = AnimationCurve.EaseInOut(0, min, 1, center);
            AnimationCurve maxCurve = AnimationCurve.EaseInOut(0, center, 1, min);
            float random = Random.Range(0f, 1f);
            if (random < .50f)
            {
                return minCurve.Evaluate(random * 2.0f);
            }

            return max - maxCurve.Evaluate(random * 2.0f);
        }

        /// <summary>
        /// Returns an int between min and max with the probability weighted towards the center of the spread.
        /// </summary>
        /// <param name="min"></param>
        /// <param name="max"></param>
        /// <returns></returns>
        public static int RangeWeighted(int min, int max)
        {
            return (int)RangeWeighted((float)min, (float)max);
        }

        public static float RangeAround(float value, float range = .2f)
        {
            return RangeWeighted(value * (1 - range), value * (1 + range));
        }

        public static int RangeAround(int value, float range = .2f)
        {
            return (int)RangeAround((float)value, range);
        }
    }
}