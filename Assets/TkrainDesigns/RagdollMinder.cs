using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class RagdollMinder : MonoBehaviour
{
    private void Awake()
    {
        SetKinimatic();
    }

    private void SetKinimatic()
    {
        foreach (var rb in GetComponentsInChildren<Rigidbody>().Where(rb => rb.gameObject != gameObject))
        {
            rb.isKinematic = true;
        }
    }

    public void Ragdoll(Vector3 DamagePosition)
    {
        Destroy(GetComponent<Rigidbody>(), 1f);
        foreach (var rb in GetComponentsInChildren<Rigidbody>())
        {
            rb.isKinematic = false;
            rb.AddExplosionForce(500, (DamagePosition), 100);
        }

        Invoke(nameof(SetKinimatic), 2.0f);
    }
}