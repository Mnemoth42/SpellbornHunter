using System;
using UnityEngine;
using UnityEditor;

namespace TkrainDesigns
{
    public abstract class DrawableHelperClass
    {
        public abstract bool DrawInspector(ScriptableObject owner, out bool isDirty);
    #if UNITY_EDITOR
        protected void SetUndo(ScriptableObject owner, string caption)
        {
            Undo.RecordObject(owner, caption);
            EditorUtility.SetDirty(owner);
        }

        protected void DrawFloatAsIntSlider(ref float value, int min, int max, ScriptableObject owner,
                                            string caption = "Change Float")
        {
            float newValue = EditorGUILayout.IntSlider(value.ToInt(), min, max);
            if (newValue != value)
            {
                SetUndo(owner, caption);
                value = newValue;
            }
        }

        protected void DrawIntSlider(ref int value, int min, int max, ScriptableObject owner,
                                     string caption = "Change Int")
        {
            int newValue = EditorGUILayout.IntSlider(value, min, max);
            if (newValue != value)
            {
                SetUndo(owner, caption);
                value = newValue;
            }
        }

        protected void DrawEnum<T>(ref T value, ScriptableObject owner, string caption = "Change Enum") where T : Enum
        {
            T newValue = (T)EditorGUILayout.EnumPopup(value);
            SetUndo(owner, caption);
            value = newValue;
        }
    #endif
    }
}