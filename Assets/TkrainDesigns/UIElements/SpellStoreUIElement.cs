using System.Collections;
using System.Collections.Generic;
using GameDevTV.Inventories;
using TkrainDesigns.Abilities;
using UnityEngine;

public class SpellStoreUIElement :ActionItemUIElement
{
    public SpellStoreUIElement()
    {
        
    }

    
    public SpellStoreUIElement(SpellStore store, int id)
    {
        spellStore = store;
        slot = id;
    }

    private SpellStore spellStore;
    private int slot;
    
    public override int MaxAcceptable(InventoryItem otherItem)
    {
        if (otherItem is Ability actionItem)
        {
            return actionItem.IsStackable() || actionItem.isConsumable() ? 0 : 1;
        }
        return 0;
    }
}
