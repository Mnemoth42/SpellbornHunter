using System.Collections.Generic;
using GameDevTV.Inventories;
using TkrainDesigns.Abilities;
using TkrainDesigns.Abilities.Targeting;
using TkrainDesigns.UIToolkit;
using UnityEngine;
using UnityEngine.UIElements;

public class ActionItemUIElement : InventoryItemVisualElement
{
    public new class UxmlFactory : UxmlFactory<ActionItemUIElement, UxmlTraits>
    {
        
    }

    public new class UxmlTraits : VisualElement.UxmlTraits
    {
        private UxmlIntAttributeDescription m_slotKey = new UxmlIntAttributeDescription
                                                     {
                                                         name = "slot-key",
                                                         defaultValue = 0
                                                     };

        public override IEnumerable<UxmlChildElementDescription> uxmlChildElementsDescription {
            get
            {
                yield break;
            }    
         }


        public override void Init(VisualElement ve, IUxmlAttributes bag, CreationContext cc)
        {
            base.Init(ve, bag, cc);
            var ate = ve as ActionItemUIElement;
            ate.slotKey = m_slotKey.GetValueFromBag(bag, cc);
        }
    }
    
    
    private int _slotKey;

    private VisualElement SlotIdentifier;
    
    private VisualElement CountdownImage;
    private VisualElement Quantity;

    private ActionStore actionStore;
    private CooldownStore cooldownStore;
    private VisualElement Ghost;
    private VisualElement tooltipManager;
    
    
    public int slotKey
    {
        get => _slotKey;
        set
        {
            _slotKey = value;
            SlotIdentifier.Q<Label>().text = slotKey.ToString();
        }
    }

    public ActionItemUIElement()
    {
        CreateCircles();
    }

    public ActionItemUIElement(int slot, ActionStore actStore, CooldownStore coolStore)
    {
        actionStore = actStore;
        cooldownStore = coolStore;
        CreateCircles();
        slotKey = slot;
        SetupElement();
    }

    private void SetupElement()
    {
        item = actionStore.GetAction(slotKey);
        if (actionStore.GetNumber(slotKey) > 1)
        {
            Quantity.Show();
            Quantity.Q<Label>().text = actionStore.GetNumber(slotKey).ToString();
        }
        else
        {
            Quantity.Hide();
        }

        CountdownImage.Hide();
    }

    private void CreateCircles()
    {
        VisualTreeAsset asset = Resources.Load<VisualTreeAsset>("QuantityIndicatorUXML");
        SlotIdentifier = Circle("slotIdentity", "slotIdentityText");
        //Add(SlotIdentifier);
        Quantity = Circle("quantity", "quantityText",30);
        Quantity.Q<Label>().style.color = Color.white;
        Add(Quantity);
        CountdownImage = Circle("countdown", "countdownText", 80);
        Add(CountdownImage);
    }

    public override void RemoveItem()
    {
       actionStore.ClearItem(slotKey);
    }

    public override void AddItem(InventoryItem sourceItem, int amount)
    {
        if (sourceItem is ActionItem actionItem)
        {
            actionStore.AddAction(actionItem, slotKey, amount);
        }
    }

    public override void Refresh()
    {
        item = actionStore.GetAction(slotKey);
        if (actionStore.GetNumber(slotKey) > 1)
        {
            Quantity.Q<Label>().text = actionStore.GetNumber(slotKey).ToString();
            Quantity.Show();
        }
        else
        {
            Quantity.Hide();
        }
    }

    public override Sprite GetSprite() => actionStore.GetAction(slotKey) ? actionStore.GetAction(slotKey).GetIcon() : null;

    public override int QuantityAvailable()
    {
        return actionStore.GetNumber(slotKey);
    }

    public override int MaxAcceptable(InventoryItem otherItem)
    {
        if (otherItem is ActionItem actionItem)
        {
            return actionItem.IsStackable() ? RemainderAcceptable(otherItem) : 1;
        }
        return 0;
    }

    public void RefreshCooldownTimer()
    {
        if (!item)
        {
            CountdownImage.Hide();
            return;
        }

        if (item is Ability ability)
        {
            float time = cooldownStore.GetTimeRemaining(ability.GetCooldownToken());
            if (time > 0)
            {
                CountdownImage.Show();
                CountdownImage.Q<Label>().text = $"{time:F0}";
                return;
            }
        }
        CountdownImage.Hide();
    }
    
    int RemainderAcceptable(InventoryItem otherItem)
    {
        if (otherItem == item)
        {
            return 30 - actionStore.GetNumber(slotKey);
        }
        return 30;
    }
}
