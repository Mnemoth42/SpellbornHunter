using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;

public class CommandButtonUIUXML : UIElementsDriver
{
    private void Start()
    {
        VisualElement buttons = root.Q<VisualElement>("CommandButtons");
        Button InventoryButton = root.Q<Button>("Inventory");
        InventoryRuntimeUIElements inventory = FindObjectOfType<InventoryRuntimeUIElements>();
        InventoryButton.clicked += () =>
        {
            inventory.Toggle();
        };
        Button TraitButton = root.Q<Button>("Traits");
        TraitStoreElementUI traitStore = FindObjectOfType<TraitStoreElementUI>();
        TraitButton.clicked += () =>
        {
            traitStore.Toggle();
        };

    }
}
