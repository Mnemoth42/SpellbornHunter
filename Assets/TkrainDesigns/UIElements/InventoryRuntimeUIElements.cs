using System;
using System.Collections.Generic;
using System.Linq;
using GameDevTV.Inventories;
using TkrainDesigns.UIToolkit;
using UnityEditor.Rendering;
using UnityEngine;
using UnityEngine.UIElements;

[RequireComponent(typeof(AudioSource))]
public class InventoryRuntimeUIElements : UIElementsDriver, IContainDraggableUIElements<InventoryItemVisualElement, InventoryItem>
{
    
    private Inventory inventory;
    private Equipment equipment;
    private VisualElement InventoryContainer;
    private VisualElement Ghost;
    private VisualElement tooltip;
    private UIElementsTooltipManager tooltipManager;
    
    
    private List<InventoryItemVisualElement> elements = new List<InventoryItemVisualElement>();

    private void Awake()
    {
        InventoryContainer = root.Q<VisualElement>("GridContainer");
        inventory = GameObject.FindWithTag("Player").GetComponent<Inventory>();
        equipment = inventory.GetComponent<Equipment>();
        inventory.inventoryUpdated += RefreshInventory;
        equipment.equipmentUpdated += RefreshInventory;
        root.Hide();
        Ghost = root.Q<VisualElement>("GhostIcon");
        VisualElement InventoryWindow = root.Q<VisualElement>("InventoryWindow");
        InventoryWindow.RegisterCallback<PointerDownEvent>(PushWindowForward);
        VisualElement EquipmentWindow = root.Q<VisualElement>("EquipmentWindow");
        EquipmentWindow.RegisterCallback<PointerDownEvent>(PushWindowForward);
        tooltipManager = FindObjectOfType<UIElementsTooltipManager>();
    }

    private void SetupEquipment()
    {
        VisualElement LeftPane = root.Q<VisualElement>("LeftPane");
        LeftPane.Clear();
        AddEquipLocation(LeftPane, EquipLocation.Helmet);
        AddEquipLocation(LeftPane, EquipLocation.Cape);
        AddEquipLocation(LeftPane, EquipLocation.Body);
        AddEquipLocation(LeftPane, EquipLocation.Shield);
        AddEquipLocation(LeftPane, EquipLocation.Trousers);
        VisualElement RightPane = root.Q<VisualElement>("RightPane");
        RightPane.Clear();
        AddEquipLocation(RightPane, EquipLocation.Necklace);
        AddEquipLocation(RightPane, EquipLocation.Gloves);
        AddEquipLocation(RightPane, EquipLocation.Weapon);
        AddEquipLocation(RightPane, EquipLocation.Ring);
        AddEquipLocation(RightPane, EquipLocation.Boots);
    }

    void AddEquipLocation(VisualElement container, EquipLocation location)
    {
        var element = new EquipmentUIElement(equipment, location);
        element.SetGhost(Ghost);
        element.SetToolTipManager(tooltipManager);
        container.Add(element);
        elements.Add(element);
    }

    private void Start()
    {
        SetupInventory();
        SetupEquipment();
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.I))
        {
            Toggle();
        }
    }



    void SetupInventory()
    {
        InventoryContainer.Clear();
        elements.Clear();
        for (int i = 0; i < inventory.GetSize(); i++)
        {
            InventoryUIElement element = new InventoryUIElement(inventory, i);
            element.SetGhost(Ghost);
            element.SetToolTipManager(tooltipManager);
            InventoryContainer.Add(element);
            elements.Add(element);
        }
    }

    
    void RefreshInventory()
    {
         Debug.Log($"RefreshInventory");
        foreach (InventoryItemVisualElement inventoryItemVisualElement in elements)
        {
            inventoryItemVisualElement.Refresh();
        }
    }

    public IEnumerable<IDraggableUIElement<InventoryItemVisualElement, InventoryItem>> GetElements()
    {
        if(!(root.style.display ==DisplayStyle.None))
        {
            foreach (var element in elements)
            {
                yield return element;
            }
        }
        yield break;
    }
    
}
