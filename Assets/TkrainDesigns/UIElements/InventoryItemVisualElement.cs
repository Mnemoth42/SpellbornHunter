using GameDevTV.Inventories;
using TkrainDesigns.UIToolkit;
using UnityEngine;
using UnityEngine.UIElements;

public class InventoryItemVisualElement : VisualElement, IDraggableUIElement<InventoryItemVisualElement, InventoryItem>
{

    public new class UxmlFactory : UxmlFactory<InventoryItemVisualElement, UxmlTraits>
    {

    }

    protected int imageSize = 100;
    protected Image image;
    private InventoryItem currentItem;
    private int currentSlot;
    private Inventory inventory;
    private VisualElement Ghost;
    protected UIElementsTooltipManager tooltipManager;

    public InventoryItemVisualElement()
    {
        style.height = imageSize;
        style.width = imageSize;
        image = new Image();
        Add(image);
        AddToClassList("inventoryContainer");
        RegisterCallback<PointerDownEvent>(OnPointerDown);
        RegisterCallback<PointerMoveEvent>(OnPointerMove);
        RegisterCallback<PointerUpEvent>(OnPointerUp);
        RegisterCallback<PointerOverEvent>((evt) =>
        {
            ShowTooltip();
        });
        RegisterCallback<PointerOutEvent>((evt) =>
        {
            HideTooltip();
        });
    }

    public static VisualElement Circle(string circleSelector, string textSelector, float size = 20)
    {
        VisualElement result = new VisualElement();
        result.style.height = size;
        result.style.width = size;
        Label label = new Label();
        result.AddToClassList(circleSelector);
        label.AddToClassList(textSelector);
        label.RemoveFromClassList(".unity-label");
        label.RemoveFromClassList(".unity-text-element");
        label.text = "99";
        result.Add(label);
        return result;
    }

    public virtual void ShowTooltip()
    {
        if (item == null || tooltipManager == null) return;
        if (UIElementsDragHandler<InventoryItemVisualElement, InventoryItem>.IsDragging) return;
        tooltipManager.ShowTooltip(this, item.GetIcon(), item.GetDisplayName(), item.GetDescription());
    }

    public virtual void HideTooltip()
    {
        if (tooltipManager != null)
        {
            tooltipManager.HideTooltip();
        }
    }

    public void SetGhost(VisualElement element)
    {
       
        Ghost = element;
        //Debug.Log($"Ghost = {Ghost.name}");
    }

    public void SetToolTipManager(UIElementsTooltipManager manager)
    {
        tooltipManager = manager;
    }
    
    public int ImageSize
    {
        get => imageSize;
        set
        {
            imageSize = value;
            style.height = imageSize;
            style.width = imageSize;
        }
    }

    public Sprite background
    {
        get => style.backgroundImage.value.sprite;
        set
        {
            var setter = style.backgroundImage.value;
            setter.sprite = value;
            style.backgroundImage = setter;
        }
    }

    public Sprite sprite
    {
        get => image.sprite;
        set => image.sprite = value;
    }

    public InventoryItem item
    {
        get => currentItem;
        set
        {
            currentItem = value;
            sprite = currentItem ? currentItem.GetIcon() : GetDefaultIcon();
        }
    }

    protected virtual Sprite GetDefaultIcon()
    {
        return null;
    }
    
    public virtual void RemoveItem()
    {
        
    }

    public virtual void Refresh()
    {
        if (item) sprite = item.GetIcon();
    }
    


    public Rect GetBoundingRect()
    {
        return worldBound;
    }

    public virtual Sprite GetSprite()
    {
        return sprite;
    }

    public InventoryItem GetItem()
    {
        return item;
    }

    public virtual int MaxAcceptable(InventoryItem otherItem)
    {
        return 0;
    }

    public virtual int QuantityAvailable()
    {
        return 0;
    }

    public virtual void AddItem(InventoryItem sourceItem, int amount)
    {
    }

    public virtual void HandleDoubleClick()
    {
        
    }
    
    private bool IsDragging;
    private bool ReadyToDrag;

    private double lastClicked;
    
    void OnPointerDown(PointerDownEvent evt)
    {
        if (evt.button != 0 || item == null) return;
        ReadyToDrag = true;
        if (Time.timeAsDouble - lastClicked < 0.25)
        {
            HandleDoubleClick();
        }
        lastClicked = Time.timeAsDouble;
    }

    void OnPointerUp(PointerUpEvent evt)
    {
        IsDragging = false;
        ReadyToDrag = false;
    }
    
    private void OnPointerMove(PointerMoveEvent evt)
    {
        if(!IsDragging && ReadyToDrag)
        {
            BeginDrag(evt);
        }
    }

    private void BeginDrag(PointerMoveEvent evt)
    {
        sprite = null;
        HideTooltip();
        IsDragging = true;
        ReadyToDrag = false;
        UIElementsDragHandler<InventoryItemVisualElement, InventoryItem>.BeginDragging(this, Ghost, evt.position);
    }
}
