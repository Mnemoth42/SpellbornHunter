using System.Collections.Generic;
using TkrainDesigns.Stats;
using UnityEditor;
using UnityEditor.UIElements;
using UnityEngine.UIElements;

namespace TkrainDesigns.UIElements
{
    public class StatModifierElement : VisualElement
    {
        public new class UxmlFactory: UxmlFactory<StatModifierElement, UxmlTraits>{}

        public new class UxmlTraits : VisualElement.UxmlTraits
        {
            private UxmlEnumAttributeDescription<EStat> m_stat = new UxmlEnumAttributeDescription<EStat>
                                                                 {
                                                                     name = "stat", 
                                                                     defaultValue = EStat.Select
                                                                 };

            private UxmlFloatAttributeDescription m_value = new UxmlFloatAttributeDescription
                                                            {
                                                                name="value",
                                                                defaultValue = 1
                                                            };

            private UxmlStringAttributeDescription m_label = new UxmlStringAttributeDescription
                                                              {
                                                                  name = "modifier-label",
                                                                  defaultValue = ""
                                                              };
            public override IEnumerable<UxmlChildElementDescription> uxmlChildElementsDescription
            {
                get
                {
                    yield break;
                }
            }

            public override void Init(VisualElement ve, IUxmlAttributes bag, CreationContext cc)
            {
                base.Init(ve, bag, cc);
                var ate = ve as StatModifierElement;
                ate.stat = m_stat.GetValueFromBag(bag, cc);
                ate.value = m_value.GetValueFromBag(bag, cc);
                ate.modifierLabel = m_label.GetValueFromBag(bag, cc);
            }
            
        }
        
        public EStat stat { get; set; }
        public float value { get; set; }
        public string modifierLabel { get; set; }

        private EnumField enumField;
        
        private Slider slider;
        private Button button;
        
        public StatModifierElement()
        {
            style.flexDirection = FlexDirection.Row;
            enumField = new EnumField(stat);
            enumField.label = modifierLabel;
            enumField.style.flexGrow = 1;
            Add(enumField);
            slider = new Slider(-100, 100);
            slider.value = value;
            slider.label = "";
            slider.showInputField = true;
            StyleLength length = new StyleLength();
            length.value = Length.Percent(35);
            slider.style.flexBasis = length;
            Add(slider);
            button = new Button();
            button.text = "X";
            length.value = Length.Percent(15);
            button.style.flexBasis = length;
            Add(button);
        }

        public void RegisterOnClick(System.Action clicked)
        {
            button.clicked += clicked;
        }

        public void UnregisterOnClick(System.Action clicked)
        {
            button.clicked -= clicked;
        }

        public void RegisterValueChanged(System.Action clicked)
        {
            enumField.RegisterValueChangedCallback((evt) =>
            {
                clicked?.Invoke();
            });
            slider.RegisterValueChangedCallback((evt) =>
            {
                clicked?.Invoke();
            });
        }
        
        
        
        public void BindProperty(SerializedProperty property)
        {
            enumField.BindProperty(property.FindPropertyRelative("stat"));
            slider.BindProperty(property.FindPropertyRelative("amount"));
        }

       
    }
}
