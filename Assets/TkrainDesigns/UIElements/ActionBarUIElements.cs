using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using GameDevTV.Inventories;
using TkrainDesigns.Abilities;
using TkrainDesigns.Attributes;
using TkrainDesigns.Stats;
using TkrainDesigns.UIToolkit;
using UnityEditor.Animations;
using UnityEngine;
using UnityEngine.UIElements;

public class ActionBarUIElements : UIElementsDriver,  IContainDraggableUIElements<InventoryItemVisualElement, InventoryItem>
{
    [SerializeField] private StyleSheet ManabarUSS;

    [SerializeField] private StyleSheet ExperienceBarUSS;
    private List<ActionItemUIElement> elements = new List<ActionItemUIElement>();

    private ActionStore actionStore;
    private CooldownStore cooldownStore;
    private VisualElement Ghost;
    private UIElementsTooltipManager tooltipManager;

    private ProgressBar HealthBar;
    private ProgressBar ManaBar;
    private ProgressBar ExperienceBar;
    private Health health;
    private Mana mana;
    private BaseStats stats;
    private Experience experience;
    
    private void Awake()
    {
        elements = new List<ActionItemUIElement>();
        actionStore = GameObject.FindWithTag("Player").GetComponent<ActionStore>();
        cooldownStore = actionStore.GetComponent<CooldownStore>();
        Ghost = root.Q<VisualElement>("Ghost");
        tooltipManager = FindObjectOfType<UIElementsTooltipManager>();
        root.RegisterCallback<PointerDownEvent>(PushWindowForward);
        health = actionStore.GetComponent<Health>();
        mana = actionStore.GetComponent<Mana>();
        stats = actionStore.GetComponent<BaseStats>();
        experience = actionStore.GetComponent<Experience>();
        HealthBar = root.Q<ProgressBar>("HealthBar");
        ManaBar = root.Q<ProgressBar>("ManaBar");
        ExperienceBar = root.Q<ProgressBar>("ExperienceBar");

    }

    private void Start()
    {
        SetupElements();
        actionStore.storeUpdated += Refresh;
        Color back = Color.red;
        Color prog = Color.blue;
        System.Action updateMethod = UpdateHealthBar;
        ProgressBar bar = HealthBar;
        SetupProgressBar(HealthBar, Color.black, Color.blue);
        health.onValueChanged += UpdateHealthBar;
        SetupProgressBar(ManaBar, Color.black, Color.green);
        mana.onValueChanged += UpdateManabar;
        ManaBar.styleSheets.Add(ManabarUSS);
        SetupProgressBar(ExperienceBar, Color.black, Color.yellow);
        ExperienceBar.styleSheets.Add(ExperienceBarUSS);
        stats.EventLevelUp.AddListener(UpdateExperienceBar);
        experience.EventExperienceChanged.AddListener(UpdateExperienceBar);


        UpdateHealthBar();
        UpdateManabar();
        UpdateExperienceBar();
    }

    private void SetupProgressBar(ProgressBar bar, Color back, Color prog, Action updateMethod=null)
    {
        VisualElement unityprogressbar = bar.Q<VisualElement>("unity-progress-bar");
        VisualElement background = unityprogressbar.Q<VisualElement>();
        VisualElement progress = background.Q<VisualElement>();
        background.style.backgroundColor = back;
        progress.style.backgroundColor = prog;
        
    }

    private void UpdateHealthBar()
    {
        HealthBar.value = health.GetValueAsPercentage() * 100;
        HealthBar.title = $"Health: {health.GetCurrentValue()}/{health.GetMaxValue()}";
    }

    void UpdateManabar()
    {
        ManaBar.value = mana.GetValueAsPercentage() * 100;
        ManaBar.title = $"Mana: {mana.GetCurrentValue()}/{mana.GetMaxValue()}";
    }

    void UpdateExperienceBar()
    {
        ExperienceBar.value = stats.GetExperienceAsPercentage() * 100;
        ExperienceBar.title = $"Level {stats.GetLevel()}: {experience.GetExperience()}/{stats.GetStat(EStat.XPNeeded)}";
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Alpha1))
        {
            actionStore.Use(0, actionStore.gameObject);
        }

        if (Input.GetKeyDown(KeyCode.Alpha2))
        {
            actionStore.Use(1, actionStore.gameObject);
        }

        if (Input.GetKeyDown(KeyCode.Alpha3))
        {
            actionStore.Use(2, actionStore.gameObject);
        }
        if (Input.GetKeyDown(KeyCode.Alpha4))
        {
            actionStore.Use(3, actionStore.gameObject);
        }
        if (Input.GetKeyDown(KeyCode.Alpha5))
        {
            actionStore.Use(4, actionStore.gameObject);
        }
        if (Input.GetKeyDown(KeyCode.Alpha6))
        {
            actionStore.Use(5, actionStore.gameObject);
        }

        foreach (ActionItemUIElement element in elements)
        {
            element.RefreshCooldownTimer();
        }

    }

   
    void SetupElements()
    {
        VisualElement container = root.Q<VisualElement>("ActionBar");
        container.Clear();
        elements.Clear();
        for (int i = 0; i < 7; i++)
        {
            ActionItemUIElement element = new ActionItemUIElement(i,actionStore,cooldownStore);
            element.SetGhost(Ghost);
            element.SetToolTipManager(tooltipManager);
            container.Add(element);
            elements.Add(element);
        }
        
    }

    void Refresh()
    {
        foreach (var element in elements)
        {
            element.Refresh();
        }
    }
    
    public IEnumerable<IDraggableUIElement<InventoryItemVisualElement, InventoryItem>> GetElements()
    {
        return elements;
    }
}
