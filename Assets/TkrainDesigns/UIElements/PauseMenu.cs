using System.Collections;
using TkrainDesigns.SceneManagement;
using UnityEngine;
using UnityEngine.UIElements;

[RequireComponent(typeof(UIDocument))]
[RequireComponent(typeof(AudioSource))]
public class PauseMenu : UIElementsDriver
{
    [SerializeField] AudioClip ClickedOpenSound;
    [SerializeField] private AudioClip QuitGameSound;

    private AudioSource audioSource;
    
    private Button ResumeButton;
    private Button QuitButton;
    private void Awake()
    {
        audioSource = GetComponent<AudioSource>();
        
        ResumeButton = root.Q<Button>("Resume");
        Debug.Log($"{ResumeButton.text}");
        ResumeButton.clicked += ToggleDisplay;
        QuitButton = root.Q<Button>("Quit");
        QuitButton.clicked += () =>
        {
            audioSource.PlayOneShot(QuitGameSound);
            StartCoroutine(ReturnToMainMenu());
        };
        Hide();
    }

    IEnumerator ReturnToMainMenu()
    {
        DontDestroyOnLoad(gameObject);
        Time.timeScale = 1;
        Fader fader = FindObjectOfType<Fader>();
        yield return fader.FadeOut(.5f);
        yield return SavingWrapper.instance.LoadMainmenu();
        Destroy(gameObject);

    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            ToggleDisplay();
        }
    }

    private void ToggleDisplay()
    {
        audioSource.PlayOneShot(ClickedOpenSound);
        if (root.style.display == DisplayStyle.None)
        {
            Show();
            Time.timeScale = 0;
        }
        else
        {
            Hide();
            Time.timeScale = 1;
        }
    }
}
