using GameDevTV.Inventories;
using UnityEngine;

public class InventoryUIElement : InventoryItemVisualElement
{
    private Inventory inventory;
    private int slot;
    
    public InventoryUIElement(Inventory _inventory, int _slot)
    {
       // EssentialConstructor();
        inventory = _inventory;
        slot = _slot;
        item = inventory.GetItemInSlot(slot);
        
    }

    public override void Refresh()
    {
        if (!inventory) return;
        item = inventory.GetItemInSlot(slot);
    }

    public override Sprite GetSprite()
    {
        if (inventory.GetItemInSlot(slot) != null) return inventory.GetItemInSlot(slot).GetIcon();
        else return style.backgroundImage.value.sprite;
    }

    public override int MaxAcceptable(InventoryItem otherItem)
    {
        if (!inventory.HasSpaceFor(item)) return 0;
        if (otherItem == null) return 0;
        return otherItem.IsStackable() ? int.MaxValue:1;
    }

    public override void RemoveItem()
    {
        inventory.ClearSlot(slot);
    }

    public override void ShowTooltip()
    {
        if (!item) return;
        if (!inventory) return;
        if (item is EquipableItem equipableItem && inventory.TryGetComponent(out Equipment equipment))
        {
            EquipableItem equippedItem = equipment.GetItemInSlot(equipableItem.GetAllowedEquipLocation());
            if (equippedItem)
            {
                tooltipManager.ShowTooltip(this, item.GetIcon(), item.GetDisplayName(),item.GetDescription(), 
                    equippedItem.GetIcon(), equippedItem.GetDisplayName(), equippedItem.GetDescription());
                return;
            }
        }
        base.ShowTooltip();
    }

    public override void AddItem(InventoryItem sourceItem, int amount)
    {
        Debug.Log($"AddItem({sourceItem}/{amount})");
        if(sourceItem == null) inventory.ClearSlot(slot);
        if (amount < 0) amount = 0;
        inventory.AddItemToSlot(slot, sourceItem, amount);
    }

    public override int QuantityAvailable()
    {
        return inventory.GetNumberInSlot(slot);
    }
}
