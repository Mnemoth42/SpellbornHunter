using System.Collections.Generic;
using System.ComponentModel;
using TkrainDesigns.Stats;
using TkrainDesigns.UIToolkit;
using UnityEngine;
using UnityEngine.UIElements;

[RequireComponent(typeof(UIDocument))]
public class TraitStoreElementUI : UIElementsDriver
{

    [System.Serializable]
    class TraitDescription
    {
        public EStat stat;
        public Sprite icon;
        public string displayName;
        public string description;
    }

    [SerializeField] private List<TraitDescription> traits = new List<TraitDescription>();


    [SerializeField] private List<TraitDescription> stats = new List<TraitDescription>();

    [SerializeField] private VisualTreeAsset TraitsContainer;
    [SerializeField] private VisualTreeAsset StatContainer;

    private event System.Action onRedrawUI;
    

    private BaseStats baseStats;
    private TraitStore store;
    private UIElementsTooltipManager tooltipManager;

    private void Awake()
    {
        GameObject player = GameObject.FindWithTag("Player");
        baseStats = player.GetComponent<BaseStats>();
        store = player.GetComponent<TraitStore>();
        tooltipManager = FindObjectOfType<UIElementsTooltipManager>();
        SetupUI();
        store.onStatsChanged += RedrawUI;
        baseStats.onLevelUp += RedrawUI;
        root.Hide();
        VisualElement container = root.Q<VisualElement>("Container");
        container.RegisterCallback<PointerDownEvent>(PushWindowForward);
    }

    void RedrawUI()
    {
        onRedrawUI?.Invoke();
    }
    
    private void SetupUI()
    {
        SetupTraitUI();
        SetupStatsUI();
        SetupAddPointsContainer();
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.T))
        {
            ToggleDisplay();
        }
    }

    public void ToggleDisplay()
    {
        Toggle();
    }

    


    void SetupTraitUI()
    {
        VisualElement traitContainer = root.Q<VisualElement>("TraitContainer");
        traitContainer.Clear();
        foreach (TraitDescription traitDescription in traits)
        {
            EStat trait = traitDescription.stat;
            VisualElement element = TraitsContainer.CloneTree();
            Label KeyLabel = element.Q<Label>("KeyLabel");
            KeyLabel.text = traitDescription.displayName;
            Label ValueLabel = element.Q<Label>("ValueLabel");
            Button AddButton = element.Q<Button>("AddButton");
            Button SubtractButton = element.Q<Button>("SubtractButton");
            if (Application.isPlaying)
            {
                if(!store) Debug.LogError("NO STORE");
                ValueLabel.text = store.GetProposedTrait(trait).ToString();
                if (store.HasPotentialTrait(trait))
                {
                    ValueLabel.style.unityFontStyleAndWeight = FontStyle.BoldAndItalic;
                    KeyLabel.style.unityFontStyleAndWeight = FontStyle.BoldAndItalic;
                }
                AddButton.SetEnabled(store.CanAdd(trait));
                SubtractButton.SetEnabled(store.CanSubtract(trait));
                AddButton.clicked += () =>
                {
                    Debug.Log($"AddTrait");
                    store.AddPoint(trait);
                };
                SubtractButton.clicked += () =>
                {
                    store.SubtractPoint(trait);
                };
                if (tooltipManager)
                {
                    SetupTooltip(KeyLabel, traitDescription);
                }
            }

            onRedrawUI += () =>
            {
                AddButton.SetEnabled(store.CanAdd(trait));
                SubtractButton.SetEnabled(store.CanSubtract(trait));
                ValueLabel.text = store.GetProposedTrait(trait).ToString();
                if (store.HasPotentialTrait(trait))
                {
                    ValueLabel.style.unityFontStyleAndWeight = FontStyle.BoldAndItalic;
                    KeyLabel.style.unityFontStyleAndWeight = FontStyle.BoldAndItalic;
                }
                else
                {
                    ValueLabel.style.unityFontStyleAndWeight = FontStyle.Normal;
                    KeyLabel.style.unityFontStyleAndWeight = FontStyle.Normal;
                }
            };
            traitContainer.Add(element);
        }
    }

    private void SetupTooltip(Label KeyLabel, TraitDescription traitDescription)
    {
        Sprite icon = traitDescription.icon;
        string displayName = traitDescription.displayName;
        string description = traitDescription.description;
        KeyLabel.RegisterCallback<PointerOverEvent>((evt) =>
        {
            tooltipManager.ShowTooltip(KeyLabel, icon, displayName, description);
        });
        KeyLabel.RegisterCallback<PointerOutEvent>((evt) => { tooltipManager.HideTooltip(); });
    }

    void SetupStatsUI()
    {
        VisualElement visualElement = root.Q<VisualElement>("StatContainer");
        visualElement.Clear();
        foreach (TraitDescription traitDescription in stats)
        {
            EStat stat = traitDescription.stat;
            VisualElement element = StatContainer.CloneTree();
            Label KeyLabel = element.Q<Label>("KeyLabel");
            KeyLabel.text = traitDescription.displayName;
            Label ValueLabel = element.Q<Label>("ValueLabel");
            if (Application.isPlaying)
            {
                ValueLabel.text = $"{baseStats.GetStat(stat):F1}";
                if (store.GetProposedModifierEffect(stat) > 0)
                {
                    ValueLabel.style.unityFontStyleAndWeight = FontStyle.BoldAndItalic;
                    KeyLabel.style.unityFontStyleAndWeight = FontStyle.BoldAndItalic;
                }
                if (tooltipManager)
                {
                    SetupTooltip(KeyLabel, traitDescription);
                }
            }

            onRedrawUI += () =>
            {
                ValueLabel.text = $"{baseStats.GetStat(stat)+store.GetProposedModifierEffect(stat):F1}";
                if (store.GetProposedModifierEffect(stat) > 0)
                {
                    ValueLabel.style.unityFontStyleAndWeight = FontStyle.BoldAndItalic;
                    KeyLabel.style.unityFontStyleAndWeight = FontStyle.BoldAndItalic;
                }
                else
                {
                    ValueLabel.style.unityFontStyleAndWeight = FontStyle.Normal;
                    KeyLabel.style.unityFontStyleAndWeight = FontStyle.Normal;
                }
            };
            visualElement.Add(element);
        }
    }

    void SetupAddPointsContainer()
    {
        VisualElement AddPointsContainer = root.Q<VisualElement>("AddPointsContainer");

        AddPointsContainer.Show();
        Label PointsAvailableValueLabel = AddPointsContainer.Q<Label>("PointsAvaliableValueLabel");
        PointsAvailableValueLabel.text = store.AvailablePoints().ToString();
        Button ConfirmButton = AddPointsContainer.Q<Button>("ConfirmButton");
        ConfirmButton.SetEnabled(store.CanConfirm());
        ConfirmButton.clicked += () =>
        {
            store.CommitPoints();
        };
        Button CancelButton = AddPointsContainer.Q<Button>("CancelButton");
        CancelButton.SetEnabled(store.CanConfirm());
        CancelButton.clicked += () =>
        {
            store.ClearPoints();
        };
        onRedrawUI += () =>
        {
            ConfirmButton.SetEnabled(store.CanConfirm());
            CancelButton.SetEnabled(store.CanConfirm());
            PointsAvailableValueLabel.text = store.AvailablePoints().ToString();
        };

    }
    
}
