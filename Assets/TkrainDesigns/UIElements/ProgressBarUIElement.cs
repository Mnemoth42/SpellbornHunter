using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.UIElements;

namespace TkrainDesigns.UIElements
{
    public class ProgressBarUIElement : VisualElement
    {
        public new class UxmlFactory : UxmlFactory<ProgressBarUIElement, UxmlTraits>
        {
            
        }

        public new class UxmlTraits : VisualElement.UxmlTraits
        {
            private UxmlFloatAttributeDescription m_Min = new UxmlFloatAttributeDescription
                                                          {
                                                              name = "min",
                                                              defaultValue = 0,
                                                          };
            private UxmlFloatAttributeDescription m_Max = new UxmlFloatAttributeDescription
                                                          {
                                                              name = "max",
                                                              defaultValue = 100
                                                          };

            private UxmlColorAttributeDescription m_Back = new UxmlColorAttributeDescription
                                                           {
                                                               name = "back",
                                                               defaultValue = Color.black
                                                           };

            private UxmlColorAttributeDescription m_Fore = new UxmlColorAttributeDescription
                                                           {
                                                               name = "fore",
                                                               defaultValue = Color.blue
                                                           };

            private UxmlStringAttributeDescription m_Title = new UxmlStringAttributeDescription
                                                             {
                                                                 name = "title",
                                                                 defaultValue = "0/100"
                                                             };

            private UxmlFloatAttributeDescription m_CurrentValue = new UxmlFloatAttributeDescription
                                                                   {
                                                                       name = "current-value",
                                                                       defaultValue = 40
                                                                   };
            
            
            public override IEnumerable<UxmlChildElementDescription> uxmlChildElementsDescription
            {
                get { yield break; }
            }
            
            

            public override void Init(VisualElement ve, IUxmlAttributes bag, CreationContext cc)
            {
                base.Init(ve, bag, cc);
                var ate = ve as ProgressBarUIElement;

                ate.min = m_Min.GetValueFromBag(bag, cc);
                ate.max = m_Max.GetValueFromBag(bag, cc);
                ate.back = m_Back.GetValueFromBag(bag, cc);
                ate.fore = m_Back.GetValueFromBag(bag, cc);
                ate.title = m_Title.GetValueFromBag(bag, cc);
                ate.currentValue = m_CurrentValue.GetValueFromBag(bag, cc);
            }
        }


        private float _min;
        private float _max;
        private float _currentValue;
        public float min
        {
            get => _min;
            set
            {
                _min = value;
            }
        }

        public float max
        {
            get => _max;
            set
            {
                _max = value;
                currentValue = _currentValue;
            }
        }

        public float currentValue
        {
            get => _currentValue;
            set
            {
                _currentValue = value;
                float maxWidth = Background.style.width.value.value;
                float foreWidth = _currentValue / _max;
                Progress.style.scale = new StyleScale(new Scale(new Vector3(foreWidth, 1,1)));
                title = _title;
            }
        }
        
        public Color back
        {
            get
            {
                return Background.style.backgroundColor.value;
            }
            set
            {
                Background.style.backgroundColor = value;
            }
        }

        public Color fore
        {
            get
            {
                return Progress.style.backgroundColor.value;
            }
            set
            {
                Progress.style.backgroundColor = value;
            }
        }

        private string _title;
        public string title
        {
            get => _title;
            set
            {
                _title = value;
                Title.text = $"{_title} {_currentValue}/{_max}";
            }
        }

        private VisualElement Background;
        private VisualElement Progress;
        private VisualElement TitleContainer;
        private Label Title;
        
        public ProgressBarUIElement()
        {
            VisualTreeAsset tree = Resources.Load<VisualTreeAsset>("ProgressBarUXML");
            VisualElement root = tree.CloneTree();
            Add(root);
            Background = root.Q<VisualElement>("Background");
            Progress = root.Q<VisualElement>("Progress");
            Title = root.Q<Label>("Title");
        }
    }
}