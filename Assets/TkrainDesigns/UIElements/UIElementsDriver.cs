using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;

[RequireComponent(typeof(UIDocument))]
[RequireComponent(typeof(PanelEventHandler))]
[RequireComponent(typeof(PanelRaycaster))]
public abstract class UIElementsDriver : MonoBehaviour
{
    [SerializeField] private AudioClip onOpenSound = null;
    [SerializeField] private AudioClip onCloseSound = null;
    protected UIDocument Document => GetComponent<UIDocument>();
    protected VisualElement root => Document.rootVisualElement;
    protected static int SortingOrder = 1;

    public void Show()
    {
        SortingOrder++;
        Document.sortingOrder = SortingOrder;
        root.Show();
        if (onOpenSound)
        {
            GetComponent<AudioSource>().PlayOneShot(onOpenSound);
        }
    }

    public void Hide()
    {
        root.Hide();
        if (onCloseSound)
        {
            GetComponent<AudioSource>().PlayOneShot(onCloseSound);
        }
    }

    protected void PushWindowForward(PointerDownEvent evt)
    {
        SortingOrder++;
        Document.sortingOrder = SortingOrder;
    }
    
    public virtual void Toggle()
    {
        if (root.style.display == DisplayStyle.None)
        {
            Show();
        }
        else
        {
            Hide();
        }
    }
    
    public bool isHidden => root.style.display == DisplayStyle.None;

}
