using System.Collections.Generic;
using GameDevTV.Inventories;
using TkrainDesigns.Shops;
using TkrainDesigns.UIToolkit;
using UnityEngine;
using UnityEngine.UIElements;

public class ShopUIUXML : UIElementsDriver
{

    [SerializeField] private VisualTreeAsset shopLineUXML;
   
   

    private Shopper shopper;
    private Shop currentShop;
    private Purse purse;
    private Equipment equipment;

    private Button SellingButton;
    private Button CancelButton;
    private Button ConfirmButton;
    private Label TotalTransaction;
    private Label PurseLabel;
    private VisualElement TooltipContainer;
    private Image TooltipImage;
    private Image EquippedItemImage;
    private UIElementsTooltipManager tooltipManager;
    
    
    private List<Button> categoryButtons = new List<Button>();

    private void Awake()
    {
        shopper = GameObject.FindWithTag("Player").GetComponent<Shopper>();
        PurseLabel = root.Q<Label>("PurseLabel");
        purse = shopper.GetComponent<Purse>();
        purse.onCurrencyChanged += UpdatePurse;
        equipment = shopper.GetComponent<Equipment>();
        SetupFilterButton("AllButton", ItemCategory.None);
        SetupFilterButton("GearButton", ItemCategory.Equipment);
        SetupFilterButton("MaterialsButton", ItemCategory.Material);
        SetupFilterButton("PotionsButton", ItemCategory.Potion);
        SetupFilterButton("AbilitiesButton", ItemCategory.Action);
        SetupFilterButton("SpecialButton", ItemCategory.Special);
        SellingButton = root.Q<Button>("SellingButton");
        SellingButton.clicked += () =>
        {
            if (currentShop == null) return;
            currentShop.ChangeTransactionType();
            SellingButton.text = currentShop.Selling() ? "Switch To Buying" : "Switch to Selling";
        };
        CancelButton = root.Q<Button>("CancelButton");
        CancelButton.clicked += () =>
        {
            if (currentShop == null) return;
            shopper.Cancel();
            Hide();
        };
        ConfirmButton = root.Q<Button>("ConfirmButton");
        ConfirmButton.clicked += () =>
        {
            currentShop.ConfirmTransaction();
        };
        TotalTransaction = root.Q<Label>("TotalTransaction");
        shopper.activeShopChanged += SetupShop;
        // TooltipContainer = root.Q<VisualElement>("TooltipContainer");
        // TooltipImage = new Image();
        // VisualElement TooltipIcon = TooltipContainer.Q <VisualElement> ("TooltipIcon");
        // TooltipIcon.Add(TooltipImage);
        // EquippedItemImage = new Image();
        // TooltipContainer.Q<VisualElement>("EquippedIcon").Add(EquippedItemImage);
        // TooltipContainer.Hide();
        tooltipManager = FindObjectOfType<UIElementsTooltipManager>();
        Hide();
    }

    private void OnEnable()
    {
        UpdatePurse();
        SetupShop();
    }

    void UpdatePurse()
    {
        PurseLabel.text = purse.GetBalance(CurrencyTypes.Copper).ToString();
    }
    
    void SetupFilterButton(string buttonName, ItemCategory category)
    {
        Button button = root.Q<Button>(buttonName);
        if (button==null) return;
        button.clicked += () =>
        {
            Filter(button, category);
        };
        categoryButtons.Add(button);
    }
    
    void Filter(Button button, ItemCategory category)
    {
        if (!currentShop) return;
        foreach (Button categoryButton in categoryButtons)
        {
            categoryButton.SetEnabled(categoryButton!=button);
        }
        currentShop.SetFilter(category);
    }
    
    void SetupShop()
    {
        if (currentShop)
        {
            currentShop.onChange -= UpdateUI;
        }
        currentShop = shopper.GetActiveShop();
        if (!currentShop)
        {
            Hide();
        }
        else
        {
            Show();
            root.Q<Button>("Title").text = currentShop.ShopName;
            currentShop.onChange += UpdateUI;
            UpdateUI();
        }
    }

    private void UpdateUI()
    {
        ScrollView scrollView = root.Q<ScrollView>("ShopItemsScrollView");
        scrollView.Clear();
        foreach (ShopItem shopItem in currentShop.GetFilteredItems())
        {
            VisualElement element = shopLineUXML.CloneTree();
            VisualElement itemImage = element.Q<VisualElement>("ItemImage");
            Image image = new Image
                          {
                              sprite = shopItem.GetItem().GetIcon()
                          };
            itemImage.Add(image);
            Label KeyLabel = element.Q<Label>("KeyLabel");
            KeyLabel.text = shopItem.GetDisplayName();
            Label ValueLabel = element.Q<Label>("ValueLabel");
            ValueLabel.text = $"{shopItem.GetPrice()}";
            Label AvailableLabel = element.Q<Label>("AvailableLabel");
            AvailableLabel.text = $"{shopItem.GetAvailablity()}";
            Label TransactionQty = element.Q<Label>("TransactionQty");
            TransactionQty.text = $"{shopItem.GetQuantityInTransaction()}";
            Button AddButton = element.Q<Button>("AddButton");
            AddButton.clicked += () => { currentShop.AddToTransaction(shopItem.GetItem(), 1); };
            AddButton.SetEnabled(shopItem.GetQuantityInTransaction() < shopItem.GetAvailablity() &&
                                 currentShop.CanPurchase(shopItem.GetItem()));
            Button SubtractButton = element.Q<Button>("SubtractButton");
            SubtractButton.clicked += () => { currentShop.AddToTransaction(shopItem.GetItem(), -1); };
            SubtractButton.SetEnabled(shopItem.GetQuantityInTransaction() > 0);
            itemImage.RegisterCallback<MouseEnterEvent>((evt) =>
            {
                ShowTooltip(itemImage, shopItem);
            });
            itemImage.RegisterCallback<MouseLeaveEvent>((evt)=>
            {
                tooltipManager.HideTooltip();
            });
            scrollView.Add(element);
        }

        TotalTransaction.text = $"Total={currentShop.TransactionTotal():F0}";
        ConfirmButton.SetEnabled(currentShop.CanTransact());
    }

    private void ShowTooltip(VisualElement container, ShopItem shopItem)
    {
        // container.Show();
        // TooltipImage.sprite = shopItem.GetItem().GetIcon();
        //
        // Label TooltipText = container.Q<Label>("TooltipText");
        // TooltipText.text = shopItem.GetItem().GetDescription();
        // Label TooltipLabel = container.Q<Label>("TooltipLabel");
        // TooltipLabel.text = shopItem.GetItem().GetDisplayName();
        //
        // VisualElement EquippedItemContainer = container.Q<VisualElement>("EquippedItemContainer");
        // if (shopItem.GetItem().GetCategory() != ItemCategory.Equipment ||
        //     !(shopItem.GetItem() is EquipableItem equipableItem) ||
        //     equipment.GetItemInSlot(equipableItem.GetAllowedEquipLocation()) == null)
        // {
        //     EquippedItemContainer.Hide();
        // }
        // else
        // {
        //     if (shopItem.GetItem() is EquipableItem item)
        //     {
        //         EquippedItemContainer.Show();
        //         EquipableItem equippedItem = equipment.GetItemInSlot(item.GetAllowedEquipLocation());
        //         EquippedItemImage.sprite = equippedItem.GetIcon();
        //         Label EquippedItemName = container.Q<Label>("EquippedItemName");
        //         EquippedItemName.text = equippedItem.GetDisplayName();
        //         Label EquippedTooltipText = container.Q<Label>("EquippedTooltipText");
        //         EquippedTooltipText.text = equippedItem.GetDescription();
        //     }
        // }

        if (shopItem == null) return;
        InventoryItem item = shopItem.GetItem();
        if (shopItem.GetItem() is EquipableItem equipableItem)
        {
            EquipableItem equippedItem = equipment.GetItemInSlot(equipableItem.GetAllowedEquipLocation());
            if (equippedItem)
            {
                tooltipManager.ShowTooltip(container, item.GetIcon(), item.GetDisplayName(),item.GetDescription(), 
                    equippedItem.GetIcon(), equippedItem.GetDisplayName(), equippedItem.GetDescription());
                return;
            }
        }
        tooltipManager.ShowTooltip(container, item.GetIcon(), item.GetDisplayName(), item.GetDescription());
    }
}
