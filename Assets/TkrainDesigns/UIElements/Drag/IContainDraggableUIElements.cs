using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;

public interface IContainDraggableUIElements<T, U> where T:VisualElement
{
    IEnumerable<IDraggableUIElement<T,U>> GetElements();
}

public interface IDraggableUIElement<T, U> where T : VisualElement
{
    Rect GetBoundingRect();
    Sprite GetSprite();
    U GetItem();
    
    int MaxAcceptable(U proposedItem);
    int QuantityAvailable();
    void AddItem(U sourceItem, int amount);
    void RemoveItem();
    void Refresh();
} 