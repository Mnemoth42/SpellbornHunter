using System.Collections;
using System.Collections.Generic;
using System.Linq;

using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UIElements;

public static class UIElementsDragHandler<T,U> where T:VisualElement
{
    private static VisualElement Ghost;
    private static IDraggableUIElement<T,U> source;
    public static bool IsDragging { get; private set; } 

    public static void BeginDragging(IDraggableUIElement<T,U> callingContainer, VisualElement ghost, Vector2 position)
    {
        Debug.Log($"UIElementsDragHandler BeginDragging.  Ghost = {ghost}, position = {position}");
        Ghost = ghost;
        Ghost.style.backgroundImage = Background.FromSprite(callingContainer.GetSprite());
        source = callingContainer;
        Ghost.visible = true;
        Ghost.style.top = position.y - Ghost.layout.height / 2f;
        Ghost.style.left = position.x - Ghost.layout.width / 2f;
        IsDragging = true;
        Ghost.RegisterCallback<PointerMoveEvent>(OnPointerMove);
        Ghost.RegisterCallback<PointerUpEvent>(OnPointerUp);
        Ghost.RegisterCallback<PointerLeaveEvent>(OnPointerLeave);
    }

    private static void OnPointerLeave(PointerLeaveEvent evt)
    {
        if (!IsDragging) return;
        Debug.Log($"Escaped bounding box, recentering to {evt.position.x}, {evt.position.y}");
        Ghost.style.top = evt.position.y - Ghost.layout.height / 2;
        Ghost.style.left = evt.position.x - Ghost.layout.width / 2;
    }

    static void OnPointerMove(PointerMoveEvent evt)
    {
        Debug.Log("PointermoveEvent");
        if (!IsDragging) return;
       // Debug.Log($"Moving {evt.position.x}, {evt.position.y}");
        Ghost.style.top = evt.position.y - Ghost.layout.height / 2;
        Ghost.style.left = evt.position.x - Ghost.layout.width / 2;
    } 
    
    static void OnPointerUp(PointerUpEvent evt)
    {
        if (!IsDragging || evt.button!=0) return;

        List<IDraggableUIElement<T,U>> elements = new List<IDraggableUIElement<T,U>>();
        foreach (IContainDraggableUIElements<T,U> container in Find<IContainDraggableUIElements<T,U>>())
        {
            Debug.Log($"Container {container}");
            foreach (IDraggableUIElement<T,U> draggableUIElement in container.GetElements())
            {
                elements.Add(draggableUIElement);
            }
        }

        IDraggableUIElement<T, U> destination =
            elements.Where(e => Ghost.worldBound.Overlaps(e.GetBoundingRect())).ToList()
                    .OrderBy(d => Vector2.Distance(d.GetBoundingRect().center, evt.position)).FirstOrDefault();
        Debug.Log($"OnPointerDown, destination = {destination}");
        Ghost.visible = false;
        IsDragging = false;
        Ghost.UnregisterCallback<PointerLeaveEvent>(OnPointerLeave);
        Ghost.UnregisterCallback<PointerMoveEvent>(OnPointerMove);
        Ghost.UnregisterCallback<PointerUpEvent>(OnPointerUp);
        if (!source.Equals(destination))
        {
            if (destination!=null)
            {
                U sourceItem = source.GetItem();
                U destinationItem = destination.GetItem();
                int sourceMaxAcceptable = source.MaxAcceptable(destinationItem);
                int destinationMaxAcceptable = destination.MaxAcceptable(sourceItem);
                int sourceAvailable = source.QuantityAvailable();
                int destinationAvailable = destination.QuantityAvailable();
                Debug.Log($"Source = {sourceItem}, qty = {sourceAvailable}, Max = {sourceMaxAcceptable}");
                Debug.Log($"Destination = {destinationItem}, qty = {destinationAvailable}, Max = {destinationMaxAcceptable}");
                if (destinationMaxAcceptable>0)
                {
                    if (!sourceItem.Equals(destinationItem) && destinationMaxAcceptable>1)
                    {
                        //Stacking items that are the same
                        Debug.Log($"Stackable items detected.  ");
                        int amountToShare = Mathf.Min(sourceAvailable, destinationMaxAcceptable);
                        int remainder = sourceAvailable - amountToShare;
                        destination.AddItem(sourceItem, amountToShare);
                        source.RemoveItem();
                        if (remainder > 0) source.AddItem(sourceItem, amountToShare);
                    }
                    else if(sourceMaxAcceptable>=destinationAvailable && destinationMaxAcceptable>=sourceAvailable)
                    {
                        Debug.Log("Both elements can be successfully swapped.");
                        source.RemoveItem();
                        destination.RemoveItem();
                        source.AddItem(destinationItem, destinationAvailable);
                        destination.AddItem(sourceItem, sourceAvailable);
                        source.Refresh();
                        destination.Refresh();
                    }
                    else
                    {
                        Debug.Log("Corner Case");
                    }
                }
                else
                {
                    Debug.Log($"No swap feasable, restoring source");
                    source.Refresh();
                    destination.Refresh();
                }
            
            }
            else
            {
                Debug.Log($"No Destination selected, restoring source");
                source.Refresh();
            }
        }
        source.Refresh();
        
    }

    public static List<W> Find<W>(  )
    {
        List<W> interfaces = new List<W>();
        GameObject[] rootGameObjects = SceneManager.GetActiveScene().GetRootGameObjects();
        foreach( var rootGameObject in rootGameObjects )
        {
            W[] childrenInterfaces = rootGameObject.GetComponentsInChildren<W>();
            foreach( var childInterface in childrenInterfaces )
            {
                interfaces.Add(childInterface);
            }
        }
        return interfaces;
    }

    
}
