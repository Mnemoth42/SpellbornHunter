using System.Collections.Generic;
using GameDevTV.Inventories;
using UnityEngine;
using UnityEngine.UIElements;

public class EquipmentUIElement : InventoryItemVisualElement
{
    public new class UxmlFactory : UxmlFactory<EquipmentUIElement, UxmlTraits> {}

    public new class UxmlTraits : VisualElement.UxmlTraits
    {
        private UxmlEnumAttributeDescription<EquipLocation> m_EquipLocation =
            new UxmlEnumAttributeDescription<EquipLocation>
            {
                name = "equip-location",
                defaultValue = EquipLocation.Body
            };

        public override IEnumerable<UxmlChildElementDescription> uxmlChildElementsDescription
        {
            get { yield break; }
        }

        public override void Init(VisualElement ve, IUxmlAttributes bag, CreationContext cc)
        {
            base.Init(ve, bag, cc);
            var ate = ve as EquipmentUIElement;
            ate.equipLocation = m_EquipLocation.GetValueFromBag(bag, cc);
            //Debug.Log($"Init - EquipLocation = {ate.equipLocation}");
        }
    }

    
    private Equipment equipment;
    private EquipLocation  m_equipLocation;

    public EquipLocation equipLocation
    {
        get => m_equipLocation;
        set
        {
            m_equipLocation = value;
            if (!item)
            {
                sprite = GetDefaultIcon();
            }
        }
    }

    protected override Sprite GetDefaultIcon()
    {
        string iconToLoad = "18_t";
        switch (m_equipLocation)
        {
            case EquipLocation.Body:
                iconToLoad = "bodySlot";
                break;
            case EquipLocation.Boots:
                iconToLoad = "bootSlot";
                break;
            case EquipLocation.Cape:
                iconToLoad = "capeSlot";
                break;
            case EquipLocation.Gloves:
                iconToLoad = "gloveSlot";
                break;
            case EquipLocation.Helmet:
                iconToLoad = "helmetSlot";
                break;
            case EquipLocation.Ring:
                iconToLoad = "ringSlot";
                break;
            case EquipLocation.Trousers:
                iconToLoad = "pantSlot";
                break;
            case EquipLocation.Weapon:
                iconToLoad = "weaponSlot";
                break;
            case EquipLocation.Necklace:
                iconToLoad = "neckSlot";
                break;
            case EquipLocation.Shield:
                iconToLoad = "shieldSlot";
                break;
        }

        return Resources.Load<Sprite>(iconToLoad);
    }

    public EquipmentUIElement()
    {
        
    }
    
    public EquipmentUIElement(Equipment playerEquipment, EquipLocation location)
    {
        equipment = playerEquipment;
        equipLocation = location;
        item = equipment.GetItemInSlot(equipLocation);
    }

    public override int MaxAcceptable(InventoryItem otherItem)
    {
        if (otherItem is EquipableItem equipableItem && equipableItem.GetAllowedEquipLocation() == equipLocation  && equipment.CanEquip(equipableItem))
        {
            return 1;
        }
        return 0;
    }

    public override int QuantityAvailable()
    {
        return equipment.GetItemInSlot(equipLocation) != null ? 1 : 0;
    }

    public override void RemoveItem()
    {
        equipment.RemoveItem(equipLocation);
    }

    public override void AddItem(InventoryItem sourceItem, int amount)
    {
        if (sourceItem is EquipableItem equipableItem)
        {
            equipment.AddItem(equipLocation, equipableItem);
        }
    }

    public override Sprite GetSprite()
    {
        return equipment.GetItemInSlot(equipLocation) ? equipment.GetItemInSlot(equipLocation).GetIcon() : null;
    }

    public override void Refresh()
    {
        item = equipment.GetItemInSlot(equipLocation);
        if (!item)
        {
            sprite = GetDefaultIcon();
        }
    }
}
