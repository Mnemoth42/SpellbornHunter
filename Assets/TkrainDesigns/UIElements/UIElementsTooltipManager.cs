using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;

namespace TkrainDesigns.UIToolkit
{
    [RequireComponent(typeof(UIDocument))]
    public class UIElementsTooltipManager :UIElementsDriver
    {
        [SerializeField] private float containerWidth = 325;
        private VisualElement tooltip;
        private VisualElement equippedItemTooltip;
        private Image iconImage;
        private Image equippedIconImage;
        private Label displayNameTextField;
        private Label equippedDisplayNameTextField;
        private Label descriptionTextField;
        private Label equippedDescriptionTextField;
        
        

        private void Awake()
        {
            tooltip = root.Q<VisualElement>("TooltipContainer");
            
            equippedItemTooltip = root.Q<VisualElement>("EquippedItemContainer");
            VisualElement iconElement = root.Q<VisualElement>("TooltipIcon");
            iconImage = new Image();
            iconElement.Add(iconImage);
            VisualElement equippedIcon = root.Q<VisualElement>("EquippedIcon");
            equippedIconImage = new Image();
            equippedIcon.Add(equippedIconImage);
            displayNameTextField = root.Q<Label>("TooltipLabel");
            equippedDisplayNameTextField = root.Q<Label>("EquippedItemName");
            descriptionTextField = root.Q<Label>("TooltipText");
            equippedDescriptionTextField = root.Q<Label>("EquippedTooltipText");
            Hide();
        }

        public void ShowTooltip(VisualElement anchor, Sprite icon, string displayName, string description)
        {
            //Debug.Log($"ShowTooltip!");
            PopulateTooltip(icon, displayName, description);
            equippedItemTooltip.Hide();
            PositionTooltip(anchor);
        }

        public void ShowTooltip(VisualElement anchor, Sprite icon, string displayName, string description,
                                Sprite equippedIcon, string equippedDisplayName, string equippedDescription)
        {
            PopulateTooltip(icon, displayName, description);
            equippedIconImage.sprite = equippedIcon;
            equippedDisplayNameTextField.text = equippedDisplayName;
            equippedDescriptionTextField.text = equippedDescription;
            equippedItemTooltip.Show();
            PositionTooltip(anchor);
        }

        public void HideTooltip()
        {
            Hide();
        }

        private void PositionTooltip(VisualElement anchor)
        {
            Show();
            Rect area = Screen.safeArea;

            float left = anchor.worldBound.x - containerWidth;
            if (left < 0) left = anchor.worldBound.x + anchor.worldBound.width*1.25f;
            float top = anchor.worldBound.y;
            Debug.Log($"Anchor = {anchor.worldBound.x},{anchor.worldBound.y}. tooltip location = {left}, {top}");
            tooltip.style.left = left;
            tooltip.style.top = top;
            
            
            if (tooltip.worldBound.x < 0)
            {
                tooltip.transform.position = new Vector2(anchor.worldBound.xMax, tooltip.transform.position.y);
            }

            if (tooltip.worldBound.height > area.height)
            {
                tooltip.transform.position = new Vector2(tooltip.transform.position.x, 0);
            }
            
        }

        private void PopulateTooltip(Sprite icon, string displayName, string description)
        {

            iconImage.sprite = icon;
            displayNameTextField.text = displayName;
            descriptionTextField.text = description;
        }
    }
}