﻿using System.Collections.Generic;
using UnityEngine;

#pragma warning disable CS0649
namespace TkrainDesigns.ScriptableObjects
{
    [CreateAssetMenu(fileName = "AudioArray", menuName = "Shared/AudioArray")]
    public class ScriptableAudioArray : ScriptableObject
    {
        [SerializeField] List<AudioClip> clips = new List<AudioClip>();

        public int Count
        {
            get { return clips.Count; }
        }

        public AudioClip GetClip(int clip)
        {
            if (clips.Count == 0) return null;
            return clips[Mathf.Clamp(clip, 0, clips.Count - 1)];
        }

        public AudioClip GetRandomClip()
        {
            if (clips.Count > 0) return clips.RandomElement();
            return null;
        }
    }
}