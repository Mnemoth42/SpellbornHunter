using System;
using System.Collections.Generic;
using System.Linq;
using GameDevTV.Inventories;
using UnityEditor;
using UnityEngine;

namespace TkrainDesigns.Inventories
{
    [CreateAssetMenu(menuName = "Equipment/SyntyEquipableItem", fileName = "SyntyEquipableItem", order = 0)]
    public class SyntyEquipableItem : StatsEquipableItem
    {
        [System.Serializable]
        public class ItemPair
        {
            public string category = "";
            public int index;
        }

        [System.Serializable]
        public class ItemColor
        {
            public string category = "";
            public Color color = Color.magenta;
        }

        [Header("The name of the object in the Modular Characters Prefab representing this item.")] [SerializeField]
        List<ItemPair> objectsToActivate = new List<ItemPair>();

        [SerializeField] List<ItemColor> colorChanges = new List<ItemColor>();

        [Header("Slot Categories to deactivate when this item is activated.")] [SerializeField]
        List<string> slotsToDeactivate = new List<string>();

        public List<string> SlotsToDeactivate => slotsToDeactivate;
        public List<ItemPair> ObjectsToActivate => objectsToActivate;
        public List<ItemColor> ColorChangers => colorChanges;

    #if UNITY_EDITOR
        void AddCategoryToRemove()
        {
            SetUndo("Add Category To Remove");
            slotsToDeactivate.Add("");
        }

        void RemoveCategoryToRemove(int i)
        {
            SetUndo("Remove Category To Remove");
            slotsToDeactivate.RemoveAt(i);
        }

        void SetCategoryToRemove(int i, string category)
        {
            if (slotsToDeactivate[i] == category) return;
            SetUndo("Change Category To Remove");
            slotsToDeactivate[i] = category;
        }

        void AddObjectToActivate()
        {
            SetUndo("Add Item To Activate");
            objectsToActivate.Add(new ItemPair());
        }

        void RemoveObjectToActivate(int i)
        {
            SetUndo("Remove Item To Activate");
            objectsToActivate.RemoveAt(i);
        }

        void AddColorToChange()
        {
            SetUndo("Add Color Changer");
            colorChanges.Add(new ItemColor());
        }

        void RemoveColorChange(int i)
        {
            SetUndo("Remove Color Change");
            colorChanges.RemoveAt(i);
        }

        void SetColorChange(int i, string category, Color index)
        {
            if (index == colorChanges[i].color && category == colorChanges[i].category) return;
            SetUndo("Modify color change");
            colorChanges[i].category = category;
            colorChanges[i].color = index;
        }

        void SetObjectToActivate(int i, string category, int index)
        {
            if (index == objectsToActivate[i].index && category == objectsToActivate[i].category) return;
            SetUndo("Change Item To Activate");
            objectsToActivate[i].index = index;
            objectsToActivate[i].category = category;
        }

        public static readonly List<string> Categories = new List<string>
                                                         {
                                                             "HeadCoverings_Base_Hair",
                                                             "HeadCoverings_No_FacialHair",
                                                             "HeadCoverings_No_Hair",
                                                             "All_01_Hair",
                                                             "Helmet",
                                                             "All_04_Back_Attachment",
                                                             "All_05_Shoulder_Attachment_Right",
                                                             "All_06_Shoulder_Attachment_Left",
                                                             "All_07_Elbow_Attachment_Right",
                                                             "All_08_Elbow_Attachment_Left",
                                                             "All_09_Hips_Attachment",
                                                             "All_10_Knee_Attachement_Right",
                                                             "All_11_Knee_Attachement_Left",
                                                             "Elf_Ear",
                                                             "Female_Head_All_Elements",
                                                             "Female_Head_No_Elements",
                                                             "Female_01_Eyebrows",
                                                             "Male_Head_All_Elements",
                                                             "Male_Head_No_Elements",
                                                             "Male_01_Eyebrows",
                                                             "Male_02_FacialHair",
                                                             "Torso",
                                                             "UpperArm",
                                                             "LowerArm",
                                                             "Hand",
                                                             "Hips",
                                                             "Leg",
                                                         };

        public static readonly List<string> PreviewParts = new List<string>
                                                           {
                                                               "Chr_HeadCoverings_Base_Hair",
                                                               "HeadCoverings_No_FacialHair",
                                                               "HeadCoverings_No_Hair",
                                                               "Hair",
                                                               "HelmetAttachment",
                                                               "BackAttachment",
                                                               "ShoulderAttachRight",
                                                               "ShoulderAttachLeft",
                                                               "ElbowAttachRight",
                                                               "ElbowAttachLeft",
                                                               "HipsAttachment",
                                                               "KneeAttachRight",
                                                               "KneeAttachLeft",
                                                               "Ear_Ear",
                                                               "Head_Female",
                                                               "Head_No_Elements_Female",
                                                               "Eyebrow_Female",
                                                               "Head_Male",
                                                               "Head_No_Elements_Male",
                                                               "Eyebrow_Male",
                                                               "FacialHair_Male" +
                                                               "Torso_Male",
                                                               "ArmUpperRight_Male",
                                                               "ArmLowerRight_Male",
                                                               "HandRight_Male",
                                                               "Hips_Male",
                                                               "LegRight_Male"
                                                           };

        public static readonly List<int> CategoryCount = new List<int>
                                                         {
                                                             10, 3, 12, 37, 12, 15, 20, 20, 5, 5, 11, 10, 10, 2,
                                                             21, 12, 9, 21, 12, 9, 9,
                                                             27, 27, 17, 16, 27, 18
                                                         };


        private bool drawSyntyData;
        private bool drawItemsToActivate = true;
        private bool drawCategoriesToRemove = true;
        private bool drawColorChanges = true;

        public override void DrawInspector()
        {
            base.DrawInspector();
            if (GetAllowedEquipLocation() == EquipLocation.Weapon) return;
            if (!DrawSection(ref drawSyntyData, "Modular Equipment Data")) return;
            BeginIndent();
            //DrawItemPairList(ref drawItemsToActivate, ref objectsToActivate, Categories, "ActivatedItem" );
            DrawItemsToActivate();
            DrawCategoriesToRemove();

            DrawColorChangers();
            EndIndent();
        }

        private void DrawColorChangers()
        {
            if (!DrawSection(ref drawColorChanges, $"Color Changes ({colorChanges.Count})")) return;
            BeginIndent();
            int itemToRemove;
            itemToRemove = -1;
            for (var i = 0; i < colorChanges.Count; i++)
            {
                EditorGUILayout.BeginHorizontal();
                int cat = SyntyStatics.GearColors.ToList().IndexOf(colorChanges[i].category);
                if (cat < 0) cat = 0;

                cat = EditorGUILayout.Popup(cat, SyntyStatics.GearColors);
                int maxColor = SyntyStatics.GetColorCount(SyntyStatics.GearColors[cat]);

                Color index = EditorGUILayout.ColorField(colorChanges[i].color);
                SetColorChange(i, SyntyStatics.GearColors[cat], index);
                //dirty=!oldCat.Equals(colorChanges[i].category) || oldIndex != colorChanges[i].color;
                if (GUILayout.Button("-"))
                {
                    dirty = true;
                    itemToRemove = i;
                }

                EditorGUILayout.EndHorizontal();
            }

            if (itemToRemove > -1) RemoveColorChange(itemToRemove);
            if (GUILayout.Button("Add Color Change"))
            {
                dirty = true;
                AddColorToChange();
            }

            EndIndent();
        }

        private void DrawCategoriesToRemove()
        {
            if (!DrawSection(ref drawCategoriesToRemove, $"Categories To Remove ({slotsToDeactivate.Count})")) return;
            BeginIndent();
            var itemToRemove = -1;
            for (int i = 0; i < slotsToDeactivate.Count; i++)
            {
                EditorGUILayout.BeginHorizontal();
                int cat = Categories.IndexOf(slotsToDeactivate[i]);
                if (cat < 0) cat = 0;
                cat = EditorGUILayout.Popup(cat, Categories.ToArray());
                SetCategoryToRemove(i, Categories[cat]);
                if (GUILayout.Button("-")) itemToRemove = i;
                EditorGUILayout.EndHorizontal();
            }

            if (itemToRemove > -1)
            {
                RemoveCategoryToRemove(itemToRemove);
            }

            if (GUILayout.Button("Add Category to Remove"))
            {
                AddCategoryToRemove();
            }

            EndIndent();
        }

        private void DrawItemsToActivate()
        {
            if (!DrawSection(ref drawItemsToActivate, $"Items To Activate ({objectsToActivate.Count})")) return;
            BeginIndent();
            int itemToRemove = -1;
            for (int i = 0; i < objectsToActivate.Count; i++)
            {
                EditorGUILayout.BeginHorizontal();
                int cat = Categories.IndexOf(objectsToActivate[i].category);
                if (cat == -1) cat = 0;
                cat = EditorGUILayout.Popup(cat, Categories.ToArray());
                int index = EditorGUILayout.IntSlider(objectsToActivate[i].index, 0, CategoryCount[cat]);
                if (GUILayout.Button("-")) itemToRemove = i;
                SetObjectToActivate(i, Categories[cat], index);
                EditorGUILayout.EndHorizontal();
            }

            if (itemToRemove > -1)
            {
                RemoveObjectToActivate(itemToRemove);
            }

            if (GUILayout.Button("Add Item To Activate")) AddObjectToActivate();
            EndIndent();
        }

        void DrawItemPairList(ref bool shouldDraw, ref List<ItemPair> items, List<string> categorySource,
                              string caption)
        {
            if (!DrawSection(ref shouldDraw, $"{caption} ({items.Count}")) return;
            BeginIndent();
            ItemPair itemToRemove = null;
            foreach (var item in items)
            {
                EditorGUILayout.BeginHorizontal();
                EditorGUI.BeginChangeCheck();
                int cat = categorySource.IndexOf(item.category);
                if (cat == -1) cat = 0;
                int newCat = EditorGUILayout.Popup(cat, categorySource.ToArray());
                int newIndex = EditorGUILayout.IntSlider(item.index, 0, CategoryCount[cat]);
                if (EditorGUI.EndChangeCheck())
                {
                    SetUndo($"Change caption{caption}");
                    item.category = categorySource[cat];
                    item.index = newIndex;
                    dirty = true;
                }

                if (GUILayout.Button("-"))
                {
                    itemToRemove = item;
                    dirty = true;
                }

                EditorGUILayout.EndHorizontal();
            }


            if (itemToRemove != null)
            {
                SetUndo($"Remove {caption}");
                items.Remove(itemToRemove);
            }

            if (GUILayout.Button($"Add {caption}"))
            {
                SetUndo($"Add {caption}");
                dirty = true;
                items.Add(new ItemPair());
            }

            EndIndent();
        }

        public override bool shouldRedraw()
        {
            if (dirty)
            {
                Debug.Log($"Dirty");
                dirty = false;
                return true;
            }

            return base.shouldRedraw();
        }

        protected override void AddToolbarString()
        {
            base.AddToolbarString();
            if (GetAllowedEquipLocation() != EquipLocation.Weapon)
            {
                toolbarStrings.Add("ArmorModel");
            }

            toolbarStrings.Add("Preview");
        }


        public override GameObject GetPreviewObject(string tooltipName)
        {
            if (tooltipName == "Preview")
            {
                {
                    //Debug.Log($"{name} preview");
                    GameObject go = AssetDatabase.LoadAssetAtPath<GameObject>("Assets/Game/Core/DemoModel.prefab");

                    if (go) return go;
                    Debug.Log("No Asset?");
                    return null;
                }
            }


            return base.GetPreviewObject(tooltipName);
        }

    #endif
    }
}