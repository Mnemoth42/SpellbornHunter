using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using GameDevTV.Inventories;
using Newtonsoft.Json.Linq;
using TkrainDesigns.Stats;
using TkrainDesigns.UIElements;
using UnityEditor;
using UnityEditor.Rendering;
using UnityEditor.UIElements;
using UnityEngine;
using UnityEngine.UIElements;
using Random = UnityEngine.Random;

namespace TkrainDesigns.Inventories
{
    [CreateAssetMenu(fileName = "New Equipment", menuName = "Equipment/Equipable Item")]
    public class StatsEquipableItem : EquipableItem, IModifierProvider, IDamageCharacteristicProvider
    {
    #region fields

        [SerializeField] private List<StatModifier> additiveModifiers = new List<StatModifier>();
        [SerializeField] private List<StatModifier> percentageModifiers = new List<StatModifier>();
        [SerializeField] private List<EStat> potentialModifiers = new List<EStat>();
        [SerializeField] private List<StatRequirement> statRequirements = new List<StatRequirement>();
        [SerializeField] private List<DamageCharacteristic> defenseCharacteristics = new List<DamageCharacteristic>();
        private Dictionary<EStat, float> actualAdditiveModifiers = new Dictionary<EStat, float>();
        private Dictionary<EStat, float> actualPercentageModifiers = new Dictionary<EStat, float>();

    #endregion

        public override int GetPrice()
        {
            int adjustedPrice = base.GetPrice();
            adjustedPrice += adjustedPrice * (GetLevel() - 1);
            foreach (KeyValuePair<EStat, float> pair in actualAdditiveModifiers)
            {
                adjustedPrice += Mathf.Max((int)pair.Value * GetLevel() * 5, 0);
            }

            foreach (KeyValuePair<EStat, float> pair in actualPercentageModifiers)
            {
                adjustedPrice += Mathf.Max((int)pair.Value * GetLevel() * 10,0);
            }

            return adjustedPrice;
        }

        public override JToken CaptureState()
        {
            JObject state = new JObject();
            IDictionary<string, JToken> stateDict = state;
            stateDict["level"] = JToken.FromObject(GetLevel());
            stateDict["additive"] = JToken.FromObject(actualAdditiveModifiers);
            stateDict["percentage"] = JToken.FromObject(actualPercentageModifiers);
            return state;
        }

        public override void RestoreState(JToken state)
        {
            IDictionary<string, JToken> stateDict = (JObject)state;
            SetLevel(stateDict["level"].ToObject<int>());
            actualAdditiveModifiers = stateDict["additive"].ToObject<Dictionary<EStat, float>>();
            actualPercentageModifiers = stateDict["percentage"].ToObject<Dictionary<EStat, float>>();
        }

        public override JToken CaptureAsJToken()
        {
            JObject state = new JObject();
            IDictionary<string, JToken> stateDict = state;
            stateDict["level"] = JToken.FromObject(GetLevel());
            stateDict["additive"] = JToken.FromObject(actualAdditiveModifiers);
            stateDict["percentage"] = JToken.FromObject(actualPercentageModifiers);
            return state;
        }

        public override void RestoreFromJToken(JToken state)
        {
            IDictionary<string, JToken> stateDict = (JObject)state;
            SetLevel(stateDict["level"].ToObject<int>());
            actualAdditiveModifiers = stateDict["additive"].ToObject<Dictionary<EStat, float>>();
            actualPercentageModifiers = stateDict["percentage"].ToObject<Dictionary<EStat, float>>();
        }

        private void Awake()
        {
            OnValidate();
        }

        /// <summary>
        /// This should be called when a character drops an item and it is a StatsEquipableItem.
        /// In this case, the item should be an instance of the original, not the original itself.
        /// </summary>
        public override void Initialize()
        {
//            Debug.Log($"{this}.Initialize();");
            OnValidate();

            foreach (EStat stat in potentialModifiers)
            {
                if (Random.Range(1, 100) < 25)
                {
                    if (!actualAdditiveModifiers.ContainsKey(stat))
                    {
                        actualAdditiveModifiers[stat] = 0;
                    }

                    actualAdditiveModifiers[stat] += CalculatePossibleBoost(1, GetLevel());
                    break;
                }
                else if (Random.Range(1, 100) < 25)
                {
                    if (!actualPercentageModifiers.ContainsKey(stat))
                    {
                        actualPercentageModifiers[stat] = 0;
                    }

                    actualPercentageModifiers[stat] += CalculatePossibleBoost(1, GetLevel());
                    break;
                }
            }
        }

        /// <summary>
        /// Cycles through collection of Stat Requirements to determine if the stat requirements have been met to
        /// equip this item.  If any one stat requirement fails, returns false.
        /// </summary>
        /// <param name="user">Whatever character wishes to wield this item.</param>
        /// <returns>true if item can be wielded/worn</returns>
        public override bool TestRequirements(GameObject user)
        {
            if (user.GetComponent<BaseStats>().GetLevel() < GetLevel())
            {
                GameLog.WriteLn($"You must be level {GetLevel()} to equip this item.");
            }

            foreach (StatRequirement requirement in statRequirements)
            {
                if (!requirement.Evaluate(user))
                {
                    requirement.LogReason();
                    return false;
                }
            }

            return true;
        }

        public override string GetDescription()
        {
            string result = base.GetDescription();
            foreach (var requirement in statRequirements)
            {
                result += $"\n{requirement.AdjustedToString(Application.isPlaying ? GetLevel() : 1)}";
            }

            foreach (var pair in actualAdditiveModifiers)
            {
                if (pair.Value>0)
                {
                    result += $"\n <color=#8888ff>{pair.Value} point bonus to {pair.Key}</color>";
                } else if (pair.Value < 0)
                {
                    result += $"\n <color=#ff8888>{pair.Value} point penalty to {pair.Key}</color>";
                }
            }

            foreach (var pair in actualPercentageModifiers)
            {
                if (pair.Value>0)
                {
                    result += $"\n<color=#8888ff>{pair.Value} percent bonus to {pair.Key}</color>";
                } else if (pair.Value < 0)
                {
                    result += $"\n<color=#ff8888>{pair.Value} percent penalty to {pair.Key}</color>";
                }
            }

            foreach (var pair in defenseCharacteristics)
            {
                if(pair.GetAmount()>0)
                {
                    result+=$"\n<color=#8888ff>Strong({pair.GetAmount()}) against {pair.GetDamageType()}</color>";
                } else if (pair.GetAmount() < 0)
                {
                    result += $"\n<color=#ff8888>Weak ({pair.GetAmount():F0}) against{pair.GetDamageType()}</color>";
                }
            }
            
            return result;
        }

        public override void OnValidate()
        {
            base.OnValidate();
            actualAdditiveModifiers = new Dictionary<EStat, float>();
            actualPercentageModifiers = new Dictionary<EStat, float>();
            foreach (StatModifier modifier in additiveModifiers)
            {
                actualAdditiveModifiers[modifier.GetStat()] = Application.isPlaying
                    ? CalculatePossibleBoost(modifier.GetAmount(), GetLevel())
                    : modifier.GetAmount();
            }

            foreach (StatModifier modifier in percentageModifiers)
            {
                actualPercentageModifiers[modifier.GetStat()] = Application.isPlaying
                    ? CalculatePossibleBoost(modifier.GetAmount(), GetLevel())
                    : modifier.GetAmount();
            }
        }


        /// <summary>
        /// Sliding scale stat boost system.  Every stat bonus will start out with the value in the list, however
        /// there is a chance the stat will be better.  This chance is based on the item's level... every stat gets a
        /// minimum 5% chance to increase.  If it does increase, then it gets another roll until it doesn't get an increase.
        /// This chance will slowly increase after level 15, by 1 point every 3 levels.  In no event will the chance be greater
        /// than 25%.  
        /// </summary>
        /// <param name="amount"></param>
        /// <returns></returns>
        float CalculatePossibleBoost(float amount, int chanceLevel)
        {
            int chance = chanceLevel * 20;
            if (Random.Range(1, 100) < chance)
            {
                return CalculatePossibleBoost(amount + 1.0f, chanceLevel - 1);
            }

            return amount;
        }

        public IEnumerable<float> GetAdditiveModifiers(EStat stat)
        {
            //Debug.Log($"{this}.GetAdditiveModifiers({stat})");
            if (actualAdditiveModifiers.ContainsKey(stat))
            {
                yield return actualAdditiveModifiers[stat];
            }
        }

        public IEnumerable<float> GetPercentageModifiers(EStat stat)
        {
            if (actualPercentageModifiers.ContainsKey(stat))
            {
                yield return actualPercentageModifiers[stat];
            }
        }

        public IEnumerable<DamageCharacteristic> GetDefensiveCharacteristics()
        {
            foreach (var characteristic in defenseCharacteristics)
            {
                yield return characteristic;
            }
        }


    #region Editor

    #if UNITY_EDITOR
        bool drawStatModifiers = true;
        private bool drawAdditive = true;
        private bool drawPercentage = true;
        private bool drawPotential = true;
        private bool drawRequirements = true;

        protected bool dirty = false;

        protected void DrawList<T>(List<T> list, T newItem, string caption = "element") where T : DrawableHelperClass
        {
            int itemToRemove = -1;
            for (int i = 0; i < list.Count; i++)
            {
                if (list[i].DrawInspector(this, out bool isDirty)) itemToRemove = i;
                if (isDirty) dirty = true;
            }

            if (itemToRemove >= 0) RemoveFromList(list, itemToRemove, $"Remove {caption}");

            if (GUILayout.Button($"Add {caption}"))
            {
                dirty = true;
                AddToList(list, newItem, caption);
            }
        }

        protected void DrawListCategory<T>(List<T> list, T newItem, ref bool draw, string caption)
            where T : DrawableHelperClass
        {
            if (DrawSection(ref draw, $"{caption}s ({list.Count})"))
            {
                DrawList(list, newItem, caption);
            }
        }

        void AddPotentialStat()
        {
            SetUndo("Add Potential Stat Modifier");
            potentialModifiers.Add(EStat.Select);
        }

        void RemovePotentialStat(int index)
        {
            SetUndo("Remove Potential Stat Modifier");
            potentialModifiers.RemoveAt(index);
        }

        void SetPotentialStat(int index, EStat stat)
        {
            if (potentialModifiers[index] == stat) return;
            SetUndo("Change Potential Modifier");
            potentialModifiers[index] = stat;
        }

        public override void DrawInspector()
        {
            base.DrawInspector();
            if (!DrawSection(ref drawStatModifiers, "Stat Modifiers")) return;
            BeginIndent();
            DrawListCategory(additiveModifiers, new StatModifier(EStat.Select, 0), ref drawAdditive,
                "Additive Modifier");
            DrawListCategory(percentageModifiers, new StatModifier(), ref drawPercentage, "Percentage Modifier");
            DrawPotentialModifiers();
            DrawListCategory(statRequirements, new StatRequirement(), ref drawRequirements, "Stat Requirement");
            DrawListCategory(defenseCharacteristics, new DamageCharacteristic(), ref drawDefenseCharacteristics,
                "Defense Characteristic");
            if (EditorUtility.IsDirty(this)) OnValidate();
            EndIndent();
        }

        private bool drawDefenseCharacteristics = true;

        private void DrawDefenseCharacteristics()
        {
            if (!DrawSection(ref drawDefenseCharacteristics,
                $"Defense Characteristics ({defenseCharacteristics.Count})")) return;
            BeginIndent();
            DrawList(defenseCharacteristics, new DamageCharacteristic(), "Defense Characteristic");
            EndIndent();
        }

        private void DrawStatRequirements()
        {
            if (!DrawSection(ref drawRequirements, $"Stat Requirements ({statRequirements.Count})")) return;
            BeginIndent();
            DrawList(statRequirements, new StatRequirement(), "Stat Requirement");
            EndIndent();
        }

        private void DrawPotentialModifiers()
        {
            if (!DrawSection(ref drawPotential, "Potential Modifiers")) return;
            BeginIndent();
            int modifierToRemove = -1;
            int modifierToEdit = -1;
            EStat modfierToEditValue = EStat.Select;
            for (int i = 0; i < potentialModifiers.Count; i++)
            {
                EditorGUILayout.BeginHorizontal();
                EStat modifier = (EStat)EditorGUILayout.EnumPopup(potentialModifiers[i]);
                if (modifier != potentialModifiers[i])
                {
                    modifierToEdit = i;
                    modfierToEditValue = modifier;
                }

                if (GUILayout.Button("-")) modifierToRemove = i;
                EditorGUILayout.EndHorizontal();
            }

            if (GUILayout.Button("Add Potential Modifier"))
            {
                AddPotentialStat();
            }

            if (modifierToEdit > -1)
            {
                SetPotentialStat(modifierToEdit, modfierToEditValue);
            }

            if (modifierToRemove > -1)
            {
                RemovePotentialStat(modifierToRemove);
            }

            EndIndent();
        }

        private void DrawPercentageModifiers()
        {
            if (!DrawSection(ref drawPercentage, "Percentage Modifiers")) return;
            BeginIndent();
            DrawList(percentageModifiers, new StatModifier(EStat.Select, 0), "Percentage Modifier");
            EndIndent();
        }

        private void DrawAdditiveModifiers()
        {
            if (!DrawSection(ref drawAdditive, "Additive Modifiers")) return;
            BeginIndent();
            DrawList(additiveModifiers, new StatModifier(EStat.Select, 0), "Additive Modifier");
            EndIndent();
        }

        [NonSerialized] private Foldout AdditiveModifierFoldout;
        [NonSerialized] private Foldout PercentageModifierFoldout;
        [NonSerialized] private Foldout PotentialModifierFoldout;
        [NonSerialized] private Foldout StatRequirementsFoldout;
        [NonSerialized] private Foldout DefenseCharacteristicsFoldout;
        [NonSerialized] protected VisualTreeAsset ModifierEntryUXML;


        public override void CreateGUI(ScrollView container, Action _callback)
        {
            base.CreateGUI(container, _callback);
            VisualTreeAsset StatsEquipableItemUXML = Resources.Load<VisualTreeAsset>("StatsEquipableItemUXML");
            container.Add(StatsEquipableItemUXML.CloneTree());
            ModifierEntryUXML = Resources.Load<VisualTreeAsset>("ModifierEntryUXML");
            Resources.Load<VisualTreeAsset>("StatRequirementEntryUXML");
            FindElement(out AdditiveModifierFoldout, "AdditiveModifierFoldout");
            SetupNewSetupStatModifier(GetProperty("additiveModifiers"),additiveModifiers, AdditiveModifierFoldout);
            FindElement(out PercentageModifierFoldout, "PercentageModifierFoldout");
            SetupNewSetupStatModifier(GetProperty("percentageModifiers"),percentageModifiers, PercentageModifierFoldout);
            FindElement(out PotentialModifierFoldout, "PotentialModifiersFoldout");
            SetupPotentialModifiers();
            FindElement(out StatRequirementsFoldout, "StatRequirementsFoldout");
            SetupStatRequirements();
            FindElement(out DefenseCharacteristicsFoldout, "DefenseCharacteristicsFoldout");
            SetupDefenseCharacteristics();
        }

        
        
        void SetupNewSetupStatModifier(SerializedProperty property, List<StatModifier> statModifiers, Foldout foldout)
        {
            foldout.Clear();
            callback?.Invoke();
            
            for (int i = 0; i < property.arraySize; i++)
            {
                
                SerializedProperty element = property.GetArrayElementAtIndex(i);
                int index = i;
                StatModifierElement statModifierElement = new StatModifierElement();
                statModifierElement.BindProperty(element);
                statModifierElement.RegisterValueChanged(() =>
                {
                    ApplyModifiedProperties();
                    RedrawPreview();
                });
                statModifierElement.RegisterOnClick(() =>
                {
                    property.DeleteArrayElementAtIndex(index);
                    ApplyModifiedProperties();
                    RedrawPreview();
                    SetupNewSetupStatModifier(property, statModifiers, foldout);
                });
                foldout.Add(statModifierElement);
            }

            Button AddButton = new Button(() =>
            {
                property.InsertArrayElementAtIndex(property.arraySize);
                ApplyModifiedProperties();
                RedrawPreview();
                SetupNewSetupStatModifier(property, statModifiers, foldout);
            });
            AddButton.text = "Add Modifier";
            foldout.Add(AddButton);
        }

        void AddModifier(List<StatModifier> statModifiers)
        {
            Undo.RecordObject(this, "add modifier");
            statModifiers.Add(new StatModifier(EStat.Select, 0));
        }

        void SetupPotentialModifiers()
        {
            Foldout foldout = PotentialModifierFoldout;
            foldout.Clear();
            SerializedProperty property = GetProperty("potentialModifiers");
            for (int i = 0; i < property.arraySize; i++)
            {
                
                int index = i;
                VisualElement container = ModifierEntryUXML.CloneTree();
                EnumField enumField = container.Q<EnumField>();
                Button deleteButton = container.Q<Button>();
                Slider slider = container.Q<Slider>();
                SliderInt sliderInt = container.Q<SliderInt>();
                slider.style.display = DisplayStyle.None;
                sliderInt.style.display = DisplayStyle.None;
                enumField.BindProperty(property.GetArrayElementAtIndex(i));
                deleteButton.clicked += () =>
                {
                    property.DeleteArrayElementAtIndex(index);
                    ApplyModifiedProperties();
                    SetupPotentialModifiers();
                };
                
                foldout.Add(container);
            }

            Button AddButton = new Button(() =>
            {
                property.InsertArrayElementAtIndex(property.arraySize);
                ApplyModifiedProperties();
                SetupPotentialModifiers();
            });
            AddButton.text = "Add Potential Modifier";
            foldout.Add(AddButton);


        }

        void SetupStatRequirements()
        {
            Foldout foldout = StatRequirementsFoldout;
            foldout.Clear();
            SerializedProperty property = GetProperty("statRequirements");
            for(int i=0;i<property.arraySize;i++)
            {
                SerializedProperty requirement = property.GetArrayElementAtIndex(i);
                int index = i;
                VisualElement container = ModifierEntryUXML.CloneTree();
                EnumField statRequirementEnumField = container.Q<EnumField>();
                Button statRequirementDeleteButton = container.Q<Button>();
                statRequirementEnumField.BindProperty(requirement.FindPropertyRelative("stat"));
                statRequirementEnumField.RegisterValueChangedCallback((evt) =>
                {
                    ApplyModifiedProperties();
                    RedrawPreview();
                });
                container.Q<Slider>().style.display = DisplayStyle.None;
                SliderInt sliderInt = container.Q<SliderInt>();
                sliderInt.BindProperty(requirement.FindPropertyRelative("level"));
                sliderInt.RegisterValueChangedCallback((evt) =>
                {
                    ApplyModifiedProperties();
                    RedrawPreview();
                });
                statRequirementDeleteButton.clicked += () =>
                {
                    property.DeleteArrayElementAtIndex(index);
                    ApplyModifiedProperties();
                    RedrawPreview();
                    SetupStatRequirements();
                };
                foldout.Add(container);
                
            }

            Button button = new Button(() =>
            {
                property.InsertArrayElementAtIndex(property.arraySize);
                RedrawPreview();
                SetupStatRequirements();
            })
                            {
                                text = "Add Requirement"
                            };
            foldout.Add(button);
        }

        void SetupDefenseCharacteristics()
        {
            Foldout foldout = DefenseCharacteristicsFoldout;
            foldout.Clear();
            var property = GetProperty("defenseCharacteristics");
            for (int i = 0; i < property.arraySize; i++)
            {
                var element = property.GetArrayElementAtIndex(i);
                int index = i;
                var container = ModifierEntryUXML.CloneTree();
                var enumField = container.Q<EnumField>();
                enumField.BindProperty(element.FindPropertyRelative("damageType"));
                enumField.RegisterValueChangedCallback((evt) =>
                {
                    ApplyModifiedProperties();
                    RedrawPreview();
                });
                Slider slider = container.Q<Slider>();
                slider.BindProperty(element.FindPropertyRelative("amount"));
                slider.RegisterValueChangedCallback((evt) =>
                {
                    ApplyModifiedProperties();
                    RedrawPreview();
                });
                Button button = container.Q<Button>();
                button.clicked += () =>
                {
                    property.DeleteArrayElementAtIndex(index);
                    ApplyModifiedProperties();
                    RedrawPreview();
                    SetupDefenseCharacteristics();
                };
                SliderInt sliderInt = container.Q<SliderInt>();
                sliderInt.Hide();
                foldout.Add(container);
            }

            Button addButton = new Button(() =>
            {
                property.InsertArrayElementAtIndex(property.arraySize);
                ApplyModifiedProperties();
                RedrawPreview();
                SetupDefenseCharacteristics();
            });
            foldout.Add(addButton);
        }
        
    #endif

    #endregion
    }
}