using System;
using System.Collections.Generic;
using System.Security.Cryptography.X509Certificates;
using GameDevTV.Inventories;
using UnityEditor;
using UnityEngine;
using UnityEngine.Rendering;
using Random = UnityEngine.Random;

namespace TkrainDesigns.Inventories
{
    [CreateAssetMenu(menuName = ("RPG/Inventory/Drop Library"))]
    public class DropLibrary : ScriptableObject
    {
        [SerializeField] DropConfig[] potentialDrops;
        [SerializeField] AnimationCurve dropChancePercentage = new AnimationCurve();
        [SerializeField] private AnimationCurve dropCount = new AnimationCurve();

        [System.Serializable]
        class DropConfig
        {
            public InventoryItem item;
            public AnimationCurve weight = new AnimationCurve();
            public AnimationCurve amount = new AnimationCurve();

            public int GetRandomNumber(int level)
            {
                if (!item.IsStackable())
                {
                    return 1;
                }

                int max = amount.Evaluate(level).ToInt().Floor(0);
                int min = max == 1 ? 1 : (int)(max * .66f);
                max = max == 1 ? 1 : (int)(max * 1.33f);
                return max == 1 ? 1 : UnityEngine.Random.Range(min, max + 1);
            }
        }

        public struct Dropped
        {
            public InventoryItem item;
            public int number;
        }

        public IEnumerable<Dropped> GetRandomDrops(int level)
        {
            if (!ShouldRandomDrop(level))
            {
                yield break;
            }

            for (int i = 0; i < GetRandomNumberOfDrops(level); i++)
            {
                yield return GetRandomDrop(level);
            }
        }

        bool ShouldRandomDrop(int level)
        {
            return Random.Range(0, 100) < GetByLevel(dropChancePercentage, level);
        }

        int GetRandomNumberOfDrops(int level)
        {
            int min = GetByLevel(dropCount, level);
            int max = GetByLevel(dropCount, level);
            if (min > 1)
            {
                min = (min * .66f).ToInt();
                max = (max * 1.33f).ToInt();
            }

            return Random.Range(min, max);
        }

        Dropped GetRandomDrop(int level)
        {
            var drop = SelectRandomItem(level);
            var result = new Dropped();
            result.item = drop.item;
            if (!result.item.IsStackable())
            {
                result.item = Instantiate(drop.item);
                result.item.SetLevel(level);
                result.item.Initialize();
            }

            result.number = result.item.IsStackable() ? drop.GetRandomNumber(level) : 1;
            return result;
        }

        DropConfig SelectRandomItem(int level)
        {
            float totalChance = GetTotalChance(level);
            float randomRoll = Random.Range(0, totalChance);
            float chanceTotal = 0;
            foreach (var drop in potentialDrops)
            {
                chanceTotal += GetByLevel(drop.weight, level);
                if (chanceTotal > randomRoll)
                {
                    return drop;
                }
            }

            return null;
        }

        float GetTotalChance(int level)
        {
            float total = 0;
            foreach (var drop in potentialDrops)
            {
                total += GetByLevel(drop.weight, level);
            }

            return total;
        }

        static int GetByLevel(AnimationCurve values, int level)
        {
            if (level <= 1)
            {
                level = 1;
            }

            return values.Evaluate(level).ToInt().Floor(0);
        }
    }
}