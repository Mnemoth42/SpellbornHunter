using System;
using System.Collections;
using System.Collections.Generic;
using GameDevTV.Inventories;
using GameDevTV.Saving;
using Newtonsoft.Json.Linq;
using UnityEngine;

namespace TkrainDesigns.Inventories
{
    [RequireComponent(typeof(Inventory))]
    [DisallowMultipleComponent]
    public class InventoryLoader : MonoBehaviour, ISaveable, IJsonSaveable
    {
        private Inventory inventory;
        [SerializeField] private List<InventoryItem> startingItems = new List<InventoryItem>();
        private bool initialItemsGranted = false;

        private void Start()
        {
            if (initialItemsGranted) return;
            initialItemsGranted = true;
            inventory = GetComponent<Inventory>();
            foreach (InventoryItem item in startingItems)
            {
                inventory.AddToFirstEmptySlot(item, 1);
            }
        }

        public JToken CaptureState()
        {
            return initialItemsGranted;
        }

        public void RestoreState(JToken state)
        {
            initialItemsGranted = state.ToObject<bool>();
        }

        public JToken CaptureAsJToken()
        {
            return initialItemsGranted;
        }

        public void RestoreFromJToken(JToken state)
        {
            initialItemsGranted = state.ToObject<bool>();
        }
    }
}