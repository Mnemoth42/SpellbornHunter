using System.Collections;
using System.Collections.Generic;
using System.Linq;
using GameDevTV.Inventories;
using TkrainDesigns.Stats;
using UnityEngine;

namespace TkrainDesigns.Inventories
{
    [DisallowMultipleComponent]
    public class StatsEquipment : Equipment, IModifierProvider
    {
        public IEnumerable<float> GetAdditiveModifiers(EStat stat)
        {
            foreach (var item in GetAllPopulatedSlots().Select(GetItemInSlot))
            {
                if ((InventoryItem)item is IModifierProvider provider)
                {
                    foreach (var value in provider.GetAdditiveModifiers(stat))
                    {
                        yield return value;
                    }
                }
            }
        }

        public IEnumerable<float> GetPercentageModifiers(EStat stat)
        {
            foreach (var item in GetAllPopulatedSlots().Select(GetItemInSlot))
            {
                if ((InventoryItem)item is IModifierProvider provider)
                {
                    foreach (var value in provider.GetAdditiveModifiers(stat))
                    {
                        yield return value;
                    }
                }
            }
        }
    }
}