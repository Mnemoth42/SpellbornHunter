using System.Collections;
using System.Collections.Generic;
using GameDevTV.Inventories;
using UnityEngine;
using UnityEngine.AI;

namespace TkrainDesigns.Inventories
{
    public class EquippedItemDropper : ItemDropper
    {
        [Tooltip("How far can the pickups be scattered from the dropper.")] [SerializeField]
        float scatterDistance = 1;


        public void ComponentDrop()
        {
            Equipment equipment = GetComponent<Equipment>();
            List<EquipableItem> items = new List<EquipableItem>();
            foreach (var slot in equipment.GetAllPopulatedSlots())
            {
                if (equipment.GetItemInSlot(slot).IsDroppable())
                {
                    items.Add(equipment.GetItemInSlot(slot));
                }
            }

            if (items.Count > 0)
            {
                DropItem(items[Random.Range(0, items.Count)]);
            }
        }

        // CONSTANTS
        const int ATTEMPTS = 30;

        protected override Vector3 GetDropLocation()
        {
            // We might need to try more than once to get on the NavMesh
            for (int i = 0; i < ATTEMPTS; i++)
            {
                Vector3 randomPoint = transform.position + Random.insideUnitSphere * scatterDistance;
                NavMeshHit hit;
                if (NavMesh.SamplePosition(randomPoint, out hit, 0.1f, NavMesh.AllAreas))
                {
                    return hit.position;
                }
            }

            return transform.position;
        }
    }
}