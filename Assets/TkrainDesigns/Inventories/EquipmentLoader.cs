using System;
using System.Collections;
using System.Collections.Generic;
using GameDevTV.Inventories;
using TkrainDesigns.Stats;
using UnityEngine;
using Random = UnityEngine.Random;

namespace TkrainDesigns.Inventories
{
    public class EquipmentLoader : MonoBehaviour
    {
        public List<EquipableItem> potentialItems;

        public Dictionary<EquipLocation, List<EquipableItem>> lookup =
            new Dictionary<EquipLocation, List<EquipableItem>>();

        private Equipment equipment;
        private int level;

        private void Start()
        {
            BaseStats stats = GetComponent<BaseStats>();
            level = stats.GetLevel();
            equipment = GetComponent<Equipment>();
            if (equipment)
            {
                BuildLookup();
            }

            foreach (var key in lookup.Keys)
            {
                SelectAndEquip(lookup[key]);
            }
        }

        void SelectAndEquip(List<EquipableItem> list)
        {
            if (list.Count == 0) return;
            EquipableItem item = Instantiate(list[Random.Range(0, list.Count)]);
            item.SetLevel(level);
            item.Initialize();
            equipment.AddItem(item.GetAllowedEquipLocation(), item);
        }

        void BuildLookup()
        {
            foreach (EquipableItem item in potentialItems)
            {
                if (item == null) continue;
                if (!lookup.ContainsKey(item.GetAllowedEquipLocation()))
                {
                    lookup[item.GetAllowedEquipLocation()] = new List<EquipableItem>();
                }

                lookup[item.GetAllowedEquipLocation()].Add(item);
            }
        }
    }
}