using System.Collections;
using System.Collections.Generic;
using GameDevTV.Inventories;
using TkrainDesigns.Movement;
using UnityEngine;

namespace TkrainDesigns.Inventories
{
    [DisallowMultipleComponent]
    public class PickupCollector : MoveableActionBehavior<Pickup>
    {
        protected override void Perform()
        {
            target.PickupItem();
        }
    }
}