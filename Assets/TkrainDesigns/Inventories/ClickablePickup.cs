using GameDevTV.Inventories;
using GameDevTV.UI.Inventories;
using UnityEngine;
using UnityEngine.InputSystem;

namespace TkrainDesigns.Inventories
{
    public class ClickablePickup : Pickup, IRaycastable
    {
        public bool HandleRayCast(GameObject controller)
        {
            if (controller.TryGetComponent(out PickupCollector collector))
            {
                if (Mouse.current.leftButton.wasPressedThisFrame)
                {
                    collector.StartAction(this);
                }

                return true;
            }

            return false;
        }

        public int GetPriority()
        {
            return 5;
        }

        public ECursorType GetCursorType()
        {
            return ECursorType.Loot;
        }
    }
}