using GameDevTV.Inventories;
using UnityEngine;

namespace TkrainDesigns.Inventories
{
    public class PlacedItemDropper : ItemDropper
    {
        [SerializeField] private InventoryItem item;

        private void Awake()
        {
            DropItem(item);
        }
    }
}