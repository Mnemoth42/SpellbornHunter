using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TkrainDesigns
{
    /// <summary>
    /// IInteraction helps IRaycastables deal with cross dependency by using generics.  An example would be
    /// a shop with an IRayCastable wishing to tell a shopper to activate it.  
    /// </summary>
    public interface IInteraction<T>
    {
        bool HandleInteraction(T caller);
    }
}