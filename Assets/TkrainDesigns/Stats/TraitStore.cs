using System;
using System.Collections.Generic;
using System.Linq;
using GameDevTV.Saving;
using Newtonsoft.Json.Linq;
using UnityEngine;

namespace TkrainDesigns.Stats
{
    [DisallowMultipleComponent]
    public class TraitStore : MonoBehaviour, IModifierProvider, ISaveable, IJsonSaveable
    {
        [System.Serializable]
        private class TraitRecipe
        {
            public EStat affectingStat;
            public EStat affectedStat;
            [Range(-10, 10)] public float perLevel = 1.0f;
        }

        [SerializeField] private List<TraitRecipe> recipes = new List<TraitRecipe>();

        public event Action onStatsChanged;

        private BaseStats stats;

        private Dictionary<EStat, int> assignedTraits = new Dictionary<EStat, int>();
        private Dictionary<EStat, int> potentialTraits = new Dictionary<EStat, int>();

        private Dictionary<EStat, Dictionary<EStat, float>> modifiers =
            new Dictionary<EStat, Dictionary<EStat, float>>();


        private void Awake()
        {
            stats = GetComponent<BaseStats>();

            foreach (TraitRecipe recipe in recipes)
            {
                if (!modifiers.ContainsKey(recipe.affectedStat))
                {
                    modifiers[recipe.affectedStat] = new Dictionary<EStat, float>();
                }

                modifiers[recipe.affectedStat][recipe.affectingStat] = recipe.perLevel;
            }
        }

        public bool HasPotentialTrait(EStat stat) => potentialTraits.ContainsKey(stat);

        public void AddPoint(EStat stat)
        {
            if (!CanAdd(stat)) return;
            if (!potentialTraits.ContainsKey(stat))
            {
                potentialTraits[stat] = 1;
            }
            else
            {
                potentialTraits[stat]++;
            }

            StatsChanged();
        }

        public void SubtractPoint(EStat stat)
        {
            if (!CanSubtract(stat) || !potentialTraits.ContainsKey(stat)) return;
            potentialTraits[stat]--;
            if (potentialTraits[stat] <= 0) potentialTraits.Remove(stat);
            StatsChanged();
        }

        void StatsChanged()
        {
            onStatsChanged?.Invoke();
        }

        public void CommitPoints()
        {
            foreach (var pair in potentialTraits)
            {
                if (!assignedTraits.ContainsKey(pair.Key))
                {
                    assignedTraits[pair.Key] = 0;
                }

                assignedTraits[pair.Key] += pair.Value;
            }

            potentialTraits.Clear();
            StatsChanged();
        }

        public void ClearPoints()
        {
            potentialTraits.Clear();
            StatsChanged();
        }

        public void ResetPoints()
        {
            assignedTraits.Clear();
            potentialTraits.Clear();
            StatsChanged();
        }

        public int AvailablePoints()
        {
            return stats.GetStatAsInt(EStat.Traits) - PointsSpent();
        }

        int PointsSpent()
        {
            return assignedTraits.Values.Sum() + potentialTraits.Values.Sum();
        }

        public bool CanConfirm()
        {
            return potentialTraits.Values.Sum() > 0;
        }

        public bool CanAdd(EStat stat)
        {
            return AvailablePoints() > 0;
        }

        public bool CanSubtract(EStat stat)
        {
            return potentialTraits.ContainsKey(stat) && potentialTraits[stat] > 0;
        }

        public int GetTrait(EStat stat)
        {
            if (!stats) stats = GetComponent<BaseStats>();
            Debug.Assert(stats, $"{name} is misconfigured and has no BaseStats.");
            return (assignedTraits.ContainsKey(stat) ? assignedTraits[stat] : 0) + stats.GetStatAsInt(stat, 0);
        }

        public int GetRawTrait(EStat stat)
        {
            return (assignedTraits.ContainsKey(stat) ? assignedTraits[stat] : 0);
        }

        public int GetProposedTrait(EStat stat)
        {
            return GetTrait(stat) + (potentialTraits.ContainsKey(stat) ? potentialTraits[stat] : 0);
        }

        public IEnumerable<float> GetAdditiveModifiers(EStat stat)
        {
            if (modifiers.ContainsKey(stat))
            {
                foreach (var pair in modifiers[stat])
                {
                    yield return GetTrait(pair.Key) * pair.Value;
                }
            }
        }


        public IEnumerable<float> GetPercentageModifiers(EStat stat)
        {
            yield break;
        }

        public float GetProposedModifierEffect(EStat stat)
        {
            float result = 0;
            if (modifiers.ContainsKey(stat))
            {
                foreach (var pair in modifiers[stat])
                {
                    if (potentialTraits.ContainsKey(pair.Key)) result += pair.Value * potentialTraits[pair.Key];
                }
            }

            return result;
        }

        public JToken CaptureState()
        {
            return JToken.FromObject(assignedTraits);
        }

        public void RestoreState(JToken state)
        {
            assignedTraits = state.ToObject<Dictionary<EStat, int>>();
        }

        public JToken CaptureAsJToken()
        {
            return JToken.FromObject(assignedTraits);
        }

        public void RestoreFromJToken(JToken state)
        {
            assignedTraits = state.ToObject<Dictionary<EStat, int>>();
        }
    }
}