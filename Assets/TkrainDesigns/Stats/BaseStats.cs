using System.Linq;
using GameDevTV.Saving;
using Newtonsoft.Json.Linq;
using UnityEngine;
using UnityEngine.Events;


namespace TkrainDesigns.Stats
{
    [DisallowMultipleComponent]
    public class
        BaseStats : MonoBehaviour, ISaveable, IJsonSaveable
    {
        [SerializeField] private Progression progression;
        [SerializeField] private int level = 1;

        private Experience experience;

        void Awake()
        {
            experience = GetComponent<Experience>();
            if (experience)
            {
                experience.onGainExperience += CalculateLevel;
                experience.EventExperienceGained.AddListener(LogExperienceGained);
            }
        }

        public event System.Action onLevelUp;
        public UnityEvent EventLevelUp;

        void LogExperienceGained(int amount)
        {
            GameLog.WriteLn($"<color=#8888ff>{name} has been awarded <color=#88ffff>{amount}</color> experience! " +
                            $"<color=#88ffff>{experience.GetExperience()}/{GetStat(EStat.XPNeeded)}</color></color>");
        }

        
        public int GetLevel()
        {
            return level;
        }

        public void SetLevel(int newLevel)
        {
            level = newLevel;
            onLevelUp?.Invoke();
        }

        void CalculateLevel()
        {
            if (!experience)
            {
                return;
            }

            int experienceNeeded = GetStatAsInt(EStat.XPNeeded);
            while (experience.GetExperience() >= experienceNeeded)
            {
                level++;
                experience.SpendExperience(experienceNeeded);
                experienceNeeded = GetStatAsInt(EStat.XPNeeded);
                GameLog.WriteLn($"<color=blue>{name} has reached level {level}</color>");
                onLevelUp?.Invoke();
                EventLevelUp?.Invoke();
            }
        }

        public float GetExperienceAsPercentage()
        {
            if (!experience) return 0;
            return experience.GetExperience() / GetStat(EStat.XPNeeded);
        }
        
        public float GetStat(EStat stat, float fallback = 1)
        {
            if (progression == null) return fallback;
            return (progression.Evaluate(stat, level, fallback) * ((100 + GetPercentageModifiers(stat)) / 100) +
                    GetAdditiveModifiers(stat));
        }

        public int GetStatAsInt(EStat stat, int fallback = 1)
        {
            return (int)GetStat(stat, fallback);
        }

        public float GetAdditiveModifiers(EStat stat)
        {
            float result = 0;
            foreach (IModifierProvider provider in GetComponents<IModifierProvider>())
            {
                foreach (float amount in provider.GetAdditiveModifiers(stat))
                {
                    result += amount;
                }
            }

            return result;
            //return GetComponents<IModifierProvider>().SelectMany(provider => provider.GetAdditiveModifiers(stat)).Sum();
        }

        public float GetPercentageModifiers(EStat stat)
        {
            return GetComponents<IModifierProvider>().SelectMany(provider => provider.GetPercentageModifiers(stat))
                                                     .Sum();
        }

        public JToken CaptureState()
        {
            return level;
        }

        public void RestoreState(JToken state)
        {
            level = (int)state;
        }


        public JToken CaptureAsJToken()
        {
            return level;
        }

        public void RestoreFromJToken(JToken state)
        {
            level = state.ToObject<int>();
        }
    }
}