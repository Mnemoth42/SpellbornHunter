using UnityEngine;


namespace TkrainDesigns.Stats
{
    [System.Serializable]
    public struct StatFormula
    {
        public EStat stat;
        public float min;
        public float max;

        public StatFormula(EStat stat, float min, float max)
        {
            this.stat = stat;
            this.min = min;
            this.max = max;
            Validate();
        }

        void Validate()
        {
            if (min > max)
            {
                float temp = max;
                max = min;
                min = temp;
            }
        }

        public float Evaluate(int level)
        {
            if (level <= 1) return min;
            if (level >= 100) return max;
            float deltaValue = (max - min) / 99;
            return min + (deltaValue * (level - 1));
        }

        public int EvaluateAsInt(int level)
        {
            return Mathf.CeilToInt(Evaluate(level));
        }
    }
}