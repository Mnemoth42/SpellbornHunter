using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

namespace TkrainDesigns.Stats.Editor
{
    [CustomEditor(typeof(Progression))]
    public class ProgressionEditor : UnityEditor.Editor
    {
        private int currentEStat;
        private List<StatFormula> currentStatFuFormulas;

        bool ValidSelection(Enum stat)
        {
            EStat statToTest = (EStat)stat;
            for (int i = 0; i < currentStatFuFormulas.Count; i++)
            {
                if (i == currentEStat) continue;
                if (currentStatFuFormulas[i].stat == statToTest) return false;
            }

            return true;
        }

        public override void OnInspectorGUI()
        {
            Progression progression = (Progression)target;
            int formulaToDelete = -1;
            currentStatFuFormulas = progression.formulas;
            for (int i = 0; i < progression.formulas.Count; i++)
            {
                StatFormula formula = progression.formulas[i];
                EditorGUI.BeginChangeCheck();
                currentEStat = i;
                EStat stat =
                    (EStat)EditorGUILayout.EnumPopup(new GUIContent(" Stat"), formula.stat, ValidSelection, false);
                float min = EditorGUILayout.IntSlider(new GUIContent(" Min (Level 1)"), Mathf.CeilToInt(formula.min), 1,
                    100000);
                float max = EditorGUILayout.IntSlider(new GUIContent(" Max (Level 100"), Mathf.CeilToInt(formula.max),
                    1, 100000);
                if (EditorGUI.EndChangeCheck())
                {
                    Undo.RecordObject(progression, $"Change Formula {stat}");
                    progression.formulas[i] = new StatFormula(stat, min, max);
                    EditorUtility.SetDirty(progression);
                }

                EditorGUILayout.LabelField(new GUIContent($"L2={formula.EvaluateAsInt(2)} | " +
                                                          $"L10={formula.EvaluateAsInt(10)} | " +
                                                          $"L50={formula.EvaluateAsInt(50)} | " +
                                                          $"L90={formula.EvaluateAsInt(90)}"));
                if (GUILayout.Button($"Remove {stat}"))
                {
                    formulaToDelete = i;
                }
            }

            if (formulaToDelete > -1)
            {
                Undo.RecordObject(progression, "Delete Formula");
                progression.formulas.RemoveAt(formulaToDelete);
                EditorUtility.SetDirty(progression);
            }

            if (GUILayout.Button("Add Formula"))
            {
                Undo.RecordObject(progression, "Add Formula");
                progression.formulas.Add(new StatFormula(EStat.Select, 10, 1000));
                EditorUtility.SetDirty(progression);
            }
        }
    }
}