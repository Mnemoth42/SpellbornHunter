using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace TkrainDesigns.Stats
{
    public interface IDamageCharacteristicProvider
    {
        public IEnumerable<DamageCharacteristic> GetDefensiveCharacteristics();
    }

    public enum DamageType
    {
        Normal,
        Blunt,
        Slashing,
        Piercing,
        Freezing,
        Flaming,
        Nature,
        Air,
        Light,
        Dark
    }

    [System.Serializable]
    public class DamageCharacteristic : DrawableHelperClass
    {
        [SerializeField] DamageType damageType = DamageType.Normal;
        [SerializeField] float amount = 1; //0=none, .5=50%, 1=100%
        public DamageType GetDamageType() => damageType;
        public float GetAmount() => amount;

        public DamageCharacteristic()
        {
        }

        public DamageCharacteristic(DamageType type, float value = 1)
        {
            damageType = type;
            amount = value;
        }


        public override bool DrawInspector(ScriptableObject owner, out bool isDirty)
        {
        #if UNITY_EDITOR
            EditorGUILayout.BeginHorizontal();
            isDirty = false;
            EditorGUI.BeginChangeCheck();
            DamageType newType = (DamageType)EditorGUILayout.EnumPopup(damageType);
            float newAmount = EditorGUILayout.Slider(amount, 0, 2);
            if (EditorGUI.EndChangeCheck())
            {
                isDirty = true;
                Undo.RecordObject(owner, "Change Damage Characteristic");
                damageType = newType;
                amount = newAmount;
                EditorUtility.SetDirty(owner);
            }

            bool result = GUILayout.Button("-");
            if (result) isDirty = true;
            EditorGUILayout.EndHorizontal();
            return result;
        #else
				return true;
        #endif
        }
    }
}