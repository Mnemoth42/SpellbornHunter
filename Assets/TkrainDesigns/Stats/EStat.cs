using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TkrainDesigns.Stats
{
    public enum EStat
    {
        Select,
        Health,
        Mana,
        Strength,
        Intelligence,
        Wisdom,
        Agility,
        Constitution,
        Charisma,
        Offense,
        Defense,
        XPNeeded,
        XPGained,
        Coin,
        Traits,
        Bartering,
        SpellPower,
        SpellDef,
        ManaRegen,
        Regen,
        CritChance,
        MagicCrit,
        CritPercent,
        MagicCritPct,
        Dodge,
        Resist
    }

    public enum EStatDescription
    {
        boring,
        immortals,
        magus,
        ape,
        raven,
        owl,
        monkey,
        ox,
        charmer,
        veteran,
        tortoise,
        learned,
        learner,
        draco,
        educated,
        haggler,
        enchanter,
        resistant,
        conjurer,
        rested,
        punisher,
        destroyer,
        damager,
        obliterator,
        dancer,
        mentalist
    }
}