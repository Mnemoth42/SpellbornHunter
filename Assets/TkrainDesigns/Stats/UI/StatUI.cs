using System;
using System.Collections;
using System.Collections.Generic;
using GameDevTV.Inventories;
using TMPro;
using UnityEngine;

namespace TkrainDesigns.Stats.UI
{
    public class StatUI : MonoBehaviour
    {
        [SerializeField] private EStat stat;
        [SerializeField] private TextMeshProUGUI statName;
        [SerializeField] private TextMeshProUGUI statValue;

        private BaseStats stats;
        private TraitStore store;
        private Equipment equipment;

        private void Awake()
        {
            stats = GameObject.FindWithTag("Player").GetComponent<BaseStats>();
            store = stats.GetComponent<TraitStore>();
            equipment = stats.GetComponent<Equipment>();
            statName.text = $"{stat}";
            stats.onLevelUp += UpdateUI;
            store.onStatsChanged += UpdateUI;
            equipment.equipmentUpdated += UpdateUI;
        }

        private void Start()
        {
            UpdateUI();
        }

        void UpdateUI()
        {
            if (store.GetProposedModifierEffect(stat) > 0)
            {
                statValue.text =
                    $"<color=#ff8888>{stats.GetStatAsInt(stat, 0) + (int)store.GetProposedModifierEffect(stat)}</color>";
                statName.text = $"<color=#ff8888>{stat}</color>";
            }
            else
            {
                statValue.text = $"{stats.GetStatAsInt(stat, 0)}";
                statName.text = $"{stat}";
            }
        }

        private void OnValidate()
        {
            statName.text = $"{stat}";
            gameObject.name = $"{stat}";
        }
    }
}