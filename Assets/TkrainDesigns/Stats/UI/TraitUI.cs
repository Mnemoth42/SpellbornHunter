using System;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace TkrainDesigns.Stats.UI
{
    public class TraitUI : MonoBehaviour
    {
        [SerializeField] private EStat stat;
        [SerializeField] private TextMeshProUGUI statName;
        [SerializeField] private TextMeshProUGUI statValue;
        [SerializeField] private Button addButton;
        [SerializeField] private Button subtractButton;

        private TraitStore store;

        private void Awake()
        {
            Debug.Assert(statName != null, $"{name} TraitUI does not have a StatName assigned.");
            Debug.Assert(statValue != null, $"{name} TraitUI does not have a StatValue assigned.");
            Debug.Assert(addButton != null, $"{name} TraitUI does not have an Add Button assigned");
            Debug.Assert(subtractButton != null, $"{name} TraitUI does not have a Subtract button assigned.");
            store = GameObject.FindWithTag("Player").GetComponent<TraitStore>();
            store.onStatsChanged += UpdateUI;
            addButton.onClick.AddListener(() => { store.AddPoint(stat); });
            subtractButton.onClick.AddListener(() => { store.SubtractPoint(stat); });
        }


        private void OnValidate()
        {
            if (statName != null) statName.text = $"{stat}";
        }

        private void OnEnable()
        {
            Debug.Assert(store, $"{name} TraitUI has not found the store.");
            UpdateUI();
        }

        private void UpdateUI()
        {
            if (statName) statName.text = $"{stat}";
            if (statValue) statValue.text = $"{store.GetProposedTrait(stat)}";
            if (addButton) addButton.interactable = store.CanAdd(stat);
            if (subtractButton) subtractButton.interactable = store.CanSubtract(stat);
        }
    }
}