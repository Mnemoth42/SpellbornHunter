using System;
using System.Collections;
using System.Collections.Generic;
using GameDevTV.Inventories;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace TkrainDesigns.Stats.UI
{
    public class TraitStoreUI : MonoBehaviour
    {
        [SerializeField] private TextMeshProUGUI unallocatePointsLabel;
        [SerializeField] private TextMeshProUGUI unallocatedPointsText;
        [SerializeField] private Button confirmButton;
        [SerializeField] private Button clearButton;

        private TraitStore store;
        private BaseStats stats;
        private Equipment equipment;

        private void Awake()
        {
            store = GameObject.FindWithTag("Player").GetComponent<TraitStore>();
            stats = store.GetComponent<BaseStats>();
            equipment = store.GetComponent<Equipment>();
            confirmButton.onClick.AddListener(() => { store.CommitPoints(); });
            clearButton.onClick.AddListener(() => { store.ClearPoints(); });
            store.onStatsChanged += UpdateUI;
            stats.onLevelUp += UpdateUI;
            equipment.equipmentUpdated += UpdateUI;
        }

        private void Start()
        {
            UpdateUI();
        }

        void UpdateUI()
        {
            unallocatedPointsText.enabled = unallocatePointsLabel.enabled = store.AvailablePoints() > 0;
            unallocatedPointsText.text = $"{store.AvailablePoints()}";
            confirmButton.gameObject.SetActive(store.CanConfirm());
            clearButton.gameObject.SetActive(store.CanConfirm());
        }
    }
}