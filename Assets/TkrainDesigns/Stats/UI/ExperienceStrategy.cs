using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TkrainDesigns.Stats.UI
{
    [CreateAssetMenu(fileName = "ExperienceDataStrategy", menuName = "UIStrategies/Experience")]
    public class ExperienceStrategy : UIDataStrategy
    {
        public override void SubscribeToValueChanged(GameObject user, Action onChangeAction)
        {
            Experience experience = user.GetComponent<Experience>();
            experience.onGainExperience += onChangeAction;
            BaseStats stats = user.GetComponent<BaseStats>();
            stats.onLevelUp += onChangeAction;
        }

        public override void UnSubscribeToValueChanged(GameObject user, Action onChangeAction)
        {
            Experience experience = user.GetComponent<Experience>();
            experience.onGainExperience -= onChangeAction;
            BaseStats stats = user.GetComponent<BaseStats>();
            stats.onLevelUp -= onChangeAction;
        }

        public override float GetValueAsPercentage(GameObject user)
        {
            Experience experience = user.GetComponent<Experience>();
            BaseStats stats = user.GetComponent<BaseStats>();
            return experience.GetExperience() / stats.GetStat(EStat.XPNeeded);
        }

        public override int GetValue(GameObject user)
        {
            Experience experience = user.GetComponent<Experience>();
            return experience.GetExperience();
        }

        public override int GetMaxValue(GameObject user)
        {
            BaseStats stats = user.GetComponent<BaseStats>();
            return stats.GetStatAsInt(EStat.XPNeeded);
        }
    }
}