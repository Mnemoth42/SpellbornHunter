using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TkrainDesigns.Stats
{
    [CreateAssetMenu(fileName = "Progression", menuName = "Character/Progression")]
    public class Progression : ScriptableObject
    {
        [SerializeField] public List<StatFormula> formulas = new List<StatFormula>();

        private Dictionary<EStat, StatFormula> lookup = null;

        void BuildLookup()
        {
            if (lookup != null) return;
            lookup = new Dictionary<EStat, StatFormula>();
            foreach (var formula in formulas)
            {
                lookup[formula.stat] = formula;
            }
        }

        public float Evaluate(EStat stat, int level, float fallback = 1.0f)
        {
            BuildLookup();
            if (!lookup.ContainsKey(stat)) return fallback;
            return lookup[stat].Evaluate(level);
        }

        public int EvaluateAsInt(EStat stat, int level, int fallback = 1)
        {
            BuildLookup();
            if (!lookup.ContainsKey(stat)) return fallback;
            return lookup[stat].EvaluateAsInt(level);
        }
    }
}