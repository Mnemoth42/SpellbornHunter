using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TkrainDesigns.Stats
{
    public interface IModifierProvider
    {
        IEnumerable<float> GetAdditiveModifiers(EStat stat);
        IEnumerable<float> GetPercentageModifiers(EStat stat);
    }
}