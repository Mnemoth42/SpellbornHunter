using System.Collections;
using System.Collections.Generic;
using GameDevTV.Saving;
using Newtonsoft.Json.Linq;
using UnityEngine;
using UnityEngine.Events;

namespace TkrainDesigns.Stats
{
    [DisallowMultipleComponent]
    public class Experience : MonoBehaviour, ISaveable, IJsonSaveable
    {
        private int experience = 0;

        public int GetExperience() => experience;

        /// <summary>
        /// Only called when experience is awarded, never when it's spent.  Generally, this is for BaseStats use.
        /// UI should subscribe to EventExperienceChanged (a UnityEvent)
        /// </summary>
        public event System.Action onGainExperience;

        /// <summary>
        /// UI should subscribe to this event for general Experience Changed information.
        /// </summary>
        public UnityEvent EventExperienceChanged;

        /// <summary>
        /// This UnityEvent is called when Experience is gained, passing the experience Gain amount to the user.
        /// Useful for UI that displays amount of experience gained.  Only called on gains, not spent experience.
        /// </summary>
        public UnityEvent<int> EventExperienceGained;


        public void AwardExperience(int amount)
        {
            experience += amount;
            onGainExperience?.Invoke();
            EventExperienceChanged?.Invoke();
            EventExperienceGained?.Invoke(amount);
        }

        public int SpendExperience(int amount)
        {
            if (amount > experience) return -1;
            experience -= amount;
            EventExperienceChanged?.Invoke();
            return experience;
        }


        public JToken CaptureState()
        {
            return experience;
        }

        public void RestoreState(JToken state)
        {
            experience = (int)state;
            onGainExperience?.Invoke();
        }

        public JToken CaptureAsJToken()
        {
            return experience;
        }

        public void RestoreFromJToken(JToken state)
        {
            experience = (int)state;
            onGainExperience?.Invoke();
        }
    }
}