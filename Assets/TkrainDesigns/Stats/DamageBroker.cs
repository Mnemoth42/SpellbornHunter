using UnityEngine;

namespace TkrainDesigns.Stats
{
    public static class DamageBroker
    {
        /// <summary>
        /// Checks the target for defenses against Melee attacks and returns a modified damage amount after factoring
        /// in the targets defense against melee attacks in general, and the DamageCharacteristic.
        /// </summary>
        /// <param name="actualDamage">Amount of damage after user's Attack stats have been applied</param>
        /// <param name="offenseCharacteristic">The DamageCharacteristic of this particular assessment.</param>
        /// <param name="target">The recipient of the damage</param>
        /// <returns></returns>
        public static float AssessMelee(float damage, DamageCharacteristic offenseCharacteristic, GameObject target,
                                        GameObject dealer)
        {
            BaseStats targetStats = target.GetComponent<BaseStats>();
            BaseStats dealerStats = dealer.GetComponent<BaseStats>();
            
            float defenseCharacteristicValue = .5f;
            if (offenseCharacteristic.GetDamageType() == DamageType.Normal)
            {
                defenseCharacteristicValue = .5f;
            }
            else
            {
                foreach (DamageCharacteristic defenseCharacteristic in
                    target.GetComponents<IDamageCharacteristicProvider>())
                {
                    if (defenseCharacteristic.GetDamageType() == offenseCharacteristic.GetDamageType())
                    {
                        defenseCharacteristicValue += defenseCharacteristic.GetAmount();
                    }
                }
            }
            GameLog.WriteLn($"{damage:F0} type {offenseCharacteristic.GetDamageType()} {dealer.name}->{target.name} (Defense characteristic = {defenseCharacteristicValue}");
            float rawDefense;
            float rawOffense;
            float actualDamage =damage * offenseCharacteristic.GetAmount();

            //GameLog.WriteLn($"Offense element = {offenseCharacteristic.GetAmount():F2} Defense element {defenseCharacteristicValue:F2}. Corrected Damage {damage:F2}");
            float critChance = 0.0f;
            float critAmount = 0.0f;
            float dodge = 0.0f;
            float damageFloor = 0.0f;
            switch (offenseCharacteristic.GetDamageType())
            {
                case DamageType.Air:
                case DamageType.Dark:
                case DamageType.Flaming:
                case DamageType.Light:
                case DamageType.Freezing:
                case DamageType.Nature:
                    critChance = dealerStats.GetStat(EStat.MagicCrit, 0f)/100;
                    critAmount = dealerStats.GetStat(EStat.MagicCritPct, 0f);
                    dodge = targetStats.GetStat(EStat.Resist, 0);
                    rawDefense = targetStats.GetStat(EStat.SpellDef);
                    rawOffense = dealerStats.GetStat(EStat.SpellPower);
                    break;
                default:
                    critChance = dealerStats.GetStat(EStat.CritChance, 0f)/100;
                    critAmount = dealerStats.GetStat(EStat.CritPercent, 0f);
                    dodge = targetStats.GetStat(EStat.Dodge, 0);
                    rawDefense = targetStats.GetStat(EStat.Defense);
                    rawOffense = dealerStats.GetStat(EStat.Offense);
                    damageFloor = 1.0f;
                    break;
            }

            GameLog.WriteLn($"Raw Offense: {rawOffense}, Crit: {critChance:P2}, Crit bonus: {critAmount:P2}, Dodge/Resistance: {dodge:F2}, Defense {rawDefense:F2}");
            if (critChance > 25) critChance = 25;
            if (Random.Range(0f, 100f) < critChance)
            {
                actualDamage *= (100 + critAmount) / 100;
                GameLog.WriteLn($"Critical hit! {actualDamage}");
            }

            rawDefense *= defenseCharacteristicValue;
            if (rawDefense <= 1) rawDefense = 1;
            rawOffense *= offenseCharacteristic.GetAmount();
            actualDamage += rawOffense;
            float defenseDivider = rawDefense / damage;
            actualDamage /= defenseDivider;

            actualDamage = Mathf.Max(actualDamage, damageFloor);
            GameLog.WriteLn($"Adjusted defense = {rawDefense:F2}, adjusted damage = {actualDamage:F0}");
            return actualDamage;
        }
    }
}