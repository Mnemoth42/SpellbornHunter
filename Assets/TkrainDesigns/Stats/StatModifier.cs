using System;
using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
using UnityEditor.UIElements;
using UnityEngine.UIElements;
#endif

namespace TkrainDesigns.Stats
{
    [System.Serializable]
    public class StatModifier : DrawableHelperClass
    {
        [SerializeField] private EStat stat;
        [SerializeField] private float amount;

        public EStat GetStat() => stat;
        public float GetAmount() => amount;

        public StatModifier()
        {
            stat = EStat.Select;
            amount = 0;
        }

        public StatModifier(EStat stat, float amount)
        {
            this.stat = stat;
            this.amount = amount;
        }
    #if UNITY_EDITOR
        public void SetStat(EStat value, ScriptableObject owner)
        {
            if (value == stat) return;
            Undo.RecordObject(owner, "Change Stat");
            stat = value;
            EditorUtility.SetDirty(owner);
        }

        public void SetAmount(float value, ScriptableObject owner)
        {
            if (Math.Abs(value - amount) < .01f) return;
            Undo.RecordObject(owner, "Change Value");
            amount = value;
            EditorUtility.SetDirty(owner);
        }



        
    #endif
        public override bool DrawInspector(ScriptableObject owner, out bool isDirty)
        {
            isDirty = false;
        #if UNITY_EDITOR

            EditorGUILayout.BeginHorizontal();
            var oldStat = stat;
            float oldAmount = amount;
            DrawEnum(ref stat, owner, "Change Stat");
            DrawFloatAsIntSlider(ref amount, -100, 100, owner, "Change value");
            bool result = GUILayout.Button("-");
            if (result || oldStat != stat || amount != oldAmount) isDirty = true;
            EditorGUILayout.EndHorizontal();
            return result;
        #else
            return true;
        #endif
            
            
        }
    }
}