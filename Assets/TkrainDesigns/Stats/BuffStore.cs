using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TkrainDesigns.Stats
{
    [DisallowMultipleComponent]
    public class BuffStore : MonoBehaviour, IModifierProvider
    {
        private List<Modifier> additiveModifiers = new List<Modifier>();
        private List<Modifier> percentageModifiers = new List<Modifier>();
        public event System.Action OnModifiersChanged;

        /// <summary>
        /// This is the public interface to add a modifier.  Once the modifier is added, it will start it's own
        /// countdown timer, and destroy itself (by removing itself from the list) at the end of the duration.
        /// </summary>
        /// <param name="mod"></param>
        /// <param name="percentage"></param>
        public void AddModifier(Modifier mod, bool percentage = false)
        {
            //determine which list 
            List<Modifier> ModifierList = percentage ? percentageModifiers : additiveModifiers;
            //Add modifier to the list
            ModifierList.Add(mod);
            //We have to Start the Coroutine from here so that we have a GameObject to tie it to.
            //The callback method will remove the mod from the list when the countdown has completed.
            StartCoroutine(mod.Countdown(() => { RemoveModifier(mod, percentage); }));
            OnModifiersChanged?.Invoke();
        }

        void RemoveModifier(Modifier mod, bool percentage)
        {
            List<Modifier> ModifierList = percentage ? percentageModifiers : additiveModifiers;
            ModifierList.Remove(mod);
            OnModifiersChanged?.Invoke();
        }

        public IEnumerable<float> GetAdditiveModifiers(EStat stat)
        {
            foreach (Modifier mod in additiveModifiers)
            {
                if (mod.GetStat() == stat) yield return mod.GetValue();
            }
        }

        public IEnumerable<float> GetPercentageModifiers(EStat stat)
        {
            foreach (Modifier mod in percentageModifiers)
            {
                if (mod.GetStat() == stat) yield return mod.GetValue();
            }
        }
    }

    [System.Serializable]
    public class Modifier:UnityEngine.Object
    {
        [SerializeField] private EStat stat;
        [SerializeField] private float value;
        [SerializeField] private float duration;

        private float remainingDuration;

        public float GetValue() => value;
        public EStat GetStat() => stat;
        public float GetRemainingDuration() => remainingDuration;
        public float GetDuration() => duration;

        /// <summary>
        /// This Constructor allows you to build a modifier from parameters.  
        /// </summary>
        /// <param name="_stat"></param>
        /// <param name="_value"></param>
        /// <param name="_duration"></param>
        public Modifier(EStat _stat, float _value, float _duration)
        {
            stat = _stat;
            value = _value;
            duration = _duration;
        }

        /// <summary>
        /// This constructor allows you to create a new Modifier based on a modifier serialized within an Ability.
        /// Rather than passing the modifier directly from the Ability SO (this will risk the modifier's duration being
        /// modified, pass new Modifier(myModifier) and it will create a new temporary modifier to be used by the
        /// BuffStore.
        /// </summary>
        /// <param name="modifier">The Modifier we wish to clone</param>
        public Modifier(Modifier modifier)
        {
            stat = modifier.stat;
            value = modifier.value;
            duration = modifier.duration;
        }


        /// <summary>
        /// Run this coroutine from a Monobehavior or it will never tick.  
        /// </summary>
        /// <param name="callback">Method to clean up modifier when done</param>
        /// <returns></returns>
        public IEnumerator Countdown(System.Action callback)
        {
            remainingDuration = duration;
            while (remainingDuration > 0)
            {
                remainingDuration -= Time.deltaTime;
                yield return new WaitForEndOfFrame();
            }

            callback?.Invoke();
        }
    }
}