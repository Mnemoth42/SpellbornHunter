using UnityEditor;
using UnityEngine;

namespace TkrainDesigns.Stats
{
    [System.Serializable]
    public class StatRequirement : DrawableHelperClass
    {
        [SerializeField] private EStat stat;
        [SerializeField] private int level;

        /// <summary>
        /// Returns true if the required stat on the provided GameObject's BaseStats is at or above the required level.
        /// </summary>
        /// <param name="owner"></param>
        /// <returns></returns>
        public bool Evaluate(GameObject owner, int itemLevel = 1)
        {
            TraitStore trait = owner.GetComponent<TraitStore>();
            return Evaluate(trait, itemLevel);
        }

        /// <summary>
        /// Returns true if the required stat on the provided BaseStats is at or above the required level.
        /// </summary>
        /// <param name="trait"></param>
        /// <returns></returns>
        public bool Evaluate(TraitStore trait, int itemLevel = 1)
        {
            if (stat == EStat.Select) return true; //weed out bad requirements.
            return trait.GetTrait(stat) >= level + (itemLevel / 3);
        }

        public void LogReason()
        {
            GameLog.WriteLn(
                $"<color=#888888>This item requires <color=#ff8888>{stat}</color> level <color=#ff8888>{level}</color> to equip.</color>");
        }

        public override string ToString()
        {
            return AdjustedToString();
        }

        public string AdjustedToString(int itemLevel = 1)
        {
            if (stat == EStat.Select) return "<color=#ff8000>Requirement Stat Misconfigured</color>";
            int requiredLevel = level + itemLevel / 3;
            return $"Requires <color=#8888ff>{stat}</color> level <color=#8888ff>{requiredLevel}</color>";
        }

    #region Editor

        public EStat GetStat() => stat;
        public int GetLevel() => level;
    #if UNITY_EDITOR
        public void SetStat(EStat stat, ScriptableObject owner)
        {
            Undo.RecordObject(owner, "Change Stat");
            this.stat = stat;
            EditorUtility.SetDirty(owner);
        }

        public void SetLevel(int level, ScriptableObject owner)
        {
            Undo.RecordObject(owner, "Change Stat Level");
            this.level = level;
            EditorUtility.SetDirty(owner);
        }
        
        
        
    #endif
        
        public override bool DrawInspector(ScriptableObject owner, out bool isDirty)
        {
        #if UNITY_EDITOR
            EditorGUILayout.BeginHorizontal();
            isDirty = false;
            EditorGUI.BeginChangeCheck();
            EStat newStat = (EStat)EditorGUILayout.EnumPopup(stat);
            int newLevel = EditorGUILayout.IntSlider(level, 1, 200);
            if (EditorGUI.EndChangeCheck())
            {
                isDirty = true;
                Undo.RecordObject(owner, "Change Required Stat");
                stat = newStat;
                level = newLevel;
                EditorUtility.SetDirty(owner);
            }


            bool result = GUILayout.Button("-");
            if (result) isDirty = true;
            EditorGUILayout.EndHorizontal();
            return result;
        #else
				return true;
        #endif
        }

    #endregion
    }
}