using System.Collections;
using System.Collections.Generic;
using TkrainDesigns.Stats;
using UnityEditor;
using UnityEditor.UIElements;
using UnityEngine;
using UnityEngine.UIElements;

[System.Serializable]
public struct NewStatModifier
{
    [SerializeField] private EStat stat;
    [SerializeField] private float amount;

    public EStat GetStat() => stat;
    public float GetAmount() => amount;
    
    #if UNITY_EDITOR
        public void SetStat(EStat value)
        {
            stat = value;
        }

        public void SetAmount(float value)
        {
            amount = value;
        }

        
        
    #endif
}
