using System;
using System.Collections.Generic;
using Animancer;
using TkrainDesigns.Stats;
using UnityEngine;
using UnityEngine.Events;
using Random = UnityEngine.Random;


namespace TkrainDesigns.Respawnables
{
    public class RespawnManager : MonoBehaviour, ISetLevel
    {
        [SerializeField] private GameObject mobPrefab;
        [SerializeField] private GameObject awakenParticlePrefab;
        [SerializeField] private float delay = 2.0f;
        private GameObject mob;
        private bool isRespawning=true;
        public UnityEvent onRespawn;
        private DungeonManager dungeonManager;
        private BaseStats player;

        private int level = 1;
        private int playerLevel = 1;

        public void SetLevel(int newLevel, int newPlayerLevel)
        {
            level = newLevel;
            playerLevel = newPlayerLevel;
        }

        private void Awake()
        {
            dungeonManager = FindObjectOfType<DungeonManager>();
            dungeonManager.OnDungeonLoadedAndSurfaceFinished += Respawn;
            player = GameObject.FindWithTag("Player").GetComponent<BaseStats>();
        }

        private void OnDestroy()
        {
            dungeonManager.OnDungeonLoadedAndSurfaceFinished -= Respawn;
        }

        private void Start()
        {
            if (dungeonManager)
            {
                playerLevel = player.GetLevel();
                level = playerLevel - 1 + Mathf.Sqrt(playerLevel).ToInt();
            }
            //Respawn();
        }


        private void Update()
        {
            if (mob || isRespawning) return;
           
            isRespawning = true;
            Invoke(nameof(Respawn), delay);
        }

        public void Respawn()
        {
            if (mobPrefab == null)
            {
                Debug.LogError($"Mob Generator {name} has no prefab attached!");
                return;
            }

            ClearTransformAndSpawnMob();
            SpawnParticle();
            isRespawning = false;
        }

        private void SpawnParticle()
        {
            if (awakenParticlePrefab)
            {
                GameObject awakenParticle =
                    Instantiate(this.awakenParticlePrefab, transform.position, Quaternion.identity);
                Destroy(awakenParticle.gameObject, 2);
            }
        }

        private void ClearTransformAndSpawnMob()
        {
            var tr = transform;
            //Clear any existing children.  This allows you to put a place holder mob on the generator.
            foreach (Transform child in tr) Destroy(child.gameObject);


            mob = Instantiate(mobPrefab, tr.position, tr.rotation);
            mob.transform.SetParent(transform);
            BaseStats playerStats = GameObject.FindWithTag("Player").GetComponent<BaseStats>();
            playerLevel = playerStats.GetLevel();
            if (playerLevel > level) (playerLevel, level) = (level, playerLevel);
            BaseStats stats = mob.GetComponent<BaseStats>();
            if (stats) stats.SetLevel(WeightedRandom(playerLevel, level + 1));
        }

        int WeightedRandom(int min, int max)
        {
            List<int> chances = new List<int>();
            int rolling = 0;
            for (int i = max; i >= min; i--, rolling++)
            for (int j = 0; j < rolling; j++)
                chances.Add(i);

            return chances[Random.Range(0, chances.Count)];
        }
    }
}