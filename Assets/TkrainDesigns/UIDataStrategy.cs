using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TkrainDesigns
{
    public abstract class UIDataStrategy : ScriptableObject
    {
        public abstract void SubscribeToValueChanged(GameObject user, System.Action onChangeAction);
        public abstract void UnSubscribeToValueChanged(GameObject user, System.Action onChangeAction);
        public abstract float GetValueAsPercentage(GameObject user);
        public abstract int GetValue(GameObject user);
        public abstract int GetMaxValue(GameObject user);
    }
}