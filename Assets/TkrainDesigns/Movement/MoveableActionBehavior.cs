using System.Collections;
using System.Collections.Generic;
using TkrainDesigns.Attributes;
using UnityEngine;

namespace TkrainDesigns.Movement
{
    [RequireComponent(typeof(Mover))]
    [RequireComponent(typeof(ActionScheduler))]
    public abstract class
        MoveableActionBehavior<T> : MonoBehaviour, IAction where T : MonoBehaviour
    {
        protected Health health;
        protected Mover mover;
        protected ActionScheduler actionScheduler;
        protected T target = null;
        protected float acceptanceRadius = 3.0f;
        private Dictionary<string, float> Cooldowns = new Dictionary<string, float>();


        protected virtual void Awake()
        {
            health = GetComponent<Health>();
            mover = GetComponent<Mover>();
            actionScheduler = GetComponent<ActionScheduler>();
        }

        public virtual void StartAction(T newTarget)
        {
            actionScheduler.StartAction(this);
            target = newTarget;
        }

        /// <summary>
        /// In most cases, nothing should be needed to add to Update.  You can override Update (call base.Update())
        /// </summary>
        protected virtual void Update()
        {
            if (!health.IsAlive())
            {
                actionScheduler.CancelCurrentAction();
                enabled = false;
                return;
            }

            UpdateTimers();
            if (target == null) return;
            if (!IsValidAction()) return;
            if (Vector3.Distance(transform.position, target.transform.position) > acceptanceRadius)
            {
                mover.MoveTo(target.transform.position);
                HandleOutOfRange();
            }
            else
            {
                mover.Stop();
                Perform();
            }
        }

        protected abstract void Perform();

        /// <summary>
        /// Override to manage things like canceling an animation state if the character moves out of range.
        /// </summary>
        protected virtual void HandleOutOfRange()
        {
        }


        protected void SetAcceptanceRadius(float value)
        {
            acceptanceRadius = value;
        }

        protected void SetTimer(string key, float value)
        {
            Cooldowns[key] = value;
        }

        protected bool IsCooldownExpired(string key)
        {
            return !Cooldowns.ContainsKey(key);
        }

        void UpdateTimers()
        {
            if (Cooldowns.Count == 0) return;
            List<string> keysToClear = new List<string>();
            foreach (string key in Cooldowns.Keys)
            {
                keysToClear.Add(key);
            }

            foreach (string key in keysToClear)
            {
                Cooldowns[key] -= Time.deltaTime;
                if (Cooldowns[key] < 0) Cooldowns.Remove(key);
            }
        }

        /// <summary>
        /// Override this method to implement custom checks, for example if T is a Health, you may wish to check to see
        /// if the target is alive.
        /// </summary>
        /// <returns></returns>
        protected virtual bool IsValidAction()
        {
            return true;
        }

        public virtual void Cancel()
        {
            //Debug.Log($"{name} cancel()");
            target = null;
        }
    }
}