using System;
using System.Collections;
using System.Collections.Generic;
using DunGen.DungeonCrawler;
using GameDevTV.Saving;
using Newtonsoft.Json.Linq;
using UnityEngine;
using UnityEngine.AI;

namespace TkrainDesigns.Movement
{
    [RequireComponent(typeof(NavMeshAgent))]
    [RequireComponent(typeof(ActionScheduler))]
    [RequireComponent(typeof(Animator))]
    [DisallowMultipleComponent]
    public class Mover : MonoBehaviour, IAction, ISaveable
    {
        [SerializeField] private bool restorePosition;
        [SerializeField] private bool restoreRotation;
        [SerializeField] private bool restoreScale;
        private DungeonCrawlerNavMeshAdapter adapter;

        private NavMeshAgent agent;
        private ActionScheduler actionScheduler;
        private Animator anim;
        private static readonly int Speed = Animator.StringToHash("Speed");

        private void Awake()
        {
            agent = GetComponent<NavMeshAgent>();
            actionScheduler = GetComponent<ActionScheduler>();
            anim = GetComponent<Animator>();
            adapter = FindObjectOfType<DungeonCrawlerNavMeshAdapter>();
            if (!adapter)
            {
                Debug.Log("Adapter not found");
            }
            if (adapter && !adapter.SurfaceIsBaked)
            {
               // Debug.Log($"{name} waiting for NavMesh to bake");
                adapter.OnNavmeshSurfaceBaked += OnNavMeshSurfaceBaked;
            }
            else
            {
                surfaceIsBaked = true;
            }
        }

        private bool surfaceIsBaked;

        void OnNavMeshSurfaceBaked()
        {
           // Debug.Log($"Surface is Baked");
            surfaceIsBaked = true;
            adapter.OnNavmeshSurfaceBaked -= OnNavMeshSurfaceBaked;
        }
        
        public void Cancel()
        {
            agent.isStopped = true;
        }

        public void StartMoveAction(Vector3 destination)
        {
            actionScheduler.StartAction(this);
            MoveTo(destination);
        }

        public void MoveTo(Vector3 destination)
        {
            if (!agent.isOnNavMesh) return;
            agent.destination = destination;
            agent.isStopped = false;
        }

        private void Update()
        {
            if (!surfaceIsBaked) return;
            if (!agent.isStopped && agent.remainingDistance < 1.0f) agent.isStopped = true; 
            if (!agent.isOnNavMesh) return;
            if (agent.isStopped) anim.SetFloat(Speed, 0);
            anim.SetFloat(Speed, agent.velocity.magnitude);
        }

        public void Stop()
        {
            Cancel();
        }

        void FootL()
        {
        }

        void FootR()
        {
        }

        public JToken CaptureState()
        {
            JObject state = new JObject();
            IDictionary<string, JToken> stateDict = state;
            stateDict["position"] = transform.position.ToToken();
            stateDict["rotation"] = transform.eulerAngles.ToToken();
            stateDict["scale"] = transform.localScale.ToToken();
            return state;
        }

        public void RestoreState(JToken state)
        {
            IDictionary<string, JToken> stateDict = (JObject)state;
            if (restorePosition) transform.position = stateDict["position"].ToVector3();
            if (restoreRotation) transform.eulerAngles = stateDict["rotation"].ToVector3();
            if (restoreScale) transform.localScale = stateDict["scale"].ToVector3();
        }
    }
}