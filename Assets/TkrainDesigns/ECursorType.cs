namespace TkrainDesigns
{
    public enum ECursorType
    {
        None,
        UI,
        Combat,
        Movement,
        Dialogue,
        Shop,
        Loot
    }
}