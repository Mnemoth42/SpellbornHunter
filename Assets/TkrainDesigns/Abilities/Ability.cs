using System.Collections.Generic;
using System.Text;
using GameDevTV.Inventories;
using TkrainDesigns.Attributes;
using UnityEditor;
using UnityEngine;

namespace TkrainDesigns.Abilities
{
    [CreateAssetMenu(fileName = "My Ability", menuName = "Abilities/Ability", order = 0)]
    public class Ability : ActionItem
    {
        [SerializeField] TargetingStrategy targetingStrategy;
        [SerializeField] List<FilterStrategy> filterStrategies = new List<FilterStrategy>();
        [SerializeField] List<EffectStrategy> effectStrategies = new List<EffectStrategy>();
        [SerializeField] private CastingStrategy castingStrategy = null;
        [SerializeField] private List<EffectStrategy> postCastStrategy = new List<EffectStrategy>();
        [SerializeField] float cooldownTime = 0;
        [SerializeField] private string cooldownToken = "Global";
        [SerializeField] int manaCost = 0;
        [SerializeField] private int healthEffect = 0;
        [SerializeField] private int castingRange = -1;

        public override bool Use(GameObject user, System.Action callback)
        {
            Mana mana = user.GetComponent<Mana>();
            if (mana.GetCurrentValue() < manaCost)
            {
                Debug.Log($"Insufficient Mana {manaCost}/{mana.GetCurrentValue()}");
                return false;
            }

            CooldownStore cooldownStore = user.GetComponent<CooldownStore>();
            if (cooldownStore.GetTimeRemaining(cooldownToken) > 0)
            {
                Debug.Log($"Cooldown remaining {cooldownStore.GetTimeRemaining(cooldownToken)}");
                return false;
            }

            AbilityData data = new AbilityData(user)
                               {
                                   HealthEffect = healthEffect
                               };
            data.SetActionStoreCallback(callback);
            ActionScheduler actionScheduler = user.GetComponent<ActionScheduler>();
            actionScheduler.StartAction(data);
            if (!targetingStrategy)
            {
                Debug.Log($"{name} has no targetting strategy");
                return false;
            }

            targetingStrategy.StartTargeting(data,
                () => { TargetAquired(data); });

            return true;
        }

        private void TargetAquired(AbilityData data)
        {
            if (data.IsCancelled()) return;
            int targetCount = data.CountTargets();
            if (targetCount > 0)
            {
                foreach (var filterStrategy in filterStrategies)
                {
                    data.SetTargets(filterStrategy.Filter(data.GetTargets(), data));
                }

                if (data.CountTargets() == 0)
                {
                    Debug.Log($"{data.GetUser().name} {name} - No Targets");
                    return;
                }
            }

            if (data.GetUser().transform.position.Distance(data.GetTargetedPoint()) > castingRange)
            {
                data.GetUser().GetComponent<AbilityPerformer>().MoveToCastingRange(data.GetTargetedPoint(),
                    castingRange,
                    () => { BeginPreEffectPhase(data); });
            }
            else
            {
                BeginPreEffectPhase(data);
            }
        }

        private void BeginPreEffectPhase(AbilityData data)
        {
            CooldownStore cooldownStore = data.GetUser().GetComponent<CooldownStore>();
            cooldownStore.StartCooldown(cooldownToken, cooldownTime);


            Mana mana = data.GetUser().GetComponent<Mana>();
            if (!mana.UseMana(manaCost)) return;

            foreach (var effect in effectStrategies)
            {
                effect.StartEffect(data, EffectFinished);
            }

            if (castingStrategy)
            {
                castingStrategy.Cast(data, () => { CastFinished(data); });
            }
            else
            {
                data.GetActionStoreCallback()?.Invoke();
                foreach (EffectStrategy effectStrategy in postCastStrategy)
                {
                    effectStrategy.StartEffect(data, EffectFinished);
                }
            }
        }

        private void CastFinished(AbilityData data)
        {
            data.GetActionStoreCallback()?.Invoke();
            foreach (var effect in postCastStrategy)
            {
                effect.StartEffect(data, EffectFinished);
            }
        }

        private void EffectFinished()
        {
        }

        public override string GetDescription()
        {
            StringBuilder builder = new StringBuilder(base.GetDescription());
            if (manaCost > 0)
            {
                builder.Append($"\nMana Cost {manaCost}");
            }

            builder.Append($"\nCooldown {cooldownTime}");
            if (castingRange > 0)
            {
                builder.Append($"\nRange {castingRange}");
            }

            return builder.ToString();
        }

    #if UNITY_EDITOR
        private bool drawAbilityData = true;
        private bool drawFilterData = true;
        private bool drawPreCastData = true;
        private bool drawPostCastData = true;

        public override void DrawInspector()
        {
            base.DrawInspector();
            if (!DrawSection(ref drawAbilityData, "Ability Data")) return;
            BeginIndent();
            DrawIntSlider(ref manaCost, 0, 200, "Mana Cost", "How much mana is required to cast this spell.");
            DrawFloatAsIntSlider(ref cooldownTime, 0, 10000, "Cooldown",
                "How much time must elapse before this action can be taken again.");
            DrawTextField(ref cooldownToken, "Cooldown token",
                "Used to assign cooldown to proper category.  Using the same cooldowntoken between abilities will make them share a cooldown.");
            DrawIntSlider(ref healthEffect, 0, 1000, "Health Effect",
                "How much target health will be affected.  (positive for both damage and health effects");
            DrawIntSlider(ref castingRange, -1, 30, "Casting Range",
                "Character will move to this distance from target point to cast.  -1 for no movement.");
            TargetingStrategy newStrategy = SelectScriptableObject("Targeting", targetingStrategy);
            if (newStrategy != targetingStrategy)
            {
                SetUndo("Change Targeting");
                targetingStrategy = newStrategy;
            }

            if (DrawSection(ref drawFilterData, "Filters"))
            {
                BeginIndent();
                DrawStrategyList(filterStrategies, "Filter");
                EndIndent();
            }

            if (DrawSection(ref drawPreCastData, "Pre Cast Effects"))
            {
                BeginIndent();
                DrawStrategyList(effectStrategies, "Pre Cast Effect");
                EndIndent();
            }

            CastingStrategy cStrategy = SelectScriptableObject("Casting Trigger", castingStrategy);
            if (cStrategy != castingStrategy)
            {
                SetUndo("Change Casting Strategy");
                castingStrategy = cStrategy;
            }

            if (DrawSection(ref drawPostCastData, "Post Cast Effects"))
            {
                BeginIndent();
                DrawStrategyList(postCastStrategy, "Post Cast Effect");
                EndIndent();
            }

            EndIndent();
        }

        private void DrawStrategyList<T>(List<T> list, string caption) where T : RetrievableScriptableObject
        {
            int itemToRemove = -1;
            for (int i = 0; i < list.Count; i++)
            {
                EditorGUILayout.BeginHorizontal();
                T newStrategy = SelectScriptableObject($"{caption}:", list[i]);
                if (newStrategy != list[i])
                {
                    SetUndo($"Change {caption}");
                    list[i] = newStrategy;
                }

                if (GUILayout.Button("-")) itemToRemove = i;
                EditorGUILayout.EndHorizontal();
            }

            if (itemToRemove >= 0)
            {
                SetUndo($"Remove {caption}");
                list.RemoveAt(itemToRemove);
            }

            if (GUILayout.Button($"Add {caption}"))
            {
                SetUndo($"Add {caption}");
                list.Add(null);
            }
        }

    #endif
        public string GetCooldownToken()
        {
            return cooldownToken;
        }
    }
}