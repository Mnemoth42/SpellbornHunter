using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TkrainDesigns.Abilities
{
    public class AbilityData : IAction
    {
        GameObject user;
        Vector3 targetedPoint;
        IEnumerable<GameObject> targets;
        bool cancelled = false;
        private System.Action actionStoreCallback;
        private System.Action abilityPerformerCallback;

        public void SetAbilityPerformerCallback(System.Action callback)
        {
            abilityPerformerCallback = callback;
        }

        public int HealthEffect { get; set; } = 1;

        public AbilityData(GameObject user)
        {
            this.user = user;
        }

        public IEnumerable<GameObject> GetTargets()
        {
            return targets;
        }

        public void SetTargets(IEnumerable<GameObject> targets)
        {
            this.targets = targets;
        }

        public void SetActionStoreCallback(System.Action callback) => actionStoreCallback = callback;
        public System.Action GetActionStoreCallback() => actionStoreCallback;

        public Vector3 GetTargetedPoint()
        {
            return targetedPoint;
        }

        public void SetTargetedPoint(Vector3 targetedPoint)
        {
            this.targetedPoint = targetedPoint;
        }

        public GameObject GetUser()
        {
            return user;
        }

        public void StartCoroutine(IEnumerator coroutine)
        {
            user.GetComponent<MonoBehaviour>().StartCoroutine(coroutine);
        }

        public void Cancel()
        {
            abilityPerformerCallback?.Invoke();
            cancelled = true;
        }

        public bool IsCancelled()
        {
            return cancelled;
        }

        public int CountTargets()
        {
            int result = 0;
            foreach (GameObject target in targets)
            {
                result += 1;
            }

            return result;
        }
    }
}