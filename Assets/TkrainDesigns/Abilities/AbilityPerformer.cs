using System;
using TkrainDesigns.Movement;
using UnityEngine;

namespace TkrainDesigns.Abilities
{
    [DisallowMultipleComponent]
    public class AbilityPerformer : MonoBehaviour
    {
        private System.Action castCallback = null;
        private Vector3 target;
        private float targetRange;
        private System.Action moveCallback = null;

        private Mover mover;

        private void Awake()
        {
            mover = GetComponent<Mover>();
        }

        public void WaitForCast(System.Action callback)
        {
            castCallback = callback;
        }

        void Cast()
        {
            castCallback?.Invoke();
            castCallback = null;
        }

        public System.Action MoveToCastingRange(Vector3 targetLocation, float range, System.Action callback)
        {
            target = targetLocation;
            targetRange = range;
            moveCallback = callback;
            return Cancel;
        }

        private void Update()
        {
            if (moveCallback != null)
            {
                if (target.Distance(transform.position) > targetRange)
                {
                    mover.MoveTo(target);
                }
                else
                {
                    mover.Stop();
                    moveCallback?.Invoke();
                    moveCallback = null;
                }
            }
        }

        void Cancel()
        {
            castCallback = null;
            moveCallback = null;
        }
    }
}