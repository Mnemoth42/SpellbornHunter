using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace TkrainDesigns.Abilities
{
    [CreateAssetMenu(fileName = "ClosestToTargetPointFilter", menuName = "Abilities/Filters/ClosestToTargetPoint")]
    public class ClosestToTargetPoint : FilterStrategy
    {
        public override IEnumerable<GameObject> Filter(IEnumerable<GameObject> objectsToFilter, AbilityData data)
        {
            var go = objectsToFilter.OrderBy(t => t.transform.position.Distance(data.GetTargetedPoint()))
                                    .FirstOrDefault();
            if (go) yield return go;
        }
    }
}