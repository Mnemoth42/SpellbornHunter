using System.Collections.Generic;
using TkrainDesigns.Attributes;
using UnityEngine;

namespace TkrainDesigns.Abilities
{
    [CreateAssetMenu(menuName = "Abilities/Filters/Alive", fileName = "Alive")]
    public class AliveFilter : FilterStrategy
    {
        [SerializeField] private bool negate = false;

        public override IEnumerable<GameObject> Filter(IEnumerable<GameObject> objectsToFilter, AbilityData data)
        {
            foreach (GameObject o in objectsToFilter)
            {
                if (o.TryGetComponent(out Health health))
                {
                    if (health.IsAlive() != negate)
                    {
                        yield return o;
                    }
                }
            }
        }
    }
}