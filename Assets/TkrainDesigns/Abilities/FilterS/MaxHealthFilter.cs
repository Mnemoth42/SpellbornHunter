using System.Collections;
using System.Collections.Generic;
using TkrainDesigns.Attributes;
using UnityEngine;

namespace TkrainDesigns.Abilities
{
    [CreateAssetMenu(menuName = "Abilities/Filters/Max Health", fileName = "Max Health")]
    public class MaxHealthFilter : FilterStrategy
    {
        [SerializeField] private bool negate = false;

        public override IEnumerable<GameObject> Filter(IEnumerable<GameObject> objectsToFilter, AbilityData data)
        {
            foreach (GameObject o in objectsToFilter)
            {
                if (o.TryGetComponent(out Health health))
                {
                    if ((health.GetValueAsPercentage() < 1f) != negate)
                    {
                        yield return o;
                    }
                }
            }
        }
    }
}