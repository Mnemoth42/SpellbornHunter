using System;
using System.Collections.Generic;
using GameDevTV.Inventories;
using TkrainDesigns.Abilities;
using UnityEngine;

namespace TkrainDesigns.Abilities
{
    public abstract class EffectStrategy : RetrievableScriptableObject
    {
        public abstract void StartEffect(AbilityData data, Action finished);

        public override string GetDisplayName()
        {
            return name;
        }

        public override string GetDescription()
        {
            return "";
        }
    }
}