using System;
using System.Collections.Generic;
using GameDevTV.Inventories;
using TkrainDesigns.Abilities;
using UnityEngine;

namespace TkrainDesigns.Abilities
{
    public abstract class TargetingStrategy : RetrievableScriptableObject
    {
        public abstract void StartTargeting(AbilityData data, Action finished);

        public override string GetDescription()
        {
            return "";
        }

        public override string GetDisplayName()
        {
            return name;
        }
    }
}