using System;
using System.Collections;
using System.Collections.Generic;
using GameDevTV.Inventories;
using GameDevTV.Saving;
using Newtonsoft.Json.Linq;
using TkrainDesigns.Abilities.Targeting;
using UnityEngine;

namespace TkrainDesigns.Abilities
{
    public class SpellStore : MonoBehaviour, IJsonSaveable, IItemStore
    {
        private List<Ability> abilities = new List<Ability>();

        public Ability GetAbilityAt(int location)
        {
            return location >= abilities.Count ? null : abilities[location];
        }

        public void AddAbility(Ability ability)
        {
            if (ability.IsStackable() || ability.isConsumable()) return;
            if (abilities.Contains(ability)) return;
            abilities.Add(ability);
        }

        public int Count => abilities.Count;
        
        public JToken CaptureAsJToken()
        {
            JArray state = new JArray();
            IList<JToken> stateArray = state;
            foreach (Ability ability in abilities)
            {
                stateArray.Add(ability.GetItemID());
            }
            return state;
        }

        public void RestoreFromJToken(JToken state)
        {
            abilities.Clear();
            JArray array = state as JArray;
            IList<JToken> stateArray = array;
            foreach (JToken token in stateArray)
            {
                InventoryItem item = InventoryItem.GetFromID(token.ToString());
                if (item is Ability ability)
                {
                    abilities.Add(ability);
                }
            }
        }

        public int AddItem(InventoryItem item, int quantity)
        {
            if (item is Ability ability && !ability.IsStackable() & !ability.isConsumable())
            {
                AddAbility(ability);
                return quantity;
            }
            return 0;
        }
    }
}