using System.Collections;
using System.Collections.Generic;
using GameDevTV.Inventories;
using UnityEngine;

namespace TkrainDesigns.Abilities
{
    [CreateAssetMenu(menuName = "Abilities/AnimationTrigger", fileName = "Ability")]
    public class CastingStrategy : RetrievableScriptableObject
    {
        [SerializeField] private string animationTrigger;

        public override string GetDisplayName()
        {
            return $"{animationTrigger}";
        }

        public override string GetDescription()
        {
            return $"triggers {animationTrigger}";
        }

        public virtual void Cast(AbilityData data, System.Action callback)
        {
            data.GetUser().GetComponent<Animator>().SetTrigger(animationTrigger);
            data.GetUser().GetComponent<AbilityPerformer>().WaitForCast(callback);
        }
    }
}