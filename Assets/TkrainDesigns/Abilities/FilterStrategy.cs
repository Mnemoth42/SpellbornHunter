using System;
using System.Collections.Generic;
using GameDevTV.Inventories;
using UnityEngine;

namespace TkrainDesigns.Abilities
{
    public abstract class FilterStrategy : RetrievableScriptableObject
    {
        public abstract IEnumerable<GameObject> Filter(IEnumerable<GameObject> objectsToFilter, AbilityData data);

        public override string GetDescription()
        {
            return "";
        }

        public override string GetDisplayName()
        {
            return name;
        }
    }
}