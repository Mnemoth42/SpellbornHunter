using System;
using System.Collections.Generic;
using GameDevTV.Inventories;
using TkrainDesigns.Abilities.Targeting;
using UnityEngine;

namespace TkrainDesigns.Abilities
{
    [DisallowMultipleComponent]
    public class CooldownStore : MonoBehaviour
    {
        Dictionary<string, float> cooldownTimers = new Dictionary<string, float>();
        Dictionary<string, float> initialCooldownTimes = new Dictionary<string, float>();

        void Update()
        {
            var keys = new List<string>(cooldownTimers.Keys);
            foreach (string token in keys)
            {
                cooldownTimers[token] -= Time.deltaTime;
                if (cooldownTimers[token] < 0)
                {
                    cooldownTimers.Remove(token);
                    initialCooldownTimes.Remove(token);
                }
            }
        }

        public void StartCooldown(string token, float cooldownTime)
        {
            cooldownTimers[token] = cooldownTime;
            initialCooldownTimes[token] = cooldownTime;
        }

        public float GetTimeRemaining(string token)
        {
            if (!cooldownTimers.ContainsKey(token))
            {
                return 0;
            }

            return cooldownTimers[token];
        }

        public float GetTimeRemaining(Ability ability)
        {
            return GetTimeRemaining(ability.GetCooldownToken());
        }

        public float GetFractionRemaining(string token)
        {
            if (token == null)
            {
                return 0;
            }

            if (!cooldownTimers.ContainsKey(token))
            {
                return 0;
            }

            return cooldownTimers[token] / initialCooldownTimes[token];
        }
    }
}