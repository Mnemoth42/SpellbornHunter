using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using DigitalRuby.ThunderAndLightning;
using TkrainDesigns.Attributes;
using TkrainDesigns.Combat;
using TkrainDesigns.Stats;
using UnityEngine;

namespace TkrainDesigns.Abilities
{
    [CreateAssetMenu(fileName = "LightningboltEffect", menuName = "Abilities/Effects/Bolt")]
    public class LightningBoltEffect : EffectStrategy
    {
        [SerializeField] private GameObject lightningBoltPrefab;

        public override void StartEffect(AbilityData data, Action finished)
        {
            data.GetUser().GetComponent<AbilityPerformer>().StartCoroutine(GenerateBolt(data, finished));
        }

        IEnumerator GenerateBolt(AbilityData data, Action finished)
        {
            Vector3 start = data.GetUser().GetComponent<Fighter>().GetHandTransform(true).transform.position;
            Vector3 end = start;
            List<GameObject> generators = new List<GameObject>();
            GameObject generator = Instantiate(lightningBoltPrefab);
            // Important, make sure this script is assigned properly, or you will get null ref exceptions.
            DigitalRuby.ThunderAndLightning.LightningBoltScript script =
                generator.GetComponent<DigitalRuby.ThunderAndLightning.LightningBoltScript>();

            int count = 5;
            float duration = 0.75f;
            float delay = 0.0f;
            int seed = 1860654769;
            System.Random r = new System.Random(seed);

            int generations = 5;
            float chaosFactor = 0.2f;
            float trunkWidth = .5f;
            float intensity = 1f;
            float glowIntensity = 0.1879804f;
            float glowWidthMultiplier = 4.0f;
            float forkedness = 0.13f;
            float singleDuration = Mathf.Max(1.0f / 30.0f, (duration / (float)count));
            float fadePercent = 0.15f;
            float growthMultiplier = 0f;
            System.Collections.Generic.List<LightningBoltParameters> paramList =
                new System.Collections.Generic.List<LightningBoltParameters>();
            foreach (GameObject o in data.GetTargets())
            {
                end = o.transform.position + Vector3.up;
                for (int i = 0; i < count; i++)
                {
                    DigitalRuby.ThunderAndLightning.LightningBoltParameters parameters =
                        new DigitalRuby.ThunderAndLightning.LightningBoltParameters
                        {
                            Start = start,
                            End = end,
                            Generations = generations,
                            LifeTime = (count == 1
                                ? singleDuration
                                : (singleDuration * (((float)r.NextDouble() * 0.4f) + 0.8f))),
                            Delay = delay,
                            ChaosFactor = chaosFactor,
                            ChaosFactorForks = chaosFactor,
                            TrunkWidth = trunkWidth,
                            GlowIntensity = glowIntensity,
                            GlowWidthMultiplier = glowWidthMultiplier,
                            Forkedness = forkedness,
                            Random = r,
                            FadePercent = fadePercent, // set to 0 to disable fade in / out
                            GrowthMultiplier = growthMultiplier
                        };
                    paramList.Add(parameters);
                    delay += (singleDuration * (((float)r.NextDouble() * 0.8f) + 0.4f));
                }

                start = end;
            }

            script.CreateLightningBolts(paramList);
            DamageCharacteristic characteristic = new DamageCharacteristic();

            yield return new WaitForSeconds(duration / 2f);
            foreach (GameObject gameObject in data.GetTargets())
            {
                if (gameObject.TryGetComponent(out Health health))
                {
                    health.TakeDamage(data.HealthEffect, data.GetUser(), new[] { characteristic });
                }

                yield return new WaitForSeconds(.05f);
            }

            Destroy(generator.gameObject);
        }
    }
}