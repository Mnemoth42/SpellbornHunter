using System;
using System.Collections.Generic;
using TkrainDesigns.Attributes;
using TkrainDesigns.Stats;
using UnityEngine;

namespace TkrainDesigns.Abilities.Effects
{
    [CreateAssetMenu(fileName = "Health Effect", menuName = "Abilities/Effects/Health", order = 0)]
    public class HealthEffect : EffectStrategy
    {
        [SerializeField] int healthChange;
        [SerializeField] private List<DamageCharacteristic> damageCharacteristcs;

        public override void StartEffect(AbilityData data, Action finished)
        {
            foreach (var target in data.GetTargets())
            {
                var health = target.GetComponent<Health>();
                if (health)
                {
                    if (healthChange < 0)
                    {
                        health.TakeDamage(-healthChange, data.GetUser(), damageCharacteristcs);
                    }
                    else
                    {
                        Debug.Log($"Healing for {healthChange}");
                        health.Heal(healthChange);
                    }
                }
            }

            finished();
        }
    }
}