using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TkrainDesigns.Attributes;
using TkrainDesigns.Combat;
using UnityEngine;

namespace TkrainDesigns.Abilities
{
    [CreateAssetMenu(fileName = "SetFighterTarget", menuName = "Abilities/Effects/Set Fighter Target")]
    public class FighterEffect : EffectStrategy
    {
        public override void StartEffect(AbilityData data, Action finished)
        {
            Fighter fighter = data.GetUser().GetComponent<Fighter>();
            foreach (GameObject go in data.GetTargets()
                                          .OrderBy(
                                              o => o.transform.position.Distance(data.GetUser().transform.position)))
            {
                Health health = go.GetComponent<Health>();
                if (health && health.IsAlive())
                {
                    fighter.StartAction(health);
                    finished?.Invoke();
                    return;
                }
            }

            finished?.Invoke();
        }
    }
}