using System;
using System.Collections;
using System.Collections.Generic;
using TkrainDesigns.Abilities;
using TkrainDesigns.Combat;
using UnityEngine;

[CreateAssetMenu(fileName = "FighterTarget", menuName = "Abilities/Targeting/Fighter Target")]
public class FighterTargeting : TargetingStrategy
{
    public override void StartTargeting(AbilityData data, Action finished)
    {
        Fighter fighter = data.GetUser().GetComponent<Fighter>();
        List<GameObject> targets = new List<GameObject>();
        if (fighter.GetTarget() != null)
        {
            targets.Add(fighter.GetTarget());
            data.SetTargets(targets);
            data.SetTargetedPoint(fighter.GetTarget().transform.position);
        }

        finished?.Invoke();
    }
}