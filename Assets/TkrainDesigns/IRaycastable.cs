using System.Collections;
using System.Collections.Generic;
using TkrainDesigns;
using UnityEngine;


public interface IRaycastable
{
    bool HandleRayCast(GameObject controller);
    int GetPriority();

    ECursorType GetCursorType();
}