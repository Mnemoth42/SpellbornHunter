using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TkrainDesigns.Attributes
{
    [CreateAssetMenu(fileName = "ManaDataStrategy", menuName = "UIStrategies/Mana")]
    public class ManaDataStrategy : UIDataStrategy
    {
        public override void SubscribeToValueChanged(GameObject user, Action onChangeAction)
        {
            Mana mana = user.GetComponent<Mana>();
            mana.onValueChanged += onChangeAction;
        }

        public override void UnSubscribeToValueChanged(GameObject user, Action onChangeAction)
        {
            Mana mana = user.GetComponent<Mana>();
            mana.onValueChanged -= onChangeAction;
        }

        public override float GetValueAsPercentage(GameObject user)
        {
            Mana mana = user.GetComponent<Mana>();
            return mana.GetValueAsPercentage();
        }

        public override int GetValue(GameObject user)
        {
            Mana mana = user.GetComponent<Mana>();
            return mana.GetCurrentValue();
        }

        public override int GetMaxValue(GameObject user)
        {
            Mana mana = user.GetComponent<Mana>();
            return mana.GetMaxValue();
        }
    }
}