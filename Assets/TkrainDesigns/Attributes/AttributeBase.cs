using GameDevTV.Saving;
using System;
using System.Collections.Generic;
using Newtonsoft.Json.Linq;
using TkrainDesigns.Stats;
using UnityEngine;

namespace TkrainDesigns.Attributes
{
    [RequireComponent(typeof(BaseStats))]
    public abstract class AttributeBase : MonoBehaviour, ISaveable, IJsonSaveable
    {
        private List<AttributeRecipe> contributingStats = new List<AttributeRecipe>();
        private List<AttributeRecipe> regeneratingStat = new List<AttributeRecipe>();

        protected event System.Action<int> onGainValue;
        protected event System.Action<int> onLoseValue;
        public event System.Action onValueChanged;

        protected BaseStats stats;
        private float currentValue = -1;

        protected int CurrentValue
        {
            get => currentValue.ToInt();
            set
            {
                float oldValue = currentValue;
                currentValue = value.Clamp(0, GetMaxValue());
                onValueChanged?.Invoke();
            }
        }

        [Serializable]
        protected struct AttributeRecipe
        {
            public EStat stat;
            public float weight;

            public AttributeRecipe(EStat stat, float weight)
            {
                this.stat = stat;
                this.weight = weight;
            }

            public float evaluate(float value) => value * weight;
        }

        protected virtual void Awake()
        {
            stats = GetComponent<BaseStats>();
            stats.onLevelUp += OnLevelUp;
        }

        protected virtual void Start()
        {
            if (currentValue < 0)
            {
                currentValue = GetMaxValue();
            }
        }

        void OnLevelUp()
        {
            CurrentValue = GetMaxValue();
        }

        public void AddRecipe(EStat stat, float weight)
        {
            contributingStats.Add(new AttributeRecipe(stat, weight));
        }

        protected void AddRegenerationRecipe(EStat stat, float weight)
        {
            regeneratingStat.Add(new AttributeRecipe(stat, weight));
        }


        public int GetMaxValue()
        {
            float result = 0.0f;
            foreach (float value in GatherMaxValuesFromRecipe())
            {
                result += value;
            }

            if (result <= 1.0f) result = 1.0f;
            return result.ToInt();
        }

        protected virtual bool CanRegenerate()
        {
            return true;
        }

        private float healBuffer = 0;

        private void Update()
        {
            if (!CanRegenerate()) return;
            float amount = 0.0f;
            int prior = currentValue.ToInt();
            foreach (float value in GatherRegenValueFromRecipe())
            {
                amount += value;
            }

            float regenAmount = currentValue * (amount / 1000.0f) * Time.deltaTime;
            //Debug.Log($"{name} HealAmount = {regenAmount}");
            healBuffer += regenAmount;
            if (healBuffer > 1)
            {
                CurrentValue += 1;
                healBuffer -= 1;
                onGainValue?.Invoke(1);
                onValueChanged?.Invoke();
            }
        }

        public int GetCurrentValue()
        {
            return currentValue.ToInt();
        }

        public int IncreaseCurrentValue(int amount)
        {
            currentValue += amount;
            currentValue.Clamp(0, GetMaxValue());
            onValueChanged?.Invoke();
            return currentValue.ToInt();
        }

        public int DecreaseCurrentValue(int amount)
        {
            currentValue -= amount;
            currentValue.Clamp(0, GetMaxValue());
            onValueChanged?.Invoke();
            return currentValue.ToInt();
        }

        public float GetValueAsPercentage()
        {
            return GetCurrentValue() / (float)GetMaxValue();
        }

        IEnumerable<float> GatherMaxValuesFromRecipe()
        {
            foreach (var recipe in contributingStats)
            {
                yield return recipe.evaluate(stats.GetStat(recipe.stat));
            }
        }

        IEnumerable<float> GatherRegenValueFromRecipe()
        {
            foreach (var recipe in regeneratingStat)
            {
                yield return recipe.evaluate(stats.GetStat(recipe.stat));
            }
        }

        public JToken CaptureState()
        {
            return currentValue;
        }

        public void RestoreState(JToken state)
        {
            currentValue = state.ToObject<float>();
        }

        public JToken CaptureAsJToken()
        {
            return currentValue;
        }

        public void RestoreFromJToken(JToken state)
        {
            currentValue = state.ToObject<float>();
            onValueChanged?.Invoke();
        }
    }
}