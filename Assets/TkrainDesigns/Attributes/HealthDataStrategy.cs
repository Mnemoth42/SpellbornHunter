using System;
using UnityEngine;

namespace TkrainDesigns.Attributes
{
    [CreateAssetMenu(fileName = "HealthDataStrategy", menuName = "UIStrategies/Health")]
    public class HealthDataStrategy : UIDataStrategy
    {
        public override void SubscribeToValueChanged(GameObject user, Action onChangeAction)
        {
            Health health = user.GetComponent<Health>();
            health.onValueChanged += onChangeAction;
            health.onDeath.AddListener(onChangeAction.Invoke);
        }

        public override void UnSubscribeToValueChanged(GameObject user, Action onChangeAction)
        {
            Health health = user.GetComponent<Health>();
            health.onValueChanged -= onChangeAction;
        }

        public override float GetValueAsPercentage(GameObject user)
        {
            Health health = user.GetComponent<Health>();
            return health.GetValueAsPercentage();
        }

        public override int GetValue(GameObject user)
        {
            Health health = user.GetComponent<Health>();
            return health.GetCurrentValue();
        }

        public override int GetMaxValue(GameObject user)
        {
            Health health = user.GetComponent<Health>();
            return health.GetMaxValue();
        }
    }
}