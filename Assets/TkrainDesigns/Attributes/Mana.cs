using System.Collections;
using System.Collections.Generic;
using TkrainDesigns.Stats;
using UnityEngine;

namespace TkrainDesigns.Attributes
{
    [DisallowMultipleComponent]
    public class Mana : AttributeBase
    {
        protected override void Awake()
        {
            base.Awake();
            AddRecipe(EStat.Mana, 1.0f);
            AddRegenerationRecipe(EStat.ManaRegen, 1.0f);
        }

        public bool UseMana(int manaCost)
        {
            if (manaCost > CurrentValue) return false;
            CurrentValue -= manaCost;
            return true;
        }
    }
}