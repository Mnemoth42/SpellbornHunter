using System.Collections.Generic;
using TkrainDesigns.Stats;
using UnityEngine;
using UnityEngine.Events;

namespace TkrainDesigns.Attributes
{
    [RequireComponent(typeof(Animator))]
    [RequireComponent(typeof(BaseStats))]
    [DisallowMultipleComponent]
    public class Health : AttributeBase
    {
        public UnityEvent<float> onTakeDamage;
        public UnityEvent onDeath;
        public UnityEvent<Vector3> onDeathAtPoint;
        public UnityEvent<float> onGainHealth;
        public UnityEvent<string> onTakeDamageString;

        public event System.Action onHealthChanged;
        public event System.Action<GameObject> onDamageDealt;

        private Animator anim;

        private static readonly int Die = Animator.StringToHash("Die");

        public bool IsAlive() => GetCurrentValue() > 0;

        protected override void Awake()
        {
            base.Awake();
            AddRecipe(EStat.Health, 1.0f);
            AddRegenerationRecipe(EStat.Regen, 1.0f);
            anim = GetComponent<Animator>();
        }


        void OnGainValue(int amount)
        {
            onGainHealth?.Invoke(amount);
            onHealthChanged?.Invoke();
        }

        protected override bool CanRegenerate()
        {
            return IsAlive();
        }

        public void TakeDamage(int amount, GameObject damageDealer)
        {
            if (!IsAlive()) return;
            int actualDamage = Mathf.Min(CurrentValue, amount);
            DecreaseCurrentValue(actualDamage);
            onTakeDamage?.Invoke(-actualDamage);
            onHealthChanged?.Invoke();
            onDamageDealt?.Invoke(damageDealer);
            GameLog.WriteLn(
                $"{damageDealer.name} hits {gameObject.name} for {actualDamage} points. {GetCurrentValue()} remaining!");
            if (!IsAlive())
            {
                GameLog.WriteLn($"{gameObject.name} has been killed by {damageDealer.name}!  Rest In Peace!");
                Experience otherExperience = damageDealer.GetComponent<Experience>();
                if (otherExperience)
                {
                    otherExperience.AwardExperience(stats.GetStatAsInt(EStat.XPGained));
                }

                anim.SetTrigger(Die);
                damagePosition = damageDealer.transform.position + Vector3.up;
                onDeath?.Invoke();
                onDeathAtPoint?.Invoke(damagePosition);
                Destroy(gameObject, 2.0f);
            }
        }

        public void TakeDamage(int amount, GameObject damageDealer, IEnumerable<DamageCharacteristic> characteristics)
        {
            if (!IsAlive()) return;
            foreach (var characteristic in characteristics)
            {
                float assignedDamage = DamageBroker.AssessMelee(amount, characteristic, gameObject, damageDealer);
                int actualDamage = Mathf.Min(CurrentValue, assignedDamage.ToInt());
                DecreaseCurrentValue(actualDamage);
                onTakeDamage?.Invoke(-actualDamage);
                string desc = characteristic.GetDamageType() == DamageType.Normal
                    ? ""
                    : $"{characteristic.GetDamageType()} ";
                onTakeDamageString?.Invoke($"{actualDamage:F0}{desc}");
                onDamageDealt?.Invoke(damageDealer);
                GameLog.WriteLn(
                    $"{damageDealer.name} hits {gameObject.name} for {actualDamage} {desc}points. {GetCurrentValue()} remaining!");
                if (!IsAlive())
                {
                    GameLog.WriteLn($"{gameObject.name} has been killed by {damageDealer.name}!  Rest In Peace!");
                    Experience otherExperience = damageDealer.GetComponent<Experience>();
                    if (otherExperience)
                    {
                        otherExperience.AwardExperience(stats.GetStatAsInt(EStat.XPGained));
                    }

                    anim.SetTrigger(Die);
                    damagePosition = damageDealer.transform.position + Vector3.up;
                    onDeath?.Invoke();
                    onDeathAtPoint?.Invoke(damagePosition);
                    Destroy(gameObject, 2.0f);
                    return;
                }
            }
        }

        private Vector3 damagePosition;

        public void DestroyAnimator()
        {
            anim.enabled = false;
        }

        public void Heal(int healthChange)
        {
            if (!IsAlive()) return;
            CurrentValue += healthChange;
        }
    }
}