using System;
using System.Collections;
using System.Collections.Generic;
using GameDevTV.Inventories;
using UnityEngine;

namespace TkrainDesigns.Crafting
{
    [CreateAssetMenu(fileName = "CraftingMaterial", menuName = "Crafting/CraftingMaterial")]
    public class CraftMaterial : InventoryItem
    {
    #if UNITY_EDITOR

        protected override bool IsCategorySelectable(Enum location)
        {
            ItemCategory candidate = (ItemCategory)location;
            return candidate == ItemCategory.Material;
        }

    #endif
    }
}