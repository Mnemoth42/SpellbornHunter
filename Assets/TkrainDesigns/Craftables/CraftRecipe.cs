using System.Collections;
using System.Collections.Generic;
using GameDevTV.Inventories;
using UnityEngine;

namespace TkrainDesigns.Crafting
{
    [System.Serializable]
    public class CraftRecipeEntry
    {
        public InventoryItem item;
        public int qtyRequired;
    }

    public class CraftRecipe : RetrievableScriptableObject
    {
        [SerializeField] private InventoryItem item;
        [SerializeField] private List<CraftRecipeEntry> recipeItems;

        public override string GetDisplayName()
        {
            if (item) return item.GetDisplayName();
            return name;
        }

        public override string GetDescription()
        {
            if (item) return item.GetDescription();
            return name;
        }
    }
}