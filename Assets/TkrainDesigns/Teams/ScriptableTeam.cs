using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TkrainDesigns.Teams
{
    [CreateAssetMenu(fileName = "New Team", menuName = "Character/Team")]
    public class ScriptableTeam : ScriptableObject
    {
        [SerializeField] private string displayName = "Team Name";
        [SerializeField] private string description = "Describe your team here.";
        [SerializeField] private List<ScriptableTeam> Allies = new List<ScriptableTeam>();

        public string DisplayName => displayName;
        public string Description => description;
        public bool IsAllied(ScriptableTeam scriptableTeamToTest) => Allies.Contains(scriptableTeamToTest);
    }
}