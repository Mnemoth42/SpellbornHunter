using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TkrainDesigns.Teams
{
    [DisallowMultipleComponent]
    public class Team : MonoBehaviour
    {
        [SerializeField] private ScriptableTeam team;

        public ScriptableTeam GetTeam() => team;

        public bool IsHostile(Team other)
        {
            if (!team) return true;
            if (other == null) return true;
            //Debug.Log($"Testing {team} against {other.GetTeam()}");
            return (!team.IsAllied(other.GetTeam()));
        }
    }
}