﻿using System;
using System.Linq;
using UnityEditor;
using UnityEditor.Callbacks;
using UnityEngine;


namespace TkrainDesigns.Dialogue.Editor
{
    public class DialogueEditor : EditorWindow
    {
        Dialogue selectedDialogue = null;
        [NonSerialized] GUIStyle nodeStyle;
        [NonSerialized] GUIStyle selectedStyle;
        GUIStyle playerStyle;
        GUIStyle playerSelectedStyle;

        [NonSerialized] DialogueNode creatingNode;
        [NonSerialized] DialogueNode deletingNode;
        [NonSerialized] DialogueNode linkingParentNode;
        [NonSerialized] DialogueNode selectedNode;
        [NonSerialized] Vector2 lastMousePosition;
        [NonSerialized] private bool showContextMenu;
        [NonSerialized] private Vector2 contextMenuPosition;

        Vector2 draggingOffset;
        DialogueNode draggingNode = null;
        const float CanvasSize = 4092;

        Texture background;

        [MenuItem("Window/Dialogue Editor")]
        public static void ShowEditorWindow()
        {
            GetWindow(typeof(DialogueEditor), false, "Dialogue Editor");
        }

        public static void ShowEditorWindow(Dialogue candidate = null)
        {
            DialogueEditor window = GetWindow(typeof(DialogueEditor), false, "Dialogue Editor") as DialogueEditor;
            if (candidate)
            {
                window.SelectionChanged();
            }
        }

        [OnOpenAsset(1)]
        public static bool OnOpenAsset(int instanceID, int line)
        {
            var candidate = EditorUtility.InstanceIDToObject(instanceID) as Dialogue;
            if (candidate != null)
            {
                ShowEditorWindow(candidate);

                return true;
            }

            return false;
        }

        void OnEnable()
        {
            Selection.selectionChanged += SelectionChanged;
            nodeStyle = new GUIStyle();
            nodeStyle.normal.background = Resources.Load("Background_06") as Texture2D;
            nodeStyle.padding = new RectOffset(20, 20, 20, 20);
            nodeStyle.border = new RectOffset(0, 0, 0, 0);

            selectedStyle = new GUIStyle();
            selectedStyle.normal.background = Resources.Load("Button_05") as Texture2D;
            selectedStyle.padding = new RectOffset(20, 20, 20, 20);
            selectedStyle.border = new RectOffset(0, 0, 0, 0);

            playerSelectedStyle = new GUIStyle();
            playerSelectedStyle.normal.background = Resources.Load("button_09") as Texture2D;
            playerSelectedStyle.padding = new RectOffset(20, 20, 20, 20);
            playerSelectedStyle.border = new RectOffset(0, 0, 0, 0);

            playerStyle = new GUIStyle();
            playerStyle.normal.background = Resources.Load("button_10_bg") as Texture2D;
            playerStyle.padding = new RectOffset(20, 20, 20, 20);
            playerStyle.border = new RectOffset(0, 0, 0, 0);

            background = Resources.Load("background") as Texture2D;
        }

        public void SelectionChanged()
        {
            Dialogue candidate = EditorUtility.InstanceIDToObject(Selection.activeInstanceID) as Dialogue;
            if (candidate != null)
            {
                selectedDialogue = candidate;
                creatingNode = null;
                Repaint();
            }
        }

        void OnDisable()
        {
            Selection.selectionChanged -= SelectionChanged;
        }


        void OnGUI()
        {
            if (selectedDialogue == null)
            {
                EditorGUILayout.HelpBox("No Dialogue Selected", MessageType.Error);
            }
            else
            {
                ProcessEvents();
                selectedDialogue.scrollPosition = EditorGUILayout.BeginScrollView(selectedDialogue.scrollPosition);

                Rect canvas = GUILayoutUtility.GetRect(CanvasSize, CanvasSize);
                //Texture2D texture = Resources.Load("Background") as Texture2D;
                Rect textCoords = new Rect(0, 0, CanvasSize / background.width, CanvasSize / background.width);
                GUI.DrawTextureWithTexCoords(canvas, background, textCoords);
                DrawBezierCurves();
                DrawAllNodes();
                EditorGUILayout.EndScrollView();
                CreateNodeIfNeeded();
                DeleteNodeIfNeeded();
                if (showContextMenu)
                {
                    ShowContextMenu();
                }
            }
        }

        void DrawAllNodes()
        {
            foreach (DialogueNode node in selectedDialogue.GetAllNodes().OrderBy(n => n.Order).ToList())
            {
                DrawNode(node);
            }
        }

        void DrawBezierCurves()
        {
            foreach (DialogueNode node in selectedDialogue.GetAllNodes())
            {
                DrawConnections(node);
            }
        }

        void DeleteNodeIfNeeded()
        {
            if (deletingNode != null)
            {
                selectedDialogue.DeleteNode(deletingNode);
                deletingNode = null;
            }
        }

        void CreateNodeIfNeeded()
        {
            if (creatingNode != null)
            {
                Debug.Log($"Creating Node under {creatingNode.name}");

                selectedDialogue.AddChildNode(creatingNode);
                creatingNode = null;
                EditorUtility.SetDirty(selectedDialogue);
            }
        }

        const float gridsnap = 10.0f;

        private void ProcessEvents()
        {
            if (Event.current.type == EventType.MouseDown && draggingNode == null)
            {
                draggingNode = GetNodeAtPoint(Event.current.mousePosition);
                if (draggingNode == null)
                {
                    lastMousePosition = Event.current.mousePosition;
                    Selection.activeObject = selectedDialogue;
                    selectedNode = null;
                }
                else
                {
                    Selection.activeObject = draggingNode;
                    if (selectedNode != draggingNode)
                    {
                        selectedNode = draggingNode;
                        if (selectedNode.Order < GetHighestNodeOrder())
                        {
                            selectedNode.Order = GetHighestNodeOrder() + 1;
                        }
                    }

                    draggingNode.RememberPosition();
                }
            }
            else if (Event.current.type == EventType.MouseDrag)
            {
                if (draggingNode != null)
                {
                    Rect dragginRect = draggingNode.GetRect();
                    dragginRect.position = (Event.current.mousePosition) - draggingOffset;
                    dragginRect.position = new Vector2(Mathf.Floor(dragginRect.x / gridsnap) * gridsnap,
                        Mathf.Floor(dragginRect.y / gridsnap) * gridsnap);
                    draggingNode.SetRect(dragginRect);
                    GUI.changed = true;
                }
                else
                {
                    Vector2 deltaMousePosition = Event.current.mousePosition - lastMousePosition;
                    selectedDialogue.scrollPosition -= deltaMousePosition;
                    lastMousePosition = Event.current.mousePosition;
                    Repaint();
                }
            }
            else if (Event.current.type == EventType.MouseUp && draggingNode != null)
            {
                draggingNode.CommitPosition();
                draggingNode = null;
            }

            if (Event.current.type == EventType.MouseDown && Event.current.button == 1 && draggingNode == null)
            {
                showContextMenu = true;
                contextMenuPosition = Event.current.mousePosition + selectedDialogue.scrollPosition;
            }
            else
            {
                showContextMenu = false;
            }
        }

        int GetHighestNodeOrder()
        {
            int highest = -1;
            foreach (DialogueNode node in selectedDialogue.GetAllNodes())
            {
                highest = Mathf.Max(highest, node.Order);
            }

            return highest;
        }

        DialogueNode GetNodeAtPoint(Vector2 point)
        {
            DialogueNode tempNode = null;
            foreach (var node in selectedDialogue.GetAllNodes())
            {
                if (node.GetRect().Contains(point + selectedDialogue.scrollPosition))
                {
                    tempNode = node;
                    draggingOffset = point - node.GetRect().position;
                }
            }

            return tempNode;
        }


        void DrawNode(DialogueNode node)
        {
            bool isSelected = selectedNode == node;
            GUIStyle currentStyle = nodeStyle;
            if (isSelected)
            {
                currentStyle = selectedStyle;
            }

            if (node.IsPlayerSpeaking())
            {
                currentStyle = isSelected ? playerSelectedStyle : playerStyle;
            }

            GUILayout.BeginArea(node.GetRect(), currentStyle);

            string speaker = "NPC";
            if (node.IsPlayerSpeaking())
            {
                speaker = "Player";
            }

            EditorGUILayout.LabelField($"Node: {speaker}", EditorStyles.boldLabel);


            string newText = EditorGUILayout.DelayedTextField(node.GetText());
            node.SetText(newText);
            EditorGUILayout.BeginHorizontal();
            EditorGUI.BeginChangeCheck();
            EditorGUI.EndChangeCheck();

            DrawAddButton(node);
            DrawLinkButtons(node);
            DrawDeleteButton(node);
            EditorGUILayout.EndHorizontal();
            GUILayout.EndArea();
        }

        void DrawDeleteButton(DialogueNode node)
        {
            if (linkingParentNode != null)
            {
                return;
            }

            if (node.GetChildren().Count == 0 && node != selectedDialogue.GetRootNode())
            {
                if (GUILayout.Button("X"))
                {
                    deletingNode = node;
                }
            }
            else
            {
                if (GUILayout.Button(" "))
                {
                }
            }
        }

        void DrawAddButton(DialogueNode node)
        {
            if (linkingParentNode != null)
            {
                return;
            }

            if (GUILayout.Button("+"))
            {
                creatingNode = node;
            }
        }

        void DrawLinkButtons(DialogueNode node)
        {
            if (linkingParentNode == null)
            {
                if (GUILayout.Button("Link"))
                {
                    linkingParentNode = node;
                }
            }
            else if (linkingParentNode == node)
            {
                if (GUILayout.Button("Cancel"))
                {
                    linkingParentNode = null;
                }
            }
            else if (linkingParentNode.GetChildren().Contains(node.name))
            {
                if (GUILayout.Button("Unlink"))
                {
                    linkingParentNode.RemoveChild(node.name);
                    linkingParentNode = null;
                }
            }
            else
            {
                if (GUILayout.Button("Child"))
                {
                    linkingParentNode.AddChild(node.name);
                    linkingParentNode = null;
                }
            }
        }

        void DrawConnections(DialogueNode node)
        {
            Vector2 startPosition = new Vector2(node.GetRect().xMax, node.GetRect().center.y);
            foreach (DialogueNode childNode in selectedDialogue.GetAllChildren(node))
            {
                Color color = node == selectedNode ? Color.green : Color.white;
                if (childNode == selectedNode)
                {
                    color = Color.blue;
                }

                Vector2 endPosition = new Vector2(childNode.GetRect().xMin, childNode.GetRect().center.y);

                Vector2 controlPointOffset = endPosition - startPosition;
                controlPointOffset.y = 0;
                controlPointOffset *= .8f;

                Handles.DrawBezier(startPosition, endPosition, startPosition + controlPointOffset,
                    endPosition - controlPointOffset, color, null, 4f);
            }
        }

        void ShowContextMenu()
        {
            GenericMenu contextMenu = new GenericMenu();
            contextMenu.AddItem(new GUIContent("Add New Node"), false,
                () => selectedDialogue.AddNode(null, contextMenuPosition));
            contextMenu.ShowAsContext();
        }
    }
}