﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using TkrainDesigns.Dialogue;
using UnityEditor;
using UnityEngine;

//public class DialogueModificationProcessor : UnityEditor.AssetModificationProcessor
//{
//    private static AssetMoveResult OnWillMoveAsset(string sourcePath, string destinationPath)
//    {
//        var dialogue = AssetDatabase.LoadMainAssetAtPath(sourcePath) as Dialogue;
//        if (dialogue == null)
//        {
//            return AssetMoveResult.DidNotMove;
//        }
//        if (MovingDirectory(sourcePath, destinationPath))
//        {
//            return AssetMoveResult.DidNotMove;
//        }
//        dialogue.name = Path.GetFileNameWithoutExtension(destinationPath);
//        return AssetMoveResult.DidNotMove;
//    }
//    private static bool MovingDirectory(string sourcePath, string destinationPath)
//    {
//        return Path.GetDirectoryName(sourcePath) != Path.GetDirectoryName(destinationPath);
//    }
//}

namespace TkrainDesigns.Dialogue.Editor
{
    public class DialogueModificationProcessor : UnityEditor.AssetModificationProcessor
    {
        private static AssetMoveResult OnWillMoveAsset(string sourcePath, string destinationPath)
        {
            //Check if asset moved/renamed is a dialogue scriptable object.
            Dialogue dialogue = AssetDatabase.LoadMainAssetAtPath(sourcePath) as Dialogue;

            if (dialogue == null)
            {
                return AssetMoveResult.DidNotMove;
            }

            //Check if object was moved to a new folder.
            if (Path.GetDirectoryName(sourcePath) != Path.GetDirectoryName(destinationPath))
            {
                return AssetMoveResult.DidNotMove;
            }


            Debug.Log(dialogue.GetType());
            Debug.Log(dialogue.name);
            // Object is a Dialogue and is renamed.
            dialogue.name = Path.GetFileNameWithoutExtension(destinationPath);


            Debug.Log(dialogue.GetType());
            Debug.Log(dialogue.name);
            Debug.Log(dialogue.GetRootNode().name);

            return AssetMoveResult.DidNotMove;
        }
    }
}