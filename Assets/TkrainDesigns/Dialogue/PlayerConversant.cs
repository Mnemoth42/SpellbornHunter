﻿using TkrainDesigns.Movement;
using System;
using System.Collections.Generic;
using System.Linq;
using GameDevTV.Utils;
using UnityEngine;
using Random = UnityEngine.Random;


namespace TkrainDesigns.Dialogue
{
    [DisallowMultipleComponent]
    public class PlayerConversant : MonoBehaviour, IAction
    {
        [SerializeField] private string playerName = "Kelwick Stealthblade";
        public event Action OnCurrentDialogueChanged;
        [NonSerialized] private Dialogue currentDialogue;

        private DialogueNode currentNode = null;
        private bool isChoosing = false;
        private AIConversant currentConversant;
        private AIConversant targetConversant;


        public bool IsChoosing()
        {
            return isChoosing;
        }

        public string CurrentSpeaker => isChoosing ? playerName : currentConversant.GetSpeakerName();

        public IEnumerable<DialogueNode> GetChoices()
        {
            return FilterOnCondition(currentDialogue.GetPlayerChildren(currentNode));
        }

        public string GetText()
        {
            return currentDialogue == null && currentNode != null ? "" : currentNode.GetText();
        }

        public bool HasNext()
        {
            // Debug.Log($"GetAllChildrenCount: {currentDialogue.GetAllChildren(currentNode).Count()}");
            // Debug.Log($"Filtered = {FilterOnCondition(currentDialogue.GetAIChildren(currentNode))}");
            return (currentDialogue != null) &&
                   FilterOnCondition(currentDialogue.GetAllChildren(currentNode)).Count() != 0;
        }

        public void SelectChoice(DialogueNode node)
        {
            SetCurrentNode(node);
            Next();
        }

        private IEnumerable<DialogueNode> FilterOnCondition(IEnumerable<DialogueNode> inputNode)
        {
            foreach (var node in inputNode)
            {
                // Debug.Log($"Testing {node.GetText()}");
                if (node.CheckCondition(GetPredicateEvaluators()))
                {
                    //   Debug.Log($"Adding {node.GetText()} conditions are true.");
                    yield return node;
                }
                else
                {
                    // Debug.Log($"Rejecting {node.GetText()}");
                }
            }
        }

        private IEnumerable<IPredicateEvaluator> GetPredicateEvaluators()
        {
            return GetComponents<IPredicateEvaluator>();
        }

        public bool IsActive() => currentDialogue != null;

        public void Next()
        {
            if (currentNode.GetChildren().Count == 0)
            {
                return;
            }

            int numPlayerResponses = FilterOnCondition(currentDialogue.GetPlayerChildren(currentNode)).Count();
            if (numPlayerResponses > 0)
            {
                isChoosing = true;
                TriggerExitAction();
                OnCurrentDialogueChanged?.Invoke();
                return;
            }

            DialogueNode[] children = FilterOnCondition(currentDialogue.GetAIChildren(currentNode)).ToArray();
            if (children.Length == 0)
            {
                QuitDialogue();
                return;
            }

            SetCurrentNode(children[UnityEngine.Random.Range(0, children.Length)]);


            isChoosing = false;


            OnCurrentDialogueChanged?.Invoke();
        }


        public void StartDialogueAction(Dialogue dialogue, AIConversant speaker)
        {
            Debug.Log($"StartDialogueAction {speaker}/{dialogue}");
            if (currentConversant != null && currentConversant == speaker) return;
            if (currentDialogue != null) QuitDialogue();
            if (dialogue == null)
            {
                return;
            }

            GetComponent<ActionScheduler>().StartAction(this);
            targetConversant = speaker;
            currentDialogue = dialogue;
        }

        private void StartDialogue()
        {
            currentConversant = targetConversant;
            targetConversant = null;
            var nodes = FilterOnCondition(currentDialogue.GetRootNodes()).ToArray();
            SetCurrentNode(nodes[Random.Range(0, nodes.Length)]);
            OnCurrentDialogueChanged?.Invoke();
        }

        private void Update()
        {
            if (targetConversant)
            {
                if (Vector3.Distance(targetConversant.transform.position, transform.position) > 3)
                {
                    GetComponent<Mover>().MoveTo(targetConversant.transform.position);
                }
                else
                {
                    GetComponent<Mover>().Cancel();
                    StartDialogue();
                }
            }
        }

        private void SetCurrentNode(DialogueNode node)
        {
            TriggerEnterAction();
            currentNode = node;
            TriggerExitAction();
        }

        public void QuitDialogue()
        {
            targetConversant = null;
            currentDialogue = null;
            SetCurrentNode(null);
            isChoosing = false;
            currentConversant = null;
            OnCurrentDialogueChanged?.Invoke();
        }

        public void Cancel()
        {
            QuitDialogue();
        }

        public Action GetResumeFunction()
        {
            return null;
        }

        private void TriggerEnterAction()
        {
            if (currentNode != null && currentNode.GetOnEnterAction() != "")
            {
                TriggerAction(currentNode.GetOnEnterAction());
            }
        }

        private void TriggerExitAction()
        {
            if (currentNode != null && currentNode.GetOnExitAction() != "")
            {
                TriggerAction(currentNode.GetOnExitAction());
            }
        }

        private void TriggerAction(string action)
        {
            if (string.IsNullOrEmpty(action)) return;
            foreach (var trigger in currentConversant.GetComponents<DialogueTrigger>())
            {
                trigger.Trigger(action);
            }
        }
    }
}