﻿using System.Collections.Generic;
using GameDevTV.Utils;
using UnityEditor;
using UnityEngine;

namespace TkrainDesigns.Dialogue
{
    [System.Serializable]
    public class DialogueNode : ScriptableObject
    {
        [TextArea] [SerializeField] private string text = "";
        [SerializeField] private bool isPlayerSpeaking = false;
        [SerializeField] private string onEntryAction = "";
        [SerializeField] private string onExitAction = "";

        [SerializeField] private Condition condition;
        [HideInInspector] [SerializeField] private Rect rect = new Rect(10, 10, 200, 100);
        [HideInInspector] [SerializeField] private int order;
        [HideInInspector] [SerializeField] private List<string> children = new List<string>();

        public string GetText()
        {
            return text;
        }

        public Rect GetRect()
        {
            return rect;
        }

        public List<string> GetChildren()
        {
            return children;
        }


        public bool IsPlayerSpeaking()
        {
            return isPlayerSpeaking;
        }

        public string GetOnEnterAction()
        {
            return onEntryAction;
        }

        public string GetOnExitAction()
        {
            return onExitAction;
        }

        public bool CheckCondition(IEnumerable<IPredicateEvaluator> evaluators)
        {
            return condition.Check(evaluators);
        }


    #if UNITY_EDITOR

        public Condition GetCondition()
        {
            if (condition == null)
            {
                condition = new Condition();
            }

            return condition;
        }

        public void SetEntryAction(string value)
        {
            if (onEntryAction == value) return;
            Undo.RecordObject(this, "Set Entry Action");
            onEntryAction = value;
            EditorUtility.SetDirty(this);
        }

        public void SetExitAction(string value)
        {
            if (onExitAction == value) return;
            Undo.RecordObject(this, "Set Exit Action");
            onExitAction = value;
            EditorUtility.SetDirty(this);
        }

        public void SetIsPlayerSpeaking(bool isPlayer)
        {
            if (isPlayer == isPlayerSpeaking) return;
            Undo.RecordObject(this, "Set IsPlayerSpeaking");
            isPlayerSpeaking = isPlayer;
            EditorUtility.SetDirty(this);
        }

        public void SetText(string newText)
        {
            if (text != newText)
            {
                Undo.RecordObject(this, "Change Node Text");
                text = newText;
                EditorUtility.SetDirty(this);
            }
        }

        public void SetRect(Rect newRect)
        {
            rect = newRect;
        }

        private Vector2 positionToRemember;

        public void RememberPosition()
        {
            positionToRemember = rect.position;
        }

        public void CommitPosition()
        {
            Vector2 newPosition = rect.position;
            rect.position = positionToRemember;
            Undo.RecordObject(this, "Move Node");
            rect.position = newPosition;
            EditorUtility.SetDirty(this);
        }


        public void AddChild(string newChild)
        {
            Undo.RecordObject(this, "Add Link");
            children.Add(newChild);
            EditorUtility.SetDirty(this);
        }

        public void RemoveChild(string childToRemove)
        {
            if (children.Contains(childToRemove))
            {
                Undo.RecordObject(this, "Remove Link");
                children.Remove(childToRemove);
            }

            EditorUtility.SetDirty(this);
        }

    #endif
        public int Order
        {
            get => order;
            set => order = value;
        }
    }
}