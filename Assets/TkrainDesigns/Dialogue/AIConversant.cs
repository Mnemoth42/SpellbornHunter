﻿using System.Collections;
using System.Collections.Generic;
using TkrainDesigns;
using TkrainDesigns.Attributes;
using TkrainDesigns.Combat;
using TkrainDesigns.Control;
using UnityEngine;

namespace TkrainDesigns.Dialogue
{
    public class AIConversant : MonoBehaviour, IRaycastable
    {
        [SerializeField] private string speakerName = "";
        [SerializeField] private Dialogue dialogue;

        public int GetPriority()
        {
            return 0;
        }

        public ECursorType GetCursorType()
        {
            return ECursorType.Dialogue;
        }


        public bool HandleRayCast(GameObject callingController)
        {
            if (dialogue == null) return false;
            if (GetComponent<Fighter>().enabled) return false;
            if (!GetComponent<Health>().IsAlive()) return false;
            if (callingController.GetComponent<Fighter>().InCombat()) return false;
            if (Input.GetMouseButtonDown(0))
            {
                callingController.GetComponent<PlayerConversant>().StartDialogueAction(dialogue, this);
            }

            return true;
        }

        public string GetSpeakerName()
        {
            return string.IsNullOrEmpty(speakerName) ? name : speakerName;
        }
    }
}