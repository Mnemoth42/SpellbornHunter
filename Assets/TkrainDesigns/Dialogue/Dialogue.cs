﻿using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;

namespace TkrainDesigns.Dialogue
{
    [CreateAssetMenu(fileName = "New Dialogue", menuName = "Dialogue", order = 0)]
    public class Dialogue : ScriptableObject, ISerializationCallbackReceiver
    {
        [SerializeField] private List<DialogueNode> nodes = new List<DialogueNode>();
        [HideInInspector] public Vector2 scrollPosition = new Vector2();

        private Dictionary<string, DialogueNode> nodeLookup = new Dictionary<string, DialogueNode>();

    #region Editor utilities.

        private void Awake()
        {
        #if UNITY_EDITOR
            Selection.activeObject = this;
        #endif
            OnValidate();
        }

    #if UNITY_EDITOR


        public void AddNode()
        {
            DialogueNode rootNode = CreateNode();
            nodes.Add(rootNode);
        }

        private DialogueNode CreateNode()
        {
            DialogueNode node = CreateInstance<DialogueNode>();
            node.name = System.Guid.NewGuid().ToString();
            //AssetDatabase.AddObjectToAsset(node, this);
            return node;
        }

        public void AddNode(DialogueNode parent, Vector2 position)
        {
            var newNode = CreateNode();
            if (parent)
            {
                parent.AddChild(newNode.name);
            }

            Rect rect = newNode.GetRect();
            rect.position = position;
            newNode.SetRect(rect);
            Undo.RegisterCompleteObjectUndo(newNode, "Create Dialogue Node");
            Undo.RecordObject(this, "Add Dialogue Node");
            nodes.Add(newNode);
        }

        public void AddChildNode(DialogueNode parent)
        {
            DialogueNode newNode = CreateNode();


            if (parent != null)
            {
                Rect newPosition = newNode.GetRect();
                newPosition.position = parent.GetRect().position + new Vector2(250, 10);
                newNode.SetRect(newPosition);
                newNode.SetIsPlayerSpeaking(!parent.IsPlayerSpeaking());
            }

            Undo.RegisterCreatedObjectUndo(newNode, "Add Child Node");


            Undo.RecordObject(this, "Add Node");


            nodes.Add(newNode);
            parent.AddChild(newNode.name);
            OnValidate();
        }

        public void DeleteNode(DialogueNode nodeToDelete)
        {
            if (nodeToDelete.GetChildren().Count > 0)
            {
                return;
            }

            Undo.RecordObject(this, "Delete Node");
            nodes.Remove(nodeToDelete);

            OnValidate();
            foreach (DialogueNode item in nodes)
            {
                if (item.GetChildren().Contains(nodeToDelete.name))
                {
                    item.RemoveChild(nodeToDelete.name);
                }
            }

            if (AssetDatabase.GetAssetPath(nodeToDelete) != "")
            {
                AssetDatabase.RemoveObjectFromAsset(nodeToDelete);
            }

            Undo.DestroyObjectImmediate(nodeToDelete);
        }

        public void MoveNodeToTop(DialogueNode nodeToMove)
        {
            nodes.Remove(nodeToMove);
            nodes.Add(nodeToMove);
            EditorUtility.SetDirty(this);
        }

    #endif
        public void OnBeforeSerialize()
        {
        #if UNITY_EDITOR

            if (AssetDatabase.GetAssetPath(this) != "")
            {
                if (nodes.Count == 0)
                {
                    AddNode();
                }

                foreach (DialogueNode node in GetAllNodes())
                {
                    if (AssetDatabase.GetAssetPath(node) == "")
                    {
                        AssetDatabase.AddObjectToAsset(node, this);
                    }
                }
            }
        #endif
        }

        public void OnAfterDeserialize()
        {
        }

    #endregion

    #region functions

        private void OnValidate()
        {
            nodeLookup.Clear();
            foreach (DialogueNode node in nodes)
            {
                nodeLookup[node.name] = node;
            }
        }

        public List<DialogueNode> GetAllNodes()
        {
            return nodes;
        }

        public DialogueNode GetRootNode()
        {
            foreach (DialogueNode node in nodes)
            {
                if (!HasParent(node)) return node;
            }

            return nodes[0];
        }

        public IEnumerable<DialogueNode> GetRootNodes()
        {
            return nodes.Where(n => !HasParent(n));
        }

        public DialogueNode GetChildNode(string key)
        {
            if (nodeLookup.ContainsKey(key))
            {
                return nodeLookup[key];
            }

            return null;
        }

        public IEnumerable<DialogueNode> GetPlayerChildren(DialogueNode node)
        {
            return node.GetChildren().Select(child => GetChildNode(child))
                       .Where(childNode => childNode.IsPlayerSpeaking());
        }

        public IEnumerable<DialogueNode> GetAIChildren(DialogueNode node)
        {
            return node.GetChildren().Select(child => GetChildNode(child))
                       .Where(childNode => !childNode.IsPlayerSpeaking());
        }

        public IEnumerable<DialogueNode> GetAllChildren(DialogueNode node)
        {
            {
                return from child in node.GetChildren() where nodeLookup.ContainsKey(child) select nodeLookup[child];
            }
        }

        private bool HasParent(DialogueNode node)
        {
            foreach (DialogueNode n in nodes)
            {
                if (n.GetChildren().Contains(node.name))
                {
                    return true;
                }
            }

            return false;
        }

    #endregion
    }
}