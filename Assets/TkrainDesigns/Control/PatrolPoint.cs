using System;
using System.Collections;
using System.Collections.Generic;
using TkrainDesigns.Teams;
using UnityEngine;

namespace TkrainDesigns.Control
{
    public class PatrolPoint : MonoBehaviour
    {
        [SerializeField] private List<Team> teams;
        [SerializeField] private bool negate;

        public bool IsTeamWaypoint(Team team)
        {
            if (teams.Count == 0) return true; //All teams can use an empty waypoint.
            bool result = teams.Contains(team);
            return negate ? !result : result;
        }

        private void OnDrawGizmos()
        {
            Gizmos.color = Color.yellow;
            Gizmos.DrawSphere(transform.position, .5f);
        }
    }
}