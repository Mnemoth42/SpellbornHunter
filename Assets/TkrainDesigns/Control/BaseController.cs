using UnityEngine;

namespace TkrainDesigns.Control
{
    public abstract class BaseController : MonoBehaviour
    {
        public void FreezeController()
        {
            GetComponent<ActionScheduler>().CancelCurrentAction();
            this.enabled = false;
        }

        public void EnableController()
        {
            this.enabled = true;
        }
    }
}