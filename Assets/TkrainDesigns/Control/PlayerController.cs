using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TkrainDesigns.Attributes;
using TkrainDesigns.Movement;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.EventSystems;
using UnityEngine.InputSystem;

namespace TkrainDesigns.Control
{
    [RequireComponent(typeof(Mover))]
    [RequireComponent(typeof(Health))]
    [DisallowMultipleComponent]
    public class PlayerController : BaseController
    {
        [System.Serializable]
        struct CursorConfig
        {
            public ECursorType cursorType;
            public Texture2D cursor;
            public Vector2 hotspot;
        }

        [SerializeField] private float navMeshProjectionDistance = 5;
        [Range(1, 5)] [SerializeField] private float cursorRadius = 1.5f;
        [SerializeField] private LayerMask GroundMask;
        [SerializeField] private List<CursorConfig> cursors = new List<CursorConfig>();
        [SerializeField] private IRaycastable raycastable;

        private Dictionary<ECursorType, CursorConfig> cursorConfigs = new Dictionary<ECursorType, CursorConfig>();

        private Mover mover;
        private Health health;
        private RaycastHit groundHit;


        private void Awake()
        {
            mover = GetComponent<Mover>();
            health = GetComponent<Health>();
            foreach (CursorConfig config in cursors)
            {
                cursorConfigs[config.cursorType] = config;
            }
        }

        private void Update()
        {
            Rect screen = new Rect(0, 0, Screen.width, Screen.height);
            if (!screen.Contains(Mouse.current.position.ReadValue()))
            {
                Cursor.SetCursor(null, Vector2.zero, CursorMode.Auto);
                // Debug.Log($"Not on screen");
                return;
            }

            if (!health.IsAlive())
            {
                SetCursor(ECursorType.None);
                return;
            }

            //Debug.Log($"Update");
            if (InteractWithUI()) return;
            if (InteractWithComponents()) return;
            if (InteractWithMovement()) return;
        }

        private ECursorType currentCursorType = ECursorType.None;

        private void SetCursor(ECursorType cursorType)
        {
            if (cursorType == currentCursorType) return;
            if (!cursorConfigs.ContainsKey(cursorType))
            {
                Debug.Log($"There is no definition for ECursorType.{cursorType}");
                return;
            }

            currentCursorType = cursorType;
            Cursor.SetCursor(cursorConfigs[cursorType].cursor, cursorConfigs[cursorType].hotspot, CursorMode.Auto);
        }

        private bool isDragging = false;


        bool InteractWithUI()
        {
            if (EventSystem.current.IsPointerOverGameObject())
            {
                if (Mouse.current.leftButton.isPressed)
                {
                    isDragging = true;
                }

                SetCursor(ECursorType.UI);
                return true;
            }

            if (isDragging && Mouse.current.leftButton.isPressed) return true;

            isDragging = false;
            return false;
        }

        bool InteractWithComponents()
        {
            Ray ray = GetMouseRay();

            if (!Physics.Raycast(ray, out groundHit, GroundMask)) return false;

            var hits = Physics.SphereCastAll(ray, cursorRadius)
                              .OrderBy(h => Vector3.Distance(h.point, groundHit.point));
            foreach (RaycastHit hit in hits)
            {
                var handlers = hit.collider.GetComponents<IRaycastable>().OrderBy(r => r.GetPriority());
                foreach (IRaycastable raycastable in handlers)
                {
                    if (raycastable.HandleRayCast(gameObject))
                    {
                        //Debug.Log(raycastable);
                        SetCursor(raycastable.GetCursorType());
                        return true;
                    }
                }
            }

            return false;
        }

        public static Ray GetMouseRay()
        {
            return Camera.main.ScreenPointToRay(new Vector3(Mouse.current.position.x.ReadValue(),
                Mouse.current.position.y.ReadValue()));
        }

        bool InteractWithMovement()
        {
            if (groundHit.transform == null) return false;
            if (!NavMesh.SamplePosition(groundHit.point, out NavMeshHit navMeshHit, navMeshProjectionDistance,
                NavMesh.AllAreas))
            {
                return false;
            }

            NavMeshPath path = new NavMeshPath();
            NavMesh.CalculatePath(transform.position, navMeshHit.position, NavMesh.AllAreas, path);

            if (Mouse.current.leftButton.isPressed)
            {
                mover.StartMoveAction(navMeshHit.position);
            }

            SetCursor(ECursorType.Movement);
            return true;
        }
    }
}