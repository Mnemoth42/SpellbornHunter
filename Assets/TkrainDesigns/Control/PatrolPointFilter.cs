using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TkrainDesigns;
using TkrainDesigns.Control;
using UnityEngine;

public class PatrolPointFilter : MonoBehaviour
{
    [SerializeField] private int maxPatrolPoints = 4;


    void Awake()
    {
        var points = GetComponentsInChildren<PatrolPoint>().ToList();
        while (points.Count > maxPatrolPoints)
        {
            PatrolPoint point = points.RandomElement();
            points.Remove(point);
            Destroy(point.gameObject);
        }
    }
}