using System;
using System.Collections.Generic;
using System.Linq;
using TkrainDesigns.Attributes;
using TkrainDesigns.Combat;
using TkrainDesigns.Movement;
using TkrainDesigns.Teams;
using UnityEngine;
using Random = UnityEngine.Random;

namespace TkrainDesigns.Control
{
    [RequireComponent(typeof(Fighter))]
    [RequireComponent(typeof(Mover))]
    [RequireComponent(typeof(Health))]
    [RequireComponent(typeof(Team))]
    public class AIController : BaseController
    {
        [Range(2, 20)] [SerializeField] private float chaseDistance = 10.0f;
        [Range(0, 100)] [SerializeField] private float suspicionDwellTime = 5.0f;
        [SerializeField] private bool shouldPatrol = true;
        [Range(1, 10)] [SerializeField] private int maxPatrolPoints = 4;
        [Range(0, 100)] [SerializeField] private float waypointDwellTime = 5;
        [Range(1, 10)] [SerializeField] private float waypointAcceptance = 2;

        private Mover mover;
        private Fighter fighter;
        private Health health;
        private Team team;


        private Vector3 startingPosition;
        private Vector3 lastKnownHostilePosition;
        private float suspicionTime = 100000;

        private Health hostileTarget = null;
        private Health aggroTarget = null;
        private List<PatrolPoint> patrolPoints = new List<PatrolPoint>();
        private int currentWaypoint = 0;
        private float currentWaypointDwell = 0;

        private void Awake()
        {
            mover = GetComponent<Mover>();
            fighter = GetComponent<Fighter>();
            health = GetComponent<Health>();
            startingPosition = lastKnownHostilePosition = transform.position;
            health.onDamageDealt += Aggrevate;
            team = GetComponent<Team>();
        }

        private void OnDrawGizmos()
        {
            Gizmos.color = Color.red;
            Gizmos.DrawWireSphere(transform.position, chaseDistance);
        }

        void Start()
        {
            if (shouldPatrol)
            {
                var points = FindObjectsOfType<PatrolPoint>().Where(p => p.IsTeamWaypoint(team))
                                                             .OrderBy(p =>
                                                                 p.transform.position.Distance(transform.position))
                                                             .ToList();
                if (points.Count() < 0)
                {
                    Debug.Log($"{name} found no patrol points.");
                    return;
                }
                else
                {
                  //  Debug.Log($"{name} found {points.Count} potential patrol points");
                }

                if (points.Count < maxPatrolPoints) maxPatrolPoints = points.Count;
                int tries = 0;
                while (patrolPoints.Count < maxPatrolPoints && points.Count > 0 && tries < 30)
                {
                    if (Random.Range(0, 10) < 7)
                    {
                        patrolPoints.Add(points[0]);
                    }

                    points.RemoveAt(0);
                    tries++;
                }

               // Debug.Log($"{name} selected {patrolPoints.Count} points randomly.");
            }
        }

        void AggrevateNeighbors(GameObject antagonist)
        {
            var otherAllies = FindObjectsOfType<AIController>()
                              .Where(a => Vector3.Distance(transform.position, a.transform.position) < chaseDistance)
                              .Where(a => !a.GetComponent<Team>().IsHostile(team))
                              .Where(a => a != this).ToList();
            foreach (var ally in otherAllies)
            {
                ally.Aggrevate(antagonist);
            }
        }

        public void Aggrevate(GameObject antagonist)
        {
            aggroTarget = antagonist.GetComponent<Health>();
        }

        Health FindNearestHostileTargetOrPlayer()
        {
            if (aggroTarget) return aggroTarget;
            List<Team> targets = FindObjectsOfType<Team>()
                                 .Where(g => g.gameObject != gameObject)
                                 .Where(a => a.GetComponent<Health>() != null)
                                 .Where(a => a.GetComponent<Health>().IsAlive())
                                 .Where(h => team.IsHostile(h))
                                 .Where(t => Vector3.Distance(t.transform.position, transform.position) < chaseDistance)
                                 .OrderBy(d => Vector3.Distance(transform.position, d.transform.position))
                                 .ToList();
            Team target = targets.FirstOrDefault();
            //Debug.Log($"{name} is considering {target} out of {targets.Count} targets");
            foreach (Team t in targets)
            {
                if (t.TryGetComponent(out PlayerController player))
                {
                    target = t;
                    break;
                }
            }

            return target ? target.GetComponent<Health>() : null;
        }

        void Update()
        {
            if (!health.IsAlive()) return;
            suspicionTime += Time.deltaTime;
            if (TryAttackBehavior()) return;
            if (TrySuspicionBehavior()) return;
            if (TryPatrolBehavior()) return;
            mover.StartMoveAction(startingPosition);
        }

        bool TryPatrolBehavior()
        {
            if (!shouldPatrol || patrolPoints.Count == 0) return false;
            if (transform.position.Distance(patrolPoints[currentWaypoint].transform.position) < waypointAcceptance)
            {
                currentWaypoint = (currentWaypoint + 1) % patrolPoints.Count;
                mover.Cancel();
                currentWaypointDwell = 0;
            }

            currentWaypointDwell += Time.deltaTime;
            if (currentWaypointDwell > waypointDwellTime)
            {
                mover.MoveTo(patrolPoints[currentWaypoint].transform.position);
            }

            return true;
        }

        private bool TrySuspicionBehavior()
        {
            fighter.Cancel();
            aggroTarget = null;
            if (suspicionTime < suspicionDwellTime)
            {
                mover.MoveTo(lastKnownHostilePosition);
                return true;
            }

            return false;
        }

        private bool TryAttackBehavior()
        {
            hostileTarget = FindNearestHostileTargetOrPlayer();
            if (hostileTarget)
            {
                lastKnownHostilePosition = hostileTarget.transform.position;
                suspicionTime = 0;
                fighter.StartAction(hostileTarget);
                AggrevateNeighbors(hostileTarget.gameObject);
                return true;
            }

            return false;
        }
    }
}