using System;
using UnityEngine;
using UnityEngine.UI;

namespace TkrainDesigns.UI
{
    public abstract class MouseOverToolTip : MonoBehaviour
    {
        private MouseOverTooltipUI tooltip;


        private void Awake()
        {
            tooltip = MouseOverTooltipUI.Find();
        }

        protected abstract string GetTooltipMessage();
        protected abstract Sprite GetSprite();
        protected abstract string GetTooltipTitle();

        private void OnMouseEnter()
        {
            tooltip.SetTitle(GetTooltipTitle());
            tooltip.SetImage(GetSprite());
            tooltip.ShowTooltip(GetTooltipMessage());
        }

        private void OnMouseOver()
        {
            tooltip.ShowTooltip(GetTooltipMessage());
        }

        private void OnMouseExit()
        {
            tooltip.Hide();
        }
    }
}