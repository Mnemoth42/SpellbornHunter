using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.InputSystem;

namespace TkrainDesigns.UI
{
    public class EquipmentUICharacterTurner : MonoBehaviour
    {
        [SerializeField] private GameObject context;
        [SerializeField] private Camera previewCamera;
        [SerializeField] private Transform player;
        private Vector2 drag = new Vector2();
        private Vector2 dragStart = new Vector2();

        public void BeginDrag()
        {
            dragStart = Mouse.current.position.ReadValue();
        }

        public void AdjustDrag()
        {
            var newDrag = Mouse.current.position.ReadValue();
            drag = dragStart - newDrag;
            dragStart = newDrag;
        }


        public void UpdateCharacter()
        {
            if (previewCamera == null || player == null)
            {
                enabled = false;
                return;
            }

            AdjustDrag();
            previewCamera.transform.position = player.transform.position;
            previewCamera.transform.rotation = Quaternion.Euler(new Vector3(-drag.y, -drag.x, 0));
            previewCamera.transform.position = previewCamera.transform.forward * -6;
        }
    }
}