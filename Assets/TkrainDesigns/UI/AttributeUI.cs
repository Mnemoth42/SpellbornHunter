using System;
using System.Collections;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace TkrainDesigns.UI
{
    public class AttributeUI : MonoBehaviour
    {
        [SerializeField] private Image image;
        [SerializeField] private TextMeshProUGUI valueText;
        [SerializeField] private TextMeshProUGUI maxText;
        [SerializeField] private TextMeshProUGUI comboText;
        [SerializeField] private TextMeshProUGUI percentageText;
        [SerializeField] private UIDataStrategy dataStrategy;
        [SerializeField] private bool isPlayer;
        private GameObject user;

        private void Awake()
        {
            if (isPlayer)
            {
                user = GameObject.FindWithTag("Player");
                dataStrategy.SubscribeToValueChanged(user, UpdateUI);
            }
        }

        private float targetValue;
        private float currentValue;
        private float targetPercentage;
        private float currentPercentage;

        IEnumerator UpdatePercentage()
        {
            float percentageDelta = targetPercentage - currentPercentage;
            float percentageDeltaSlice = percentageDelta / 20;
            float valueDelta = targetValue - currentValue;
            float valueDeltaSlice = valueDelta / 20;
            for (int i = 0; i < 20; i++)
            {
                currentPercentage += percentageDeltaSlice;
                currentValue += valueDeltaSlice;
                if (percentageText)
                {
                    percentageText.text = $"{currentPercentage:P0}";
                }

                if (image)
                {
                    image.fillAmount = currentPercentage;
                }

                if (valueText)
                {
                    valueText.text = $"{currentValue:f0}";
                }

                if (comboText)
                {
                    comboText.text = $"{currentValue:f0}/{dataStrategy.GetMaxValue(user):f0}";
                }

                yield return null;
            }

            if (percentageText)
            {
                percentageText.text = $"{targetPercentage:P0}";
            }

            if (image)
            {
                image.fillAmount = targetPercentage;
            }

            if (valueText)
            {
                valueText.text = $"{targetValue:f0}";
            }

            if (comboText)
            {
                comboText.text = $"{targetValue}/{dataStrategy.GetMaxValue(user):f0}";
            }
        }

        private Coroutine currentCoroutine = null;

        void UpdateUI()
        {
            if (currentCoroutine != null)
            {
                StopCoroutine(currentCoroutine);
            }

            targetPercentage = dataStrategy.GetValueAsPercentage(user);
            targetValue = dataStrategy.GetValue(user);
            if (currentValue > dataStrategy.GetMaxValue(user))
            {
                currentValue = dataStrategy.GetMaxValue(user);
            }

            currentCoroutine = StartCoroutine(UpdatePercentage());
            if (maxText)
            {
                maxText.text = $"{dataStrategy.GetMaxValue(user):f0}";
            }
        }
    }
}