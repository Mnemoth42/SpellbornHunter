using System.Collections;
using System.Collections.Generic;
using System.Text;
using TkrainDesigns.Attributes;
using TkrainDesigns.Stats;
using UnityEngine;

namespace TkrainDesigns.UI
{
    [DisallowMultipleComponent]
    [RequireComponent(typeof(Health))]
    [RequireComponent(typeof(BaseStats))]
    public class CharacterMouseOver : MouseOverToolTip
    {
        [SerializeField] private Sprite sprite;
        [SerializeField] private string displayName = "Brigand";

        protected override string GetTooltipMessage()
        {
            Health health = GetComponent<Health>();
            BaseStats stats = GetComponent<BaseStats>();
            Experience experience = GetComponent<Experience>();
            StringBuilder builder = new StringBuilder();
            builder.Append($"Level {stats.GetLevel()}\n");
            if (experience)
            {
                builder.Append($"XP: {experience.GetExperience()}/{stats.GetStat(EStat.XPNeeded)}\n");
            }

            if (health.IsAlive()) builder.Append($"Health: {health.GetCurrentValue()}/{health.GetMaxValue()}\n");
            else
            {
                builder.Append($"Dead.");
            }


            return builder.ToString();
        }

        protected override Sprite GetSprite()
        {
            return sprite;
        }

        protected override string GetTooltipTitle()
        {
            return displayName;
        }
    }
}