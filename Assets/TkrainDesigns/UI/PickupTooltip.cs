using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GameDevTV.Inventories;

namespace TkrainDesigns.UI
{
    [RequireComponent(typeof(Pickup))]
    public class PickupTooltip : MouseOverToolTip
    {
        private Pickup pickup;

        protected override string GetTooltipMessage()
        {
            if (!pickup) pickup = GetComponent<Pickup>();
            return pickup.GetItem().GetDescription();
        }

        protected override Sprite GetSprite()
        {
            if (!pickup) pickup = GetComponent<Pickup>();
            return pickup.GetItem().GetIcon();
        }

        protected override string GetTooltipTitle()
        {
            if (!pickup) pickup = GetComponent<Pickup>();
            return pickup.GetItem().GetDisplayName();
        }
    }
}