using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TkrainDesigns.UI
{
    public class ToggleGameObject : MonoBehaviour
    {
        [SerializeField] private GameObject objectToToggle;

        public void Toggle()
        {
            if (objectToToggle)
            {
                objectToToggle.SetActive(!objectToToggle.activeSelf);
            }
        }
    }
}