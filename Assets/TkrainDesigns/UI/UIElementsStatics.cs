﻿using UnityEngine.UIElements;

public static class UIElementsStatics
{
    public static void Show(this VisualElement element)
    {
        element.style.display = DisplayStyle.Flex;
    }
    /// <summary>
    /// Hides VisualElement, effectively removing it from the heirarchy.
    /// </summary>
    /// <param name="element">Any UnityEngine.UIElements.VisualElement</param>
    public static void Hide(this VisualElement element)
    {
        element.style.display = DisplayStyle.None;
    }
}