using UnityEngine;
using UnityEngine.EventSystems;

namespace TkrainDesigns.UI
{
    public class WindowDragger : MonoBehaviour, IDragHandler, IBeginDragHandler, IEndDragHandler
    {
        [SerializeField] private RectTransform handle = null;
        [SerializeField] private string windowKey = "";


        private Vector2 mousePosition;
        private Vector2 startingPosition;

        private void OnEnable()
        {
            Vector3 newPosition = GetStoredWindowLocation();
            Rect rect = new Rect(0, 0, Screen.width, Screen.height);
            if (rect.Contains(newPosition))
            {
                handle.position = newPosition;
            }
        }

        void StoreWindowLocation()
        {
            PlayerPrefs.SetFloat($"{windowKey}|X", handle.position.x);
            PlayerPrefs.SetFloat($"{windowKey}|Y", handle.position.y);
        }

        Vector2 GetStoredWindowLocation()
        {
            Vector2 location = new Vector2();
            location.x = PlayerPrefs.GetFloat($"{windowKey}|X", handle.position.x);
            location.y = PlayerPrefs.GetFloat($"{windowKey}|Y", handle.position.y);
            return location;
        }

        private void SetWindowLocation(Vector2? location)
        {
            if (handle == null) return;
            handle.position = (Vector2)location;
        }

        public void OnDrag(PointerEventData eventData)
        {
            handle.transform.position =
                startingPosition + eventData.position - mousePosition;
        }

        public void OnBeginDrag(PointerEventData eventData)
        {
            startingPosition = handle.transform.position;
            mousePosition = eventData.position;
        }

        public void OnEndDrag(PointerEventData eventData)
        {
            StoreWindowLocation();
        }
    }
}