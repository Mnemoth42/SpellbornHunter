using UnityEngine;
using UnityEngine.UI;
using TMPro;

namespace TkrainDesigns.UI
{
    public class MouseOverTooltipUI : MonoBehaviour
    {
        [SerializeField] private GameObject window;
        [SerializeField] private TextMeshProUGUI title;
        [SerializeField] private TextMeshProUGUI text;
        [SerializeField] private Image image;

        public static MouseOverTooltipUI Find()
        {
            return FindObjectOfType<MouseOverTooltipUI>();
        }

        private void Awake()
        {
            Hide();
        }

        public void ShowTooltip(string message)
        {
            Show();
            text.text = message;
        }

        void Show()
        {
            window.SetActive(true);
            transform.SetAsLastSibling();
        }

        public void Hide()
        {
            window.SetActive(false);
        }

        public void SetTitle(string titleToSet)
        {
            title.text = titleToSet;
        }

        public void SetImage(Sprite spriteToSet)
        {
            image.sprite = spriteToSet;
            image.gameObject.SetActive(spriteToSet != null);
        }
    }
}