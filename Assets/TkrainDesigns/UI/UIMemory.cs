using System.Collections;
using System.Collections.Generic;
using GameDevTV.Saving;
using Newtonsoft.Json.Linq;
using UnityEditor;
using UnityEngine;

namespace TkrainDesigns.UI
{
    [DisallowMultipleComponent]
    public class UIMemory : MonoBehaviour, ISaveable, IJsonSaveable
    {
        public event System.Action onWindowsRestored;

        [System.Serializable]
        struct FloatPair
        {
            public float x, y;

            public FloatPair(Vector2 location)
            {
                x = location.x;
                y = location.y;
            }

            public Vector2 AsVector2()
            {
                return new Vector2(x, y);
            }
        }

        private Dictionary<string, FloatPair> windowPositions = new Dictionary<string, FloatPair>();
        private Dictionary<string, FloatPair> windowSizes = new Dictionary<string, FloatPair>();

        public Vector2? GetWindowLocation(string key)
        {
            if (windowPositions.ContainsKey(key)) return windowPositions[key].AsVector2();
            return null;
        }

        public void SetWindowLocation(string key, Vector2 location)
        {
            windowPositions[key] = new FloatPair(location);
        }

        public Vector2? GetWindowSize(string key)
        {
            if (windowSizes.ContainsKey(key)) return windowSizes[key].AsVector2();
            return null;
        }

        public void SetWindowSize(string key, Vector2 location)
        {
            windowSizes[key] = new FloatPair(location);
        }

        [System.Serializable]
        struct UIMemorySaveStruct
        {
            public Dictionary<string, FloatPair> windowSizes;
            public Dictionary<string, FloatPair> windowPositions;

            public UIMemorySaveStruct(Dictionary<string, FloatPair> sizes,
                                      Dictionary<string, FloatPair> positions)
            {
                windowSizes = sizes;
                windowPositions = positions;
            }
        }

        public JToken CaptureState()
        {
            return JToken.FromObject(new UIMemorySaveStruct(windowSizes, windowPositions));
        }

        public void RestoreState(JToken state)
        {
            var data = state.ToObject<UIMemorySaveStruct>();
            windowSizes = data.windowSizes;
            windowPositions = data.windowPositions;
            onWindowsRestored?.Invoke();
        }

        public JToken CaptureAsJToken()
        {
            return JToken.FromObject(new UIMemorySaveStruct(windowSizes, windowPositions));
        }

        public void RestoreFromJToken(JToken state)
        {
            var data = state.ToObject<UIMemorySaveStruct>();
            windowSizes = data.windowSizes;
            windowPositions = data.windowPositions;
            onWindowsRestored?.Invoke();
        }
    }
}