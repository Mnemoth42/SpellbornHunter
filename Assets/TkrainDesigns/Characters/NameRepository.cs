using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TkrainDesigns.Characters
{
    [CreateAssetMenu(fileName = "NameRepository", menuName = "TkrainDesigns/NameRepository")]
    public class NameRepository : ScriptableObject
    {
        [SerializeField] private List<string> potentialNames = new List<string>();

        private Queue<string> queue;

        void Initialize()
        {
            if (queue == null)
            {
                queue = new Queue<string>();
                List<string> tempList = new List<string>(potentialNames);
                tempList.Shuffle();
                foreach (var shuffledName in tempList)
                {
                    queue.Enqueue(shuffledName);
                }
            }
        }

        public string GetName()
        {
            Initialize();
            string result = queue.Dequeue();
            queue.Enqueue(result);
            return result;
        }
    }
}