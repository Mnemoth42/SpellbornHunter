using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEditor.UIElements;
using UnityEngine;
using UnityEngine.UIElements;

namespace TkrainDesigns.Characters.Edit
{
    [CustomEditor(typeof(NameRepository))]
    public class NameRepositoryEditor : Editor
    {
        private SerializedProperty potentialNames;
        private TextField textField;
        private PropertyField propertyField;
        private VisualElement container;

        private void OnEnable()
        {
            VisualTreeAsset asset = Resources.Load<VisualTreeAsset>("NameRepositoryUXML");
            container = asset.CloneTree();
            potentialNames = serializedObject.FindProperty("potentialNames");
            textField = container.Q<TextField>();
            propertyField = container.Q<PropertyField>();
        }

        public override VisualElement CreateInspectorGUI()
        {
            textField.value = "";
            textField.isDelayed = true;
            textField.RegisterValueChangedCallback((evt) =>
            {
                if (evt.newValue == "") return;
                potentialNames.InsertArrayElementAtIndex(potentialNames.arraySize);
                SerializedProperty newProperty = potentialNames.GetArrayElementAtIndex(potentialNames.arraySize - 1);
                newProperty.stringValue = evt.newValue;
                serializedObject.ApplyModifiedProperties();
                textField.SetValueWithoutNotify("");
                textField.Focus();
            });
            propertyField.BindProperty(potentialNames);

            return container;
        }
    }
}