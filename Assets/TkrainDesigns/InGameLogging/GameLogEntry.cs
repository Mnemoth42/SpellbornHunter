using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

[RequireComponent(typeof(TextMeshProUGUI))]
public class GameLogEntry : MonoBehaviour
{
    private TextMeshProUGUI text;
    private float alpha = 1;
    private float delay = 10;

    private void Awake()
    {
        text = GetComponent<TextMeshProUGUI>();
        text.text = "";
    }


    public void SetMessage(string message)
    {
        StopAllCoroutines();
        text.text = message;
        StartCoroutine(FadeOut());
    }

    public IEnumerator FadeOut()
    {
        delay = 10;
        while (delay > 0)
        {
            delay -= Time.deltaTime;
            yield return null;
        }

        while (alpha > 0)
        {
            alpha -= Time.deltaTime;
            Color color = text.color;
            color.a = alpha;
            text.color = color;
            yield return null;
        }
    }


    public void FadeInImmediate()
    {
        StopAllCoroutines();
        alpha = 1;
        delay = 10;
        Color color = text.color;
        color.a = 1;
        text.color = color;
        StartCoroutine(FadeOut());
    }


    private void OnDestroy()
    {
        StopAllCoroutines();
    }
}