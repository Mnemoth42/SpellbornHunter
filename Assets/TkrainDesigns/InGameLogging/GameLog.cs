using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.EventSystems;

public class GameLog : MonoBehaviour
{
    public static void WriteLn(string message)
    {
        GameLog log = FindObjectOfType<GameLog>();
        if (log)
        {
            log.AddMessage(message);
            Debug.Log($"Message: {message}");
        }
        else
        {
            Debug.Log($"No GameLog found: {message}");
        }
    }

    [SerializeField] private GameLogEntry entryPrefab;

    private Queue<GameLogEntry> queue;

    private void Awake()
    {
        queue = new Queue<GameLogEntry>();
        foreach (var entry in GetComponentsInChildren<GameLogEntry>())
        {
            queue.Enqueue(entry);
        }
    }

    public void AddMessage(string message)
    {
        GameLogEntry topEntry = queue.Dequeue();
        GameLogEntry newEntry = Instantiate(entryPrefab, transform);
        newEntry.SetMessage(message);
        queue.Enqueue(newEntry);
        Destroy(topEntry.gameObject);
    }

    public void Refresh()
    {
        foreach (var entry in transform.GetComponentsInChildren<GameLogEntry>())
        {
            entry.FadeInImmediate();
        }
    }
}