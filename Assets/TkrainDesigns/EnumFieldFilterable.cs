using System;
using System.Collections.Generic;
using System.Linq;
using TkrainDesigns.Stats;
using UnityEditor;
using UnityEditor.Rendering;
using UnityEditor.UIElements;
using UnityEngine.UIElements;

namespace TkrainDesigns.UIElements
{
    public class EnumFieldFilterable : VisualElement
    {
       

        private DropdownField dropdownField;
        private List<string> choices=new List<string>();
        private SerializedProperty boundProperty = null;

        
        public EnumFieldFilterable()
        {
            dropdownField = new DropdownField();
        }

        private Type classtype;
        
        public void RemoveChoices(IEnumerable<string> choicesToRemove)
        {
            foreach (var choice in choicesToRemove)
            {
                choices.Remove(choice);
            }
            string currentChoice = dropdownField.value;
            
            if (!choices.Contains(currentChoice))
            {
                currentChoice = choices[0];
                boundProperty.enumValueIndex = boundProperty.enumNames.ToList().IndexOf(currentChoice);

            }
            dropdownField.choices = choices;
            dropdownField.value = currentChoice;
        }

        public void BindProperty(SerializedProperty property) 
        {
            boundProperty = property;
            dropdownField.choices = choices = property.enumNames.ToList();
            dropdownField.index = property.enumValueIndex;
        }

        
    }
}