using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

namespace TkrainDesigns.SceneManagement
{
    public class SaveAdapter : MonoBehaviour
    {
        public void Save(InputAction.CallbackContext context)
        {
            if (context.started)
            {
                FindObjectOfType<SavingWrapper>().Save();
            }
        }

        public void Load(InputAction.CallbackContext context)
        {
            if (context.started)
            {
                FindObjectOfType<SavingWrapper>().LoadLastScene();
            }
        }

        public void Delete(InputAction.CallbackContext context)
        {
            if (context.started)
            {
                FindObjectOfType<SavingWrapper>().Delete();
            }
        }
    }
}