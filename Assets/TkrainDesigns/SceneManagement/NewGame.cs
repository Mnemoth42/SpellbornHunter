using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace TkrainDesigns.SceneManagement
{
    public class NewGame : MonoBehaviour
    {
        [SerializeField] private TMP_InputField gameName;
        [SerializeField] private Button newButton;
        private SavingWrapper savingWrapper;


        private void Awake()
        {
            savingWrapper = FindObjectOfType<SavingWrapper>();
        }

        private void OnEnable()
        {
            UpdateButton();
        }

        public void CreateNewGame()
        {
            if (CanCreateGame())
            {
                savingWrapper.CreateNewGame(gameName.text);
            }
        }

        bool CanCreateGame()
        {
            if (gameName.text == "") return false;
            if (savingWrapper.FileExists(gameName.text)) return false;
            return true;
        }

        public void UpdateButton()
        {
            newButton.interactable = CanCreateGame();
        }
    }
}