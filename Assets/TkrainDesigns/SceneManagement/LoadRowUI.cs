using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

namespace TkrainDesigns.SceneManagement
{
    public class LoadRowUI : MonoBehaviour
    {
        [SerializeField] private TextMeshProUGUI text;
        private string fileName = "";

        public void Setup(string f)
        {
            fileName = f;
            text.text = f;
        }

        public void LoadGame()
        {
            SavingWrapper savingWrapper = FindObjectOfType<SavingWrapper>();
            savingWrapper.LoadSavedGame(fileName);
        }
    }
}