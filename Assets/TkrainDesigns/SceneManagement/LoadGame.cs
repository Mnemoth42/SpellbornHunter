using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TkrainDesigns.SceneManagement
{
    public class LoadGame : MonoBehaviour
    {
        [SerializeField] private LoadRowUI loadRowUIPrefab;
        [SerializeField] private Transform content;

        private SavingWrapper savingWrapper;

        private void Awake()
        {
            savingWrapper = FindObjectOfType<SavingWrapper>();
        }

        private void OnEnable()
        {
            foreach (Transform child in content)
            {
                Destroy(child.gameObject);
            }

            foreach (var fileName in savingWrapper.ListSaves())
            {
                LoadRowUI row = Instantiate(loadRowUIPrefab, content);
                row.Setup(fileName);
            }
        }
    }
}