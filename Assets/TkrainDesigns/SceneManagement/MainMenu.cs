using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace TkrainDesigns.SceneManagement
{
    public class MainMenu : MonoBehaviour
    {
        [SerializeField] private Button continueButton = null;
        [SerializeField] private Button loadButton = null;

        [SerializeField] private Button quitButton = null;
        private SavingWrapper wrapper;

        private void Awake()
        {
            wrapper = FindObjectOfType<SavingWrapper>();
        }

        private void OnEnable()
        {
            if (continueButton)
            {
                continueButton.interactable = wrapper.CanContinue();
            }
        }

        private void Start()
        {
            wrapper = FindObjectOfType<SavingWrapper>();
            if (continueButton)
            {
                continueButton.onClick.AddListener(() => { wrapper.LoadLastScene(); });
            }

            if (quitButton)
            {
                quitButton.onClick.AddListener(Application.Quit);
            }
        }
    }
}