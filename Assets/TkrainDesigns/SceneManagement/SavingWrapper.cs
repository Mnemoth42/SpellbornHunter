using System;
using System.Collections;
using System.Collections.Generic;
using GameDevTV.Saving;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace TkrainDesigns.SceneManagement
{
    public class SavingWrapper : MonoBehaviour
    {
        [SerializeField] private string filename = "Save";

        public static SavingWrapper instance;

        private void Awake()
        {
            if (instance == null)
            {
                instance = this;
            }
            else
            {
                Destroy(gameObject);
            }
        }

        private JSonSavingSystem savingSystem => GetComponent<JSonSavingSystem>();

        public string FileName
        {
            get { return PlayerPrefs.GetString("FileName", filename); }
            set
            {
                if (filename == "") return; //never set a blank filename.
                filename = value;
                PlayerPrefs.SetString("FileName", value);
            }
        }

        private void Start()
        {
            FindObjectOfType<Fader>().FadeOutImmediate();
            StartCoroutine(LoadMainmenu());
        }

        public void LoadLastScene()
        {
            StartCoroutine(savingSystem.LoadLastScene(FileName));
        }

        public IEnumerator LoadMainmenu()
        {
            yield return SceneManager.LoadSceneAsync(0);
            savingSystem.Load(FileName);
            yield return FindObjectOfType<Fader>().FadeIn(.5f);
        }

        public void Load()
        {
            savingSystem.Load(FileName);
        }

        public void Save()
        {
            savingSystem.Save(FileName);
        }

        public void Delete()
        {
            savingSystem.Delete(FileName);
        }

        public IEnumerable<string> ListSaves()
        {
            return savingSystem.ListSaves();
        }

        public bool CanContinue()
        {
            foreach (string s in ListSaves())
            {
                if (s == FileName) return true;
            }

            return false;
        }

        public bool FileExists(string gameNameText)
        {
            foreach (string s in ListSaves())
            {
                if (s == gameNameText) return true;
            }

            return false;
        }

        public void CreateNewGame(string gameNameText)
        {
            FileName = gameNameText;
            StartCoroutine(StartNewGame());
        }

        IEnumerator StartNewGame()
        {
            Fader fader = FindObjectOfType<Fader>();
            yield return fader.FadeOut(1);
            yield return SceneManager.LoadSceneAsync(1);
        }

        public void LoadSavedGame(string gameNameText)
        {
            FileName = gameNameText;
            LoadLastScene();
        }
    }
}